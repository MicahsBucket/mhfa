({
	prepTreeGrid: function(cmp) {
		var actions = this.getRowActions.bind(this, cmp);
		var taskColumnLabel = 'Task';
		if (cmp.get('v.taskType') == 'Milestone') {
			taskColumnLabel = 'Milestone';
		}
		var columns = [];
		//Phase
		columns.push({
				type: 'url'
				, fieldName: 'phaseLink'
				, label: 'Phase'
				, typeAttributes: {
					label: {fieldName: 'phaseName'}
					, target: '_self'
				}
				, initialWidth: 200
		});
		//Stage
		columns.push({
				type: 'url'
				, fieldName: 'stageLink'
				, label: 'Stage'
				, typeAttributes: {
					label: {fieldName: 'stageName'}
					, target: '_self'
				}
				, initialWidth: 250
		});
		//Milestone or Task - only if configured on page
		console.log('Task Type: ' + cmp.get('v.taskType'));
		if (cmp.get('v.taskType') != 'None') {
			columns.push({
					type: 'url'
					, fieldName: 'taskLink'
					, label: taskColumnLabel
					, typeAttributes: {
						label: {fieldName: 'taskName'}
						, target: '_self'
					}
					, initialWidth: 250
			});
		}
		//Status
		columns.push({
				type: 'text'
				, fieldName: 'status'
				, label: 'Status'
				, initialWidth: 125
		});
		//Target Start Date
		columns.push({
				type: 'date-local'
				, fieldName: 'targetStartDate'
				, label: 'Target Start'
				, initialWidth: 125
		});
		//Is Duration Editable
		columns.push({
			type: 'boolean'
			, fieldName: 'editableDuration'
			, label: 'Duration Editable?'
			, initialWidth: 150
		});
		//Duration
		columns.push({
				type: 'number'
				, fieldName: 'duration'
				, label: 'Duration (Days)'
				, initialWidth: 150
				, editable: 'true'
		});
		//Due Date
		columns.push({
				type: 'date-local'
				, fieldName: 'dueDate'
				, label: 'Due Date'
				, initialWidth: 125
		});
		//Last Modified Date
		columns.push({
				type: 'date'
				, typeAttributes: {
					day: 'numeric',
					month: 'short',
					year: 'numeric',
					hour: '2-digit',
					minute: '2-digit',
					hour12: true
				}
  				, fieldName: 'lastModifiedDate'
				, label: 'Last Modified Date'
				, initialWidth: 175
		});
		//Last Modified By
		columns.push({
				type: 'text'
				, fieldName: 'lastModifiedBy'
				, label: 'Last Modified By'
				, initialWidth: 200
		});

		columns.push({
			type: 'action'
			, typeAttributes: {
				rowActions: actions
			}
		});

		cmp.set('v.gridColumns', columns);
	},

	getRowActions: function(cmp, row, doneCallback) {
		var actions = [];
		if (row['phaseName'] != undefined) {
			actions = [
				{label: 'View', name: 'phase_edit'},
			];
		} else if (row['stageName'] != undefined) {
			actions = [
				{label: 'Edit', name: 'stage_edit'},
			];
		} else if (row['taskName'] != undefined) {
			actions = [
				{label: 'Edit', name: 'task_edit'}
			];
		}

        //Add the actions to the current row
        doneCallback(actions);
	}, 

	loadPhaseStageTable: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.retrievePhaseStageTree');
		action.setParams({
			projectId: cmp.get('v.record.Id'),
			taskType: cmp.get('v.taskType'),
			taskOwner: cmp.get('v.taskOwner')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				var phaseRecords = [];
				var phaseStageRecords = [];
				var stageTaskRecords = [];

				//loop through the phases
				var phaseCount = 0;
				var stageCount = 0;
				var targetStartDate_Phase = new Date();
				var targetStartDate_Stage = new Date();
				var previousDueDate_Phase = new Date();
				var previousDueDate_Stage = new Date();
				debugger;
				for (var oPhaseTree of result) {
					targetStartDate_Phase = new Date(oPhaseTree.oPhase.Project__r.CreatedDate);
					if (oPhaseTree.oPhase.Due_Date_Baseline__c == 'Stage Duration - Funding Round') {
						targetStartDate_Phase = new Date(oPhaseTree.oPhase.Funding_Round_Phase__r.Start_Date__c);
					}
					targetStartDate_Stage = targetStartDate_Phase;
					if (phaseCount > 0) {
						targetStartDate_Phase = previousDueDate_Phase;
					}

					var dueDatePhase = new Date(oPhaseTree.oPhase.Project_Phase_Due_Date__c);
					//dueDatePhase.setHours(0,0,0,0);
					previousDueDate_Phase = dueDatePhase;

					//loop through the stages under each phase if they exist
					phaseStageRecords = [];
					if (oPhaseTree.lstStageDetailsAndTasks != undefined) {
						for (var oStageDetail of oPhaseTree.lstStageDetailsAndTasks) {
							if (stageCount > 0) {
								targetStartDate_Stage = previousDueDate_Stage;
							}

							//loop through the tasks associated to each stage if they exist and if the page parameter 'taskType' has a value
							stageTaskRecords = [];
							if (oStageDetail.oStage.Project_Stage_Tasks__r != undefined 
									&& cmp.get('v.taskType') != 'None') {
								for (var oTask of oStageDetail.oStage.Project_Stage_Tasks__r) {
									stageTaskRecords.push({id: oTask.Id
															, taskLink: '/' + oTask.Id
															, taskName: oTask.Name
															, dueDate: oTask.Due_Date__c
															, status: oTask.Task_Status__c
															, lastModifiedBy: oTask.LastModifiedBy.FirstName + ' ' + oTask.LastModifiedBy.LastName
															, lastModifiedDate: oTask.LastModifiedDate});
								}
							}
							phaseStageRecords.push({id: oStageDetail.oStage.Id
													, stageLink: '/' + oStageDetail.oStage.Id
													, stageName: oStageDetail.oStage.Name
													, status: oStageDetail.oStage.Stage_Status__c
													, editableDuration: oStageDetail.oStage.Is_Stage_Duration_Editable__c
													, duration: oStageDetail.oStage.Estimated_Days_to_Complete__c
													, targetStartDate: targetStartDate_Stage
													, dueDate: oStageDetail.dueDate
													, lastModifiedBy: oStageDetail.oStage.LastModifiedBy.FirstName + ' ' + oStageDetail.oStage.LastModifiedBy.LastName
													, lastModifiedDate: oStageDetail.oStage.LastModifiedDate});
							previousDueDate_Stage = new Date(oStageDetail.dueDate);
							console.log('previousDueDate_Stage: ' + previousDueDate_Stage);

							phaseStageRecords = phaseStageRecords.concat(stageTaskRecords);
							stageCount++;
						}
					}
					phaseRecords.push({id: oPhaseTree.oPhase.Id
										, phaseLink: '/' + oPhaseTree.oPhase.Id
										, phaseName: oPhaseTree.oPhase.Name
										, status: oPhaseTree.oPhase.Phase_Status__c
										, targetStartDate: targetStartDate_Phase
										, editableDuration: oPhaseTree.oPhase.Is_Stage_Duration_Editable__c
										, dueDate: oPhaseTree.oPhase.Project_Phase_Due_Date__c
										, lastModifiedBy: oPhaseTree.oPhase.LastModifiedBy.FirstName + ' ' + oPhaseTree.oPhase.LastModifiedBy.LastName
										, lastModifiedDate: oPhaseTree.oPhase.LastModifiedDate});
					phaseRecords = phaseRecords.concat(phaseStageRecords);
					phaseCount++;
				}
				cmp.set('v.gridData', phaseRecords);
			}
		});
		//make the call
		$A.enqueueAction(action);
	},

	editRecord: function(cmp, recId) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recId
        });
        editRecordEvent.fire();

    },

	navigateToPhase : function (component, event, helper) {
		var clickedId = event.getSource().get('v.keyField');

		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": checkedId,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	},

	daysBetweenDates: function(dtFirst, dtSecond) {
		// Take the difference between the dates and divide by milliseconds per day.
		// Round to nearest whole number to deal with DST.
		return Math.round((dtSecond-dtFirst)/(1000*60*60*24));
	}, 

    toastMsg : function(strType, strMessage, strMode ) {
		if (strMode == null) {
			strMode = 'dismissible';
		}       
        var showToast = $A.get("e.force:showToast");
        showToast.setParams({                 
            message : strMessage,  
            type : strType,  
            mode : strMode                
        });   
        showToast.fire();          
    }
})