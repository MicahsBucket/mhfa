({
	prepTreeGrid: function(cmp) {
		var columns = [
			{
				type: 'url'
				, fieldName: 'recordLink'
				, label: 'Task'
				, typeAttributes: {
					label: {fieldName: 'recName'}
					, target: '_self'
				}
			},
			{
				type: 'text'
				, fieldName: 'baselineOffset'
				, label: 'Due Date Baseline/Offset'
			},
			{
				type: 'date-local'
				, fieldName: 'startDate'
				, label: 'Start Date'
			},
			{
				type: 'date-local'
				, fieldName: 'dueDate'
				, label: 'Due Date'
			},
			{
				type: 'text'
				, fieldName: 'assigneeType'
				, label: 'Assignee Type'
			},
			{
				type: 'text'
				, fieldName: 'assigneeRole'
				, label: 'Assignee Role'
			},

		];

		cmp.set('v.gridColumns', columns);
	},
	
	loadTaskTree: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.TaskTree');
		action.setParams({
			frId: cmp.get('v.record.Id')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
			}
		});
		//make the call
		$A.enqueueAction(action);
	},
})