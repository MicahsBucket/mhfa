({
	handleRecordUpdated: function (component, event, helper) {
		helper.prepTreeGrid(component);
		helper.loadTaskTree(component);
	}
})