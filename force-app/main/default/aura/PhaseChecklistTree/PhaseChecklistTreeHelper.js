({
	prepTreeGrid: function(cmp) {
		var columns = [
            {
				type: 'url'
				, fieldName: 'phaseLink'
				, label: 'Phase/Checklist/Checklist Item'
				, typeAttributes: {
					label: {fieldName: 'recName'}
					, target: '_self'
				}
			},
			{
				type: 'text'
				, fieldName: 'selectionType'
				, label: 'Selection Type'
			},
			{
				type: 'text'
				, fieldName: 'stage'
				, label: 'Stage'
			},
			{
				type: 'text'
				, fieldName: 'approvalFirst'
				, label: 'First Approval'
			},
			{
				type: 'text'
				, fieldName: 'approvalSecond'
				, label: 'Second Approval'
			},
			{
				type: 'text'
				, fieldName: 'approvalThird'
				, label: 'Third Approval'
			},
		];
		cmp.set('v.gridColumns', columns);
	},
	
	loadPhaseChecklistTree: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.retrievePhaseChecklistTree');
		action.setParams({
			frId: cmp.get('v.record.Id')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				var phaseRecords = [];
				var expandedRows = [];
				//loop through the phases
				for (var oPhaseTree of result) {
					//loop through the checklists under each phase if they exist
					if (oPhaseTree.lstChecklistsAndItems != undefined) {
						var checklistRecords = [];
						for (var oChecklist of oPhaseTree.lstChecklistsAndItems) {
							//loop through the items associated to each checklist if they exist
							if (oChecklist.Phase_Checklist_Items__r != undefined) {
								var cliRecords = [];
								for (var oCLI of oChecklist.Phase_Checklist_Items__r) {
                                    var strStageName = '';
                                    if (oCLI.Funding_Round_Stage__c != null) {
                                        strStageName = oCLI.Funding_Round_Stage__r.Name;
                                    }
									cliRecords.push({id: oCLI.Id
														, phaseLink: '/' + oCLI.Id
														, recName: oCLI.Funding_Round_Checklist_Item__r.Name
														, stage: strStageName
														, approvalFirst: oCLI.Approval_First__c
														, approvalSecond: oCLI.Approval_Second__c
														, approvalThird: oCLI.Approval_Third__c});
								}
								//checklist records with checklist items
								checklistRecords.push({id: oChecklist.Id
													, phaseLink: '/' + oChecklist.Id
													, recName: oChecklist.Name + ' Checklist'
													, selectionType: oChecklist.Selection_Type__c
													, _children: cliRecords});
								//if the stage has tasks below it, expand the tree
								expandedRows.push(oChecklist.Id);

							} else {
								//checklistRecords records without associated tasks
								checklistRecords.push({id: oChecklist.Id
													, phaseLink: '/' + oChecklist.Id
													, recName: oChecklist.Name + ' Checklist'
													, selectionType: oChecklist.Selection_Type__c});
							}
						}
						//phase records with stages
						phaseRecords.push({id: oPhaseTree.oPhase.Id
											, phaseLink: '/' + oPhaseTree.oPhase.Id
											, recName: oPhaseTree.oPhase.Name + ' Phase'
											, _children: checklistRecords});
						//if the phase has stages below it, expand the tree
						expandedRows.push(oPhaseTree.oPhase.Id);
					} else {
						//phase records without related stages
						phaseRecords.push({id: oPhaseTree.oPhase.Id
											, phaseLink: '/' + oPhaseTree.oPhase.Id
											, recName: oPhaseTree.oPhase.Name + ' Phase'});
					}
				}
				cmp.set('v.gridData', phaseRecords);
				cmp.set('v.gridExpandedRows', expandedRows);
			}
		});
		//make the call
		$A.enqueueAction(action);
	},

})