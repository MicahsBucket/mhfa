({
	handleRecordUpdated: function (component, event, helper) {
		helper.prepTreeGrid(component);
		helper.loadPhaseChecklistTree(component);
	}
})