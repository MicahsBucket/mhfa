({
	///////////////////////////////////////////////////////////////////////////////////
    //This function is fired after a button is clicked
	///////////////////////////////////////////////////////////////////////////////////
	handleClick: function(cmp, event, helper) {
		var clickedItem = event.getSource().get('v.name');

		//Retrieve the selected values and store them in an array 
		if (clickedItem === 'btnSave') {
			helper.saveChecklists(cmp);
		}
        if (clickedItem === 'btnInclude') {
            helper.saveCharacteristics(cmp);
        }
        if (clickedItem === 'btnRemove') {
            helper.removeCharacteristics(cmp);
        }
	},

	///////////////////////////////////////////////////////////////////////////////////
    //This function is fired after the Funding Round Checklist Item record is retrieved
	///////////////////////////////////////////////////////////////////////////////////
	handleRecordUpdated: function (cmp, event, helper) {
		//build checkbox groups by retrieving the characteristic groups and characteristics
        cmp.set('v.Spinner', true); 
		helper.prepTreeGrid(cmp);        
		helper.retrieveGroupings_Tree(cmp);
		helper.retrieveSelectedGroupings_Tree(cmp);

		//build listing of checklists to associate to the checklist items
        cmp.set('v.Spinner', true); 
		helper.retrieveChecklists(cmp);
	},
    
	///////////////////////////////////////////////////////////////////////////////////
    //If a parent item in a tree grid is selected, check all of its
    // children; if the parent is unchecked, unselect all children
	///////////////////////////////////////////////////////////////////////////////////
    handleGridRowSelection: function (cmp, event, helper) {
        helper.processGridRowClick(cmp, event, helper);      
    },    
   
})