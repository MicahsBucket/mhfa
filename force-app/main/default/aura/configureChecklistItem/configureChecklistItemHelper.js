({
    /////////////////////////////////////////////////
    // prepTreeGrid
    /////////////////////////////////////////////////
	prepTreeGrid: function(cmp) {
		var columns = [
			{
				type: 'text'
				, fieldName: 'recName'
				, label: 'Group/Characteristic'
			},

			{
				type: 'text'
				, fieldName: 'recDescription'
				, label: 'Description'
			},
           
		];

		cmp.set('v.gridColumns', columns);
	}, 

    /////////////////////////////////////////////////
    // retrieveGroupings_Tree
    /////////////////////////////////////////////////
    retrieveGroupings_Tree: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.getAvailableCharacteristics');
		action.setParams({
			frId: cmp.get('v.record.Funding_Round__c'),
			frcliId: cmp.get('v.record.Id'),
			selectionType: cmp.get('v.selectionType')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				//console.log('retrieveGroupings_Tree Results:\n' + JSON.stringify(result));
				var groupRecords = [];
				var expandedRows = [];
				var selectedRows = [];
				for (var oGrpRec of result) {
					//build the array of characteristics to display under each group
					var characteristicRecords = [];
					for (var oCharRec of oGrpRec.oGrpCharacteristics) {
                        //characteristic records
                        characteristicRecords.push({id: oCharRec.Id
                                                    , recName: oCharRec.Name
                                                    , recDescription: oCharRec.Trigger__r.Help_Text__c
                                                    , });
					}
					//Now load the main array which holds the group name, the characteristics to display under the group,
					// and the characteristics to pre-select
                    groupRecords.push({id: oGrpRec.oGroup.Id
                                       , recName: oGrpRec.oGroup.Name
//                                       , recDescription: oGrpRec.oGroup.Description__c
                                       , recDescription: ''
                                       , _children: characteristicRecords});
                    //if the group has characteristics below it, expand the tree
                    expandedRows.push(oGrpRec.oGroup.Id);
                    //console.log(oGrpRec.oGroup.Name + ' selectedChars: ' + oGrpRec.selectedChars.length);
                    if (oGrpRec.selectedChars.length > 0) {
                    	selectedRows.push(oGrpRec.selectedChars);
                    }
				}
                //console.log('selectedRows: ' + selectedRows);
				cmp.set('v.gridData', groupRecords);
				cmp.set('v.gridExpandedRows', expandedRows);
		        cmp.set('v.Spinner', false); 
			}
		});
		//make the call
		$A.enqueueAction(action);
	},  

    /////////////////////////////////////////////////
    // retrieveSelectedGroupings_Tree
    /////////////////////////////////////////////////
    retrieveSelectedGroupings_Tree: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.getAssignedCharacteristics');
		action.setParams({
			frId: cmp.get('v.record.Funding_Round__c'),
			frcliId: cmp.get('v.record.Id'),
			selectionType: cmp.get('v.selectionType')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				//console.log('retrieveSelectedGroupings_Tree Results:\n' + JSON.stringify(result));
				var groupRecords = [];
				var expandedRows = [];
				var selectedRows = [];
				for (var oGrpRec of result) {
					//build the array of characteristics to display under each group
					var characteristicRecords = [];
					for (var oCharRec of oGrpRec.oGrpCharacteristics) {
                        //characteristic records
                        characteristicRecords.push({id: oCharRec.Id
                                                    , recName: oCharRec.Name
                                                    , recDescription: oCharRec.Trigger__r.Help_Text__c
                                                    , });
					}
					//Now load the main array which holds the group name, the characteristics to display under the group,
					// and the characteristics to pre-select
                    groupRecords.push({id: oGrpRec.oGroup.Id
                                       , recName: oGrpRec.oGroup.Name
                                       , recDescription: ''
                                       , _children: characteristicRecords});
                    //if the group has characteristics below it, expand the tree
                    expandedRows.push(oGrpRec.oGroup.Id);
				}
				cmp.set('v.gridDataSelected', groupRecords);
				cmp.set('v.gridExpandedRowsSelected', expandedRows);
		        cmp.set('v.Spinner', false); 
			}
		});
		//make the call
		$A.enqueueAction(action);
	},  
        
    /////////////////////////////////////////////////
    // retrieveChecklists
    /////////////////////////////////////////////////
	retrieveChecklists: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.getChecklistOptions');
		action.setParams({
			frId: cmp.get('v.record.Funding_Round__c'),
			frcliId: cmp.get('v.record.Id'),
			selectionType: cmp.get('v.selectionType')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('retrieveChecklists Results:\n' + JSON.stringify(result));
				var arrChecklists = [];
				for (var oChklstRec of result) {
					arrChecklists.push(oChklstRec);
				}
				cmp.set('v.mapChecklists', arrChecklists);
				console.log('v.mapChecklists: ' + JSON.stringify(cmp.get('v.mapChecklists')));
		        cmp.set('v.Spinner', false); 

			}
		});
		//make the call
		$A.enqueueAction(action);
	},
	
	/////////////////////////////////////////////////
	// saveCharacteristics
	/////////////////////////////////////////////////
    saveCharacteristics: function(cmp) {
		//prepare the selected characteristics
        cmp.set('v.Spinner', true); 
		var arrSelectedChars = [];
        var selectedRows = cmp.find('grdAvailable').getSelectedRows();
		console.log('Selected Rows: ', selectedRows);
        if (selectedRows != undefined) {
            for (var i=0; i<selectedRows.length; i++) {
                console.log('Selected Char: ' + JSON.stringify(selectedRows[i]));
                //only include the characteristics in the list and not the groups
                if (selectedRows[i].level == 2) {
	                arrSelectedChars.push(selectedRows[i].id);
                }
            }
        }
        console.log('arrSelectedChars: ' + arrSelectedChars);

		//insert the records
		var action = cmp.get('c.insertCliCharacteristics');
		action.setParams({
			frcliId: cmp.get('v.record.Id'),
			lstCharIds: arrSelectedChars,
			blnClearExisting: false
		});
		action.setCallback(this, function(response) {
			console.log('Response for insertCliCharacteristics:\n' + JSON.stringify(response));
            var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('Results for insertCliCharacteristics:\n' + JSON.stringify(result));
                //this.retrieveGroupings(cmp);
                this.prepTreeGrid(cmp);        
                this.retrieveGroupings_Tree(cmp);
                this.retrieveSelectedGroupings_Tree(cmp);
                cmp.find('notifLib').showToast({
					"title": "Save Complete",
					"message": "Characteristics successfully associated to Checklist Item",
					"variant": "success",
					"mode": "dismissable"
				});
			} else {
				console.log('Response for insertCliCharacteristics:\n' + JSON.stringify(response));
				cmp.find('notifLib').showToast({
					"title": "Error",
					"message": "The save failed.  Check dev console.",
					"variant": "error",
					"mode": "sticky"
				});
			}
            
	        cmp.set('v.Spinner', false); 

		});
		//make the call
		$A.enqueueAction(action);
	},        

	/////////////////////////////////////////////////
	// removeCharacteristics
	/////////////////////////////////////////////////
    removeCharacteristics: function(cmp) {
		//prepare the selected characteristics
        cmp.set('v.Spinner', true); 
		var arrSelectedChars = [];
        var selectedRows = cmp.find('grdAssigned').getSelectedRows();
		console.log('Selected Rows: ', selectedRows);
        if (selectedRows != undefined) {
            for (var i=0; i<selectedRows.length; i++) {
                console.log('Selected Char: ' + JSON.stringify(selectedRows[i]));
                //only include the characteristics in the list and not the groups
                if (selectedRows[i].level == 2) {
	                arrSelectedChars.push(selectedRows[i].id);
                }
            }
        }
        console.log('arrSelectedChars: ' + arrSelectedChars);

		//remove the records
		var action = cmp.get('c.removeCliCharacteristics');
		action.setParams({
			frcliId: cmp.get('v.record.Id'),
			lstCharIds: arrSelectedChars
		});
		action.setCallback(this, function(response) {
			console.log('Response for removeCliCharacteristics:\n' + JSON.stringify(response));
            var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('Results for removeCliCharacteristics:\n' + JSON.stringify(result));
                //this.retrieveGroupings(cmp);
                this.prepTreeGrid(cmp);        
                this.retrieveGroupings_Tree(cmp);
                this.retrieveSelectedGroupings_Tree(cmp);
                cmp.find('notifLib').showToast({
					"title": "Save Complete",
					"message": "Characteristics successfully disassociated from Checklist Item",
					"variant": "success",
					"mode": "dismissable"
				});
			} else {
				console.log('Response for removeCliCharacteristics:\n' + JSON.stringify(response));
				cmp.find('notifLib').showToast({
					"title": "Error",
					"message": "The save failed.  Check dev console.",
					"variant": "error",
					"mode": "sticky"
				});
			}
            
	        cmp.set('v.Spinner', false); 

		});
		//make the call
		$A.enqueueAction(action);
	},    
        
    /////////////////////////////////////////////////
    // saveConfiguration
    /////////////////////////////////////////////////
	saveChecklists: function(cmp) {
		//prepare the checklists
        cmp.set('v.Spinner', true); 
		var arrChecklists = [];
		var checklistChoices = cmp.find('cmpChecklist');
		console.log('Inside saveChecklists: ' + JSON.stringify(checklistChoices) + '\nmapChecklists: ' + JSON.stringify(cmp.get('v.mapChecklists')));

		var i = 0;
		var arrChecklistRow = cmp.get('v.mapChecklists');
		for (var oChecklist in arrChecklistRow) {
			i++;
			console.log(i + ' -> ' + JSON.stringify(arrChecklistRow[oChecklist]));
//			arrChecklistRow[oChecklist].isChanged = false;
			arrChecklists.push(JSON.stringify(arrChecklistRow[oChecklist]));
		}
		console.log('arrChecklists: ' + JSON.stringify(arrChecklists));
		cmp.set('v.mapChecklists', arrChecklistRow);

		//insert the records
		var action = cmp.get('c.saveCliChecklists');
		action.setParams({
			frcliId: cmp.get('v.record.Id'),
			lstJsonChecklists: arrChecklists
		});
		action.setCallback(this, function(response) {
			console.log('Response for saveCliConfiguration:\n' + JSON.stringify(response));
            var callState = response.getState();
			debugger;
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('Results for saveCliConfiguration:\n' + JSON.stringify(result));
				cmp.find('notifLib').showToast({
					"title": "Save Complete",
					"message": "Checklist Item Configuration Saved",
					"variant": "success",
					"mode": "dismissable"
				});
				//reset changed flag on changed items
/*
				var checklistChoices = cmp.find('cmpChecklist');
				for (var i=0; i<checklistChoices.length; i++) {
					if (checklistChoices[i].get('v.isChanged')) {
						checklistChoices[i].set('v.isChanged', false);
						var changedIcon = checklistChoices[i].find('iconChanged');
						$A.util.addClass(changedIcon, 'slds-hide');
					}
				}
*/
				var arrChecklistRow = cmp.get('v.mapChecklists');
				for (var oChecklist in arrChecklistRow) {
					arrChecklistRow[oChecklist].isChanged = false;
				}
				cmp.set('v.mapChecklists', arrChecklistRow);

			} else {
				console.log('Response for saveCliConfiguration:\n' + JSON.stringify(response));
				var errors = action.getError();
				var errorMsg = "The item could not be saved due to the following error:  " + errors[0].message;
				console.log('Errors for saveCliConfiguration:\n' + JSON.stringify(errors));
				cmp.find('notifLib').showToast({
					"title": "Error",
					"message": errorMsg,
					"variant": "error",
					"mode": "sticky"
				});
			}
	        cmp.set('v.Spinner', false); 

		});
		//make the call
		$A.enqueueAction(action);
	},
        
    /////////////////////////////////////////////////
    // processGridRowClick
    /////////////////////////////////////////////////
	processGridRowClick: function(cmp, event) {
        var currentGridId = event.getSource().getLocalId();
        var gridDataAttribute = 'v.gridDataSelected';
        var assignedRowsAttribute = 'v.assignedRowsSelected';
        if (currentGridId == 'grdAvailable') {
            gridDataAttribute = 'v.gridData';
            assignedRowsAttribute = 'v.availableRowsSelected';
        }
        if (currentGridId == 'grdAssigned') {
            gridDataAttribute = 'v.gridDataSelected';
            assignedRowsAttribute = 'v.assignedRowsSelected';
        }
        var vGridData = cmp.get(gridDataAttribute);
		var vExistingValues = cmp.get(assignedRowsAttribute);
        var selectedRows = event.getParam('selectedRows');
        
        var newSelectedRows = [];
        console.log('vGridData start: ' + vGridData.length + ' records - ' + JSON.stringify(vGridData));
        console.log('selectedRows start: ' + selectedRows.length + ' records - ' + JSON.stringify(selectedRows));
        console.log('vExistingValues start: ' + vExistingValues.length + ' records - ' + JSON.stringify(vExistingValues));

		//find deselected rows
		if (selectedRows.length < vExistingValues.length) {
			console.log('Process deselected item(s)');
			for (var eIndex=0; eIndex<vExistingValues.length; eIndex++) {
				console.log('Existing value (eIndex): ' + eIndex);
				var rowSelected = false;
				for (var sIndex=0; sIndex<selectedRows.length; sIndex++) {
					if (vExistingValues[eIndex] == selectedRows[sIndex].id) {
						rowSelected = true;
						newSelectedRows.push(selectedRows[sIndex].id);
						break;
					}
				}
				if (rowSelected == false) {
					//this row was deselected so determine which level it is
					console.log('Deselected Item: ' + vExistingValues[eIndex]);
					var deselectedId = vExistingValues[eIndex];
					for (var gIndex=0; gIndex<vGridData.length; gIndex++) {
						console.log('Existing value (gIndex): ' + gIndex + ' - ' + vGridData[gIndex].id);
						var parentDeselected = false;
						if (vGridData[gIndex].id == deselectedId)  {
							parentDeselected = true;
							console.log('Deselected Record: ' + JSON.stringify(vGridData[gIndex]));
							console.log('Children: ' + vGridData[gIndex]._children.length);
							//If a parent item is deselected, skip the index ahead as many items as their are children so 
							// the children are not included in the array of selected items.
							if (vGridData[gIndex]._children.length > 0) {
								eIndex = eIndex + vGridData[gIndex]._children.length;
								break;
							}
						}
						if (parentDeselected == false && vGridData[gIndex]._children.length > 0) {
							//if a child record was deselected, deselect the parent
							for (var crIndex=0; crIndex<vGridData[gIndex]._children.length; crIndex++) {
								if (vGridData[gIndex]._children[crIndex].id == deselectedId
										&& newSelectedRows.indexOf(vGridData[gIndex].id) > -1) {
									newSelectedRows.splice(newSelectedRows.indexOf(vGridData[gIndex].id), 1);
									parentDeselected = true;
									break;
								}
							}
						}
						if (parentDeselected == true) {
							break;
						}
					}
				}
			}
		} else {    
			console.log('Process selected item(s)');
			for (var sIndex=0; sIndex<selectedRows.length; sIndex++){
				newSelectedRows.push(selectedRows[sIndex].id);
				if (selectedRows[sIndex].level == 1 && selectedRows[sIndex].hasChildren) {
					var childRecs = vGridData[selectedRows[sIndex].posInSet-1]._children;                
					for (var cIndex=0; cIndex<childRecs.length; cIndex++) {
						newSelectedRows.push(childRecs[cIndex].id);
					}
					console.log("You selected: " + JSON.stringify(selectedRows[sIndex]) 
								+ '\n\nnewSelectedRows: ' + JSON.stringify(newSelectedRows) 
								+ '\n\nchildRecs: ' + JSON.stringify(childRecs));
				}
			}
		}
		cmp.set(assignedRowsAttribute, Array.from(new Set(newSelectedRows)));
//		cmp.set(assignedRowsAttribute, newSelectedRows);
            
	},

})