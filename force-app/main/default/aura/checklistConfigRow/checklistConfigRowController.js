({
	doInit: function (cmp, event, helper) {
		helper.setPicklistOptions(cmp, 'selStage', 'checklistRec.stageOptions', true);
		helper.setPicklistOptions(cmp, 'selDueDateOffset', 'checklistRec.dueDateOffsetOptions', true);
		helper.setPicklistOptions(cmp, 'selApprovalFirst', 'checklistRec.approverOptions', true);
		helper.setPicklistOptions(cmp, 'selApprovalSecond', 'checklistRec.approverOptions', true);
		helper.setPicklistOptions(cmp, 'selApprovalThird', 'checklistRec.approverOptions', true);
		helper.setPicklistOptions(cmp, 'chkgrpClosingType', 'checklistRec.closingTypeOptions', false);

		//multipick fields must be converted to an array of strings when displayed in a checkbox group
		var existingClosingTypes = cmp.get('v.checklistRec.oChecklistItem.Closing_Types__c');
		var availableClosingTypes = cmp.get('v.checklistRec.oChecklist.Closing_Types__c');
		console.log('existingClosingTypes: ' + existingClosingTypes + '\navailableClosingTypes: ' + availableClosingTypes);
		cmp.set('v.closingTypeValues', helper.convertMultipickToCheckboxGroup(existingClosingTypes, availableClosingTypes));

		//date fields must be handled
		if (cmp.get('v.checklistRec.oChecklistItem.Due_Date__c') != undefined) {
			cmp.set('v.dueDate', cmp.get('v.checklistRec.oChecklistItem.Due_Date__c'));
		}
		var hideStage = (cmp.get('v.checklistRec.stageOptions').length == 0);
		helper.toogleEditFields(cmp, 'cbxChecklist', hideStage);
        helper.toogleDueDateFields(cmp);
	},

	handleChange: function(cmp, event, helper) {
		var changedItem = event.getSource().get('v.name');
		console.log('Inside handleChange.  changedItem:\n' + changedItem + '\nisChanged start value: '  + cmp.get('v.checklistRec.isChanged'));
		//if any field changed, update the flag so we know to indicate the Save button must be clicked
		cmp.set('v.checklistRec.isChanged', true);
		console.log('isChanged end value: ' + cmp.get('v.checklistRec.isChanged'));
		//***cbxChecklist
		if (changedItem === 'cbxChecklist') {
			var hideStage = (cmp.get('v.checklistRec.stageOptions').length == 0);
			helper.toogleEditFields(cmp, changedItem, hideStage);
			console.log('Checklist checked: ' + event.getSource().get('v.value'));
		}
		//***chkgrpClosingType
		if (changedItem ==='chkgrpClosingType') {
			console.log('Closing Types: ' + event.getSource().get('v.value'));
			cmp.set('v.checklistRec.oChecklistItem.Closing_Types__c', helper.convertCheckboxGroupToMulitpick(event.getSource().get('v.value')));
			console.log('New Closing Types: ' + cmp.get('v.checklistRec.oChecklistItem.Closing_Types__c'));
		}
	},
    
    resetOffset: function(cmp, event, helper) {
        helper.toogleDueDateFields(cmp);
		cmp.set('v.checklistRec.isChanged', true);
    }, 

	dueDateChange: function(cmp, event, helper) {
		cmp.set('v.checklistRec.isChanged', true);
	}

})