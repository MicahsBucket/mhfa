({
	setPicklistOptions: function (cmp, picklistField, picklistOptions, includeNone) {
		var opts = [];
		var picklistOpts = cmp.get('v.' + picklistOptions);
		console.log('Inside setPicklistOptions:\n' + JSON.stringify(picklistOpts));

        if (picklistOpts != undefined && picklistOpts.length > 0 && includeNone) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
        }
		for (var i = 0; i < picklistOpts.length; i++) {
            for (var i = 0; i < picklistOpts.length; i++) {
				if (picklistOpts.length == 1) {
				//if only value is in the picklist, select it
					opts.push({
						class: "optionClass",
						label: picklistOpts[i].itemLabel,
						value: picklistOpts[i].itemValue,
						selected: true
					});
				} else {
					opts.push({
						class: "optionClass",
						label: picklistOpts[i].itemLabel,
						value: picklistOpts[i].itemValue
					});
				}
            }
		}
		console.log('Inside setPicklistOptions length for ' + picklistField + ': ' + opts.length);
		cmp.find(picklistField).set('v.options', opts);
	},

	//since checkbox groups store data in a string array and multipick fields store them as semi-colon separated
	// values, we must convert the data appropriately
	convertMultipickToCheckboxGroup: function (dataField, defaultValues) {
		if (dataField == undefined || dataField == null) {
			return defaultValues.split(';');
		} else {
			return dataField.split(';');
		}
	},

	convertCheckboxGroupToMulitpick: function (arrValues) {
		if (arrValues == undefined || arrValues == null) return null;
		if (Array.isArray(arrValues) == false) return arrValues;
		return arrValues.join(';');
	},

	formatDateForUpdate: function(dateValue) {
		var formattedDate = dateValue + ' 00:00:00';
		console.log('Inside formatDate: ' + dateValue + ' -> ' + formattedDate);
		return formattedDate;
	},

	//enable or disable editable fields based on the checkbox being checked
	toogleEditFields: function(cmp, checkboxItem, hideStage) {
		console.log(checkboxItem + ' checked: ' + cmp.find(checkboxItem).get('v.checked') + ' hideStage: ' + hideStage);
		var arrFields = ['selStage'
						, 'closingTypeCheckboxes'
						, 'chkgrpClosingType'
						, 'txtDueDateOffset'
						, 'selDueDateOffset'
						, 'selApprovalFirst'
						, 'selApprovalSecond'
						, 'selApprovalThird'
						, 'cellDueDateSection'];

		var addToChecklist = cmp.find(checkboxItem).get('v.checked');

		//show/hide fields
		for (var i=0; i<arrFields.length; i++) {
			var selBox = cmp.find(arrFields[i]);
			if (addToChecklist == true) {
				$A.util.removeClass(selBox, 'slds-hide');
				//update the Closing Types to the default values
				cmp.set('v.checklistRec.oChecklistItem.Closing_Types__c', this.convertCheckboxGroupToMulitpick(cmp.get('v.closingTypeValues')));
			} else {
				//clear all the editable fields and hide them
				//selBox.set('v.value', '');
				$A.util.addClass(selBox, 'slds-hide');
			}

			//always hide the stage field if no options are available for it
			if (arrFields[i] == 'selStage' && hideStage) {
				$A.util.addClass(selBox, 'slds-hide');
			}
		}

		
		var tblAppr = cmp.find('tblApprovers');
		if (addToChecklist == true) {			
			//enable the editable fields
			$A.util.removeClass(tblAppr, 'slds-hide');
		} else {
			$A.util.addClass(tblAppr, 'slds-hide');
		}
	},

	toogleDueDateFields: function(cmp) {
		var dueDateDays = cmp.find('txtDueDateOffset');
        var dueDateDaysArea = cmp.find('cellOffsetDays');
		var dueDate = cmp.find('dtDueDate');
        var dueDateArea = cmp.find('cellDueDate');
		var dueDateOffset = cmp.find('selDueDateOffset').get('v.value');

		if (dueDateOffset == 'Prior to Due Date') {
			//show the Offset Days
			$A.util.removeClass(dueDateDaysArea, 'slds-hide');
		} else {
			//clear the Offset Days field and hide it
			dueDateDays.set('v.value', '');
			$A.util.addClass(dueDateDaysArea, 'slds-hide');
		}

		if (dueDateOffset == 'Manually Set Date') {
			//show the Due Date field
			$A.util.removeClass(dueDateArea, 'slds-hide');
		} else {
			//clear the Due Date field and hide it
			dueDate.set('v.value', '');
			$A.util.addClass(dueDateArea, 'slds-hide');
		}
	},

})