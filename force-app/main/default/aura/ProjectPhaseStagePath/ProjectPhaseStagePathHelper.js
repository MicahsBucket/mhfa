({
	loadPaths: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.retrieveProjectPhases');
		action.setParams({
			projectId: cmp.get('v.recordId')
		});
		//Step 2 - process returned data
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				var phaseRecords = [];
				var stageRecords = [];
				//loop through the phases
				for (var oPhase of result) {
					if (oPhase.Project__r.Current_Phase__c == null
							&& (cmp.get('v.currentPhase') == null || cmp.get('v.currentPhase') == '')) {
						cmp.set('v.currentPhase', oPhase.Id);
					} else if (oPhase.Project__r.Current_Phase__c != null) {
						cmp.set('v.currentPhase', oPhase.Project__r.Current_Phase__c);
					}
					phaseRecords.push({label: oPhase.Name
										, value: oPhase.Id});
					//need to determine what the current phase should be so we can set the current phase variable
					// and load the stages for that phase
					if (oPhase.Id == cmp.get('v.currentPhase')) {
						for (var oStage of oPhase.Project_Sub_Phases__r) {
							stageRecords.push({label: oStage.Name
												, value: oStage.Id});
							//if a current stage isn't set yet, assign the first stage
							if (oPhase.Project__r.Current_Stage__c == null
									&& (cmp.get('v.currentStage') == null || cmp.get('v.currentStage') == '')) {
								cmp.set('v.currentStage', oStage.Id);
								cmp.set('v.currentStageName', oStage.Name + ' Details');
							} else if (oPhase.Project__r.Current_Stage__c != null && oPhase.Project__r.Current_Stage__c == oStage.Id) {
								cmp.set('v.currentStage', oPhase.Project__r.Current_Stage__c);
								cmp.set('v.currentStageName', oStage.Name + ' Details');

							}
						}
					}
				}
				cmp.set('v.pathPhases', phaseRecords);
				console.log('phaseRecords: ' + JSON.stringify(phaseRecords));

				cmp.set('v.pathStages', stageRecords);
				console.log('stageRecords: ' + JSON.stringify(stageRecords));

				//once complete, fire the event to load the stage tasks
				console.log('Fire ProjectStageChange');
				var evtProjStageChange = $A.get('e.c:ProjectStageChange');
				evtProjStageChange.setParams({'stageId' : cmp.get('v.currentStage')});
				evtProjStageChange.fire();
				console.log('After fire ProjectStageChange');
			}
		});
		//Step 3 - make the call
		$A.enqueueAction(action);
	},

})