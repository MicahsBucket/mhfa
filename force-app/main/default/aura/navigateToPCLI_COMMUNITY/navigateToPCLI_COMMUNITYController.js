({  
    handleRecordUpdated: function (component, event, helper) {
        var recordId = component.get("v.record.Id");
        var projId = component.get("v.record.Opportunity_ID__c");
        var phaseId = component.get("v.record.Funding_Round_Phase__c");
        var chklstId = component.get("v.record.Funding_Round_Checklist__c");      	
        //external url
        var urlToOpen = '/MultifamilyPortal/s/sfdcpage/' + encodeURIComponent('/apex/ProjectPhase?Id=') + projId + encodeURIComponent('&phaseId=') + phaseId + encodeURIComponent('&checklistId=') + chklstId;        
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
            "url":urlToOpen
        });
        urlEvent.fire();
    }
})