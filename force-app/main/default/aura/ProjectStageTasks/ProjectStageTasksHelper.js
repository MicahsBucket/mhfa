({
	////////////////////////////////////////////////////
	//Show the tasks associated to the provided stage id
	////////////////////////////////////////////////////
	displayStageTasks: function (cmp, event, helper) {
		var stageId = event.getParam('stageId');

		//build columns 
        var actions = [
            { label: 'Edit', name: 'edit_details'},
        ];

		var columns = [
			{
				label: 'Task'
				, fieldName: 'taskLink'
				, type: 'url'
				, typeAttributes: {
					label: {fieldName: 'recName'}
					, target: '_self'
				}
			},
			{
				label: 'Assigned To'
				, fieldName: 'owner'
				, type: 'text'
			},
			{
				label: 'Status'
				, type: 'text'
				, fieldName: 'status'
			},
			{
				label: 'Due Date'
				, type: 'date'
				, fieldName: 'dueDate'
			},
			{
				type: 'action'
				, typeAttributes: {rowActions: actions}
			},
		];
		cmp.set('v.taskColumns', columns);


		//Step 1 - prep the apex code call
		var action = cmp.get('c.retrieveStageTasks');
		action.setParams({
			stageId: stageId
		});
		//Step 2 - process returned data
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				var taskRecords = [];
				//loop through the tasks
				for (var oTask of result) {
					taskRecords.push({id: oTask.Id
										, taskLink: '/' + oTask.Id
										, recName: oTask.Name
                                        , owner: oTask.Owner.Name
                                        , status: oTask.Task_Status__c
										, dueDate: oTask.Due_Date__c});
				}
			}
			cmp.set('v.taskRecords', taskRecords);

		});
		//Step 3 - make the call
		$A.enqueueAction(action);
	},

	////////////////////////////////////////////////////
	//'Edit' menu selected from the data table
	////////////////////////////////////////////////////
	editTaskDetails: function(cmp, event, helper, row) {
		var editRecordEvent = $A.get("e.force:editRecord");
		editRecordEvent.setParams({
			"recordId": row.id
		});
		editRecordEvent.fire();
	}, 

})