({
	////////////////////////////////////////////////////
	//handler for the application event "ProjectStageChange"
	////////////////////////////////////////////////////
	handleStageChange: function (cmp, event, helper) {
		helper.displayStageTasks(cmp, event, helper);
	},

	////////////////////////////////////////////////////
	//handler for the selection of a datatable row action
	////////////////////////////////////////////////////
	handleRowAction: function (cmp, event, helper) {
		var action = event.getParam('action');
		var row = event.getParam('row');

		switch (action.name) {
			case 'edit_details':
				helper.editTaskDetails(cmp, event, helper, row);
				break;
		}
	},

})