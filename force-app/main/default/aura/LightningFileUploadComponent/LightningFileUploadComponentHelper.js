// LightningFileUploadComponentHelper.js
({  
    getUploadedFiles : function(component, event){
        console.log('Inside getUploadedFiles');
        var action = component.get("c.getFiles");  
        action.setParams({  
            "recordId": component.get("v.recordId") 
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                var result = response.getReturnValue();           
                component.set("v.files",result);

                var docIds = result.map(doc => doc.Id);
                component.set("v.documentIds", docIds);
            }  
        });  
        $A.enqueueAction(action);  
    },
    
    deleteUploadedFile : function(component, event) {  
        console.log('Inside deleteUploadedFile.');
        debugger;
        var fileId = component.get("v.idOfFileToDelete");
        console.log('idOfFileToDelete: ' + fileId);
        var action = component.get("c.deleteFileById");
        action.setParams({
            "contentDocumentId": fileId            
        });  
        action.setCallback(this,function(response){  
            console.log('response: ' + response);
            var state = response.getState();  
            component.set("v.showSpinner", false); 
            if(state=='SUCCESS'){  
                this.getUploadedFiles(component);
                component.set("v.showSpinner", false); 
                // show toast on file deleted successfully
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": "File has been deleted successfully!",
                    "type": "success",
                    "duration" : 2000
                });
                toastEvent.fire();
            }
        });  
        $A.enqueueAction(action);  
    },  

 })