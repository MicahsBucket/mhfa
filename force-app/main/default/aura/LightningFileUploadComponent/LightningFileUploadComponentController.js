// LightningFileUploadComponentController.js
({
    doInit : function(component, event, helper){  
       helper.getUploadedFiles(component, event);
    },      
    
    previewFile : function(component, event, helper){  
        $A.get('e.lightning:openFiles').fire({ 
            recordIds: [event.currentTarget.id]
        });  
    },  
    
    uploadFinished : function(component, event, helper) {  
        helper.getUploadedFiles(component, event);    
        var toastEvent = $A.get("e.force:showToast");
        // show toast on file uploaded successfully 
        toastEvent.setParams({
            "message": "Files have been uploaded successfully!",
            "type": "success",
            "duration" : 2000
        });
        toastEvent.fire();
    }, 
    
    confirmDeleteFile : function(component, event, helper){
        console.log('Inside confirmDeleteFile');
        component.set("v.showConfirm", true);
        component.set("v.idOfFileToDelete", event.currentTarget.id);
    },

    handleConfirm : function(component, event, helper) {
        console.log('Inside handleConfirm');
        component.set("v.showSpinner", true); 
        component.set("v.showConfirm", false); 
        helper.deleteUploadedFile(component, event);                
},    

 })