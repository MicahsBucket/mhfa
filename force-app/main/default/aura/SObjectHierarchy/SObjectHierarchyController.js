({
    doInit: function (component, event, helper) { 
        console.log('doInit of component called');
        var trecid = component.get('v.recordId');
        var tsObjectName= component.get('v.ltngSobjectname');
        var tparentFieldAPIname= component.get('v.ltngParentFieldAPIName');
        var thierarchyFieldAPIname= component.get('v.ltngHierarchyFieldAPIName');
        var tchildRelationshipAPIName= component.get('v.ltngChildRelationshipAPIName');
        var tlabelFieldAPIName= component.get('v.ltngLabelFieldAPIName');
		var tadditionalFields= component.get('v.ltngAdditionalFields');
		var apexClassName = 'c.findHierarchyData';

		helper.showGridData(component, event, helper);
    },

    showSpinner: function(component, event, helper) {
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
    },

    hideSpinner: function(component, event, helper) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },

    handleSelect: function (cmp, event, helper) {
        //return name of selected tree item
        var myName = event.getParam('name');
        alert("You selected: " + myName);
    }
})