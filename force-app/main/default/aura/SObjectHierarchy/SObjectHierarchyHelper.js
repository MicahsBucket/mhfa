({
	callToServer : function(component, method, callback, params) {
        console.log('Calling helper callToServer function');
		var action = component.get(method);
        if(params){
            action.setParams(params);
        }
        console.log(JSON.stringify(params));
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert('Processed successfully at server');
                callback.call(this,response.getReturnValue());
            }else if(state === "ERROR"){
                alert('Problem with connection. Please try again.');
            }
        });
		$A.enqueueAction(action);
    },

	//prepTreeGrid
	prepTreeGrid: function(cmp, labelField, additionalFields) {
		var columns = [];
		var colDetails = {};
		colDetails = {
						type: 'url'
						, fieldName: 'recordLink'
						, label: 'Name'
						, typeAttributes: {
							label: {fieldName: 'recName'}
							, target: '_self'
						}
					};
		columns.push(colDetails);
		//load a column for each field provided
		additionalFields.split(',').forEach(function(fld) {
			var fieldDetails = fld.split(':');
			var cleanedFieldName = fieldDetails[0].trim();
			//strip "toLabel(...)" from the field name if it exists
			if (cleanedFieldName.includes('toLabel(')) {
				cleanedFieldName = cleanedFieldName.slice(8);
				cleanedFieldName = cleanedFieldName.slice(0,cleanedFieldName.length-1);
			}
			colDetails = {
							type: fieldDetails[2]
							, fieldName: cleanedFieldName
							, label: fieldDetails[1]
						};
			if (fieldDetails[2] = 'url') {
				var attr = {
							label: {fieldName: fieldDetails[1]}
							, target: '_self'
							};
				colDetails.typeAttributes = attr;
			}
			columns.push(colDetails);
		});		 

		cmp.set('v.gridColumns', columns);
	},

	 showGridData: function (component, event, helper) {
        console.log('doInit of component called');
        var trecid = component.get('v.recordId');
        var tsObjectName= component.get('v.ltngSobjectname');
        var tparentFieldAPIname= component.get('v.ltngParentFieldAPIName');
        var thierarchyFieldAPIname= component.get('v.ltngHierarchyFieldAPIName');
        var tchildRelationshipAPIName= component.get('v.ltngChildRelationshipAPIName');
        var tlabelFieldAPIName= component.get('v.ltngLabelFieldAPIName');
		var tadditionalFields= component.get('v.ltngAdditionalFields');
		var apexClassName = 'c.findHierarchyData';
		var expandedRows = [];

		helper.prepTreeGrid(component, tlabelFieldAPIName, tadditionalFields);

        helper.callToServer(
            component,
            "c.findHierarchyData",
            function(response) {
                var apexResponse = response;
                var results = apexResponse;
				console.log('*******apexResponse:'+JSON.stringify(apexResponse));

				//build the holding structure for the hierarchy records.
                var roles = {};
                roles[undefined] = {
					id: null,
					recordLink: null,
					recName: "Root", 
					_children: [] 
				}; 

				//loop through the records the first time to give each record a position in the array
                apexResponse.forEach(function(v) {
					if (v.expanded) {
						expandedRows.push(v.rec.Id);
					}
                    roles[v.rec.Id] = { 
                        id: v.rec.Id, 
						recordLink: '/' + v.rec.Id,
                        recName: v.rec[tlabelFieldAPIName]
					};

					if (tadditionalFields != null) {
						var holdObj = roles[v.rec.Id];
						tadditionalFields.split(',').forEach(function(fld) {
							var fieldDetails = fld.split(':');
							var fldName = fieldDetails[0].trim();
							//strip "toLabel(...)" from the field name if it exists
							if (fldName.includes('toLabel(')) {
								fldName = fldName.slice(8);
								fldName = fldName.slice(0,fldName.length-1);
							}
							holdObj[fldName] = v.rec[fldName];
						});
						roles[v.rec.Id] = holdObj;						
					}
					console.log('***roles for ' + v.rec.Id + '\n' + JSON.stringify(roles[v.rec.Id]));
	                console.log('***all roles after record:\n'+JSON.stringify(roles));
                });

                console.log('*******Roles after first loop:\n'+JSON.stringify(roles));

				//loop through the records a second time to establish the hierarchical relationship so it can be displayed appropriately
                apexResponse.forEach(function(v) {
					//debugger;
					console.log('Value for v: ' + JSON.stringify(v) + '\nArray position: ' + v.rec[thierarchyFieldAPIname]);
					if (roles[v.rec[thierarchyFieldAPIname]]._children == undefined) {
						roles[v.rec[thierarchyFieldAPIname]]._children = [];
					}
                    roles[v.rec[thierarchyFieldAPIname]]._children.push(roles[v.rec.Id]);   
	                console.log('***all roles after record:\n'+JSON.stringify(roles));
                });
        
                //console.log('*******Roles after children assignment:\n'+JSON.stringify(roles));
				component.set("v.gridData", roles[undefined]._children);
				component.set('v.gridExpandedRows', expandedRows);
            }, 
            {
				recId: trecid,
				sObjectName: tsObjectName,
				parentFieldAPIname: tparentFieldAPIname,
				hierarchyFieldAPIname: thierarchyFieldAPIname,
				childRelationshipAPIname: tchildRelationshipAPIName,
				labelFieldAPIName: tlabelFieldAPIName,
				additionalFieldDetails: tadditionalFields
            }
        );        

	 },

})