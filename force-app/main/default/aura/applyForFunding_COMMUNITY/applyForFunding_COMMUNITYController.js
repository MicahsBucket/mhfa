({  
    handleRecordUpdated: function (component, event, helper) {
        var recordId = component.get("v.recordId");

        //external url
        var urlToOpen = '/MultifamilyPortal/s/sfdcpage/' + encodeURIComponent('/apex/Opportunity_New?frid=') + recordId;        
        var urlEvent = $A.get("e.force:navigateToURL");

        urlEvent.setParams({
            "url":urlToOpen
        });
        urlEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
    }
})