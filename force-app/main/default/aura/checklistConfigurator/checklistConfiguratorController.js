({
    buildChecklist: function (component, event, helper) {
        component.set('v.columns', [
            {label: 'Checklist', fieldName: 'checklistName', type: 'text'},
            {label: 'Approver', fieldName: 'approverRole', type: 'text'}
        ]);
		
        var fetchData = {
            checklistName: "checklistName"
            //approverRole: "approver.roleone"
        };
		
        helper.fetchData(component,fetchData, 10);		
    }
})