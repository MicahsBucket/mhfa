({
    
	fetchData: function (component, fetchData, numberOfRecords) {
        var dataPromise,
		    checklistName;

        fetchData.checklistName = {type : function () {
            return {
				checklistName : helper.getChecklists
			}
        }};

		component.set('v.data', checklistName);
		/*
        dataPromise = this.mockdataLibrary.lightningMockDataFaker(fetchData,numberOfRecords);
        dataPromise.then(function(results) {
            cmp.set('v.data', results);
        });
		*/
    },
	
	
	getChecklists: function(component) {
        // Create the action - call apex class
        var action = component.get("c.getItems");  

		// Set parameters for action/apex class		
		action.setParams({
			frid: component.get("v.frcli.Funding_Round__c"),
		});

        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var results = response.getReturnValue();
				//var arrCols = [];

				//arrCols.push({label: 'Checklist', fieldName: 'Name', type: 'text', sortable: true});
				//component.set('v.columns', arrCols);
				//component.set("v.data", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    }
	/*
	getApprovers: function(component) {
		var action = component.get("c.getApprovers");
        var inputApprovers = component.find("InputApprover");
        var opts=[];
        action.setCallback(this, function(a) {
            opts.push({
                class: "optionClass",
                label: "--- None ---",
                value: ""
            });
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputApprovers.set("v.options", opts);             
        });
        $A.enqueueAction(action);
	}
	*/
})