({
	prepTableColumns: function(cmp) {
		var actions = this.getRowActions.bind(this, cmp);
		var taskColumnLabel = 'Task';
		if (cmp.get('v.taskType') == 'Milestone') {
			taskColumnLabel = 'Milestone';
		}
		var columns = [
			{
				type: 'url'
				, fieldName: 'phaseLink'
				, label: 'Phase'
				, typeAttributes: {
					label: {fieldName: 'phaseName'}
					, target: '_self'
				}
			},
			{
				type: 'url'
				, fieldName: 'stageLink'
				, label: 'Stage'
				, typeAttributes: {
					label: {fieldName: 'stageName'}
					, target: '_self'
				}
			},
			{
				type: 'url'
				, fieldName: 'taskLink'
				, label: taskColumnLabel
				, typeAttributes: {
					label: {fieldName: 'taskName'}
					, target: '_self'
				}
			},
			{
				type: 'boolean'
				, fieldName: 'notApplicable'
				, label: 'Not Applicable'
				, editable: 'true'
			},
			{
				type: 'text'
				, fieldName: 'dueDateBaseline'
				, label: 'Due Date Baseline'
			},
			{
				type: 'date-local'
				, fieldName: 'startDate'
				, label: 'Start Date'
			},
			{
				type: 'number'
				, fieldName: 'durationDays'
				, label: 'Days to Complete'
				, editable: 'true'
			},
			{
				type: 'date-local'
				, fieldName: 'dueDate'
				, label: 'Due Date'
			}
		];
		//The Milestone page has fewer columns than the other page
		if (cmp.get('v.taskType') != 'Milestone') {
			columns.push({
				type: 'text'
				, fieldName: 'assigneeType'
				, label: 'Assignee Type'
			});
			columns.push({
				type: 'text'
				, fieldName: 'projTeamRole'
				, label: 'Assignee Role (Project)'
			});
			columns.push({
				type: 'text'
				, fieldName: 'frTeamRole'
				, label: 'Assignee Role (Funding Round)'
			});
		}
		columns.push({
			type: 'action'
			, typeAttributes: {
				rowActions: actions
			}
		});


		cmp.set('v.gridColumns', columns);
		cmp.set('v.tableColumns', columns);
	},

	getRowActions: function(cmp, row, doneCallback) {
		var actions = [];
		debugger;
		if (row['phaseName'] != undefined) {
			actions = [
				{label: 'Edit', name: 'phase_edit'},
			];
		} else if (row['stageName'] != undefined) {
			actions = [
				{label: 'Edit', name: 'stage_edit'},
			];
		} else if (row['taskName'] != undefined) {
			actions = [
				{label: 'Edit', name: 'task_edit'},
				{label: 'Delete', name: 'task_delete'},
			];
		}

        //Add the actions to the current row
        doneCallback(actions);
	}, 

	loadShowCascade: function(cmp) {
		debugger;
		var action = cmp.get('c.fundingRoundDurationCount');
		action.setParams({
			frId: cmp.get('v.record.Id')
		});
		action.setCallback(this, function(response) {
			debugger;
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				if (result > 0) {
					cmp.set('v.showCascade', true);
				}
			}
		});
		//make the call
		$A.enqueueAction(action);
	},

	loadPhaseStageTable: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.retrievePhaseStageTree');
		action.setParams({
			frId: cmp.get('v.record.Id'),
			taskType: cmp.get('v.taskType')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				var phaseRecords = [];
				//loop through the phases
				for (var oPhaseTree of result) {
					//add phase to grid
					phaseRecords.push({id: oPhaseTree.oPhase.Id
										, phaseLink: '/' + oPhaseTree.oPhase.Id
										, phaseName: oPhaseTree.oPhase.Name
										, dueDateBaseline: oPhaseTree.oPhase.Due_Date_Baseline__c
										, startDate: oPhaseTree.oPhase.Start_Date__c
										, dueDate: oPhaseTree.oPhase.End_Date__c});

					//loop through the stages under each phase if they exist
					if (oPhaseTree.lstStageDetailsAndTasks != undefined) {
						var stageRecords = [];
						for (var oStageDetail of oPhaseTree.lstStageDetailsAndTasks) {
							var baselineText = '';
							if (oStageDetail.oStage.Not_Applicable__c) {
								baselineText = '-- Not Applicable --';
								oStageDetail.oStage.Estimated_Days_to_Complete__c = 0;
							}
							//add stage to grid
							phaseRecords.push({id: oStageDetail.oStage.Id
												, stageLink: '/' + oStageDetail.oStage.Id
												, stageName: oStageDetail.oStage.Name
												, dueDateBaseline: baselineText
												, notApplicable: oStageDetail.oStage.Not_Applicable__c
												, startDate: oStageDetail.oStage.Start_Date__c
												, durationDays: oStageDetail.oStage.Estimated_Days_to_Complete__c
												, dueDate: oStageDetail.oStage.End_Date__c});

							//loop through the tasks associated to each stage if they exist
							if (oStageDetail.oStage.Funding_Round_Tasks__r != undefined) {
								var taskRecords = [];
								for (var oTask of oStageDetail.oStage.Funding_Round_Tasks__r) {
									phaseRecords.push({id: oTask.Id
														, taskLink: '/' + oTask.Id
														, taskName: oTask.Name
														, dueDateBaseline: oTask.Due_Date_Baseline__c
														, durationDays: oTask.Expected_Duration_Days__c
														, dueDate: oTask.Target_Due_Date__c
														, assigneeType: oTask.Assignee_Type__c
														, frTeamRole: oTask.Assignee_Role_Funding_Round_Team__c
														, projTeamRole: oTask.Assignee_Role_Project_Team__c
														});
								}
							}
						}
					}
				}
				cmp.set('v.tableData', phaseRecords);
			}
		});
		//make the call
		$A.enqueueAction(action);
	},
	
	editRecord: function(cmp, recId) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recId
        });
        editRecordEvent.fire();

	},
	
	cascadeStageDateChanges: function(cmp) {
		//Step 1 - prep the apex code call
		//if (confirm('Copy changes to projects associated to this funding round?')) {
			var action = cmp.get('c.cascadeStageChanges');
			action.setParams({
				frId: cmp.get('v.record.Id')
			});
			action.setCallback(this, function(response) {
				var callState = response.getState();
				if (callState === 'SUCCESS') {
				}
			});
			//make the call
			$A.enqueueAction(action);	
		//}
	},

    toastMsg : function(strType, strMessage, strMode ) {
		if (strMode == null) {
			strMode = 'dismissible';
		}       
        var showToast = $A.get("e.force:showToast");
        showToast.setParams({                 
            message : strMessage,  
            type : strType,  
            mode : strMode                
        });   
        showToast.fire();          
    }
})