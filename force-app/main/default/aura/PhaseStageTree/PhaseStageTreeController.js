({
	handleRecordUpdated: function (cmp, event, helper) {
		cmp.set('v.isLoadingData', true);
		helper.prepTableColumns(cmp);
		helper.loadShowCascade(cmp);
		helper.loadPhaseStageTable(cmp);
		cmp.set('v.isLoadingData', false);
	},

	handleRowAction: function(cmp, event, helper) {
		var action = event.getParam('action');
		var row = event.getParam('row');

		switch (action.name) {
			case 'phase_edit':
				//proceed action things to do goes here
				helper.editRecord(cmp, row.id);
                break;
			case 'stage_edit':
				//proceed action things to do goes here
				helper.editRecord(cmp, row.id);
                break;
			case 'task_edit':
				//proceed action things to do goes here
				helper.editRecord(cmp, row.id);
                break;
		}
	},

	handleCellChange: function(cmp,event, helper) {
		cmp.find('dataTable').set('v.suppressBottomBar', false);  		
	},

	handleInlineSave: function(cmp, event, helper) {
		var draftValues = JSON.stringify(event.getParam('draftValues'));
        console.log('draftValues: ' + draftValues);
		var action = cmp.get('c.saveChangedRecords_Duration');
		action.setParams({
			jsonRecs: draftValues
		});

        action.setCallback(this, function(response) {
            var state = response.getState();   
            if (state == 'SUCCESS') {  
				cmp.find('dataTable').set('v.suppressBottomBar', true);  
				cmp.set('v.draftValues', null);
				cmp.find('dataTable').set('v.draftValues', null);  
				helper.loadPhaseStageTable(cmp);
	            $A.get('e.force:refreshView').fire();
                helper.toastMsg( 'success', 'Records Saved Successfully.', 'dismissible');  
            } else {  
				var errors = action.getError();
				var errorMsg = "The item could not be saved due to the following error:  " + errors[0].message;
                helper.toastMsg( 'error', errorMsg, 'sticky');  
            } 
        });
		$A.enqueueAction(action);
	},

	handleRefreshClick: function(cmp, event, helper) {
		var strDrafts = JSON.stringify(cmp.get('v.draftValues'));
		console.log('Draft Values: ' + strDrafts);
		strDrafts = JSON.stringify(cmp.find('dataTable').get('v.draftValues'));
		console.log('Draft Values in table: ' + strDrafts);
		cmp.set('v.draftValues', null);
		cmp.set('v.tableData', null);
        cmp.find('dataTable').set('v.draftValues', null);  
		$A.get('e.force:refreshView').fire();
		helper.loadPhaseStageTable(cmp);
        helper.toastMsg( 'success', 'Page Refreshed.', 'dismissible');  
		$A.get('e.force:refreshView').fire();
	},

	handleCascadeClick: function(cmp, event, helper) {
		cmp.set("v.isRequestingCascade", true);		
	},

    handleConfirm : function(cmp, event, helper) {
		cmp.set("v.isRequestingCascade", false);
		helper.cascadeStageDateChanges(cmp);
		helper.toastMsg( 'success', 'Cascading changes to projects.  An email will be sent when the process is complete.', 'dismissible');
		$A.get('e.force:refreshView').fire();
    }	

})