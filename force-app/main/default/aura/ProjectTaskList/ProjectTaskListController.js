({
	handleRecordUpdated: function (component, event, helper) {
		component.set('v.isLoading', true);
		helper.prepGrid(component);
		helper.loadGrid(component);
	},

    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set('v.sortedBy', fieldName);
        cmp.set('v.sortedDirection', sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
        cmp.set('v.isLoading', false);
    },
})