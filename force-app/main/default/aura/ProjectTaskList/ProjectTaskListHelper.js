({
	prepGrid: function(cmp) {
		var columns = [
			{
				type: 'url'
				, fieldName: 'taskLink'
				, label: 'Task'
				, sortable: true
				, typeAttributes: {
					label: {fieldName: 'recName'}
					, target: '_self'
				}
			},

			{
				type: 'text'
				, fieldName: 'phaseName'
				, label: 'Phase'
				, sortable: true
			},
			{
				type: 'text'
				, fieldName: 'stageName'
				, label: 'Stage'
				, sortable: true
			},		
			{
				type: 'date-local'
				, fieldName: 'dueDate'
				, label: 'Due Date'
				, sortable: true
			},
			{
				type: 'text'
				, fieldName: 'taskStatus'
				, label: 'Task Status'
				, sortable: true
			},
			{
				type: 'text'
				, fieldName: 'taskOwnerRole'
				, label: 'Assignee Role'
				, sortable: true
			},
			{
				type: 'text'
				, fieldName: 'taskOwner'
				, label: 'Task Owner'
				, sortable: true
			},

		];

		var actionsPhase = [
			{label: 'Edit', name: 'task_edit'}
		];

		cmp.set('v.gridColumns', columns);
	},
	
	loadGrid: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.retrieveProjectTasks');
		action.setParams({
			projectId: cmp.get('v.record.Id'),
			taskOwner: cmp.get('v.taskOwner')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				//loop through the tasks
				var taskRecords = [];
				for (var oTask of result) {					
					var ownerRole = null;
					if(oTask.Funding_Round_Task__c != null){  //process project tasks linked to funding round tasks - not ad hoc tasks
						var ownerRole = oTask.Funding_Round_Task__r.Assignee_Role_Project_Team__c;
						if (ownerRole == null || ownerRole == '') {
							ownerRole = oTask.Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c;
						}
					}
					var checklistName = '';
					if (oTask.Project_Checklist__c != null) {
						checklistName = oTask.Project_Checklist__r.Name;
					}
					taskRecords.push({id: oTask.Id
										, taskLink: '/' + oTask.Id
										, recName: oTask.Name
										, phaseName: oTask.Phase__c
										, stageName: oTask.Phase_Stage__c
										, dueDate: oTask.Due_Date__c
										, taskOwnerRole: ownerRole
										, taskStatus: oTask.Task_Status__c
										, taskOwner: oTask.Owner.FirstName + ' ' + oTask.Owner.LastName
									});
				}
				cmp.set('v.gridData', taskRecords);
				cmp.set('v.isLoading', false);
			}
		});
		//make the call
		$A.enqueueAction(action);
	},

	sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get('v.gridData');
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        cmp.set('v.gridData', data);
    },

    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
})