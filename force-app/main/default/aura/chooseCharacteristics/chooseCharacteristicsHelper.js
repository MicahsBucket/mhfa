({
	retrieveGroupings: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.getGroupings');
		action.setParams({
			frId: cmp.get('v.record.Funding_Round__c'),
			frcliId: cmp.get('v.record.Id')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('retrieveGroupings Results:\n' + JSON.stringify(result));
				var arrGroupOptions = [];
				var arrGroupSelections = [];
				var maxOptions = 0;
				for (var oGrpRec of result) {
					//build the array of characteristics to display under each group
					var arrOptions = [];
					for (var oCharRec of oGrpRec.oGrpCharacteristics) {
						var sLabel = oCharRec.Name;
						var sValue = oCharRec.Id;
						arrOptions.push({label: sLabel, value: sValue});
					}
					//Now load the main array which holds the group name, the characteristics to display under the group,
					// and the characteristics to pre-select
					arrGroupOptions.push({key: oGrpRec.oGroup.Name,
										options: arrOptions,
										values: oGrpRec.selectedChars,
										optCount: oGrpRec.oGroup.Characteristic_Count__c});
					if (oGrpRec.oGroup.Characteristic_Count__c > maxOptions) {
						maxOptions = oGrpRec.oGroup.Characteristic_Count__c;
					}

				}
				cmp.set('v.mapOptions', arrGroupOptions);
				cmp.set('v.maximumOptions', maxOptions);
		        cmp.set('v.Spinner', false); 
			}
		});
		//make the call
		$A.enqueueAction(action);
	},
	
	retrieveChecklists: function (cmp) {
		//Step 1 - prep the apex code call
		var action = cmp.get('c.getChecklistOptions');
		action.setParams({
			frId: cmp.get('v.record.Funding_Round__c'),
			frcliId: cmp.get('v.record.Id')
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('retrieveChecklists Results:\n' + JSON.stringify(result));
				var arrChecklists = [];
				for (var oChklstRec of result) {
						arrChecklists.push(oChklstRec);
				}
				cmp.set('v.mapChecklists', arrChecklists);
		        cmp.set('v.Spinner', false); 

			}
		});
		//make the call
		$A.enqueueAction(action);
	},
	
	saveConfiguration: function(cmp) {
		//prepare the selected characteristics
        cmp.set('v.Spinner', true); 
		var arrSelectedChars = [];
		var chkboxGroups = cmp.find('cmpChkboxGrp');
		if (chkboxGroups != undefined) {
			for (var i=0; i<chkboxGroups.length; i++) {
				if (chkboxGroups[i].get('v.value') != null) {
					var selectedItems = chkboxGroups[i].get('v.value');
					for (var j=0; j<selectedItems.length; j++) {
						console.log('Add item: ' + selectedItems[j]);
						arrSelectedChars.push(selectedItems[j]);
					}
				}
			}
		}
		console.log('arrSelectedChars: ' + arrSelectedChars);

		//prepare the checklists
		var arrChecklists = [];
		var checklistChoices = cmp.find('cmpChecklist');
		if (checklistChoices != undefined) {
			for (var i=0; i<checklistChoices.length; i++) {
				arrChecklists.push(JSON.stringify(checklistChoices[i].get('v.checklistRec')));
			}
		}
		console.log('arrChecklists: ' + JSON.stringify(arrChecklists));

		//insert the records
		var action = cmp.get('c.saveCliConfiguration');
		action.setParams({
			frcliId: cmp.get('v.record.Id'),
			lstCharIds: arrSelectedChars,
			lstJsonChecklists: arrChecklists
		});
		action.setCallback(this, function(response) {
			var callState = response.getState();
			if (callState === 'SUCCESS') {
				var result = response.getReturnValue();
				console.log('Results for saveCliConfiguration:\n' + JSON.stringify(result));
				cmp.find('notifLib').showToast({
					"title": "Save Complete",
					"message": "Checklist Item Configuration Saved",
					"variant": "success",
					"mode": "dismissable"
				});
			} else {
				console.log('Results for saveCliConfiguration:\n' + JSON.stringify(response));
				cmp.find('notifLib').showToast({
					"title": "Error",
					"message": "The save failed.  Check dev console.",
					"variant": "error",
					"mode": "sticky"
				});
			}
	        cmp.set('v.Spinner', false); 

		});
		//make the call
		$A.enqueueAction(action);
	},
/*
    // fetch picklist values dynamic from apex controller 
    fetchPickListVal: function(component, fieldName, picklistOptsAttributeName) {
        var action = component.get("c.retrievePicklistValues");
        action.setParams({
            "objectName": component.get("v.objInfoForPicklistValues"),
            "fieldName": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i].itemLabel,
                        value: allValues[i].itemValue
                    });
                }
                component.set("v." + picklistOptsAttributeName, opts);
            }
        });
        $A.enqueueAction(action);
    },
*/
})