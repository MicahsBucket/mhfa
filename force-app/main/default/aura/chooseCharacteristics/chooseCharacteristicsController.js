({
	doInit: function (cmp, event, helper) {
	},

	handleClick: function(cmp, event, helper) {
		var clickedItem = event.getSource().get('v.name');

		//***btnSelected
		//Retrieve the selected values and store them in an array 
		if (clickedItem === 'btnSave') {
			helper.saveConfiguration(cmp);
		}
	},

	//This function is fired after the Funding Round Checklist Item record is retrieved
	handleRecordUpdated: function (cmp, event, helper) {
		//build checkbox groups by retrieving the characteristic groups and characteristics
        cmp.set('v.Spinner', true); 
		helper.retrieveGroupings(cmp);

		//build listing of checklists to associate to the checklist items
        cmp.set('v.Spinner', true); 
		helper.retrieveChecklists(cmp);
	}, 

})