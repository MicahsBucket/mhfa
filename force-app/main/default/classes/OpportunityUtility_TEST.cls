@isTest
private class OpportunityUtility_TEST {

    @testSetup static void setupData() {
		//SUPPORTING DATA=======================================================================================================
		//custom metadata types
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'Opportunity'];
		
		List<Object_ID__mdt> RecordIds = [SELECT ID__c
						  		       	   FROM Object_ID__mdt
						  		       	   WHERE MasterLabel = 'Multifamily User'];

		//FUNDING ROUND DATA FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

		//fetch & stage required funding round data
		Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																      FROM Funding_Round_Checklist_Item__c
																      WHERE Funding_Round__c = :oFundingRound.Id
																      AND Name = 'Pre Selection Checklist Item'];

		Funding_Round_Task__c oFundingRoundStageTaskPreSelection = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
		
		//SUPPORTING DATA FRAMEWORK=================================================================================================
		/* kdj
		//user partner account - added to new model - create from class vs query from class
		Account userAccountPartner = TestDataUtility.createAccount();

		//user partner contact - added to new model - create from class vs query from class
		Contact userContactPartner = TestDataUtility.createContact('PartnerContact',
		                                                           'Test',
															       'tcontact@test.com',
															       '111-111-1111',
															        userAccountPartner.Id);
		
		//user partner - added to new model - create from class vs query from class
        User ownerUserPartner = TestDataUtility.createUserPartner(userContactPartner.Id);
		*/

		//property - added to new model - create from class vs query from class
        Property__c oProperty = TestDataUtility.createProperty();
		/* kdj
		//unacceptable practice
		Unacceptable_Practices__c oUnacceptablePractice = TestDataUtility.createUnacceptablePractice('To Be Determined',
																									 System.today());
		
		//unacceptable practice entity: contact
		Unacceptable_Practices_Entity__c oUnacceptablePracticeContact = TestDataUtility.createUnacceptablePracticeEntity(oUnacceptablePractice.Id,
																														 ownerUserPartner.Id,
																														 null);

		//unacceptable practice entity: account
		Unacceptable_Practices_Entity__c oUnacceptablePracticeAccount = TestDataUtility.createUnacceptablePracticeEntity(oUnacceptablePractice.Id,
																														 null,
																														 userAccountPartner.Id);
		*/
	}

	@isTest static void testOpportunityUtility() {
        //run tests
        Test.startTest();
	        //create project
            System.debug('createProject');
            Funding_Round__c oFundingRound = [Select Id from Funding_Round__c LIMIT 1];
	        Opportunity oProject = TestDataUtility.createProject(oFundingRound.Id);
			
			//user partner account - added to new model - create from class vs query from class
			Account oUserAccountPartner = TestDataUtility.createAccount();

			//user partner contact - added to new model - create from class vs query from class
			Contact oUserContactPartner = TestDataUtility.createContact('PartnerContact',
																		'Test',
																		'tcontact@test.com',
																		'111-111-1111',
																		oUserAccountPartner.Id);

			//user partner - added to new model - create from class vs query from class
			User oOwnerUserPartner = TestDataUtility.createUserPartner(oUserContactPartner.Id);
			
			//unacceptable practice
			Unacceptable_Practices__c recUnacceptablePractice = TestDataUtility.createUnacceptablePractice('To Be Determined',
																											System.today(),
																											oProject.Id);
			
			//unacceptable practice entity: contact
			Unacceptable_Practices_Entity__c recUnacceptablePracticeContact = TestDataUtility.createUnacceptablePracticeEntity(recUnacceptablePractice.Id,
																															   oOwnerUserPartner.Id,
																															   null);

			//unacceptable practice entity: account
			Unacceptable_Practices_Entity__c recUnacceptablePracticeAccount = TestDataUtility.createUnacceptablePracticeEntity(recUnacceptablePractice.Id,
																															  null,
																															  oUserAccountPartner.Id);

            //test for property count aggregation
            Property__c oProperty = [Select Id from Property__c LIMIT 1];
			oProject.Property__c = oProperty.Id;
            update oProject;

			oProject.Property__c = null;
			update oProject;

            //create project workbook
	        System.debug('createProjectWorkbook');
            Project_Workbook__c oProjectWorkbook = TestDataUtility.createProjectWorkbook(oProject.Id);
            
            //create project workbook version
	        System.debug('createProjectWorkbookVersion');
            Project_Workbook_Version__c oProjectWorkbookVersion = TestDataUtility.createProjectWorkbookVersion(oProjectWorkbook.Id, 
            	                                                                                                'Selection Meeting');

            //create project unacceptable practice
            Unacceptable_Practices__c oUnacceptablePractice = [Select Id, Penalty__c From Unacceptable_Practices__c LIMIT 1];
			System.debug('createProjectUnacceptablePractice');
			Project_Unacceptable_Practice__c oProjectUnacceptablePractice = TestDataUtility.createProjectUnacceptablePractice(oUnacceptablePractice.Penalty__c,
																															  oProject.Id,
																															  oUnacceptablePractice.Id);
			//fetch & stage required data
			Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
																  FROM Project_Checklist__c
																  WHERE Name = 'Application'
																  AND Project__c = :oProject.Id];

			Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
																   , Name
															  FROM Project_Sub_Phase__c
															  WHERE Name = 'Application Submittals'
															  AND Project_Phase__r.Name = 'Application'
															  AND Project__c = :oProject.Id];	
            //create project checklist item
            Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
                                                                            FROM Funding_Round_Checklist_Item__c
                                                                            WHERE Funding_Round__c = :oFundingRound.Id
                                                                            AND Name = 'Pre Selection Checklist Item'];
        	System.debug('createProjectChecklistItem');
			Project_Checklist_Item__c oProjectChecklistItem = TestDataUtility.createProjectChecklistItem(oProject.Id, 
																										oFundingRoundChecklistItem.Id,
																										false,
																										null,
																										null,
																										oProjectStagePreSelection.Id,
																										oProjectChecklistPreSelection.Id);
           
            //create project task
            Funding_Round_Task__c oFundingRoundStageTaskPreSelection = [Select Id From Funding_Round_Task__c LIMIT 1];
			System.debug('createProjectStageTask');
			Project_Task__c oProjectTask = TestDataUtility.createProjectStageTask(oProjectStagePreSelection.Id,
																	              oProject.Id,
																	             'Stage Pre Selection Task',
																	             'Pre-Application',
																	              oFundingRoundStageTaskPreSelection.Id);

            //update owner
            Contact userContactPartner = [Select Id, LastName From Contact Where Lastname = 'PartnerContact' LIMIT 1];
            User ownerUserPartner = [Select Id From User Where ContactId = :userContactPartner.Id LIMIT 1];
            oProject.OwnerId = ownerUserPartner.Id;
			update oProject;
			System.debug('Before purge test');
        	System.debug(oProject);
			/* kdj
			//test purge files routine
			oProject.Purge_Date__c = Date.today();
			oProject.Purge_Files__c = true;
			update oProject;
			system.debug('Calling purgeProjectRecords manually');
			List<Opportunity> lstOpps = new List<Opportunity>();
			lstOpps.add(oProject);
			OpportunityUtility.purgeProjectRecords(lstOpps);
			*/
            //delete project
 			delete oProject;
        Test.stopTest();
    }
    /* kdj
	@isTest static void testBatchPurge() {
        //create project
        System.debug('createProject');
        Funding_Round__c oFundingRound = [Select Id from Funding_Round__c LIMIT 1];
        Opportunity oProject = TestDataUtility.createProject(oFundingRound.Id);

        //test purge files batch routine
        oProject.Purge_Date__c = Date.today();
        oProject.Purge_Files__c = true;
        update oProject;

        Test.startTest();
        system.debug('Starting batch test with ' + oProject);

        String soqlStmt = 'Select Id, Name, Project_Status__c, Purge_Files__c, Purge_Date__c, RecordTypeId From Opportunity LIMIT 1';
        batchPurgeProjectFiles oBatch = new batchPurgeProjectFiles(soqlStmt);
		Database.executeBatch(oBatch, 1); 

		//test scheduler
		String jobId_00 = System.schedule('Schedule batch - Test Top of Hour',    '0 0 * * * ?', new batchPurgeProjectFiles_Scheduler());       
		Test.stopTest();	
	}
	*/
}