@isTest
public class FundingRoundTeamMemberUtility_TEST  {
	@isTest static void testFundingRoundTeamMemberUtility() {
	//SUPPORTING DATA=======================================================================================================
    //custom metadata type
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       		     FROM Enable_Triggers__mdt
					  		       		     WHERE MasterLabel = 'FundingRoundTeamMember'];

	//user
	User oTestUser = TestDataUtility.createUser('Multifamily Standard User', 'testuser@mhfa.com');

	//FUNIDNG ROUND FRAMEWORK===============================================================================================
	Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
	
	//PROJECT FRAMEWORK=====================================================================================================
	//create project
    Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

	//run tests                                                        
    Test.startTest();
			Funding_Round_Task__c oFundingRoundStageTaskPreSelection = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
			oFundingRoundStageTaskPreSelection.Assignee_Role_Funding_Round_Team__c = 'LeadArchitect';
			oFundingRoundStageTaskPreSelection.Assignee_Type__c = 'Internal Task';
			update oFundingRoundStageTaskPreSelection;

			Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
																   , Name
															  FROM Project_Sub_Phase__c
															  WHERE Name = 'Application Submittals'
															  AND Project_Phase__r.Name = 'Application'
															  AND Project__c = :oProject.Id];

	    //project task===========================================
		Project_Task__c ptInternal = TestDataUtility.createProjectStageTask(oProjectStagePreSelection.Id,
																        oProject.Id,
																        'Stage Pre Selection Task Internal',
																        'Pre-Application',
																         oFundingRoundStageTaskPreSelection.Id);
		
		//create funding round team member
		Funding_Round_Team_Member__c oFundingRoundTeamMember = TestDataUtility.createFundingRoundTeamMember(oFundingRound.Id,
																										    'LeadArchitect',
																											oTestUser.Id);

		oFundingRoundTeamMember.Team_Role__c = 'LeadHMO';
		update oFundingRoundTeamMember;
	Test.stopTest();
	}
}