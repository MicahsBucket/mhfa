@IsTest
private class FileProcessingParameters_TEST  {
	@isTest static void testFileProcessingParameters() { 
		Test.startTest();
			FileProcessingParameters.assignRunValue(false, //file validation
													true, //share file with non project team members
													true, //share file with project team members
													false, //send email notification
													false); //update parent upload status
		
			FileProcessingParameters.runEmailNotification();
			FileProcessingParameters.runFileValidation();
			FileProcessingParameters.runShareFileNonOpptyTeamMembers();
			FileProcessingParameters.runShareFileOpptyTeamMembers();
			FileProcessingParameters.runUpdateFileParentAttributes();		
		Test.stopTest();
	}
}