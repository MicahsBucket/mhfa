/*
*   {purpose}  	Manage the functionality tied to Project Task automation                         
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   20190912	Eric Gronholz DCS		Created
*   =============================================================================
*/
public class ProjectTaskUtility  {

	///////////////////////////////////////////////////////////////
	// Calculate the Due Date for the given project tasks based
	//  on the configuration of the related Funding Round Task
	///////////////////////////////////////////////////////////////
	public static void assignDueDate(List<Project_Task__c> lstNewTasks, Map<Id, Project_Task__c> mapNewTasks, Map<Id, Project_Task__c> mapOldTasks) {
		//find associated FR Task Records
		Set<Id> setTasksToProcess = new Set<Id>();
		Set<Id> setFRTaskIds = new Set<Id>();
		Set<Id> setProjStageIds = new Set<Id>();
		Set<Id> setProjChecklistIds = new Set<Id>();
		Set<Id> setCompletedTaskIds = new Set<Id>();

		for (Project_Task__c oTask : lstNewTasks) {
			if (oTask.Funding_Round_Task__c != null) {
				setFRTaskIds.add(oTask.Funding_Round_Task__c);
			}
			if (oTask.Project_Stage__c != null) {
				setProjStageIds.add(oTask.Project_Stage__c);
			}
			if (oTask.Project_Checklist__c != null) {
				setProjChecklistIds.add(oTask.Project_Checklist__c);
			}
			//for edited tasks, only assign a Due Date if specific fields changed
			if (mapOldTasks != null) {
				//Project Stage
				if (oTask.Project_Stage__c != mapOldTasks.get(oTask.Id).Project_Stage__c) {
					setTasksToProcess.add(oTask.Id);
				}
				//Funding Round Task
				if (oTask.Funding_Round_Task__c != mapOldTasks.get(oTask.Id).Funding_Round_Task__c) {
					setTasksToProcess.add(oTask.Id);
				}
				//Preceding Task
				if (oTask.Preceding_Task__c != mapOldTasks.get(oTask.Id).Preceding_Task__c) {
					setTasksToProcess.add(oTask.Id);
				}
				//Project Checklist
				if (oTask.Project_Checklist__c != mapOldTasks.get(oTask.Id).Project_Checklist__c) {
					setTasksToProcess.add(oTask.Id);
				}
				//Completed tasks - track ids to determine if there are subsequent tasks needing
				//  due dates assigned
				if ((oTask.Task_Status__c == 'Complete' || oTask.Task_Status__c == 'NA')
						&& oTask.Task_Status__c != mapOldTasks.get(oTask.Id).Task_Status__c) {
					setCompletedTaskIds.add(oTask.Id);
				}
			}
		}

		//fetch Funding Round Task details
		Map<Id, Funding_Round_Task__c> mapFRTasks = new Map<Id, Funding_Round_Task__c>();
		if (setFRTaskIds.size() > 0) {
			mapFRTasks = new Map<Id, Funding_Round_Task__c>([Select Id
																	, Due_Date_Baseline__c
																	, Due_Date_Offset__c
																	, Funding_Round_Due_Date__c
																	, Funding_Round__r.Carryover_Due_Date__c
																From Funding_Round_Task__c
																Where Id in :setFRTaskIds]);
		}

		//fetch Project Stage details
		Map<Id, Project_Sub_Phase__c> mapProjStages = new Map<Id, Project_Sub_Phase__c>();
		if (setProjStageIds.size() > 0) {
			mapProjStages = new Map<Id, Project_Sub_Phase__c>([Select Id
																	, Base_Due_Date__c
																	, Due_Date_Offset__c
																	, Due_Date__c
																From Project_Sub_Phase__c
																Where Id in :setProjStageIds]);
		}

		//loop through the tasks to assign a due date
		for (Project_Task__c oTask : lstNewTasks) {
			Date newDueDate = oTask.Due_Date__c;
			Decimal intOffset = 0;
			if (oTask.Funding_Round_Task__c != null
					&& mapFRTasks.containsKey(oTask.Funding_Round_Task__c)) {
				Funding_Round_Task__c oFrTask = mapFRTasks.get(oTask.Funding_Round_Task__c);
				if (oFrTask.Due_Date_Offset__c != null) {
					intOffset = oFrTask.Due_Date_Offset__c;
				}

				switch on mapFRTasks.get(oTask.Funding_Round_Task__c).Due_Date_Baseline__c {
					when 'Funding Round Due Date' {
						if (oFrTask.Funding_Round_Due_Date__c == 'Carryover Due Date'
								&& oFrTask.Funding_Round__r.Carryover_Due_Date__c != null) {
							newDueDate = oFrTask.Funding_Round__r.Carryover_Due_Date__c;
						}
					}
					when 'Project Stage Due Date' {
						if (oTask.Project_Stage__c != null
								&& mapProjStages.containsKey(oTask.Project_Stage__c)) {
							newDueDate = mapProjStages.get(oTask.Project_Stage__c).Due_Date__c;
						}
					}
					when 'Checklist Submission Status' {
						//due date will be set when the associated checklist is submitted; no work to do here
					}
					when 'Preceding Task Status' {
						//due date will be set when the preceding task is marked complete; no work to do here
					}
				}
				//20191016 EBG - ensure due date does not land on a weekend or holiday.  If it does, subtract 1 business day
				if (newDueDate != null && DateUtility.isBusinessDay(newDueDate) == false) {
					newDueDate = DateUtility.addBusinessDaysToDate(newDueDate, 1);
				}

				//if an offset exists, subtract business days from the due date
				if (newDueDate != null && intOffset > 0) {
					//newDueDate = DateUtility.subtractBusinessDaysFromDate(newDueDate, Integer.valueOf(intOffset));	
					//20191016 EBG - instead of subtracting business days, subtract the offset and make sure the due date does not
					//					land on a weekend or holiday
					newDueDate = newDueDate.addDays(Integer.valueOf(intOffset) * -1);
					if (DateUtility.isBusinessDay(newDueDate) == false) {
						newDueDate = DateUtility.subtractBusinessDaysFromDate(newDueDate, 1);
					}
				}
				oTask.Due_Date__c = newDueDate;
			}
		}

		//update any child tasks whose parent was marked complete
		if (setCompletedTaskIds.size() > 0) {
			ProjectTaskUtility.assignChildTaskDueDates(setCompletedTaskIds, mapNewTasks);	
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Given a set of Task ids, set due dates for any children tasks.
	//  Make this a future method so update the Project Task object does not
	//  interfere with the trigger which caused it to fire
	////////////////////////////////////////////////////////////////////////
	public static void assignChildTaskDueDates(Set<Id> setParentTaskIds, Map<Id, Project_Task__c> mapNewTasks) {
		//fetch child task details
		List<Project_Task__c> lstChildTasks = new List<Project_Task__c>();
		for (Project_Task__c oTask: [Select Id
											, Name
											, Assigned_Date__c
											, Due_Date__c
											, Expected_Duration_Days__c
											, Preceding_Task__c
											, Task_Status__c
										From Project_Task__c
										Where Preceding_Task__c in :setParentTaskIds]) {
			oTask.Assigned_Date__c = Datetime.now();
			//add calendar days and make sure the end date is not a weekend or holiday
			Date newStartDate = Date.today();
			if (mapNewTasks != null && 
					mapNewTasks.containsKey(oTask.Preceding_Task__c) && 
					mapNewTasks.get(oTask.Preceding_Task__c).Completed_Date__c != null) {
				Datetime dttNewStartDate = mapNewTasks.get(oTask.Preceding_Task__c).Completed_Date__c;
				newStartDate = Date.newInstance(dttNewStartDate.year()
												, dttNewStartDate.month()
												, dttNewStartDate.day());
			}

			oTask.Due_Date__c = newStartDate.addDays(Integer.valueOf(oTask.Expected_Duration_Days__c));
			if (DateUtility.isBusinessDay(oTask.Due_Date__c) == false) {
				oTask.Due_Date__c = DateUtility.addBusinessDaysToDate(oTask.Due_Date__c, 1);
			}
			if (oTask.Task_Status__c == 'Not Ready') {
				oTask.Task_Status__c = 'Not Started';
			}
			lstChildTasks.add(oTask);
		}

		if (lstChildTasks.size() > 0) {
			update lstChildTasks;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Given updated checklists, assign a due date to Project Tasks associated to 
	//  the completed checklists
	////////////////////////////////////////////////////////////////////////
	public static void assignChecklistTaskDueDate(List<Project_Checklist__c> lstChecklists, Map<Id, Project_Checklist__c> mapOldChecklists) {
		Set<Id> setChecklistIds = new Set<Id>();
		for (Project_Checklist__c oChecklist : lstChecklists) {
			if (oChecklist.Checklist_Status__c == 'Submitted'
					&& mapOldChecklists != null
					&& oChecklist.Checklist_Status__c != mapOldChecklists.get(oChecklist.Id).Checklist_Status__c) {
				setChecklistIds.add(oChecklist.Id);
			}
		}

		//fetch checklist task details
		List<Project_Task__c> lstChildTasks = new List<Project_Task__c>();
		for (Project_Task__c oTask: [Select Id
											, Name
											, Assigned_Date__c
											, Due_Date__c
											, Expected_Duration_Days__c
											, Funding_Round_Task__r.Due_Date_Baseline__c
											, Project_Checklist__c
											, Task_Status__c
										From Project_Task__c
										Where Project_Checklist__c in :setChecklistIds
											And Funding_Round_Task__r.Due_Date_Baseline__c = 'Checklist Submission Status']) {
			oTask.Assigned_Date__c = Datetime.now();

			//add calendar days and make sure the end date is not a weekend or holiday
			if (oTask.Expected_Duration_Days__c != null) {
				oTask.Due_Date__c = oTask.Assigned_Date__c.date().addDays(Integer.valueOf(oTask.Expected_Duration_Days__c));
				oTask.Due_Date__c = DateUtility.addBusinessDaysToDate(oTask.Due_Date__c, 0);
			}

			if (oTask.Task_Status__c == 'Not Ready') {
				oTask.Task_Status__c = 'Not Started';
			}
			lstChildTasks.add(oTask);
		}

		if (lstChildTasks.size() > 0) {
			update lstChildTasks;
		}	
	}

	///////////////////////////////////////////////////////////////////////////
	// If a final stage task is completed, mark the project stage Complete
	////////////////////////////////////////////////////////////////////////
	public static void completeProjectStage(List<Project_Task__c> lstNewTasks, Map<Id, Project_Task__c> mapOldTasks) {
		Set<Id> setProjectStage = new Set<Id>();

		for (Project_Task__c oPT : lstNewTasks) {
			if (oPT.Final_Stage_Task__c == true
					&& oPT.Project_Stage__c != null
					&& oPT.Task_Status__c == 'Complete'
					&& mapOldTasks.get(oPT.Id).Task_Status__c != oPT.Task_Status__c) {
				setProjectStage.add(oPT.Project_Stage__c);
			}
		}

		Map<Id, Id> mapProjecStage = new Map<Id, Id>();
		List<Project_Sub_Phase__c> lstUpdateStage = new List<Project_Sub_Phase__c>();
		for (Project_Sub_Phase__c oStage : [Select Id
												, Project_Phase__c
												, Stage_Status__c 
											From Project_Sub_Phase__c 
											Where Id in :setProjectStage 
												AND Stage_Status__c != 'Complete']) {
			mapProjecStage.put(oStage.Project_Phase__c, oStage.Id);
			oStage.Stage_Status__c = 'Complete';
			lstUpdateStage.add(oStage);
		}

		if (lstUpdateStage.size() > 0) {
			update lstUpdateStage;
		}

		//determine if all stages under the phases are complete, if so update the phase
		List<Project_Phase__c> lstUpdatePhases = new List<Project_Phase__c>();
		for (Project_Phase__c oPhase : [Select Id
												, Phase_Status__c
												, (SELECT Id
														, Stage_Status__c 
													FROM Project_Sub_Phases__r 
													WHERE Stage_Status__c != 'Complete')
										From Project_Phase__c
										Where Id in :mapProjecStage.keySet()]) {
			//if all stages are complete or if the only one not complete is the one we just updated, mark the phase complete
			if (oPhase.Project_Sub_Phases__r.size() == 0
					|| (oPhase.Project_Sub_Phases__r.size() == 1
						&& setProjectStage.contains(oPhase.Project_Sub_Phases__r[0].Id))) {
				lstUpdatePhases.add(new Project_Phase__c(Id = oPhase.Id, Phase_Status__c = 'Complete'));
			}
		}

		if (lstUpdatePhases.size() > 0) {
			update lstUpdatePhases;
		}
	}
    
	///////////////////////////////////////////////////////////////////////////
	// Assign the owner of the task to the Task Owner field for adhoc tasks
	////////////////////////////////////////////////////////////////////////
    public static void assignTaskOwner(List<Project_Task__c> lstNewTasks) {
        for (Project_Task__c oTask : lstNewTasks) {
            if (oTask.Funding_Round_Task__c == null
                	&& oTask.Task_Owner__c != null
            	    && oTask.OwnerId != oTask.Task_Owner__c) {
				oTask.OwnerId = oTask.Task_Owner__c;
			}
        }
    }

}