global class batchPurgeProjectFiles_Scheduler implements schedulable{
    global void execute(SchedulableContext ctx) {       
        batchPurgeProjectFiles oBatch = new batchPurgeProjectFiles(null);
        Database.executeBatch(oBatch);
    }
}