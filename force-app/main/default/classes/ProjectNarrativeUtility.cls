/*
*   {Purpose}  	Manage the functionality tied to Project Narrative automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   03/01/18	Kevin Johnson DCS		Created
*   =============================================================================
*/

public with sharing class ProjectNarrativeUtility {
	public static string updateNarrativeParent(List<Project_Narrative__c> lstProjectNarrative){
		/*
		*{purpose}		Manages status of parent Project Narrative records: Author and Group
		*
		*{function}		Applies logic stored in Custom Metadata Type against returned Project Narratives.  
		*				-Project Narrative Author Status value is derived and updated to the respective Project Narrative Author record.
		*				-Project Narrative Group Status value is derived and updated to the respective Project Narrative Group record
		*				Status logic contained in Custom Metadata Type 'Project Narrative Status Logic'
		*
		*{assumption}	Mass Narrative status updates will not happen
		*			
		*{trigger}  	ProjectNarrativeEventListener: after update
		*/

		//obtain custom setting ids for reference for functions below
		List<Project_Narrative_Status_Logic__mdt> csAuthorStatusValues = [SELECT Status__c
																		       , Status_Condition__c
						  		       	   							      FROM Project_Narrative_Status_Logic__mdt
						  		       	   							      WHERE Applies_To_Narrative_Author__c = TRUE];

		List<Project_Narrative_Status_Logic__mdt> csGroupStatusValues = [SELECT Status__c
																		       , Status_Condition__c
						  		       	   							      FROM Project_Narrative_Status_Logic__mdt
						  		       	   							      WHERE Applies_To_Narrative_Group__c = TRUE];

		Set<ID> setPNAID = new set<ID>();
		Set<ID> setPNGID = new set<ID>();
		List<String> strNarrativeStatus = new List<String>();
		Set<String> setNarrativeStatus = new Set<String>();
		List<String> strAuthorStatus = new List<String>();
		Set<String> setAuthorStatus = new Set<String>();
		List<Project_Narrative_Author__c> lstProjectNarrativeAuthor = new List<Project_Narrative_Author__c>();
		String errorMsg = null;
		
		for(Project_Narrative__c pnID : lstProjectNarrative){
			setPNAID.add(pnID.Project_Narrative_Author__c);
		}

		//PROCESS PROJECT NARRATIVE AUTHOR
		//fetch narratives related to the author record
		for(Project_Narrative__c pn : [SELECT Id
		                                    , Status__c
		                                    , Project_Narrative_Author__r.Project_Narrative_Group__r.Id
		                               FROM Project_Narrative__c 
		                               WHERE Project_Narrative_Author__c = :setPNAID]){

			strNarrativeStatus.add(pn.Status__c);
			setPNGID.add(pn.Project_Narrative_Author__r.Project_Narrative_Group__r.Id);
		}

		strNarrativeStatus.sort(); //sort list
		setNarrativeStatus.addAll(strNarrativeStatus); //dedupe values
		system.debug('SETNARRATIVESTATUS: ' + setNarrativeStatus);

		Set<String> setAuthorStatusCondition = new Set<String>();
		String parentAuthorStatus = null;
		List<Project_Narrative_Author__c> lstPNAToUpdate = new List<Project_Narrative_Author__c>();
		//process returned Project Narrative records
		for(Project_Narrative__c pn : lstProjectNarrative){
			for(Project_Narrative_Status_Logic__mdt conditions : csAuthorStatusValues){
				String[] splitCondition = conditions.Status_Condition__c.split(',');
				if(setAuthorStatusCondition.isEmpty()){ //first time through loop				           
		            for(String c : splitCondition){
		                setAuthorStatusCondition.add(c);
		            }
				} else { //reset for subsequent loops
					setAuthorStatusCondition.clear();
		            for(String c : splitCondition){
		                setAuthorStatusCondition.add(c);
		            }
				}

				//apply status logic
				if(setNarrativeStatus.contains('Changes Required')){
					parentAuthorStatus = 'Changes Required';
					break;
				} else if(setAuthorStatusCondition.equals(setNarrativeStatus)){
					parentAuthorStatus = conditions.Status__c;
					break;
				} else {  //catch statuses that did not match
					parentAuthorStatus = 'In Process';
				}					
			}
			system.debug('parentAuthorStatus: ' + parentAuthorStatus);
			Project_Narrative_Author__c pnaUpdate = new Project_Narrative_Author__c(Id = pn.Project_Narrative_Author__c,
				                                                                    Status__c = parentAuthorStatus);
			lstPNAToUpdate.add(pnaUpdate);
		}
		
		Boolean authorUpdateSuccess = TRUE;
		//update Project Narrative Author status
		if(!lstPNAToUpdate.isEmpty()){
			Database.SaveResult[] updResult = Database.update(lstPNAToUpdate, false);				
			
			for(Database.SaveResult sr : updResult){
				if(!sr.isSuccess()){
					authorUpdateSuccess = FALSE;
					for(Database.Error err : sr.getErrors()){
						errorMsg = 'Error updating Narrative Author status: ' + err.getStatusCode() + ' - ' + err.getMessage();
					}
				}
			}
		}

		//PROCESS PROJECT NARRATIVE GROUP
		if(authorUpdateSuccess == TRUE){  //continue processing update for narrative group
			//fetch authors related to group record
			for(Project_Narrative_Author__c pna : [SELECT Id
			                                            , Status__c
			                                            , Project_Narrative_Group__r.Id
			                                       FROM Project_Narrative_Author__c 
			                                       WHERE Project_Narrative_Group__c = :setPNGID]){

				strAuthorStatus.add(pna.Status__c);
				lstProjectNarrativeAuthor.add(pna);
			}
			system.debug('lstProjectNarrativeAuthor: ' + lstProjectNarrativeAuthor);
			strAuthorStatus.sort(); //sort list
			setAuthorStatus.addAll(strAuthorStatus); //dedupe values
			system.debug('SETAUTHORSTATUS: ' + setAuthorStatus);

			Set<String> setGroupStatusCondition = new Set<String>();
			String parentGroupStatus = null;
			Map<Id, String> mapGroupStatus = new Map<Id, String>();
			List<Project_Narrative_Group__c> lstPNGToUpdate = new List<Project_Narrative_Group__c>();
			//process returned Project Narrative Author records
			for(Project_Narrative_Author__c pna : lstProjectNarrativeAuthor){
				for(Project_Narrative_Status_Logic__mdt conditions : csGroupStatusValues){
					String[] splitCondition = conditions.Status_Condition__c.split(',');
					if(setGroupStatusCondition.isEmpty()){ //first time through loop				           
			            for(String c : splitCondition){
			                setGroupStatusCondition.add(c);
			            }
					} else { //reset for subsequent loops
						setGroupStatusCondition.clear();
			            for(String c : splitCondition){
			                setGroupStatusCondition.add(c);
			            }
					}

					//apply status logic
					if(setGroupStatusCondition.equals(setAuthorStatus)){
						parentGroupStatus = conditions.Status__c;
						break;
					} else {  //catch statuses that did not match
						parentGroupStatus = 'In Process';
					}						
				}
				//add to map being same parent group ids are processed across narrative author records which could result in dupe ids in update call
				mapGroupStatus.put(pna.Project_Narrative_Group__c, parentGroupStatus);  
			}
			system.debug('mapGroupStatus: ' + mapGroupStatus);
			//build Project Narrative Group update list
			for(ID key : mapGroupStatus.keySet()){
				Project_Narrative_Group__c pngUpdate = new Project_Narrative_Group__c(Id = key,
				                                                                      Status__c = mapGroupStatus.get(key));
				lstPNGToUpdate.add(pngUpdate);
			}
			system.debug('lstPNGToUpdate: ' + lstPNGToUpdate);
			//update Project Narrative Group status
			if(!lstPNGToUpdate.isEmpty()){
				Database.SaveResult[] updResult = Database.update(lstPNGToUpdate, false);				
				
				for(Database.SaveResult sr : updResult){
					if(!sr.isSuccess()){
						authorUpdateSuccess = FALSE;
						for(Database.Error err : sr.getErrors()){
							errorMsg = 'Error updating Narrative Group status: ' + err.getStatusCode() + ' - ' + err.getMessage();
						}
					}
				}
			}
		}
		return errorMsg;
	}
}