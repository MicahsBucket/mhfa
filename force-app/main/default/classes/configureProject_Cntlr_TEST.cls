@isTest 
private class configureProject_Cntlr_TEST {

	@isTest
	private static void test_configureProject() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		Funding_Round_Checklist_Item_Trigger__c oCLIChar = [Select Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
															From Funding_Round_Checklist_Item_Trigger__c
															LIMIT 1];
		List<String> lstCharIds = new List<String>();
		lstCharIds.add(oCLIChar.Funding_Round_Trigger__c);

		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

		
		Test.startTest();

		configureProject_Cntlr.getAvailableCharacteristics(oFundingRound.Id, oProject.Id, 'Pre-Selection');
		configureProject_Cntlr.getAvailableCharacteristics(oFundingRound.Id, oProject.Id, 'Post-Selection');

		configureProject_Cntlr.getAssignedCharacteristics(oFundingRound.Id, oProject.Id, 'Pre-Selection');
		configureProject_Cntlr.getAssignedCharacteristics(oFundingRound.Id, oProject.Id, 'Post-Selection');

		configureProject_Cntlr.insertProjectCharacteristics(oProject.Id, lstCharIds, true);
		configureProject_Cntlr.removeProjectCharacteristics(oProject.Id, lstCharIds);

		Test.stopTest();
	}
}