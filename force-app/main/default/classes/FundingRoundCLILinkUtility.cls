/*
*   {purpose}  	Manage the functionality tied to Funding Round Checklist Item Link automation                         
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   07/26/19	Kevin Johnson DCS		Created
*   =============================================================================
*/

public with sharing class  FundingRoundCLILinkUtility  {
	public static string createPCLILink(List<Funding_Round_Checklist_Item_Link__c> lstFRCLILink){
        /*
        *{purpose}      Manages cascading of new Funding Round Checklist Item Link records to existing Project Checklist Item records
        *
        *{function}     -Fetches related PCLI records via Funding Round Checklist Item ID.  
		*				-New PCLI Link records created reflecting new Funding Round Checklist Item Link record
        *               
        *{trigger}      FundingRoundChecklistItemLinkEventListener: after insert     
        */

		Set<Id> setFRCLIid = new Set<Id>();
		List<Project_Checklist_Item_Link__c> lstPCLILinkToCreate = new List<Project_Checklist_Item_Link__c>();
		String errorMsg = null;

		for(Funding_Round_Checklist_Item_Link__c frclilink : lstFRCLILink){
			setFRCLIid.add(frclilink.Funding_Round_Checklist_Item__c);
		}

		for(Project_Checklist_Item__c pcli : [SELECT Id
		                                           , Funding_Round_Checklist_Item__c
											  FROM Project_Checklist_Item__c
											  WHERE Funding_Round_Checklist_Item__c IN :setFRCLIid]){
			
			for(Funding_Round_Checklist_Item_Link__c frclilink : lstFRCLILink){
				if(pcli.Funding_Round_Checklist_Item__c == frclilink.Funding_Round_Checklist_Item__c){
					Project_Checklist_Item_Link__c newPCLILink = new Project_Checklist_Item_Link__c(Project_Checklist_Item__c = pcli.Id,
			                                                                                        Name = frclilink.Name,
																							        Funding_Round_Checklist_Item_Link__c = frclilink.Id); 
					lstPCLILinkToCreate.add(newPCLILink);
				}
			}
		}
		system.debug('LSTPCLILINKTOCREATE: ' + lstPCLILinkToCreate);
        if(!lstPCLILinkToCreate.isEmpty()){
            Database.SaveResult[] insResult = Database.insert(lstPCLILinkToCreate, false);
            
            for(Database.SaveResult sr : insResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding resouce link record: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
		return errorMsg;
	}
}