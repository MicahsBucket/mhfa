/*
*   {purpose}  	Manage determination of attributes of a running user
*
*   {contact}   ...                
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   3/27/17		Kevin Johnson DCS		Created
*   =============================================================================
*/

public with sharing class UserAttributeUtility {

	public Profile userProfile {get;set;}

	public UserAttributeUtility() {
		/*
		*{purpose}		Manage determination of attributes of a running user.
		*
		*{function}		-Determine Profile of running user.  Return Profile to page to support conditional attribute rendering.
		*				
		*{trigger}  	Override pages: FundingRoundHome, ProjectHome
		*/
	   	userProfile = [SELECT Name
	   						, Id 
		              FROM Profile 
		              WHERE Id = :userinfo.getProfileId()];
	}
}