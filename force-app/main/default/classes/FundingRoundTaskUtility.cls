/*
*   {purpose}   Manage the functionality tied to Funding Round Task automation           
*
*   CHANGE HISTORY
*   =============================================================================
*   Date        Name                    Description
*   04/08/19    Kevin Johnson DCS       Created
*   04/29/19	Eric Gronholz DCS		Added code to set the Target Due Date
*	02/07/20	Kevin Johnson DCS		Refactored processUpdatedFRTask in addition to renaming from processProjectTaskOwner
*   =============================================================================
*/

public without sharing class FundingRoundTaskUtility  {
	public static string processNewFRTask(List<Funding_Round_Task__c> lstFRT, Boolean isInsert){
		/*
		*{purpose}		Populates required data elements upon creation of new Funding Round Task record
		*
		*{function}		-Fetches reqired Stage record to be associated to Funding Round Task based on selected Phase and Stage combination. 
		*				-Obtains and defaults related Task Item.Type value to the Task Type field
		*  
		*{assumptions}	-Phase and Stage picklist values set at creation
		*				-Task Item linked at creation
		*				-Funding Round linked at creation
		*				-Funding Round Phase record exists reflecting Funding Round Task Phase and Stage combination 
		*     
		*{trigger}		FundingRoundTaskEventListener: before insert
		*/	

		Set<Id> setTaskItemId = new Set<Id>();
		Set<Id> setFundingRoundId = new Set<Id>();
		Map<Id, Task_Item__c> mapIDTaskItem = new Map<Id, Task_Item__c>();
		Map<Id, Funding_Round__c> mapIDFundingRound = new Map<Id, Funding_Round__c>();
		String errorMsg = null;
		
		for(Funding_Round_Task__c frt : lstFRT){
			setTaskItemId.add(frt.Task_Item__c);
			setFundingRoundId.add(frt.Funding_Round__c);
		}

		//build map of related task items
		for(Task_Item__c ti : [SELECT Id
								    , Name
								    , Task_Type__c
									, Description__c
							   FROM Task_Item__c
							   WHERE Id IN :setTaskItemId]){
			mapIDTaskItem.put(ti.Id, ti);
		}	

		//build map of related funding rounds
		for(Funding_Round__c fr : [SELECT Id
									    , Name
									    , Close_Date_Time__c
									    , Close_Date__c
								  FROM Funding_Round__c
							      WHERE Id IN :setFundingRoundId]){
			mapIDFundingRound.put(fr.Id, fr);
		}	

		//process passed funding round tasks
		for(Funding_Round_Task__c frt : lstFRT){
			if(!mapIDFundingRound.get(frt.Funding_Round__c).Name.contains('Clone')){   //run if funding round is not cloned
				Id frtFundingRoundId = frt.Funding_Round__c;
				String frtPhase = frt.Phase__c;
				String frtStage = frt.Stage__c;
				if (isInsert) {
					//set the name to the associate task item's name
					frt.Name = mapIDTaskItem.get(frt.Task_Item__c).Name;
					frt.Description__c = mapIDTaskItem.get(frt.Task_Item__c).Description__c;
				}
				if (frtStage != null) {
					for(Phase__c oStage :[SELECT Id
											   , End_Date__c
											   , Phase__c
											   , Sub_Phase__c
										  FROM Phase__c
										  WHERE RecordType.DeveloperName = 'Sub_Phase' //stage record type
										  AND Parent_Phase__r.Funding_Round__c = :frtFundingRoundId
										  AND Phase__c = :frtPhase
										  AND Sub_Phase__c = :frtStage]){
						system.debug('oStage: ' + oStage);
						frt.Stage_Lookup__c = oStage.Id;
						if (oStage.End_Date__c != null 
								&&	(frt.Due_Date_Baseline__c == 'Funding Round Stage Due Date' 
									|| frt.Due_Date_Baseline__c == 'Project Stage Due Date')) {
							frt.Target_Due_Date__c = Date.valueOf(oStage.End_Date__c);
/*
2020.02.17 EBG - need to determine what the due date baseline means.  Does expected duration drive due date?
							if (frt.Final_Stage_Task__c) {
								frt.Target_Due_Date__c = Date.valueOf(oStage.End_Date__c);
							} else {
								frt.Target_Due_Date__c = frt.Target_Due_Date__c.addDays(Integer.valueOf(frt.Expected_Duration_Days__c));
								if (DateUtility.isBusinessDay(frt.Target_Due_Date__c) == false) {
									frt.Target_Due_Date__c = DateUtility.addBusinessDaysToDate(frt.Target_Due_Date__c, 1);
								}
							}
*/
						}
					}
				}
				if (frt.Due_Date_Baseline__c == 'Funding Round Due Date'
						|| frt.Due_Date_Baseline__c == 'Project Due Date') {
					frt.Target_Due_Date__c = mapIDFundingRound.get(frt.Funding_Round__c).Close_Date__c;
				}
				//set the 'ultimate parent' for linked tasks
				if (frt.Preceding_Task__c != null) {
					frt.Initial_Task__c = FundingRoundTaskUtility.findInitialTask(frt.Preceding_Task__c);
				} else {
					frt.Initial_Task__c = null;
				}
			}
		}
		return errorMsg;	
	}

	public static string processUpdatedFRTask(List<Funding_Round_Task__c> lstNewFRT, Map<Id, Funding_Round_Task__c> mapOldFRT){
		/*
		*{purpose}		Cascades inserted/updated Funding Round Task updates to related Project Tasks
		*
		*{function}		Fetches open project tasks along with related project team members,	related funding round team members.
		*				Processes returned project tasks respective to how related funding round task has been dispositioned
		*					Fields updated:
		*					Owner:
		*						if = internal task
		*							-Assign owner by designated project team role if project team designated
		*							-Assign owner by designated funding round team member if funding round team designated
		*						if = external task
		*							-Assign owner = project tasks project owner
		*					Description
		*  
		*{assumptions}	If returned project stage task does not have new owner reflecting related funding round task the owner
		*				will not be updated
		*     
		*{trigger}		FundingRoundTaskEventListener: after update
		*/	

		Set<Id> setFundingRoundTaskIds = new Set<Id>();
		Set<Id> setFundingRoundIds = new Set<Id>();
		Set<Id> setOpptyIDs = new Set<Id>();
		List<Project_Task__c> lstProjectTasks = new List<Project_Task__c>();
		List<Project_Task__c> lstProjectTasksToUpdate = new List<Project_Task__c>();
		Map<ID, Map<String, ID>> mapOTMRoles = new Map<ID, Map<String, ID>>();
		Map<ID, Map<String, ID>> mapFRTMRoles = new Map<ID, Map<String, ID>>();
		String errorMsg = null;

		for(Funding_Round_Task__c frt : lstNewFRT){
			setFundingRoundTaskIds.add(frt.Id);
		}

		//return affected project tasks
		for(Project_Task__c oPT :[SELECT Id
		                                , OwnerId
										, Project_Stage__r.Project__r.OwnerId
										, Project_Stage__r.Project__r.Id
										, Project_Stage__r.Project__r.Funding_Round__r.Id
										, Funding_Round_Task__c
										, Funding_Round_Task__r.Assignee_Type__c
										, Funding_Round_Task__r.Assignee_Role_Project_Team__c
										, Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c
										, Funding_Round_Task__r.Description__c
									FROM Project_Task__c
									WHERE Funding_Round_Task__c IN :setFundingRoundTaskIds
									AND (Task_Status__c != 'Complete'
										OR Task_Status__c != 'NA'
										OR Task_Status__c != 'In Process')]){
			lstProjectTasks.add(oPT);

			//get additional data if role has changed
			if(oPT.Funding_Round_Task__r.Assignee_Role_Project_Team__c != mapOldFRT.get(oPT.Funding_Round_Task__c).Assignee_Role_Funding_Round_Team__c){
				setFundingRoundIds.add(oPT.Project_Stage__r.Project__r.Funding_Round__r.Id);
			}		   
			if(oPT.Funding_Round_Task__r.Assignee_Role_Project_Team__c != mapOldFRT.get(oPT.Funding_Round_Task__c).Assignee_Role_Project_Team__c){
				setOpptyIDs.add(oPT.Project_Stage__r.Project__r.Id);
			}				
		}
		system.debug('FRT lstProjectTasks: ' + lstProjectTasks);

		//fetch project team members - only run if need to gather team roles when role has changed at Funding Round Task
		if(setOpptyIDs.size() >= 1){		
			for(OpportunityTeamMember otm : [SELECT OpportunityId
												  , TeamMemberRole
												  , UserId 
											 FROM OpportunityTeamMember 
											 WHERE OpportunityId = :setOpptyIDs]){        

				Map<String, ID> mapUserRoles = new Map<String, ID>();
				if(mapOTMRoles.containsKey(otm.OpportunityId)){
					mapUserRoles = mapOTMRoles.get(otm.OpportunityId);
				}
				mapUserRoles.put(otm.TeamMemberRole, otm.UserId);
				mapOTMRoles.put(otm.OpportunityId, mapUserRoles);
			}
		}
        system.debug('FRT MAPOTMROLES: ' + mapOTMRoles);
		
		//fetch funding round team members - only run if need to gather team roles when role has changed at Funding Round Task
		if(setFundingRoundIds.size() >= 1){
			for(Funding_Round_Team_Member__c frtm : [SELECT Funding_Round__c
														  , Team_Role__c
														  , Team_Member__c
													 FROM Funding_Round_Team_Member__c
													 WHERE Funding_Round__c = :setFundingRoundIds]){        

				Map<String, ID> mapTeamRoles = new Map<String, ID>();
				if(mapFRTMRoles.containsKey(frtm.Funding_Round__c)){
					mapTeamRoles = mapFRTMRoles.get(frtm.Funding_Round__c);
				}
				mapTeamRoles.put(frtm.Team_Role__c, frtm.Team_Member__c);
				mapFRTMRoles.put(frtm.Funding_Round__c, mapTeamRoles);
			}
			system.debug('FRT MAPFRTMROLES: ' + mapFRTMRoles);
		}

		//process funding round task per project tasks	
		for(Funding_Round_Task__c frt : lstNewFRT){
			for(Project_Task__c pst : lstProjectTasks){
				ID taskOwner = null;

				if(pst.Funding_Round_Task__r.Assignee_Type__c == 'Internal Task'  
                       && pst.Funding_Round_Task__r.Assignee_Role_Project_Team__c != null 
                       && !mapOTMRoles.isEmpty()){  //internal | project team
					if (mapOTMRoles.containsKey(pst.Project_Stage__r.Project__r.Id)
                        	&& mapOTMRoles.get(pst.Project_Stage__r.Project__r.Id).containsKey(frt.Assignee_Role_Project_Team__c)) {
						taskOwner = mapOTMRoles.get(pst.Project_Stage__r.Project__r.Id).get(frt.Assignee_Role_Project_Team__c);
					}
				} else if(pst.Funding_Round_Task__r.Assignee_Type__c == 'Internal Task'
				          && pst.Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c != null
						  && !mapFRTMRoles.isEmpty()){  //internal | funding round team
					if (mapFRTMRoles.containsKey(pst.Project_Stage__r.Project__r.Funding_Round__r.Id)
                        	&& mapFRTMRoles.get(pst.Project_Stage__r.Project__r.Funding_Round__r.Id).containsKey(frt.Assignee_Role_Funding_Round_Team__c)) {
						taskOwner = mapFRTMRoles.get(pst.Project_Stage__r.Project__r.Funding_Round__r.Id).get(frt.Assignee_Role_Funding_Round_Team__c);
					}
				} else {  //external | project owner
					taskOwner = pst.Project_Stage__r.Project__r.OwnerId;
				}
				System.debug('FRT DESCRIPTION: ' + pst.Funding_Round_Task__r.Description__c);
				if(taskOwner != null){  //process if owner has changed - cannot assign null as record owner - leave existing records owner in place
					Project_Task__c pstUpdate = new Project_Task__c(Id = pst.Id
																  , OwnerId = taskOwner
																  , Description__c = pst.Funding_Round_Task__r.Description__c);
					lstProjectTasksToUpdate.add(pstUpdate);
				} else {  //process all other updates outside of owner changing
					Project_Task__c pstUpdate = new Project_Task__c(Id = pst.Id
																  , Description__c = pst.Funding_Round_Task__r.Description__c);
					lstProjectTasksToUpdate.add(pstUpdate);				
				}
			}
		}

		system.debug('FRT lstProjectTasksToUpdate: ' + lstProjectTasksToUpdate);
		if(!lstProjectTasksToUpdate.isEmpty()){
            Database.SaveResult[] updResult = Database.update(lstProjectTasksToUpdate, false);
            
            for(Database.SaveResult sr : updResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding assignee: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
		return errorMsg;		
	}

	public static Id findInitialTask(Id taskId) {
		/*
		*{purpose}		Locates the top task in a series of linked tasks
		*
		*{function}		A recursive routine which peruses the hierarchy of tasks to find the top level
		*  
		*/
		Id retVal = null;

		for (Funding_Round_Task__c frt : [Select Id
												, Preceding_Task__c
												, Preceding_Task__r.Preceding_Task__c 
											From Funding_Round_Task__c
											Where Id = :taskId]) {
			if (frt.Preceding_Task__c == null) {
				retVal = frt.Id;
			} else if (frt.Preceding_Task__r.Preceding_Task__c == null) {
				retVal = frt.Preceding_Task__c;
			} else {
				retVal = FundingRoundTaskUtility.findInitialTask(frt.Preceding_Task__r.Preceding_Task__c);
			}
		}

		return retVal;
	}

	
}