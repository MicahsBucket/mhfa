@isTest
public class ProjectCharacteristicUtility_TEST  {
	@isTest static void testProjectCharacteristicUtility() {
		//SUPPORTING DATA=======================================================================================================
		//custom metadata type
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       				 FROM Enable_Triggers__mdt
					  		       				 WHERE MasterLabel = 'ProjectCharacteristic'];
		
		//FUNIDNG ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

		//PROJECT FRAMEWORK=====================================================================================================
		//create project
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		
		//run tests                                                        
		Test.startTest();
			//move fetching of data w/in test to re-set limits
			//fetch required data
			Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																		  FROM Funding_Round_Checklist_Item__c
																		  WHERE Funding_Round__c = :oFundingRound.Id
																		  And Name = 'Pre Selection Checklist Item'];
	 
			Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
																	   , Project__c
																	   , Project__r.Funding_Round__c
																  FROM Project_Checklist__c
																  WHERE Name = 'Application'
																  AND Project__c = :oProject.Id];
	
			Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
																   , Name
																   , Project__c
																   , Project__r.Funding_Round__c
															  FROM Project_Sub_Phase__c
															  WHERE Name = 'Application Submittals'
															  AND Project_Phase__r.Name = 'Application'
															  AND Project__c = :oProject.Id];

			Funding_Round_Task__c oFundingRoundStageTaskPreSelection = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
			Funding_Round_Task__c oFundingRoundStageTaskPreSelectionChild = TestDataUtility.createFundingRoundTaskPreSelection(oFundingRound.Id, 
																															   oFundingRoundStageTaskPreSelection.Id);
			Funding_Round_Task__c oFundingRoundChecklistTaskPreSelection = TestDataUtility.createFundingRoundChecklistTaskPreSelection(oFundingRound.Id);

			Funding_Round_Trigger__c oFundingRoundCharacteristicPreSelection = [SELECT Id
																					 , Name
																				FROM Funding_Round_Trigger__c
																				WHERE Trigger_Group__r.Funding_Round__c = :oFundingRound.Id
																				AND Name = 'New Construction'
																				AND Trigger_Group__r.Selection_Type__c = 'Pre-Selection'];																

			Funding_Round_Trigger__c oFundingRoundCharacteristicPostSelection = [SELECT Id
																				FROM Funding_Round_Trigger__c
																				WHERE Trigger_Group__r.Funding_Round__c = :oFundingRound.Id
																				AND Trigger_Group__r.Selection_Type__c = 'Post-Selection'];
	
			//create pcli (existing)===========================================
			Project_Checklist_Item__c pcli = TestDataUtility.createProjectChecklistItem(oProject.Id, 
																						oFundingRoundChecklistItem.Id,
																						false,
																						null,
																						null,
																						oProjectStagePreSelection.Id,
																						oProjectChecklistPreSelection.Id);
	 
			//project task (existing)===========================================
			Project_Task__c projstagetask = TestDataUtility.createProjectStageTask(oProjectStagePreSelection.Id,
																				   oProject.Id,
																				   'Stage Pre Selection Task',
																				   'Application Submittals',
																				    oFundingRoundStageTaskPreSelection.Id);
		   
		    Project_Task__c projectstagechildtask = TestDataUtility.createProjectWorkflowTask(projstagetask.Id,
																						      'Stage Pre Selection Task Child',
																						       oProject.Id,
																						       oFundingRoundStageTaskPreSelectionChild.Id);				
			/*
			Project_Task__c projectchecklisttask = TestDataUtility.createChecklistTask(oProjectChecklistPreSelection.Id,
																					   'Checklist Pre Selection Task',
																					   oProject.Id,
																					   oFundingRoundStageTaskPreSelection.Id);
			*/			
			Project_Task__c projecttask = ProjectCharacteristicUtility.createProjectTask(oFundingRoundStageTaskPreSelection,
																						oProjectStagePreSelection,
																						oProjectChecklistPreSelection,
																						oProject);			
			
			Project_Characteristic__c pcpre = new Project_Characteristic__c(Project__c = oProject.Id,
																			Selection_Type__c = 'Pre-Selection',
																			Funding_Round_Characteristic__c = oFundingRoundCharacteristicPreSelection.Id);
			insert pcpre;
			delete pcpre;

			Project_Characteristic__c pcpost = new Project_Characteristic__c(Project__c = oProject.Id,
																			 Selection_Type__c = 'Post-Selection',
																			 Funding_Round_Characteristic__c = oFundingRoundCharacteristicPostSelection.Id);
			insert pcpost;	
		Test.stopTest();
	}
}