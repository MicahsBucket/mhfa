public without sharing class ProjectDataController {
    @AuraEnabled
    public static ProjectDataWrapper getProjectData(Id recordId) {
        System.debug(recordId);
        // List<ProjectDataWrapper> pwrapList = new List<ProjectDataWrapper>();
        Project_Scoresheet__c ps = [SELECT Name, Id, Project__c, Project__r.Name,
                                        (SELECT Id, Name, Project_Scoresheet__c, Available_Points__c, Category_Number__c, 
                                            Category_Sort_Order__c, Category__c, Description__c, Max_Points__c, Points_Claimed__c 
                                            FROM Scoresheet_Points__r), (SELECT Id, Name, Description__c, File_Location__c, Files_Required__c FROM Scoresheet_File_Groups__r)
                                        FROM Project_Scoresheet__c WHERE Id =: recordId];
        System.debug(ps);
        map<String, List<Scoresheet_Points__c>> CategoryToScoresheetMap = new  map<String, List<Scoresheet_Points__c>>();
        map<String, List<Scoresheet_File_Group__c>> CategoryNameToFileGroupId = new  map<String, List<Scoresheet_File_Group__c>> ();
        ProjectDataWrapper pwrap = new ProjectDataWrapper();
        pwrap.pName = ps.Project__r.Name;
        
        for(Scoresheet_Points__c sp: ps.Scoresheet_Points__r){
            if(!CategoryToScoresheetMap.containskey(sp.Category__c)){
                CategoryToScoresheetMap.put(sp.Category__c, new List<Scoresheet_Points__c>());
                CategoryToScoresheetMap.get(sp.Category__c).add(sp);
            }
            else{
                CategoryToScoresheetMap.get(sp.Category__c).add(sp);
            }
            CategoryNameToFileGroupId.put(sp.Category__c, new List<Scoresheet_File_Group__c>());
        }
    
        for(Scoresheet_File_Group__c fg: ps.Scoresheet_File_Groups__r){
            System.debug(fg);
            System.debug(CategoryNameToFileGroupId.get(fg.Name));
            System.debug(CategoryNameToFileGroupId);
            if(CategoryNameToFileGroupId.containsKey(fg.Name)){
                CategoryNameToFileGroupId.get(fg.Name).add(fg);
            }
            System.debug(CategoryNameToFileGroupId);
        }
        System.debug(CategoryNameToFileGroupId);
        List<CategoryList> CL = new List<CategoryList>();
        

        for(String s: CategoryToScoresheetMap.keyset()){
            Decimal catMaxPoints = 0;
            Decimal catClaimedPoints = 0;
            Decimal catAvailablePoints = 0;
            CategoryList l = new CategoryList();
            l.pCategory = s;
            l.pCategoryId = CategoryToScoresheetMap.get(s)[0].Project_Scoresheet__c;
            if(CategoryNameToFileGroupId.get(s).size() > 0){
                l.pCategoryFileId = CategoryNameToFileGroupId.get(s)[0].Id;
                l.pCategoryFileLevel = String.valueOf(CategoryNameToFileGroupId.get(s)[0].File_Location__c);
                if(CategoryNameToFileGroupId.get(s)[0].Description__c != null){
                    l.pCategoryDescription = String.valueOf(CategoryNameToFileGroupId.get(s)[0].Description__c);
                    Integer maxSize = 30;
                    String descriptionString = String.valueOf(CategoryNameToFileGroupId.get(s)[0].Description__c);
                    if(descriptionString.length() > maxSize){
                        l.pCategoryDescriptionTruncated = descriptionString.substring(0,maxSize)+ '...';
                    }
                    else{
                        l.pCategoryDescriptionTruncated = descriptionString.substring(0,descriptionString.length()) + '...';
                    }
                }
                else{
                    l.pCategoryDescriptionTruncated = null;
                }
                
                
            }
            l.pScoreSheetList = CategoryToScoresheetMap.get(s);
            for(Scoresheet_Points__c c: CategoryToScoresheetMap.get(s)){
                if(c.Max_Points__c > 0){
                    catMaxPoints += c.Max_Points__c;
                    pwrap.pMaxPoints += c.Max_Points__c;
                }
                if(c.Points_Claimed__c == true){
                    catClaimedPoints += c.Available_Points__c;
                    pwrap.pClaimedPoints += c.Available_Points__c;
                }
                if(c.Available_Points__c != null){
                    pwrap.pAvailablePoints += c.Available_Points__c;
                    catAvailablePoints += c.Available_Points__c;
                }
            }
            l.pCategoryMaxPoints = catMaxPoints;
            l.pCategoryClaimedPoints = catClaimedPoints;
            l.pCategoryAvailablePoints = catAvailablePoints;
            CL.add(l);
        }
        System.debug(CL);
        pwrap.CatList = CL;

        return pwrap;
    }

    @AuraEnabled
    public static Scoresheet_Points__c setPointsClaimed(Id recordId, boolean claimPoints){
        Scoresheet_Points__c scpnt = [SELECT Id, Name, Points_Claimed__c, Available_Points__c, Max_Points__c, Description__c, Category__c FROM Scoresheet_Points__c WHERE Id =: recordId];
        scpnt.Points_Claimed__c = claimPoints;
        update scpnt;
        return scpnt;
    }

    public class ProjectDataWrapper {

        @AuraEnabled public String pName;
        @AuraEnabled public Decimal pMaxPoints = 0;
        @AuraEnabled public Decimal pClaimedPoints = 0;
        @AuraEnabled public Decimal pAvailablePoints = 0;
        @AuraEnabled public List<CategoryList> CatList;
    }

    public class CategoryList {
        @AuraEnabled public String pCategory;
        @AuraEnabled public String pCategoryDescription;
        @AuraEnabled public String pCategoryDescriptionTruncated;
        @AuraEnabled public String pCategoryId;
        @AuraEnabled public String pCategoryFileId;
        @AuraEnabled public String pCategoryFileLevel;
        @AuraEnabled public Decimal pCategoryMaxPoints;
        @AuraEnabled public Decimal pCategoryClaimedPoints;
        @AuraEnabled public Decimal pCategoryAvailablePoints;
        @AuraEnabled public List<Scoresheet_Points__c> pScoreSheetList;
    }

    @AuraEnabled
    public static string LoadFileToRecord(Id RecordId, contentDocument cd){
        System.debug(RecordId);
        System.debug(cd);

        return 'Success';
    }

    @AuraEnabled
    public static string saveFile(Id idParent, String strFileName, String base64Data) {
        System.debug('idParent: '+ idParent);
        System.debug('strFileName: '+ strFileName);
        System.debug('base64Data: '+ base64Data);
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        // inserting file
        // ContentVersion cv = new ContentVersion();
        // cv.Title = strFileName;
        // cv.PathOnClient = '/' + strFileName;
        // cv.FirstPublishLocationId = idParent;
        // cv.VersionData = EncodingUtil.base64Decode(base64Data);
        // cv.IsMajorVersion = true;
        // insert cv;

        // ContentDocumentLink cdl = new ContentDocumentLink();
        // cdl.LinkedEntityId = idParent;
        // cdl.ContentDocumentId = cv.Id;
        // cdl.ShareType = 'V';
        // cdl.Visibility = 'AllUsers';
        // insert cdl;
        // return cv;
        return 'Success';
    }

    @AuraEnabled
    public static list<FileWrapper> releatedFiles(Id idParent){
        System.debug('idParent'+ idParent);
        list<id> lstConDocs = new list<id>();
        list<contentversion> CVtoReturn = new list<contentversion>();
        list<FileWrapper> fwList = new list<FileWrapper>();
        if(idParent != null){
            for(ContentDocumentLink cntLink : [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: idParent]) {
                lstConDocs.add(cntLink.ContentDocumentId);
            }
            if(!lstConDocs.isEmpty()) {
                CVtoReturn = [SELECT Id, Title, ContentDocumentId, CreatedDate, VersionData  FROM ContentVersion WHERE ContentDocumentId IN :lstConDocs];
                FileWrapper fw = new FileWrapper();
                for(ContentVersion cv: CVtoReturn){
                    fw.Id = cv.Id;
                    fw.Title = cv.Title;
                    fw.ContentDocumentId = cv.ContentDocumentId;
                    fw.CreatedDate = cv.CreatedDate;
                    String urls = String.valueOf(URL.getSalesforceBaseUrl().toExternalForm());
                    system.debug(urls);
                    string newUrl = urls.replace('.my.salesforce', '--c.documentforce');
                    system.debug(newUrl);
                    // fw.fileURL = newUrl + '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId='+cv.Id+'&operationContext=CHATTER&contentId='+cv.ContentDocumentId;
                    fw.fileURL = URL.getSalesforceBaseUrl().toExternalForm() +'/lightning/r/ContentDocument/'+ cv.ContentDocumentId + '/view';
                    //fw.fileURL = EncodingUtil.Base64Encode(cv.VersionData);
                    // fw.fileURL = 'https://mnhousing--dev.lightning.force.com/lightning/r/Scoresheet_File_Group__c/' + cv.ContentDocumentId+ '/view';
                    fwList.add(fw);
                }
                
            }
            else {
                fwList = null;
            }
        }
        return fwList;
    }

    @AuraEnabled
    public static String deleteFile(Id FileIdToDelete){
        System.debug('FileIdToDelete: '+ FileIdToDelete);
        ContentDocument cdToDelete = [SELECT Id FROM ContentDocument WHERE Id =: FileIdToDelete];
        delete cdToDelete;

        return 'Success';
    }

    @AuraEnabled(cacheable=true)
    public static list<String> getApprovedFileMetadata(){
        list<String> fileTypeListToReturn = new list<String>();
        List<Approved_Attachment_Extension__mdt> fileTypeList = [SELECT Id, DeveloperName, MasterLabel, Label, QualifiedApiName 
                                                                    FROM Approved_Attachment_Extension__mdt];

        for(Approved_Attachment_Extension__mdt ext: fileTypeList){
            fileTypeListToReturn.add('.'+ext.DeveloperName);
        }

        return fileTypeListToReturn;
    }

    public class FileWrapper{
        @AuraEnabled public Id Id;
        @AuraEnabled public String Title;
        @AuraEnabled public Id ContentDocumentId;
        @AuraEnabled public Datetime CreatedDate;
        @AuraEnabled public String fileURL;
    }
}
