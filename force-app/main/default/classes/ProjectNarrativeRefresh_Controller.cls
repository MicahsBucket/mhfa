/*
*   {Purpose}   Manage functionality tied to Project Narrative automation              
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   10/26/16    Kevin Johnson DCS       Created
*   02/12/18    Kevin Johnson DCS       Updated to pull pnid from URL parameter to work with new Narrative interface
*   =============================================================================
*/

public with sharing class ProjectNarrativeRefresh_Controller {
    public Id pnID {get;set;}
    public Id frnID {get;set;}
    public Id opptyID {get;set;}
    public boolean editAccess {get;set;}    
    public String narrativeGroup {get;set;}
    public String narrativeAuthor {get;set;}
    public String narrativeType {get;set;}

    //constructor
    public ProjectNarrativeRefresh_Controller() {
        pnID = ApexPages.currentPage().getParameters().get('pnid');
        opptyID = ApexPages.currentPage().getParameters().get('opptyid');
        narrativeGroup = ApexPages.currentPage().getParameters().get('group');
        narrativeAuthor = ApexPages.currentPage().getParameters().get('author');

        //determine if user has access to refresh narrative
        List<Project_Narrative__c> lstPN = [SELECT UserRecordAccess.HasEditAccess
                                                 , Name
                                            FROM Project_Narrative__c
                                            WHERE Id = :pnID];

        editAccess = lstPN[0].UserRecordAccess.HasEditAccess;
        narrativeType = lstPN[0].Name;
    }


    public PageReference refreshNarrative(){
        /*
        *{purpose}      Manages narrative refresh
        *
        *{function}     Returns narrative template data from related Funding Round Narrative template and overwrites
        *               Project Narrative data with returned data
        *               
        *{trigger}      Refresh button on ProjectNarrative_Refresh page
        */
        //return project narrative record to update
        system.debug('REFRESH PNID: ' + pnId);        
        List<Project_Narrative__c> lstPN = [SELECT Narrative__c
                                                 , Funding_Round_Narrative__c
                                                 , Help_Text__c
                                            FROM Project_Narrative__c
                                            WHERE Id = :pnID];
        system.debug('REFRESH LSTPN: ' + lstPN);                                    
        frnID = lstPN[0].Funding_Round_Narrative__c;
                            
        //return related funding round narrative data                            
        List<Funding_Round_Narrative__c> lstRelatedNarrative = [SELECT Narrative__c
                                                                     , Help_Text__c
                                                                FROM Funding_Round_Narrative__c
                                                                WHERE Id = :frnID];

        //update project narrative with returned data
        lstPN[0].Narrative__c = lstRelatedNarrative[0].Narrative__c;
        lstPN[0].Help_Text__c = lstRelatedNarrative[0].Help_Text__c;
        update lstPN;
        
        PageReference resultPage = new PageReference('/apex/ProjectNarrative_Edit?opptyid='+opptyID+'&group='+narrativeGroup+'&author='+narrativeAuthor);
        resultPage.setRedirect(true);
        return resultPage;
    }

  public PageReference returnToNarrativeListPage(){
    PageReference pg =  new Pagereference('/apex/ProjectNarrative_Edit?opptyid='+opptyID+'&group='+narrativeGroup+'&author='+narrativeAuthor);
    pg.setRedirect(true);
    return pg;
  }
}