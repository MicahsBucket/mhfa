global class batchPurgeProjectFiles implements Database.Batchable<SObject> {

    public String soqlStmt;
    global final Datetime dttJobStart;

    //constructor

	public batchPurgeProjectFiles(String soql) {
        dttJobStart = Datetime.now();
        system.debug('batchPurgeProjectFiles Constructor.  soqlStmt: ' + soql);
        if (soql == '' || soql == null) {
            soqlStmt = 'Select Id'
                        + ', Name'
                        + ', Project_Status__c'
                        + ', Purge_Files__c'
                        + ', Purge_Date__c'
                        + ', RecordTypeId'
                    + ' From Opportunity'
                    + ' Where Purge_Files__c = true';
        } else {
            soqlStmt = soql;
        }
        
        system.debug('End batchPurgeProjectFiles Constructor.  soql: ' + soqlStmt);
	}


    ////////////////////////////////////////////////
    //base method that runs when the class is instantiated.  Its return value is sent to the "execute" routine
    ////////////////////////////////////////////////
    global Database.QueryLocator start(Database.BatchableContext BC) {
	    system.debug('\n\n******Inside start.  soqlStmt:\n' + soqlStmt + '\n*********\n');
      
        return Database.getQueryLocator(soqlStmt);
    }

    ////////////////////////////////////////////////
    // EXECUTE
    ////////////////////////////////////////////////
    global void execute(Database.BatchableContext BC, List<SObject> jobRecords) {
        system.debug('inside Execute: ' + jobRecords);
        List<Opportunity> lstOpps = new List<Opportunity>();

		for (SObject oRec : jobRecords) {
            Opportunity oProject = (Opportunity)oRec;
            system.debug('Processing project ' + oProject.Name);
            lstOpps.add(oProject);
        }

        if (lstOpps.size() > 0) {
            system.debug('calling updateDueDates with \n' + lstOpps);
            OpportunityUtility.purgeProjectRecords(lstOpps);
        }

    }

    ////////////////////////////////////////////////
    // FINISH
    ////////////////////////////////////////////////
    global void finish(Database.BatchableContext BC) {
              
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [Select Id
                                , CompletedDate
                                , Status
                                , ExtendedStatus
                                , JobType
                                , NumberOfErrors
                                , JobItemsProcessed
                                , TotalJobItems 
                            from AsyncApexJob 
                            where Id = :BC.getJobId()];
        
        // Send an email to the user in the custom metadata object "System Notification Setting".
        String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        for (System_Notification_Setting__mdt oEmailSetting : [Select MasterLabel, Email_Address__c
                                                                From System_Notification_Setting__mdt
                                                                Where MasterLabel = 'batchPurgeProjectFiles']) {
            toAddresses = new String[]{oEmailSetting.Email_Address__c};
            break;
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setSubject('Project Purge routine completed');
        String strBody = 'The routine to purge projects that are \'Withdrawn\' or \'Non-Select\' has completed.' +
                        '\n\nStart Date/Time: ' + dttJobStart.format('MM/dd/yyyy h:mm a') +
                        '\nCompleted Date/Time: ' + a.CompletedDate.format('MM/dd/yyyy h:mm a') +
                        '\nJob Status: ' + a.Status +
                        '\nTotal Job Items Processed: ' + a.JobItemsProcessed +
                        '\nTotal Batches Processed: ' + a.TotalJobItems;
        if (a.NumberOfErrors > 0) {
            strBody += '\nProjects with Errors: ' + a.NumberOfErrors +
                        '\nError Details: ' + a.ExtendedStatus;
        }

        system.debug(strBody);

        if (a.NumberOfErrors > 0 || a.JobItemsProcessed > 0) {
            mail.setPlainTextBody(strBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }    

}