public class characteristicTaskLookup_Cntrl {
    
    ////////////////////////////////////////////////////////////////////
    //  Controller for Lightning Component characeteristicTaskLookup.
    //  Retrieve Funding Round Task Characteristic records for
    //  the provided funding round characteristic.
    ////////////////////////////////////////////////////////////////////
    @AuraEnabled
    public static List<String> getCharacteristicTasks(Id characteristicId) {
        List<String> lstTaskIds = new List<String>();
        
        for (Funding_Round_Task_Characteristic__c oFRTC : [Select Funding_Round_Task__c
                                                          	From Funding_Round_Task_Characteristic__c
															Where Funding_Round_Characteristic__c = :characteristicId]) {
        	lstTaskIds.add((String)oFRTC.Funding_Round_Task__c);                                                       
		}
        return lstTaskIds;
    }

}