@isTest 
private class ProjectStageUtility_TEST {

	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());
        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		System.debug('After buildProjectDataStructure.  Queries: ' + Limits.getQueries());
	}

	@isTest
	private static void testAssignPhaseStage() {
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id
									, Name
									, Funding_Round__c 
									, (Select Id, Name From Project_Phases__r)
									, (Select Id, Name, Project_Phase__c, Stage_Status__c From Project_Sub_Phases__r)
								From Opportunity LIMIT 1];

		Project_Sub_Phase__c oStage = new Project_Sub_Phase__c(Id = oProject.Project_Sub_Phases__r[0].Id
																, Stage_Status__c = oProject.Project_Sub_Phases__r[0].Stage_Status__c);
		oStage.Stage_Status__c = 'Complete';
		update oStage;

		oStage.Stage_Status__c = 'In Process';
		update oStage;
	}
}