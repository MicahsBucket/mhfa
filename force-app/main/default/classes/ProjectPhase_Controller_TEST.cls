@isTest
private class ProjectPhase_Controller_TEST {
	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());

        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		System.debug('After buildProjectDataStructure.  Queries: ' + Limits.getQueries());

		for (Funding_Round_Trigger__c oFrChar : [Select Id
													, Name
													, Trigger_Group__r.Name
													, Selection_Type__c 
												From Funding_Round_Trigger__c 
												Where Trigger_Group__r.Funding_Round__c = :oFundingRound.Id]) {
			System.debug('oFrChar: ' + oFrChar);
			TestDataUtility.createProjectCharacteristic(oFrChar.Id, oProject.Id, oFrChar.Selection_Type__c);
		}

	}
	
	@isTest static void test_ProjectPhaseController() {
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];

		//get Application checklist and an item linked to it
		Phase_Checklist_Item__c oAppChecklistItem = [Select Id
														, Name
														, Funding_Round_Checklist_Item__c
														, Phase_Checklist__c
														, Phase_Checklist__r.Name
														, Phase_Checklist__r.Funding_Round__c
														, Phase_Checklist__r.Create_Project_Workbook_Version__c
														, Phase_Checklist__r.Workbook_To_Create_Primary__c
														, Phase_Checklist__r.Workbook_To_Create_Secondary__c
														, Phase_Checklist__r.Workbook_Version_Parent_Primary__c
														, Phase_Checklist__r.Workbook_Version_Parent_Secondary__c
													From Phase_Checklist_Item__c 
													Where Phase_Checklist__r.Funding_Round__c = :oFundingRound.Id 
														And Phase_Checklist__r.Name = 'Application' LIMIT 1];
		System.debug('oAppChecklistItem: ' + oAppChecklistItem + '\nChecklist Name: ' + oAppChecklistItem.Phase_Checklist__r.Name);

        List<Project_Phase__c> lstProjectPhases = [Select Id
							, Name
							, Funding_Round_Phase__c
							, Opt_Out__c
							, Phase_Status__c
							, Show_in_Developer_View__c
							, Sort_Order__c
							, Submission_Date__c
							, Submitted_By__c
							, View_Checklist__c
						From Project_Phase__c
						Where Project__c = :oProject.Id
							And Name = 'Application'
						Order by Sort_Order__c LIMIT 1]; 		                                               

        Test.startTest();

	system.debug('lstProjectPhases:\n' + lstProjectPhases);        											

        PageReference pageRef = Page.ProjectPhase;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('phaseId', String.valueOf(lstProjectPhases[0].Funding_Round_Phase__c));
        ApexPages.currentPage().getParameters().put('checklistid', String.valueOf(oAppChecklistItem.Phase_Checklist__c));
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(oProject.Id));

        ApexPages.StandardController sc = new ApexPages.StandardController(oProject);
        ProjectPhase_Controller oController = new ProjectPhase_Controller(sc);

	PhaseTriggers_Controller oPhaseConfigure_Cntlr = new PhaseTriggers_Controller();
        oPhaseConfigure_Cntlr.oCurrentPhaseDetails = oController.oPhaseDetails_Data;
	oPhaseConfigure_Cntlr.oCurrentProject = oProject;

	//create project checklist items by looping through each trigger group and trigger in the phase, and checking
	// each trigger
	for (Integer counterTrgGroups = 0; 
			counterTrgGroups < oPhaseConfigure_Cntlr.oCurrentPhaseDetails.lstTriggerGroups.size(); 
			counterTrgGroups++) {

		for (Integer counterTrigs = 0; 
					counterTrigs < oPhaseConfigure_Cntlr.oCurrentPhaseDetails.lstTriggerGroups[counterTrgGroups].lstTriggers.size(); 
					counterTrigs++) {
			oPhaseConfigure_Cntlr.oCurrentPhaseDetails.lstTriggerGroups[counterTrgGroups].lstTriggers[counterTrigs].isChecked = true;
		}
	}

	oPhaseConfigure_Cntlr.saveChanges();

	//reset the controller
	oController = new ProjectPhase_Controller(sc);
        oController.getPhaseTabs();
        oController.optOutChange();

        //oController.loadConfigureTab();  //kdj: added
        oController.loadManageTab();  //kdj: added
        
//	Map<Id, Map<Id, Project_Checklist_Item_Attributes__c>> mapTriggers = ProjectCLI_Methods.retrievePCLIs_Trigger(oProject.Id);
//	Map<Id, Map<Id, Project_Checklist_Item_Attributes__c>> mapSubPhases = ProjectCLI_Methods.retrievePCLIs_SubPhase(oProject.Id);
	Test.stopTest();
	}	
}