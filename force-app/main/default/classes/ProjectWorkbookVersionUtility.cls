/*
*   {purpose}   Manage the functionality tied to Project Workbook Version automation                
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   01/17/17    Kevin Johnson DCS       Created
*   09/06/17    Kevin Johnson DCS       Updated processPWBVExternalShare to share with Project Team in addition to Project Owner
*   10/30/17    Kevin Johnson DCS       Updated processPWBVExternalShare to accomodate pre/post selection requirements
*   10/22/18    Kevin Johnson DCS       Added updateParentPWBAttributes method
*   =============================================================================
*/

public without sharing class ProjectWorkbookVersionUtility {
    //without sharing on class to ensure users have capability to insert sharing records
    public static String processPWBVExternalShare(List<Project_Workbook_Version__c> lstPWBV, String function, String phase){
        /*
        *{purpose}      Manages share of Project Workbook Version to partner user.
        *
        *{function}     Pre-Selection: Determines FACP partners shared at Project's related Funding Round.  Data used to share PWBV to all returned partner users
        *               so as to expose record/related File to FACP partners in the Partner Community.  Note, deletion of shares is triggered via Funding Round 
        *               button 'Unshare Related Projects'.  All PWBV records shared during pre-selection phase are removed.
        *
        *               Post Selection: Determines PWBV related Project ID and its owner/project team.  Data used to share PWBV to the partner user (owner of related Project)
        *               and Project's team so as to expose record/related File to partners in the Partner Community and Project's team members.  
        *               Trigger determines if PWBV is flagged to be public.  If public record passed from trigger to function.
        *
        *{assumption}   Mass/bulk sharing will not occur accross funding rounds
        *               
        *{trigger}      ProjectWorkbookVersionEventListener: after insert, after update
        */

        //create id sets used for soql queries
        Set<ID> setOpptyids = new Set<ID>();
        Set<ID> setPWBVids = new Set<ID>();
        Set<ID> setFRid = new Set<ID>();
        Map<ID, ID> mapOpptyIdPWBVId = new Map<ID, ID>();
        for(Project_Workbook_Version__c pwbv : lstPWBV){
            setOpptyids.add(pwbv.Opportunity_ID__c);
            setPWBVids.add(pwbv.Id);
            mapOpptyIdPWBVId.put(pwbv.Opportunity_ID__c, pwbv.Id);
        }

        //obtain pwbv related data
        for(Project_Workbook_Version__c pwbv : [SELECT Project_Workbook__r.Project__r.Funding_Round__c
                                                FROM Project_Workbook_Version__c
                                                WHERE Id = :setPWBVids]){
            setFRid.add(pwbv.Project_Workbook__r.Project__r.Funding_Round__c);
        }

        String errorMsg = null;

        if(function == 'Insert'){
            List<Project_Workbook_Version__Share> lstShareInsert = new List<Project_Workbook_Version__Share>();         
            //create share records respective to selection phase
            if(phase == 'PreSelection'){
                //create project owner share records: pre-selection
                //share with facp shared at funding round:
                //-adds funding rounds manual shares to funding round's related projects and projects related project document
                List<Funding_Round__Share> lstFRShare = new List<Funding_Round__Share>();
                //build list of related funding_round__share records
                for(Funding_Round__Share frs : [SELECT AccessLevel
                                                     , UserOrGroupId
                                                     , RowCause
                                                FROM Funding_Round__Share
                                                WHERE ParentId = :setFRid]){
                    String strUserOrGroupId = frs.UserOrGroupId;
                    if(strUserOrGroupId.startswith('00G') && frs.RowCause == 'Manual'){  //add manually shared entities with 00G prefix DO NOT add custom object sharing rule
                        lstFRShare.add(frs);
                    }           
                }
                system.debug('LSTFRSHARE: ' + lstFRShare);
                //build list of pwbv share records to insert
                for(Opportunity oOppty : [SELECT Id
                                               , OwnerId
                                          FROM Opportunity
                                          WHERE Id IN :setOpptyids]){
                    system.debug('OOPPTY: ' + oOppty);
                    for(Project_Workbook_Version__c pwbv : lstPWBV){  //process passed pwbv
                            for(Funding_Round__Share frs : lstFRShare){
                                                                    Project_Workbook_Version__Share newPWBVShare = new Project_Workbook_Version__Share();
                                                                    newPWBVShare.ParentId = mapOpptyIdPWBVId.get(oOppty.Id);
                                                                    newPWBVShare.AccessLevel = 'Read';
                                                                    newPWBVShare.UserOrGroupId = frs.UserOrGroupId;
                                                                    newPWBVShare.RowCause = 'Public_Share__c';

                                lstShareInsert.add(newPWBVShare);
                        }
                    }
                }
            } else if(phase == 'PostSelection'){
                //create project owner share records: post selection
                //-share with project owner (developer)
                for(Opportunity oOppty : [SELECT Id
                                               , OwnerId
                                          FROM Opportunity
                                          WHERE Id IN :setOpptyids]){
                    system.debug('OOPPTY: ' + oOppty);
                    for(Project_Workbook_Version__c pwbv : lstPWBV){  //process passed pwbv
                        Project_Workbook_Version__Share newPWBVShare = new Project_Workbook_Version__Share();
                        newPWBVShare.ParentId = mapOpptyIdPWBVId.get(oOppty.Id);
                        newPWBVShare.AccessLevel = 'Read';
                        newPWBVShare.UserOrGroupId = oOppty.OwnerId;
                        newPWBVShare.RowCause = 'Public_Share__c';

                        lstShareInsert.add(newPWBVShare);
                    }
                }
            }
            system.debug('LSTSHARETOINSERTOWNER: ' + lstShareInsert);
            //create project team share records
            //-share with project team users
            for(OpportunityTeamMember oOpptyTeam : [SELECT OpportunityId
                                                         , UserId
                                                   FROM OpportunityTeamMember
                                                   WHERE OpportunityId IN :setOpptyids]){
                system.debug('OPPTYTEAM: ' + oOpptyTeam);
                for(Project_Workbook_Version__c pwbv : lstPWBV){  //process passed pwbv
                    Project_Workbook_Version__Share newPWBVShare = new Project_Workbook_Version__Share();
                    newPWBVShare.ParentId = mapOpptyIdPWBVId.get(oOpptyTeam.OpportunityId);
                    newPWBVShare.AccessLevel = 'Read';
                    newPWBVShare.UserOrGroupId = oOpptyTeam.UserId;
                    newPWBVShare.RowCause = 'Public_Share__c';

                    lstShareInsert.add(newPWBVShare);
                }
            }
            system.debug('LSTSHARETOINSERTOWNERTEAM: ' + lstShareInsert);

            if(!lstShareInsert.isEmpty()){
                Database.SaveResult[] insResult = Database.insert(lstShareInsert, false);               
                
                for(Database.SaveResult sr : insResult){
                    if(!sr.isSuccess()){
                        for(Database.Error err : sr.getErrors()){
                            errorMsg = 'Error adding share: ' + err.getStatusCode() + ' - ' + err.getMessage();
                        }
                    }
                }
            }
        } 

        //DELETE SHARE RECORDS==================================================
        if(function == 'Delete'){
            List<Project_Workbook_Version__Share> lstShareDelete = new List<Project_Workbook_Version__Share>();
            String queryShare = 'SELECT ID' +
                                ' FROM Project_Workbook_Version__Share' +
                                ' WHERE ParentId IN :setPWBVids' +
                                ' AND RowCause = \'Public_Share__c\'';  //owner of related project added to share
            lstShareDelete = Database.query(queryShare);
            system.debug('DELETE SHARE: ' + lstShareDelete);
            if(!lstShareDelete.isEmpty()){
                Database.DeleteResult[] delResult = Database.delete(lstShareDelete, false);
                
                for(Database.DeleteResult dr : delResult){
                    if(!dr.isSuccess()){
                        for(Database.Error err : dr.getErrors()){
                            errorMsg = 'Error removing share: ' + err.getStatusCode() + ' - ' + err.getMessage();
                        }
                    }
                }
            }
        }
        return errorMsg;
    }

    public static String validateRecordParameters(List<Project_Workbook_Version__c> lstPWBV){
        /*
        *{purpose}      Manages validation of Project Workbook Version records being added
        *
        *{function}     Builds list of Project Workbook Version (PWBV) records per parent Project Workbook for use in validating attributes of new PWBV record.  
        *               If a PWBV record (inserted new) does not meet requirements an error is passed notifying user of flagged validation.
        *               
        *{trigger}      ProjectWorkbookVersionEventListener: before insert, before update
        */

        Set<ID> setPWBids = new Set<ID>();
        Set<ID> setVersionIds = new Set<ID>();
        for(Project_Workbook_Version__c pwbv : lstPWBV){
            setVersionIds.add(pwbv.Id);
            setPWBids.add(pwbv.Project_Workbook__c);
        }

        Map<Id, List<String>> mapPWBVersions = new Map<Id, List<String>>();

        //compile existing pwbv records
        for(Project_Workbook_Version__c oPWBV : [SELECT Project_Workbook__c
                                                      , Workbook_Version__c
                                                 FROM Project_Workbook_Version__c
                                                 WHERE Project_Workbook__c IN :setPWBids
                                                 AND Id NOT IN :setVersionIds]) {
                    if(mapPWBVersions.containsKey(oPWBV.Project_Workbook__c)) {
                        List<String> ver = mapPWBVersions.get(oPWBV.Project_Workbook__c);
                        ver.add(oPWBV.Workbook_Version__c);
                        mapPWBVersions.put(oPWBV.Project_Workbook__c, ver);
                    } else {
                        mapPWBVersions.put(oPWBV.Project_Workbook__c, new List<String> {oPWBV.Workbook_Version__c});
                    }
        }
        system.debug('MAPPWBVERSIONS: ' + mapPWBVersions);

        String errorMsg = null;
        for(Project_Workbook_Version__c pwbv : lstPWBV){
            //validate if newly added project workbook version's version already exists for parent project workbook
            List<String> lstExistingVersions = mapPWBVersions.get(pwbv.Project_Workbook__c);
            system.debug('LSTEXISTINGVERSIONS: ' + lstExistingVersions);
            Set<String> setExistingVersions = new Set<String>();
            if(lstExistingVersions != null){
                setExistingVersions.addAll(lstExistingVersions);
                
                String addedVersion = pwbv.Workbook_Version__c;
                if(setExistingVersions.contains(addedVersion)){
                    errorMsg = 'Version ' + addedVersion + ' already exists for this Project Workbook and thus cannot be added.';
                    break;
                }
            }           
        }
        return errorMsg;        
    }

    public static String updateParentPWBAttributes(List<Project_Workbook_Version__c> lstPWBV){
        /*
        *{purpose}      Manages updating attributes at parent Project Workbook record
        *
        *{function}     Builds list of Project Workbook Version (PWBV) records per parent Project Workbook (PWB) for use in updating attributes of parent PWBV record.  
        *               -File count update: Related PWBV records file counts are queried and aggregated.  Aggregated count populated at parent PWB record.
        *               
        *{trigger}      ProjectWorkbookVersionEventListener: after update
        */

        Set<Id> setPWBids = new Set<Id>();
        List<Project_Workbook__c> lstPWBToUpdate = new List<Project_Workbook__c>();
        String errorMsg = null;

        for(Project_Workbook_Version__c pwbv : lstPWBV){
            setPWBids.add(pwbv.Project_Workbook__c);
        }

        //map of number of attachments currently existing per record
        Map<String,AggregateResult> mapAttCount = new Map<String,AggregateResult>([SELECT Project_Workbook__c Id
                                                                                        , SUM(Number_of_Uploaded_Files__c) attCount
                                                                                    FROM Project_Workbook_Version__c 
                                                                                    WHERE Project_Workbook__c IN :setPWBids
                                                                                    GROUP BY Project_Workbook__c]);
    
        for(ID pwbid : setPWBids){  //process parent records
            if(mapAttCount.containsKey(pwbid)){
                Project_Workbook__c pwbToUpdate = new Project_Workbook__c(Id = pwbid,
                                                                          Number_of_Uploaded_Files__c = integer.valueOf(mapAttCount.get(pwbid).get('attCount')));
    
                lstPWBToUpdate.add(pwbToUpdate);
            }           
            //System.debug('LSTPWBTOUPDATE: ' + lstPWBToUpdate);
            if(!lstPWBToUpdate.isEmpty()){
                Database.SaveResult[] updResult = Database.update(lstPWBToUpdate, false);
                
                for(Database.SaveResult ur : updResult){
                    if(!ur.isSuccess()){
                        for(Database.Error err : ur.getErrors()){
                            errorMsg = 'Error updating file parent record : ' + err.getStatusCode() + ' - ' + err.getMessage();
                        }
                    }
                }
            }       
        }
        return errorMsg;
    }
}