/*
*   {purpose}   Manage the functionality tied to Project Checklist Item Phase automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date            Name                    Description
*   01/30/17        Kevin Johnson DCS       Created
*   11/12/18        Kevin Johnson DCS       Updated processApprovalUsers to accomodate update of role at PCLIP level
*	04/11/19		Kevin Johnson DCS		Updated processApprovalUsers to account for lstRoleFieldUpdated being null when processing approval roles
*   =============================================================================
*/  

public with sharing class ProjectChecklistItemPhaseUtility {
    public static string processApprovalUsers(List<Project_Checklist_Item_Phase__c> lstObj, List<String> lstRoleFieldUpdated){
        /*
        *{purpose}      Manages assignment of sub phase approvers
        *
        *{function}     Returns users role reflected in related Project Team as well as sub phase expected approvers designated at related
        *               Funding Round Sub Phase.  Approver assigned based on Project Team designation / approver role designation relationship
        *               
        *{trigger}      ProjectChecklistItemPhaseEventListener: after insert, after update
        *               
        */

        Set<ID> setPCLIPID = new Set<ID>();
        Set<ID> setOpptyID = new Set<ID>();
        Map<ID, Map<String, ID>> mapOTMRoles = new Map<ID, Map<String, ID>>();
        List<Project_Checklist_Item_Phase__c> lstPCLIPToProcess = new List<Project_Checklist_Item_Phase__c>();
        List<Project_Checklist_Item_Phase__c> lstPCLIPUpdate = new List<Project_Checklist_Item_Phase__c>();
        String errorMsg = null;

        //obtain pclip ids to be used in query
        for(Project_Checklist_Item_Phase__c pclip : lstObj){
            setPCLIPID.add(pclip.Id);  
        }

        //obtain unique set of opportunity ids passed from trigger.  need to query so as to obtain related record attributes.       
        for(Project_Checklist_Item_Phase__c pclip : [SELECT Id
                                                          , Approval_First__c
                                                          , Approval_Second__c
                                                          , Approval_Third__c
                                                          , Approval_Role_First__c
                                                          , Approval_Role_Second__c
                                                          , Approval_Role_Third__c
                                                          , Project_Checklist_Item__r.Project__r.Id
                                                     FROM Project_Checklist_Item_Phase__c
                                                     WHERE Id IN :setPCLIPID]){
            setOpptyID.add(pclip.Project_Checklist_Item__r.Project__r.Id);
            lstPCLIPToProcess.add(pclip);
        }
        //system.debug('SETOPPTYIDS: ' + setOpptyID);
        //return all team members existing on affected opportunities (projects)
        for(OpportunityTeamMember otm : [SELECT OpportunityId, 
                                                TeamMemberRole, 
                                                UserId 
                                        FROM OpportunityTeamMember 
                                        WHERE OpportunityId = :setOpptyID]){        

            //build map to reference while looping through returned project checklist items passed from trigger
            Map<String, ID> mapUserRoles = new Map<String, ID>();
            if(mapOTMRoles.containsKey(otm.OpportunityId)){
                mapUserRoles = mapOTMRoles.get(otm.OpportunityId);
            }
            mapUserRoles.put(otm.TeamMemberRole, otm.UserId);
            mapOTMRoles.put(otm.OpportunityId, mapUserRoles);
        }
        system.debug('MAPOTMROLES: ' + mapOTMRoles);
        system.debug('lstRoleFieldUpdated: ' + lstRoleFieldUpdated); 
        for(Project_Checklist_Item_Phase__c pclip : lstPCLIPToProcess){  //process passed pclip records
            ID approverFirstUserID = null;
            ID approverSecondUserID = null;
            ID approverThirdUserID = null;          
            //determine who/where to place team member based on their role / funding round checklist item approval mapping
            system.debug('ROLE CASCADE PCLI OPPTY ID: ' + pclip.Project_Checklist_Item__r.Project__c);
			system.debug('ROLE CASCADE mapOTMRoles.isEmpty(): ' + mapOTMRoles.isEmpty());
			system.debug('ROLE CASCADE mapOTMRoles.get team: ' + mapOTMRoles.containsKey(pclip.Project_Checklist_Item__r.Project__c));	
			//system.debug('ROLE CASCADE mapOTMRoles.get role: ' + mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).containsKey(pclip.Approval_Role_First__c));			
            if(pclip.Approval_Role_First__c != null){
                if(!mapOTMRoles.isEmpty() &&  //is map of otm roles populated
				  mapOTMRoles.containsKey(pclip.Project_Checklist_Item__r.Project__c) &&  //does project have team members assigned
				  mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).containsKey(pclip.Approval_Role_First__c)){  //process users/roles assigned to project team
                    approverFirstUserID = mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).get(pclip.Approval_Role_First__c);
                }
                if(mapOTMRoles.isEmpty() && lstRoleFieldUpdated != null){  //process roles not assigned to project team - clear invalid name if exist
                    if(lstRoleFieldUpdated.contains('First')){
                        approverFirstUserID = null;
                    } 
                }
            }
            if(pclip.Approval_Role_Second__c != null){
                if(!mapOTMRoles.isEmpty() && //is map of otm roles populated
				   mapOTMRoles.containsKey(pclip.Project_Checklist_Item__r.Project__c) &&  //does project have team members assigned
				   mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).containsKey(pclip.Approval_Role_Second__c)){  //process users/roles assigned to project team
                    approverSecondUserID = mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).get(pclip.Approval_Role_Second__c);
                }
                if(mapOTMRoles.isEmpty() && lstRoleFieldUpdated != null){  //process roles not assigned to project team - clear invalid name
                    if(lstRoleFieldUpdated.contains('Second')){
                        approverSecondUserID = null;
                    }
                }
            }
            if(pclip.Approval_Role_Third__c != null){
                if(!mapOTMRoles.isEmpty() &&  //is map of otm roles populated
				   mapOTMRoles.containsKey(pclip.Project_Checklist_Item__r.Project__c) &&  //does project have team members assigned
				   mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).containsKey(pclip.Approval_Role_Third__c)){  //process users/roles assigned to project team
                    approverThirdUserID = mapOTMRoles.get(pclip.Project_Checklist_Item__r.Project__c).get(pclip.Approval_Role_Third__c);
                }
                if(mapOTMRoles.isEmpty() && lstRoleFieldUpdated != null){  //process roles not assigned to project team - clear invalid name
                    if(lstRoleFieldUpdated.contains('Third')){
                        approverThirdUserID = null;
                    }
                }
            }

            Project_Checklist_Item_Phase__c pclipToUpdate = new Project_Checklist_Item_Phase__c(Id = pclip.Id,
                                                                                                Approval_First__c = approverFirstUserID,                           
                                                                                                Approval_Second__c = approverSecondUserID,
                                                                                                Approval_Third__c = approverThirdUserID);                                                                                
            lstPCLIPUpdate.add(pclipToUpdate);
        }
        system.debug('LSTPCLITOUPDATE: ' + lstPCLIPUpdate);
        if(!lstPCLIPUpdate.isEmpty()){
            Database.SaveResult[] updResult = Database.update(lstPCLIPUpdate, false);
            
            for(Database.SaveResult sr : updResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding approvers: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
    }
}