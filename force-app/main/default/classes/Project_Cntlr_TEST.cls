@isTest 
private class Project_Cntlr_TEST {

	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());

        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		System.debug('After buildProjectDataStructure.  Queries: ' + Limits.getQueries());

		for (Funding_Round_Trigger__c oFrChar : [Select Id
													, Name
													, Trigger_Group__r.Name
													, Selection_Type__c 
												From Funding_Round_Trigger__c 
												Where Trigger_Group__r.Funding_Round__c = :oFundingRound.Id]) {
			System.debug('oFrChar: ' + oFrChar);
			TestDataUtility.createProjectCharacteristic(oFrChar.Id, oProject.Id, oFrChar.Selection_Type__c);
		}
		System.debug('After createProjectCharacteristic.  Queries: ' + Limits.getQueries());
	}

	public class hclsJSONTest {
		String id;
		String duration;

		public hclsJSONTest(Id recId, String days) {
			id = recId;
			duration = days;
		}
	}

	@isTest static void test_loadData() {
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];

		Project_Sub_Phase__c oStage = [Select Id
										From Project_Sub_Phase__c 
										Where Funding_Round_Phase__r.Due_Date_Baseline__c = 'Stage Duration - Project' LIMIT 1]; 

		Test.startTest();
		Project_Cntlr.retrievePhasesAndStages(oProject.Id);
		Project_Cntlr.retrievePhaseStageTree(oProject.Id, null, 'Current User');
		Project_Cntlr.retrievePhaseStageTree(oProject.Id, null, null);
		Project_Cntlr.retrieveProjectTasks(oProject.Id, 'Current User');

		//json example: [{"durationDays":"30","id":"a0G0r000000GiAKEA0"}]
		List<hclsJSONTest> hlcTest = new List<hclsJSONTest>();
		hlcTest.add(new hclsJSONTest(oStage.Id, '30'));
		Project_Cntlr.saveChangedRecords_Duration(JSON.serialize(hlcTest));

		Test.stopTest();
	}
}