@isTest
private class PhaseTriggers_Controller_TEST {

	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());

        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		System.debug('After buildProjectDataStructure.  Queries: ' + Limits.getQueries());

		for (Funding_Round_Trigger__c oFrChar : [Select Id
													, Name
													, Trigger_Group__r.Name
													, Selection_Type__c 
												From Funding_Round_Trigger__c 
												Where Trigger_Group__r.Funding_Round__c = :oFundingRound.Id]) {
			System.debug('oFrChar: ' + oFrChar);
			TestDataUtility.createProjectCharacteristic(oFrChar.Id, oProject.Id, oFrChar.Selection_Type__c);
		}
		System.debug('After createProjectCharacteristic.  Queries: ' + Limits.getQueries());
	}

	//additional data staging to avoid Too Many Queries error
    @isTest static PhaseTriggers_Controller prepData() {
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];

        List<Project_Phase__c> lstProjectPhases = [Select Id
                                                        , Name
                                                        , Funding_Round_Phase__c
                                                        , Opt_Out__c
                                                        , Phase_Status__c
                                                        , Show_in_Developer_View__c
                                                        , Sort_Order__c
                                                        , Submission_Date__c
                                                        , Submitted_By__c
                                                        , View_Checklist__c
														, (select id from Project_Sub_Phases__r)
                                                    From Project_Phase__c
                                                    Where Project__c = :oProject.Id
                                                        And Name = 'Construction'
                                                    Order by Sort_Order__c];

		List<Project_Checklist__c> lstProjectChecklists = [Select Id
																, Name
																, Funding_Round_Checklist__c
															From Project_Checklist__c
															Where Project__c = :oProject.Id];

        system.debug('lstProjectPhases:\n' + lstProjectPhases); 
		Set<Id> setCharIds = new Set<Id>();
		for (Funding_Round_Trigger__c oFrChar : [Select Id
													, Name
													, Trigger_Group__r.Name
													, Selection_Type__c 
												From Funding_Round_Trigger__c 
												Where Trigger_Group__r.Funding_Round__c = :oFundingRound.Id]) {
			System.debug('oFrChar: ' + oFrChar);
			setCharIds.add(oFrChar.Id);
		}		                                                

		Test.startTest();
		//build opportunity team
        List<OpportunityTeamMember> lstTeamMembers = new List<OpportunityTeamMember>();

        lstTeamMembers.add(new OpportunityTeamMember(OpportunityID = oProject.Id
                                                    , TeamMemberRole = 'HDO'
                                                    , UserId = UserInfo.getUserId()));
        lstTeamMembers.add(new OpportunityTeamMember(OpportunityID = oProject.Id
                                                    , TeamMemberRole = 'Architect'
                                                    , UserId = UserInfo.getUserId()));
        lstTeamMembers.add(new OpportunityTeamMember(OpportunityID = oProject.Id
                                                    , TeamMemberRole = 'Attorney'
                                                    , UserId = UserInfo.getUserId()));

        insert lstTeamMembers;
		System.debug('After insert lstTeamMembers.  Queries: ' + Limits.getQueries());


        PageReference pageRef = Page.ProjectPhase;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('phaseId', String.valueOf(lstProjectPhases[0].Funding_Round_Phase__c));
        ApexPages.currentPage().getParameters().put('checklistId', String.valueOf(lstProjectChecklists[0].Funding_Round_Checklist__c));
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(oProject.Id));

        ApexPages.StandardController sc = new ApexPages.StandardController(oProject);
        ProjectPhase_Controller oPhaseController = new ProjectPhase_Controller(sc);
		System.debug('After new ProjectPhase_Controller.  Queries: ' + Limits.getQueries());

        PhaseTriggers_Controller oPTC = new PhaseTriggers_Controller();
		System.debug('After new PhaseTriggers_Controller.  Queries: ' + Limits.getQueries());

        oPTC.oCurrentPhaseDetails = oPhaseController.oPhaseDetails_Data;
        oPTC.oCurrentPhaseDetails.showFilters_Trigger = true;
        oPTC.oCurrentPhaseDetails.showFilters_ApprovalStatus = true;
        oPTC.oCurrentPhaseDetails.showFilters_UploadStatus = true;
        oPTC.oCurrentPhaseDetails.showFilters_ApproverRole = true;
        oPTC.oCurrentPhaseDetails.showFilters_ApproverName = true;
        oPTC.oCurrentProject = oProject;

        //create project checklist items by looping through each trigger group and trigger in the phase, and checking
        // each trigger
        for (Integer counterTrgGroups = 0; 
                counterTrgGroups < oPTC.oCurrentPhaseDetails.lstTriggerGroups.size(); 
                counterTrgGroups++) {

            for (Integer counterTrigs = 0; 
                        counterTrigs < oPTC.oCurrentPhaseDetails.lstTriggerGroups[counterTrgGroups].lstTriggers.size(); 
                        counterTrigs++) {
                oPTC.oCurrentPhaseDetails.lstTriggerGroups[counterTrgGroups].lstTriggers[counterTrigs].isChecked = true;
            }
        }

        oPTC.saveChanges();
		System.debug('After oPTC.saveChanges.  Queries: ' + Limits.getQueries());

        //reset the controller so the data in the manage tab is retrieved
        oPhaseController = new ProjectPhase_Controller(sc);

        //prep the approver role filters
        oPTC.oCurrentPhaseDetails = oPhaseController.oPhaseDetails_Data;
        oPTC.oCurrentPhaseDetails.lstApproverRoles.add(new typeFundingRoundPhase.hclsStatus('HDO', true));
        oPTC.oCurrentPhaseDetails.lstApproverNames.add(new typeFundingRoundPhase.hclsStatus(UserInfo.getUserId(), UserInfo.getUserName(), true));

        oPTC.oCurrentPhaseDetails.lstApproverRoles.add(new typeFundingRoundPhase.hclsStatus('Architect', true));
        oPTC.oCurrentPhaseDetails.lstApproverNames.add(new typeFundingRoundPhase.hclsStatus(UserInfo.getUserId(), UserInfo.getUserName(), true));

        oPTC.oCurrentPhaseDetails.lstApproverRoles.add(new typeFundingRoundPhase.hclsStatus('Attorney', true));
        oPTC.oCurrentPhaseDetails.lstApproverNames.add(new typeFundingRoundPhase.hclsStatus(UserInfo.getUserId(), UserInfo.getUserName(), true));

        //oPTC.oCurrentPhaseDetails.oPhase.Require_Developer_Submission__c = true;
        oPTC.oCurrentPhaseDetails.oProjectPhase.Phase_Status__c = 'Not Started';

        //oPhaseController.oPhaseDetails_Configure = oPTC.oCurrentPhaseDetails;
        oPhaseController.oPhaseDetails_Data = oPTC.oCurrentPhaseDetails;
		oPTC.oCurrentProject = oPhaseController.oProject;
        
        return oPTC;
    }
    
	@isTest static void test_SubmitWithoutUpload() {
        PhaseTriggers_Controller oPTC = prepData();

		oPTC.doNothing();
		oPTC.refreshTab();

		oPTC.applyFilters();
		System.debug('After applyFilters.  Queries: ' + Limits.getQueries());
		oPTC.applySelectedFilters();
		System.debug('After applySelectedFilters.  Queries: ' + Limits.getQueries());
		oPTC.submitChecklistItemsAndRefresh();		
		System.debug('After submitChecklistItemsAndRefresh.  Queries: ' + Limits.getQueries());
		oPTC.submitChecklistItems();
		System.debug('After submitChecklistItems.  Queries: ' + Limits.getQueries());

		Test.stopTest();
	}
    
    @isTest static void test_SubmitWithUpload() {
        PhaseTriggers_Controller oPTC = prepData();
        
		//mark all PCLIPs as Uploaded and then submit again
		List<Project_Checklist_Item_Phase__c> lstUpdatePCLIPs = new List<Project_Checklist_Item_Phase__c>();
		for (Project_Checklist_Item_Phase__c oPCLIP : [Select Id
														, Upload_Status__c 
														From Project_Checklist_Item_Phase__c
														Where Funding_Round_Phase__c = :oPTC.oCurrentPhaseDetails.oFRPhase.Id]) {
			oPCLIP.Upload_Status__c = 'Uploaded';
			lstUpdatePCLIPs.add(oPCLIP);
		}
		update lstUpdatePCLIPs;
		oPTC.getTabDisplayText();
		oPTC.submitChecklistItems();
		System.debug('After submitChecklistItems.  Queries: ' + Limits.getQueries());
		oPTC.getShowTriggerFilters();
		System.debug('After getShowTriggerFilters.  Queries: ' + Limits.getQueries());
		oPTC.getShowTriggerGroups();
		System.debug('After getShowTriggerGroups.  Queries: ' + Limits.getQueries());
		oPTC.getShowStageFilters();
		System.debug('After getShowStageFilters.  Queries: ' + Limits.getQueries());
		oPTC.getShowStages();
		System.debug('After getShowStages.  Queries: ' + Limits.getQueries());
		oPTC.getShowClosingTypeFilters();
		System.debug('After getShowClosingTypeFilters.  Queries: ' + Limits.getQueries());
		oPTC.getLstPCLIPs();
		System.debug('After getLstPCLIPs.  Queries: ' + Limits.getQueries());
		oPTC.getLstPCLIs();
		System.debug('After getLstPCLIs.  Queries: ' + Limits.getQueries());		

		Test.stopTest();        
    }

	@isTest static void test_GeneralItems() {
        PhaseTriggers_Controller oPTC = prepData();

		oPTC.getCountNotUploaded();
		System.debug('After getCountNotUploaded.  Queries: ' + Limits.getQueries());
		oPTC.blnShowFilterButton = true;
		oPTC.showItemsNotUploaded();
		System.debug('After showItemsNotUploaded.  Queries: ' + Limits.getQueries());
		oPTC.checkAllApproverFilters(false);
		System.debug('After checkAllApproverFilters.  Queries: ' + Limits.getQueries());
		oPTC.checkAllCliFilterTags(false);
		System.debug('After checkAllCliFilterTags.  Queries: ' + Limits.getQueries());
		oPTC.checkAllClosingTypeFilters(false);
		System.debug('After checkAllClosingTypeFilters.  Queries: ' + Limits.getQueries());
		oPTC.checkAllRoleFilters(false);
		System.debug('After checkAllRoleFilters.  Queries: ' + Limits.getQueries());
		oPTC.checkAllStageFilters(false);
		System.debug('After checkAllStageFilters.  Queries: ' + Limits.getQueries());
		oPTC.checkAllStatusFilters_Approval(false);
		System.debug('After checkAllStatusFilters_Approval.  Queries: ' + Limits.getQueries());
		oPTC.checkAllStatusFilters_Upload(false);
		System.debug('After checkAllStatusFilters_Upload.  Queries: ' + Limits.getQueries());
//		oPTC.clearAllFilters();
//		System.debug('After clearAllFilters.  Queries: ' + Limits.getQueries());
		oPTC.calculateStageDueDate();
		System.debug('After calculateStageDueDate.  Queries: ' + Limits.getQueries());

		Test.stopTest();
	}
    
    @isTest static void test_ClearFilters() {
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];
        List<Project_Phase__c> lstProjectPhases = [Select Id
                                                        , Name
                                                        , Funding_Round_Phase__c
                                                        , Opt_Out__c
                                                        , Phase_Status__c
                                                        , Show_in_Developer_View__c
                                                        , Sort_Order__c
                                                        , Submission_Date__c
                                                        , Submitted_By__c
                                                        , View_Checklist__c
                                                    From Project_Phase__c
                                                    Where Project__c = :oProject.Id
                                                        And Name = 'Construction'
                                                    Order by Sort_Order__c];

		List<Project_Checklist__c> lstProjectChecklists = [Select Id
																, Name
																, Funding_Round_Checklist__c
															From Project_Checklist__c
															Where Project__c = :oProject.Id];

        system.debug('lstProjectPhases:\n' + lstProjectPhases);                                                 

        PageReference pageRef = Page.ProjectPhase;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('phaseId', String.valueOf(lstProjectPhases[0].Funding_Round_Phase__c));
        ApexPages.currentPage().getParameters().put('checklistId', String.valueOf(lstProjectChecklists[0].Funding_Round_Checklist__c));
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(oProject.Id));

        ApexPages.StandardController sc = new ApexPages.StandardController(oProject);
        ProjectPhase_Controller oPhaseController = new ProjectPhase_Controller(sc);
		System.debug('After new ProjectPhase_Controller.  Queries: ' + Limits.getQueries());

        PhaseTriggers_Controller oPTC = new PhaseTriggers_Controller();
		System.debug('After new PhaseTriggers_Controller.  Queries: ' + Limits.getQueries());

        oPTC.oCurrentPhaseDetails = oPhaseController.oPhaseDetails_Data;
        oPTC.oCurrentPhaseDetails.showFilters_Trigger = true;
        oPTC.oCurrentPhaseDetails.showFilters_ApprovalStatus = true;
        oPTC.oCurrentPhaseDetails.showFilters_UploadStatus = true;
        oPTC.oCurrentPhaseDetails.showFilters_ApproverRole = true;
        oPTC.oCurrentPhaseDetails.showFilters_ApproverName = true;
        oPTC.oCurrentProject = oProject;

        Test.startTest();
		oPTC.clearAllFilters();
		System.debug('After clearAllFilters.  Queries: ' + Limits.getQueries());
        Test.stopTest();
    }


	@isTest static void test_RetrieveData() {
        PhaseTriggers_Controller oPTC = prepData();
		system.debug('PCLIPs: ' + oPTC.oCurrentPhaseDetails.getCountPCLIPs());
		oPTC.blnShowFilterButton = true;
		oPTC.getTabDisplayText();
		oPTC.showItemsNotUploaded();

		Test.stopTest();
	}

	@isTest static void test_Filters() {
        PhaseTriggers_Controller oPTC = prepData();
		oPTC.checkAllFilters();
		
		Test.stopTest();
	}

	@isTest static void test_typeFundingRoundPhase() {
		Funding_Round__c oFundingRound = [Select Id
												, Name
												, (Select Id, Name From Phases__r Order By Sort_Order__c)
												, (Select Id, Name From Funding_Round_Checklists__r Order By Sort_Order__c)
											From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id
									, Name
									, Funding_Round__c
									, (Select Id, Name, Checklist_Status__c From Project_Checklists__r Order By Sort_Order__c)
								From Opportunity LIMIT 1];

		Phase_Checklist__c oFRChecklist = new Phase_Checklist__c(Id = oFundingRound.Funding_Round_Checklists__r[0].Id);
		oFRChecklist.Require_Developer_Submission__c = true;
		update oFRChecklist;
		Project_Checklist__c oProjChecklist = oProject.Project_Checklists__r[0];
		oProjChecklist.Checklist_Status__c = 'Open';
		update oProjChecklist;

		Test.startTest();

		typeFundingRoundPhase.FR_StageDetails oStageDetails  = new typeFundingRoundPhase.FR_StageDetails();
		typeFundingRoundPhase oTypeTest = new typeFundingRoundPhase(oFundingRound.Phases__r[0].Id
																	, oProject.Id
																	, oFundingRound.Funding_Round_Checklists__r[0].Id
																	, 'Configure');

		typeFundingRoundPhase.hclsStatus hlp = new typeFundingRoundPhase.hclsStatus();
		Test.stopTest();

	}
}