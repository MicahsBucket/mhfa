/*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   02/16/18    Kevin Johnson DCS       Added testDeleteTeamMember to increase coverage.  Note that this method is also called in the 
*                                       test class OpportunityTeamMemberDelete_Contlr_TEST
*   =============================================================================
*/

@isTest
public class OpportunityTeamMemberUtility_TEST {
    @isTest static void testOpportunityTeamMember() {
        //SUPPORTING DATA=======================================================================================================
        //custom metadata types
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'OpportunityTeamMember'];

        List<File_Sharing_Settings_Objects__mdt> SharingRules = [SELECT MasterLabel
                                                                      , Object_Prefix__c
                                                                      , Fields_To_Query__c
                                                                      , Parent_Lookup__c
                                                                 FROM File_Sharing_Settings_Objects__mdt];
        
        //create test users
        User oTestUser1 = TestDataUtility.createUser('Multifamily Standard User',
                                                     'testOTM1@mhfatest.com');
            
        User oTestUser2 = TestDataUtility.createUser('Multifamily Standard User',
                                                     'testOTM2@mhfatest.com');
        
        //FUNDING ROUND FRAMEWORK===============================================================================================
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

        //PROJECT FRAMEWORK=====================================================================================================
        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

        //fetch required data
        Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
                                                                      FROM Funding_Round_Checklist_Item__c
                                                                      WHERE Funding_Round__c = :oFundingRound.Id
                                                                      And Name = 'Pre Selection Checklist Item'];

        Funding_Round_Task__c oFundingRoundStageTaskPreSelection = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
        oFundingRoundStageTaskPreSelection.Assignee_Type__c = 'Internal Task';
        oFundingRoundStageTaskPreSelection.Assignee_Role_Project_Team__c = 'HDO';
        update oFundingRoundStageTaskPreSelection;

        Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
                                                              FROM Project_Checklist__c
                                                              WHERE Name = 'Application'
                                                              AND Project__c = :oProject.Id];

        Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
                                                               , Name
                                                          FROM Project_Sub_Phase__c
                                                          WHERE Name = 'Application Submittals'
                                                          AND Project_Phase__r.Name = 'Application'
                                                          AND Project__c = :oProject.Id];       

        //create project checklist item
        Project_Checklist_Item__c oProjectChecklistItem = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                                    oFundingRoundChecklistItem.Id,
                                                                                                    false,
                                                                                                    null,
                                                                                                    null,
                                                                                                    oProjectStagePreSelection.Id,
                                                                                                    oProjectChecklistPreSelection.Id);  
        
        //create project task
        Project_Task__c oProjectTask = TestDataUtility.createProjectStageTask(oProjectStagePreSelection.Id,
                                                                              oProject.Id,
                                                                              'Stage Pre Selection Task',
                                                                              'Pre-Application',
                                                                               oFundingRoundStageTaskPreSelection.Id);
        
        /*
        //project narrative (project narrative already created upon project insert)
        //test will occur for updating project narrative author upon project team member insert/delete

        //insert file: content version / contentdocumentlink
        ContentVersion oContentVersion = TestDataUtility.createContentVersion('/Test.jpg');
        ContentDocumentLink oContentDocumentLink = TestDataUtility.createContentDocumentLink(oProjectChecklistItem.Id, 
                                                                                             oContentVersion.Id);
        */
        //fetch project workbook (project workbook already created upon project insert)
        Project_Workbook__c oProjectWorkbook = [SELECT Id
                                               FROM Project_Workbook__c
                                               WHERE Project__c = :oProject.Id
                                               AND Workbook_Level__c = 'Primary'];

        //run tests                                                        
        Test.startTest();
            //project narrative (project narrative already created upon project insert)
            //test will occur for updating project narrative author upon project team member insert/delete
            //insert file: content version / contentdocumentlink
            ContentVersion oContentVersion = TestDataUtility.createContentVersion('/Test.jpg');
            ContentDocumentLink oContentDocumentLink = TestDataUtility.createContentDocumentLink(oProjectChecklistItem.Id, 
                                                                                                 oContentVersion.Id);

            //make PWBV public
            Project_Workbook_Version__c oProjectWorkbookVersion = TestDataUtility.createProjectWorkbookVersion(oProjectWorkbook.Id,
                                                                                                               'Selection Meeting');
            oProjectWorkbookVersion.Make_Public__c = TRUE;
            update oProjectWorkbookVersion;
                        
            OpportunityTeamMember oProjectTeamMember1 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
                                                                                                  'Loan Processor', 
                                                                                                   oTestUser1.Id);
            
            OpportunityTeamMember oProjectTeamMember2 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
                                                                                                   'Architect', 
                                                                                                    oTestUser2.Id);                                                             
    
            oProjectChecklistItem.Approval_Role_First__c = 'LoanProcessor';
            oProjectChecklistItem.Approval_Role_Second__c = 'Architect';
            oProjectChecklistItem.Approval_Role_Third__c = 'HDO';
            update oProjectChecklistItem;

            oProjectTeamMember1.TeamMemberRole = 'HDO';
            update oProjectTeamMember1;
            //delete oProjectTeamMember1;
        Test.stopTest();
    }

    @isTest static void testOpportunityTeamMemberDelete(){
        //custom metadata types
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'OpportunityTeamMember'];

        List<File_Sharing_Settings_Objects__mdt> SharingRules = [SELECT MasterLabel
                                                                      , Object_Prefix__c
                                                                      , Fields_To_Query__c
                                                                      , Parent_Lookup__c
                                                                 FROM File_Sharing_Settings_Objects__mdt];
        
        //create test users
        User oTestUser1 = TestDataUtility.createUser('Multifamily Standard User',
                                                     'testOTM1@mhfatest.com');
        
        //FUNDING ROUND FRAMEWORK===============================================================================================
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

        //PROJECT FRAMEWORK=====================================================================================================
        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
        
        OpportunityTeamMember oProjectTeamMember1 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
                                                                                                'Architect', 
                                                                                                 oTestUser1.Id);
        delete oProjectTeamMember1; 
    }

    @isTest static void testDeleteTeamMember_AllowDelete(){
        //SUPPORTING DATA=======================================================================================================
        //custom metadata types
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'OpportunityTeamMember'];
        //create test user
        User oTestUser = TestDataUtility.createUser('Multifamily Standard User',
                                                     'testOTM1@mhfatest.com');
        
        //FUNDING ROUND FRAMEWORK===============================================================================================
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

        //PROJECT FRAMEWORK=====================================================================================================
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

        Test.startTest();
            //create opportunity (project) team member
            OpportunityTeamMember oProjectTeamMember = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
                                                                                                   'LoanProcessor', 
                                                                                                   oTestUser.Id);
            
            PageReference pageRef = Page.OpportunityTeamMemberDelete;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id', String.valueOf(oProjectTeamMember.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(oProjectTeamMember);
            OpportunityTeamMemberDelete_Controller controller = new OpportunityTeamMemberDelete_Controller(sc);

            controller.deleteTeamMember();
            controller.cancelDelete();
        Test.stopTest();
    }
}