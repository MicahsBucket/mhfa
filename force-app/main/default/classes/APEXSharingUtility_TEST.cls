@isTest
private class APEXSharingUtility_TEST {
	@isTest static void testAPEXSharingUtility() {
		//stage custom settings
        //insert RunTrigger;
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'OpportunityTeamMember'];

        List<File_Sharing_Settings_Objects__mdt> csSharingRules = [SELECT MasterLabel
                                                                        , Object_Prefix__c
                                                                        , Fields_To_Query__c
                                                                        , Parent_Lookup__c
                                                                   FROM File_Sharing_Settings_Objects__mdt];

        //stage data
        //create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));                     
/*
        //create fr phase
        Phase__c frp = TestDataUtility.createFundingRoundPhase('Pre-Construction Due Diligence', 
                                                               system.Now(), 
                                                               system.Now().addMonths(6), 
                                                               1, 
                                                               'phase',
                                                               fr.Id, 
                                                               null);
        //create fr sub phase
        Phase__c frsp = TestDataUtility.createFundingRoundPhase('Credit Approval',
                                                                 system.Now(), 
                                                                 system.Now().addMonths(6), 
                                                                  1, 
                                                                  'subphase',
                                                                  fr.Id, 
                                                                  frp.Id);
*/
        //create project
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //create otm
    	  User otmUser1 = TestDataUtility.createUser('Multifamily Standard User',
                                                    'testOTMAPEXShare1@mhfatest.com');
        
        User otmUser2 = TestDataUtility.createUser('Multifamily Standard User',
                                                    'testOTMAPEXShare2@mhfatest.com');
        
        OpportunityTeamMember otm1 = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                                'Loan Processor', 
                                                                                otmUser1.Id);

        OpportunityTeamMember otm2 = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                                'Architect', 
        																		otmUser2.Id); 

        Project_Workbook__c pwb = TestDataUtility.createProjectWorkbook(p.Id);

        //run tests                                                        
        Test.startTest();
	         //create pcli
	        Project_Checklist_Item__c pcli = TestDataUtility.createProjectChecklistItem(p.id, 
	                                                                                    null,
	                                                                                    false,
	                                                                                    null,
	                                                                                    null,
																						null,
																						null);
	        //create ppd
	        Project_Phase_Decision__c ppd = TestDataUtility.createProjectPhaseDecision(p.Id
                                                                                  , '811'
                                                                                  , 'Eligible');


	        //create pwbv
	        Project_Workbook_Version__c pwbv = TestDataUtility.createProjectWorkbookVersion(pwb.Id, 
	        																			                                          'Selection Meeting');
        Test.stopTest();
	 }
}