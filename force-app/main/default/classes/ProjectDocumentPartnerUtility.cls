/*
*   {purpose}  	Manage the functionality tied to Project Document: Partner automation                
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   05/29/18	Kevin Johnson DCS		Created
*   =============================================================================
*/

public without sharing class ProjectDocumentPartnerUtility  {
	//without sharing on class to ensure users have capability to insert sharing records
	public static String validateRecordParameters(List<Project_Documents_Partner__c> lstPDP){
		/*
		*{purpose}		Manages validation of Project Document: Partner records being added
		*
		*{function}		Builds list of Project Document: Partner (PDP) records per parent Project for use in validating attributes of new PDP record.  
		*				If a PDP record (inserted new/updated) does not meet requirements an error is passed notifying user of flagged validation.
		*				
		*{trigger}  	ProjectDocumentPartnerEventListener: before insert, before update
		*/

		Set<ID> setProjectIds = new Set<ID>();
		Set<ID> setPDPIds = new Set<ID>();
		for(Project_Documents_Partner__c pdp : lstPDP){
			setPDPIds.add(pdp.Id);
			setProjectIds.add(pdp.Project__c);
		}

		Map<Id, List<String>> mapPDP = new Map<Id, List<String>>();

		//compile existing pdp records
		for(Project_Documents_Partner__c oPDP : [SELECT Project__c
													  , Type__c
											     FROM Project_Documents_Partner__c
												 WHERE Project__c IN :setProjectIds
											     AND Id NOT IN :setPDPIds]) {
					if(mapPDP.containsKey(oPDP.Project__c)) {
						List<String> custDoc = mapPDP.get(oPDP.Project__c);
						custDoc.add(oPDP.Type__c);
						mapPDP.put(oPDP.Project__c, custDoc);
					} else {
						mapPDP.put(oPDP.Project__c, new List<String> {oPDP.Type__c});
					}
		}
		system.debug('MAPPDP: ' + mapPDP);

		String errorMsg = null;
		for(Project_Documents_Partner__c pdp : lstPDP){
			//validate if project document: partner record already exists for parent project
			List<String> lstExistingDocuments = mapPDP.get(pdp.Project__c);
			system.debug('LSTEXISTINGDOCUMENT: ' + lstExistingDocuments);
			Set<String> setExistingDocuments = new Set<String>();
			if(lstExistingDocuments != null){
				setExistingDocuments.addAll(lstExistingDocuments);
				
				String addedDocument = pdp.Type__c;
				if(setExistingDocuments.contains(addedDocument)){
					errorMsg = 'Document ' + addedDocument + ' already exists for this Project and thus cannot be added.';
					break;
				}
			}			
		}
		return errorMsg;		
	}
}