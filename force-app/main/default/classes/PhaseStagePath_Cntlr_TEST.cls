@isTest 
private class PhaseStagePath_Cntlr_TEST {

	@isTest
	private static void test_PhaseStagePath() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		Funding_Round_Task__c oFRTask_Pre = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
		Funding_Round_Task__c oFRTask_Post = TestDataUtility.createFundingRoundStageTaskPostSelection(oFundingRound.Id);		
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

		Test.startTest();
		PhaseStagePath_Cntlr.retrieveProjectPhases(oProject.Id);

		Project_Sub_Phase__c oStage = [Select Id From Project_Sub_Phase__c Where Project__c = :oProject.Id LIMIT 1];
		PhaseStagePath_Cntlr.retrieveStageTasks(oStage.Id);
		Test.stopTest();
	}
}