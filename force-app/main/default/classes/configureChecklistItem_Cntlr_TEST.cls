@isTest 
private class configureChecklistItem_Cntlr_TEST {

	@isTest
	private static void test_configureChecklistItem() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

		Funding_Round_Checklist_Item_Trigger__c oCLIChar = [Select Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
															From Funding_Round_Checklist_Item_Trigger__c
															LIMIT 1];

		List<String> lstCharIds = new List<String>();
		lstCharIds.add(oCLIChar.Funding_Round_Trigger__c);
		
		Test.startTest();

		configureChecklistItem_Cntlr.getGroupings(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Pre-Selection');
		configureChecklistItem_Cntlr.getGroupings(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Post-Selection');

		configureChecklistItem_Cntlr.getAvailableCharacteristics(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Pre-Selection');
		configureChecklistItem_Cntlr.getAvailableCharacteristics(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Post-Selection');

		configureChecklistItem_Cntlr.getAssignedCharacteristics(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Pre-Selection');
		configureChecklistItem_Cntlr.getAssignedCharacteristics(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Post-Selection');

		List<configureChecklistItem_Cntlr.checklistOption> lstPreOptions = configureChecklistItem_Cntlr.getChecklistOptions(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Pre-Selection');
		List<configureChecklistItem_Cntlr.checklistOption> lstPostOptions = configureChecklistItem_Cntlr.getChecklistOptions(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c, 'Post-Selection');

		configureChecklistItem_Cntlr.insertCliChecklists(oCLIChar.Funding_Round_Checklist_Item__c, lstPreOptions);
		configureChecklistItem_Cntlr.insertCliChecklists(oCLIChar.Funding_Round_Checklist_Item__c, lstPostOptions);

		configureChecklistItem_Cntlr.insertCliCharacteristics(oCLIChar.Funding_Round_Checklist_Item__c
															, lstCharIds
															, true);

		configureChecklistItem_Cntlr.removeCliCharacteristics(oCLIChar.Funding_Round_Checklist_Item__c
															, lstCharIds);

		configureChecklistItem_Cntlr.buildPicklistValues('value1;value2');

		Test.stopTest();


	}
}