@isTest
private class FundingRoundProjectShare_Controller_TEST {
	@isTest static void testFundingRoundProjectShare_Add() {
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //fetch group to share
        List<Group> lstGroup = new List<Group>([SELECT Id
        	                                    FROM Group
        	                                    WHERE Type = 'Role'
        	                                    LIMIT 1]);

        //create funding round share
        Funding_Round__Share frShare = new Funding_Round__Share();
					frShare.ParentId = fr.Id;
					frShare.AccessLevel = 'Read';
					frShare.UserOrGroupId = lstGroup[0].Id;
					frShare.RowCause = 'Manual';

		insert frShare;

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);
		/*
        //create project document
        Project_Documentation__c pd = TestDataUtility.createProjectDocumentation(p.Id, 
                                                                                'Documents Shared with Partners', 
                                                                                 null, 
                                                                                 null);
		*/
        Test.setCurrentPage(Page.FundingRound_ProjectShareAdd);
        FundingRoundProjectShare_Controller ext = new FundingRoundProjectShare_Controller(new ApexPages.StandardController(fr));

        //run tests
        Test.startTest();
            ext.addProjectShare();
            ext.returnToFundingRound();
        Test.stopTest();
	}

	@isTest static void testFundingRoundProjectShare_Remove() {
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //fetch group to share
        List<Group> lstGroup = new List<Group>([SELECT Id
        	                                    FROM Group
        	                                    WHERE Type = 'Role'
        	                                    LIMIT 1]);

        //create funding round share
        Funding_Round__Share frShare = new Funding_Round__Share();
					frShare.ParentId = fr.Id;
					frShare.AccessLevel = 'Read';
					frShare.UserOrGroupId = lstGroup[0].Id;
					frShare.RowCause = 'Manual';

		insert frShare;

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);
		/*
        //create project document
        Project_Documentation__c pd = TestDataUtility.createProjectDocumentation(p.Id, 
                                                                                'Documents Shared with Partners', 
                                                                                 null, 
                                                                                 null);
        */
		//create project workbook
        Project_Workbook__c pwb = TestDataUtility.createProjectWorkbook(p.Id);

        //create project workbook version
        Project_Workbook_Version__c pwbv = TestDataUtility.createProjectWorkbookVersion(pwb.Id,
                                                                                        'MUPT Application');
        pwbv.Make_Public__c = TRUE;
        update pwbv;

        //create project share
        OpportunityShare pShare = new OpportunityShare();
					pShare.OpportunityId = p.Id;
					pShare.OpportunityAccessLevel = 'Read';
					pShare.UserOrGroupId = lstGroup[0].Id;

		insert pShare;
		/*
        //create project document share
        Project_Documentation__Share pdShare = new Project_Documentation__Share();
                    pdShare.ParentId = pd.Id;
                    pdShare.AccessLevel = 'Read';
                    pdShare.UserOrGroupId = lstGroup[0].Id;
                    pdShare.RowCause = 'Funding_Round__c';

        insert pdShare;	
		*/
        //create project workbook version share
        Project_Workbook_Version__Share pwbvShare = new Project_Workbook_Version__Share();
                    pwbvShare.ParentId = pwbv.Id;
                    pwbvShare.AccessLevel = 'Read';
                    pwbvShare.UserOrGroupId = lstGroup[0].Id;
                    pwbvShare.RowCause = 'Public_Share__c';

        insert pwbvShare; 

        Test.setCurrentPage(Page.FundingRound_ProjectShareRemove);
        FundingRoundProjectShare_Controller ext = new FundingRoundProjectShare_Controller(new ApexPages.StandardController(fr));

        //run tests
        Test.startTest();
            ext.removeProjectShare();
            ext.returnToFundingRound();
        Test.stopTest();
	}
}