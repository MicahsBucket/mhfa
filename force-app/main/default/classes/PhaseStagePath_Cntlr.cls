public class PhaseStagePath_Cntlr  {

    @AuraEnabled
    public static List<Project_Phase__c> retrieveProjectPhases(Id projectId) {
        System.debug('Inside retrieveProjectPhases.  projectId: ' + projectId);
        List<Project_Phase__c> lstPhases = [Select Id
                                            , Name
                                            , Phase_Status__c
                                            , Funding_Round_Phase__r.Name 
                                            , Funding_Round_Phase__r.Sort_Order__c
											, Project__r.Current_Phase__c
											, Project__r.Current_Stage__c
											, Project__r.Current_Phase__r.Name
											, Project__r.Current_Stage__r.Name
                                            , (Select Id
                                                        , Name
                                                        , Stage_Status__c
                                                        , Funding_Round_Sub_Phase__r.Name
                                                        , Funding_Round_Sub_Phase__r.Sort_Order__c 
                                                From Project_Sub_Phases__r
                                                Where Stage_Status__c != 'Not Applicable'
                                                Order By Funding_Round_Sub_Phase__r.Sort_Order__c)
                                    From Project_Phase__c
                                    Where Project__c = :projectId
                                    Order By Funding_Round_Phase__r.Sort_Order__c];
		
		for (Project_Phase__c oPhase : lstPhases) {			
			System.debug('Current Phase: ' + oPhase.Project__r.Current_Phase__c + '\nPhase: ' + oPhase + '\nStages:\n' + oPhase.Project_Sub_Phases__r);
		}

        return lstPhases;
    }

    @AuraEnabled
    public static List<Project_Task__c> retrieveStageTasks(Id stageId) {
        List<Project_Task__c> lstStageTasks = [Select Id
                                                    , Name
                                                    , Owner.Name
                                                    , Task_Status__c
                                                    , Due_Date__c
                                                    , Description__c
                                                    , OwnerId
                                                    , Funding_Round_Task__r.Name
                                            From Project_Task__c
                                            Where Project_Stage__c = :stageId
                                            Order By Due_Date__c, CreatedDate, Id];

        return lstStageTasks;
    }
}