@isTest 
private class DateUtility_TEST {

	@isTest
	private static void testDates() {
		DateUtility.addBusinessDaysToDate(Date.today(), 10);
		DateUtility.findBusinessDate(Date.today(), 'Prior to Due Date', 10);
		DateUtility.findBusinessDate(Date.today(), 'Equal Due Date', 10);
		DateUtility.findBusinessDate(Date.today(), 'After Due Date', null);
		DateUtility.findBusinessDate(null, 'After Due Date', null);
		DateUtility.seperateBitMask(63);
		DateUtility.getMonths(Date.today(), Date.today().addDays(100));
		DateUtility.isBusinessDay(Date.today());
	}

	@isTest
	private static void testHolidays1() {
		List<Holiday> lstHolidays = new List<Holiday>();
		Holiday h = new Holiday(Name='First of Each Month'
									, IsAllDay = true
									, ActivityDate = Date.newInstance(Date.today().year(), Date.today().month(), 1)
									, RecurrenceDayOfMonth = 1
									, RecurrenceInstance = 'First'
									, RecurrenceType = 'RecursMonthly');
		insert h;
		Test.startTest();
		DateUtility.getMonthlyHoliday(h, Date.newInstance(Date.today().year(), 1, 1), Date.newInstance(Date.today().year(), 2, 2));
		Test.stopTest();
	}
	@isTest
	private static void testHolidays2() {
		List<Holiday> lstHolidays = new List<Holiday>();
		Holiday h = new Holiday(Name='First Sunday of Each Month'
									, IsAllDay = true
									, ActivityDate = Date.newInstance(Date.today().year(), Date.today().month(), 1)
									, RecurrenceDayOfMonth = 1
									, RecurrenceInstance = 'First'
									, RecurrenceDayOfWeekMask = 1
									, RecurrenceType = 'RecursMonthlyNth');
		insert h;
		Test.startTest();
		DateUtility.getMonthlyNthHoliday(h, Date.newInstance(Date.today().year(), 1, 1), Date.newInstance(Date.today().year(), 2, 2));
		Test.stopTest();
	}
	@isTest
	private static void testHolidays3() {
		List<Holiday> lstHolidays = new List<Holiday>();
		Holiday h = new Holiday(Name='Each Sunday of Month'
									, IsAllDay = true
									, ActivityDate = Date.newInstance(Date.today().year(), Date.today().month(), 1)
									, RecurrenceDayOfMonth = 1
									, RecurrenceDayOfWeekMask = 1
									, RecurrenceInstance = 'First'
									, RecurrenceType = 'RecursWeekly');
		insert h;
		Test.startTest();
		DateUtility.getWeeklyHoliday(h, Date.newInstance(Date.today().year(), 1, 1), Date.newInstance(Date.today().year(), 2, 2));
		Test.stopTest();
	}
	@isTest
	private static void testHolidays4() {
		List<Holiday> lstHolidays = new List<Holiday>();
		Holiday h = new Holiday(Name='Daily Test'
									, IsAllDay = true
									, ActivityDate = Date.newInstance(Date.today().year(), Date.today().month(), 1)
									, RecurrenceStartDate = Date.newInstance(Date.today().year(), Date.today().month(), 1)
									, RecurrenceDayOfMonth = 1
									, RecurrenceInterval = 1
									, RecurrenceInstance = 'First'
									, RecurrenceType = 'RecursDaily');
		insert h;
		Test.startTest();
		DateUtility.getDailyHoliday(h, Date.newInstance(Date.today().year(), 1, 1), Date.newInstance(Date.today().year(), 1, 14));
		Test.stopTest();
	}

	@isTest
	private static void testCurrentYearHolidays() {
		DateUtility.getHolidaysForDates(Date.newInstance(Date.today().year(), 1, 1)
										, Date.newInstance(Date.today().year(), 12, 31));
	}
}