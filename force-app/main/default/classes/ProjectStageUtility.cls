/*
*   {purpose}   Manage the functionality tied to Project Stage automation                         
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   20191024    Eric Gronholz DCS       Created
*   20200617    Eric Gronholz DCS       Altered Phase Status assignments
*   =============================================================================
*/
public without sharing class ProjectStageUtility  {

    ////////////////////////////////////////////////////////////////
    // If a project stage record is marked complete, update the current phase and stage
    //  fields on the associated project
    ////////////////////////////////////////////////////////////////
    public static void assignProjectPhaseAndStage(List<Project_Sub_Phase__c> lstNewRecs, Map<Id, Project_Sub_Phase__c> mapOldRecs) {
        Set<Id> setProjectIds = new Set<Id>();
        Set<Id> setProjectPhases = new Set<Id>();
        Set<Id> setProjectStageIds_Complete = new Set<Id>();
        
        Map<Id, Project_Phase__c> mapUpdatePhases = new Map<Id, Project_Phase__c>();
        Map<Id, String> mapStageStatus = new Map<Id, String>();
 
         for (Project_Sub_Phase__c oProjectStage1 : lstNewRecs) {
            //check to see if the stage status changed
            if (mapOldRecs != null
                    && mapOldRecs.containsKey(oProjectStage1.Id)
                    && oProjectStage1.Stage_Status__c != mapOldRecs.get(oProjectStage1.Id).Stage_Status__c) {
                //hold the new status value for all impacted stages so we can reference them later
                mapStageStatus.put(oProjectStage1.Id, oProjectStage1.Stage_Status__c);
                //hold impacted projects
                setProjectIds.add(oProjectStage1.Project__c);
                //hold impacted project phases
                setProjectPhases.add(oProjectStage1.Project_Phase__c);
            }
        }

        //investigate all stages under the impacted phases to determine what the phase status should be
        for (Project_Phase__c oPhase : [Select Id
                                                , Phase_Status__c
                                                , (SELECT Id
                                                        , Stage_Status__c 
                                                    FROM Project_Sub_Phases__r)
                                        From Project_Phase__c
                                        Where Id in :setProjectPhases]) {
            //no need to process this phase if it's already in process
            if (mapUpdatePhases.containsKey(oPhase.Id)) {
                break;
            }
            String newStatus = oPhase.Phase_Status__c;
            Integer countNotStarted = 0;
            Integer countNotApplicable = 0;
            Integer countComplete = 0;
            Integer countInProcess = 0;
            //loop through all the stages and interrogate the status.  If a stage is one edited coming into this routine,
            // use its value instead of the queried value
            for (Project_Sub_Phase__c oPhaseStage2 : oPhase.Project_Sub_Phases__r) {
                String stageStatus = oPhaseStage2.Stage_Status__c;
                if (mapStageStatus.containsKey(oPhaseStage2.Id)) {
                    stageStatus = mapStageStatus.get(oPhaseStage2.Id);
                }
                switch on stageStatus {
                    when 'Not Started' {
                        countNotStarted++;
                    }
                    when 'In Process' {
                        countInProcess++;
                    }
                    when 'Complete' {
                        countComplete++;
                    }
                    when 'Not Applicable' {
                        countNotApplicable++;
                    }
                }
            }
            if (countInProcess > 0) {
                //if any stage is In Process, the phase is In Process
                newStatus = 'In Process';
            } else if (countComplete > 0 && countNotStarted > 0) {
                //if all stages are Complete or Not Started, the phase is In Process
                newStatus = 'In Process';
            } else if (countComplete > 0) {
                //if all stages are Complete or Not Applicable, the phase is Complete
                newStatus = 'Complete';
            } else if (countNotStarted > 0) {
                //if all stages are Not Started or Not Applicable, the phase is Not Started
                newStatus = 'Not Started';
            } else {
                newStatus = 'Not Applicable';
            }

            if (oPhase.Phase_Status__c != newStatus) {
                mapUpdatePhases.put(oPhase.Id
                                    , new Project_Phase__c(Id = oPhase.Id, Phase_Status__c = newStatus));
            }
        }
        update mapUpdatePhases.values();

        //retrieve the first open stage under each project specified and update the project's current phase and stage
        Map<Id, Opportunity> mapProjectUpdates = new Map<Id, Opportunity>();
        for (Project_Sub_Phase__c oProjectStage : [Select Id
                                                    , Name
                                                    , Stage_Status__c
                                                    , Project_Phase__c
                                                    , Project_Phase__r.Name
                                                    , Project_Phase__r.Sort_Order__c
                                                    , Sort_Order__c
                                                    , Project__c
                                                    , Project__r.Current_Phase__c
                                                    , Project__r.Current_Stage__c
                                                From Project_Sub_Phase__c
                                                Where Project__c in :setProjectIds
                                                    And Stage_Status__c NOT IN ('Complete', 'Not Applicable')
                                                Order By Project__c, Project_Phase__r.Sort_Order__c, Sort_Order__c]) {
            //if we haven't processed a project yet, update it; otherwise move to the next record
            if (mapProjectUpdates.containsKey(oProjectStage.Project__c) == false) {
                    mapProjectUpdates.put(oProjectStage.Project__c
                                            , new Opportunity(Id=oProjectStage.Project__c
                                                                //, Phase__c = oProjectStage.Project_Phase__r.Name
                                                                , Current_Phase__c = oProjectStage.Project_Phase__c
                                                                //, Stage__c = oProjectStage.Name
                                                                , Current_Stage__c = oProjectStage.Id));
            }
        }

        update mapProjectUpdates.values();
    }

    ////////////////////////////////////////////////////////////////
    // Reset phase and stage due dates
    ////////////////////////////////////////////////////////////////
    public static void updateDueDates(Map<Id, Project_Sub_Phase__c> mapNewRecs, Map<Id, Project_Sub_Phase__c> mapOldRecs) {

        //do not run if called from an insert
        if (mapOldRecs == null) {
            return; 
        }

        Set<Id> setProjectIds = new Set<Id>();
        Set<Id> setStageIds_ChangedDuration = new Set<Id>();
        List<Project_Phase__c> lstUpdatePhases = new List<Project_Phase__c>();
        List<Project_Sub_Phase__c> lstUpdateStages = new List<Project_Sub_Phase__c>();

        for (Project_Sub_Phase__c oStage : mapNewRecs.values()) {
            setProjectIds.add(oStage.Project__c);
            if (oStage.Estimated_Days_to_Complete__c != mapOldRecs.get(oStage.Id).Estimated_Days_to_Complete__c) {
                setStageIds_ChangedDuration.add(oStage.Id);
            }
        }
        system.debug('Processing projects: ' + setProjectIds);

        Id currentProjectId = null;
        Datetime stageStartDate = null;
        Date stageDueDate = null;

        for (Project_Phase__c oProjectPhase : [Select Id
                                                    , Name
                                                    , Project_Phase_Due_Date__c
                                                    , Due_Date_Baseline__c
                                                    , Sort_Order__c
                                                    , Funding_Round_Phase__r.Start_Date__c
                                                    , Project__c
                                                    , Project__r.Name
                                                    , Project__r.CreatedDate
                                                    , Project__r.Funding_Round__r.Close_Date_Time__c
                                                    , (Select Id
                                                            , Name
                                                            , Estimated_Days_to_Complete__c
                                                            , Stage_Begin__c
                                                            , Due_Date__c
                                                            , Sort_Order__c
                                                            , Funding_Round_Sub_Phase__r.Start_Date__c
                                                            , Funding_Round_Sub_Phase__r.Estimated_Days_to_Complete__c
                                                        From Project_Sub_Phases__r
                                                        Order By Sort_Order__c)
                                                From Project_Phase__c
                                                Where Project__c in :setProjectIds
                                                Order By Project__c, Sort_Order__c]) {
            if (currentProjectId == null) {
                currentProjectId = oProjectPhase.Project__c;
                stageStartDate = Date.valueOf(oProjectPhase.Project__r.CreatedDate);
                if (oProjectPhase.Due_Date_Baseline__c == 'Stage Duration - Funding Round') {
                    stageStartDate = oProjectPhase.Funding_Round_Phase__r.Start_Date__c;
                }
            }

            //if changed projects, reset variables
            system.debug('Evaluate projects.  currentProjectId: ' + currentProjectId + ' oProjectPhase.Project__c: ' + oProjectPhase.Project__c + ' stageDueDate: ' + stageDueDate);
            if (currentProjectId != oProjectPhase.Project__c) {
                currentProjectId = oProjectPhase.Project__c;
                stageStartDate = Date.valueOf(oProjectPhase.Project__r.CreatedDate);
                if (oProjectPhase.Due_Date_Baseline__c == 'Stage Duration - Funding Round') {
                    stageStartDate = oProjectPhase.Funding_Round_Phase__r.Start_Date__c;
                }
                stageDueDate = null;
            }

            //loop through the stages to calculate new due dates
            for (Project_Sub_Phase__c oProjectStage : oProjectPhase.Project_Sub_Phases__r) {
                Decimal durationDays = oProjectStage.Estimated_Days_to_Complete__c;
                //set time on start date to match the original time value
                stageStartDate = Datetime.newInstance(stageStartDate.year()
                                                        , stageStartDate.month()
                                                        , stageStartDate.day()
                                                        , oProjectStage.Stage_Begin__c.hour()
                                                        , oProjectStage.Stage_Begin__c.minute()
                                                        , oProjectStage.Stage_Begin__c.second());
                System.debug(oProjectStage.Name + ' existing begin date: ' + oProjectStage.Stage_Begin__c + '; SavedStartDate: ' + stageStartDate);

                if (setStageIds_ChangedDuration.contains(oProjectStage.Id)) {
                    durationDays = mapNewRecs.get(oProjectStage.Id).Estimated_Days_to_Complete__c;
                }
                if (oProjectPhase.Due_Date_Baseline__c == 'Stage Duration - Funding Round') {
                    system.debug('Due Date Baseline is Funding Round.'
                                    + '\nFR Start Date: ' + oProjectStage.Funding_Round_Sub_Phase__r.Start_Date__c
                                    + '\nStage Begin: ' + oProjectStage.Stage_Begin__c);
                    stageStartDate = Datetime.newInstance(oProjectStage.Funding_Round_Sub_Phase__r.Start_Date__c.year()
                                                            , oProjectStage.Funding_Round_Sub_Phase__r.Start_Date__c.month()
                                                            , oProjectStage.Funding_Round_Sub_Phase__r.Start_Date__c.day()
                                                            , oProjectStage.Stage_Begin__c.hour()
                                                            , oProjectStage.Stage_Begin__c.minute()
                                                            , oProjectStage.Stage_Begin__c.second());
                    durationDays = oProjectStage.Funding_Round_Sub_Phase__r.Estimated_Days_to_Complete__c;
                }

                //initiate due date to be the start date without time
                stageDueDate = Date.newInstance(stageStartDate.year()
                                                , stageStartDate.month()
                                                , stageStartDate.day());
                stageDueDate = stageDueDate.addDays(Integer.valueOf(durationDays));
                //make sure the due date doesn't end on a weekend or holiday.  If it does, advance to next business day
                while (DateUtility.isBusinessDay(stageDueDate) == false) {
                    stageDueDate = stageDueDate.addDays(1);
                }

                if (oProjectStage.Due_Date__c != stageDueDate) {
                    System.debug('Stage Date changed.  Existing: ' + oProjectStage.Due_Date__c + '; New: ' + stageDueDate);
                    lstUpdateStages.add(new Project_Sub_Phase__c(Id = oProjectStage.Id
                                                                , Stage_Begin__c = stageStartDate
                                                                , Due_Date__c = stageDueDate
                                                                , Estimated_Days_to_Complete__c = durationDays));
                }
                System.debug('Phase: ' + oProjectPhase.Name + '; Stage: ' + oProjectStage.Name + '; Duration: ' + durationDays + '; Due Date: ' + stageDueDate);
                //set the start date for the next stage so it matches the due date of the current stage
                stageStartDate = Datetime.newInstance(stageDueDate.year()
                                                        , stageDueDate.month()
                                                        , stageDueDate.day()
                                                        , oProjectStage.Stage_Begin__c.hour()
                                                        , oProjectStage.Stage_Begin__c.minute()
                                                        , oProjectStage.Stage_Begin__c.second());
            }

            //set the phase due date to the last stage due date; ensure the time matches the close date/time 
            // on the funding round if it's populated
            Datetime baseTime = oProjectPhase.Project__r.CreatedDate;
            if (oProjectPhase.Project__r.Funding_Round__r.Close_Date_Time__c != null) {
                baseTime = oProjectPhase.Project__r.Funding_Round__r.Close_Date_Time__c;
            }
            system.debug('stageDueDate: ' + stageDueDate + '  baseTime: ' + baseTime);
            if (stageDueDate != null && baseTime != null) {
                Datetime phaseDueDate = Datetime.newInstance(stageDueDate.year()
                                                                , stageDueDate.month()
                                                                , stageDueDate.day()
                                                                , baseTime.hour()
                                                                , baseTime.minute()
                                                                , baseTime.second());
                if (Date.valueOf(oProjectPhase.Project_Phase_Due_Date__c) != stageDueDate) {
                    lstUpdatePhases.add(new Project_Phase__c(Id = oProjectPhase.Id
                                                            , Project_Phase_Due_Date__c = phaseDueDate));
                }
            }
        }

        update lstUpdatePhases;
        update lstUpdateStages;
    }

}