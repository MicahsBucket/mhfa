/*
*   {purpose}  	Manage the functionality tied to Project Phase Decision automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date			Name             		Description
*   02/28/17		Kevin Johnson DCS		Created
*	08/07/17		Kevin Johnson DCS 		Added MinnDocs decision to Project update logic in updateProjectPhaseDecisionStatus method
*	01/31/19		Kevin Johnson DCS		Removed updateProjectPhaseDecisionStatus method to support updated requirements
*   =============================================================================
*/

public with sharing class ProjectPhaseDecisionUtility {
	public static String validateRecordParameters(List<Project_Phase_Decision__c> lstPPD){
		/*
		*{purpose}		Manages validation of Project Phase Decision records being added
		*
		*{function}		Builds list of Project Phase Decision (PPD) records per parent Project for use in validating attributes of new PPD record.  
		*				If a PPD record (inserted new) does not meet requirements an error is passed notifying user of flagged validation.
		*				
		*{trigger}  	ProjectPhaseDecisionEventListener: before insert, after insert, before update
		*/

		Set<ID> setProjectids = new Set<ID>();
		Set<ID> setPPDids = new Set<ID>();
		for(Project_Phase_Decision__c ppd : lstPPD){
			setPPDids.add(ppd.Id);
			setProjectids.add(ppd.Project__c);
		}

		Map<Id, List<String>> mapPPDTypes = new Map<Id, List<String>>();

		//compile existing ppd records
		for(Project_Phase_Decision__c oPPD : [SELECT Project__c
												   , Decision_Type__c
											     FROM Project_Phase_Decision__c
												 WHERE Project__c IN :setProjectids
												 AND Id NOT IN :setPPDids]){
					if(mapPPDTypes.containsKey(oPPD.Project__c)) {
						List<String> decType = mapPPDTypes.get(oPPD.Project__c);
						decType.add(oPPD.Decision_Type__c);
						mapPPDTypes.put(oPPD.Project__c, decType);
					} else {
						mapPPDTypes.put(oPPD.Project__c, new List<String> {oPPD.Decision_Type__c});
					}
		}
		system.debug('mapPPDTypes: ' + mapPPDTypes);

		String errorMsg = null;
		for(Project_Phase_Decision__c ppd : lstPPD){
			//validate if newly added ppd phase already exists for project
			List<String> lstExistingTypes = mapPPDTypes.get(ppd.Project__c);
			system.debug('LSTEXISTINGTYPES: ' + lstExistingTypes);
			Set<String> setExistingTypes = new Set<String>();
			if(lstExistingTypes != null){
				setExistingTypes.addAll(lstExistingTypes);
				
				String addedType = ppd.Decision_Type__c;
				if(setExistingTypes.contains(addedType)){
					errorMsg = 'Type ' + addedType + ' already exists for this Project and thus cannot be added.';
					break;
				}
			}			
		}
		return errorMsg;		
	}
}