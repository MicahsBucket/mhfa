@isTest
private class ProjectDocumentTypeUtility_TEST {
	@isTest static void testProjectDocumentTypeUtility() {
		//stage custom settings
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		     FROM Enable_Triggers__mdt
						  		       		     WHERE MasterLabel = 'ProjectDocumentationType'];

		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //create supporting document
        Project_Documentation__c pd = TestDataUtility.createProjectDocumentation(p.Id, 
                                                                                 'Closing', 
                                                                                 '1', 
                                                                                 'jpg');

		//run tests
        Test.startTest();
	        //create supporting document type
	        Project_Documentation_Type__c pdt = TestDataUtility.createProjectDocumentationType(pd.Id, 
	                                                                                       'Closing', 
	                                                                                       'Correspondence');
        	delete pdt;
        Test.stopTest();
	}
}