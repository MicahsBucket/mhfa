@isTest
public class ProjectDocumentPartnerUtility_TEST  {
	@isTest static void testRecordValidation() {
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

		//create project document: partner
		Project_Documents_Partner__c pdp = TestDataUtility.createProjectDocumentPartner(p.Id,
		                                                                                'Selection Narrative');

		Test.startTest();
		    //test duplicate pdp
            //insert
			try{
                Project_Documents_Partner__c pdp2 = TestDataUtility.createProjectDocumentPartner(p.Id,
		                                                                                          'Selection Narrative');
            } catch (Exception e){  //'Document Selection Narrative already exists for this Project and thus cannot be added.'
                Boolean validationErrorRecord =  TRUE;
                System.assertEquals(validationErrorRecord, true, e.getMessage());
            } 
			//update
			try{
                pdp.Type__c = 'Selection Narrative';
				update pdp;
            } catch (Exception e){  //'Document Selection Narrative already exists for this Project and thus cannot be added.'
                Boolean validationErrorRecord =  TRUE;
                System.assertEquals(validationErrorRecord, true, e.getMessage());
            } 
        Test.stopTest();
	}
}