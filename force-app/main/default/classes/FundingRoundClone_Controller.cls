/*
*   {Purpose}  	Manage Funding Round clone functionality                   
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   09/20/16	Kevin Johnson DCS		Created
*	12/12/17	Kevin Johnson DCS 		-Added logic to newFR function to account for dependent picklist values
*										-Added derived keys to accurately map records across phases / trigger groups
*	01/18/18	Kevin Johnson DCS 		Added funding round milestone date cloning capability
*	02/21/18	Kevin Johnson DCS 		Added ability to clone manually shared groups
*	03/06/18	Kevin Johnson DCS 		Added funding round narrative group & author cloning capability
*	03/16/18	Kevin Johnson DCS 		Added funding round team member cloning capability
*	04/16/18	Kevin Johnson DCS 		Updated derived keys to leverage source record id as key
*	04/17/19	Kevin Johnson DCS		-Refactored to account for Multifamily Business Flow model
*										-Refactored to leverage Record Cloned From ids
*	06/14/19	Eric Gronholz DCS		Created functions for each sObject clone routine
*	08/21/19	Kevin Johnson DCS 		Added funding round checklist item link cloning capability
*	09/12/19	Kevin Johnson DCS		Deprecated funding round milestone cloning functionality
*   =============================================================================
*/

public with sharing class FundingRoundClone_Controller {
	private ApexPages.StandardController controller {get;set;}
	private final Funding_Round__c oFundingRound;
	public String fundingRoundID;
	public Set<ID> setFilterID = new Set<ID>();
	public List<Funding_Round__c> lstFR;
	public List<Phase__c> lstFRPhase;
	//phase checklist
	public List<Phase_Checklist__c> lstFRPhaseChecklist;
	//phase checklist item
	public List<Phase_Checklist_Item__c> lstFRPCLI;
	public List<Phase__c> lstFRPhaseStage;
	public List<Trigger_Group__c> lstFRTriggerGroup;
	public List<Funding_Round_Trigger__c> lstFRTrigger;
	public List<Funding_Round_Checklist_Item__c> lstFRCLI;
	public List<Funding_Round_Checklist_Item_Trigger__c> lstFRCLITrigger;
	public List<Funding_Round_Checklist_Item_SubPhase__c> lstFRCLISubPhase;	
	//funding round task
	public List<Funding_Round_Task__c> lstFRTask;
	//funding round task characterisitc
	public List<Funding_Round_Task_Characteristic__c> lstFRTaskCharacteristic;
	public List<Funding_Round_Narrative_Group__c> lstFRNarrativeGroup;
	public List<Funding_Round_Narrative_Author__c> lstFRNarrativeAuthor;
	public List<Funding_Round_Narrative__c> lstFRNarrative;
	public List<Funding_Round_Documentation__c> lstFRDocumentation;
	public List<Funding_Round_Documentation_Type__c> lstFRDocumentationType;
	public List<Funding_Round_Team_Member__c> lstFRTeamMember;
	//funding round checklist item link
	public List<Funding_Round_Checklist_Item_Link__c> lstFRCLILink;
	public Map<String, Id> mapClonedIds = new Map<String, Id>();

	//constructor
	public FundingRoundClone_Controller(ApexPages.StandardController controller){
		this.controller = controller;
		oFundingRound = (Funding_Round__c)controller.getRecord();
		fundingRoundID = oFundingRound.Id;
	}

	public PageReference cloneFundingRound(){
		/*
		*	clones funding round
		*	fetchRecordsToClone function paramaters: param 1: record Ids used to filter required data set
		*										  	 param 2: object being queried
		*										  	 param 3: filter field referencing param 1 Ids
		*										  	 param 4: (optional) lookup fields to be queried
		*/
		//System.debug('Inside cloneFundingRound');
		Savepoint svpt = Database.setSavepoint();
		Exception ex = null;
		//clone funding round
		setFilterID.add(fundingRoundID);  //set filterid
		lstFR = fetchRecordsToClone(setFilterID, 
									'Funding_Round__c', 
									'ID', 
									null);		

		Funding_Round__c newFR = lstFR[0].clone();
		newFR.Name = 'Clone: ' + lstFR[0].Name;
		newFR.Make_Public__c = FALSE;
		newFR.Are_Projects_Being_Shared__c = 'No';
		if(lstFR[0].Project_Selection_By__c == 'Funding Round'){
			newFR.Default_Project_View__c = null;  //dependent picklist - need to set to null if value not mapped to selected parent value
		}
		try{
			insert newFR;
		} catch (Exception e) {
			ex = e;
		}
		if (ex == null) {
			ex = cloneFundingRoundShare(fundingRoundID, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFundingRoundCLI(setFilterId, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFundingRoundPhase(setFilterId, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFundingRoundChecklist(setFilterId, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			//lstFRPhase is populated in cloneFundingRoundPhase
			for(Phase__c oPhase : lstFRPhase){
				setFilterID.add(oPhase.ID);
			}
			ex = cloneFundingRoundPhaseStage(setFilterId);
		}
		if (ex == null) {
			setFilterID.clear();
			//lstFRPhaseStage is populated in cloneFundingRoundPhaseStage
			for(Phase__c oStage : lstFRPhaseStage){
				setFilterID.add(oStage.ID);
			}
			ex = clonePhaseChecklistItem(setFilterId);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = clonePhaseCharacteristicGroup(setFilterID, newFR.Id);				
		}
		if (ex == null) {
			setFilterID.clear();
			//lstFRTriggerGroup populated in clonePhaseCharacteristicGroup
			for(Trigger_Group__c oTG : lstFRTriggerGroup){
				setFilterID.add(oTG.ID);
			}
			ex = clonePhaseCharacteristic(setFilterID);
		}
		if(ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFRCLICharacteristic(setFilterID);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFRCLIStage(setFilterID);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFundingRoundTask(setFilterID, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			//lstFRTask populated in cloneFundingRoundTask
			for(Funding_Round_Task__c oFRTask : lstFRTask){
				setFilterID.add(oFRTask.Id);
			}
			ex = cloneFRTaskCharacteristic(setFilterID);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFundingRoundTeamMember(setFilterID, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFundingRoundDocumentation(setFilterID, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			//lstFRDocumentation populated in cloneFundingRoundDocumentation
			for(Funding_Round_Documentation__c oFRD : lstFRDocumentation){
				setFilterID.add(oFRD.ID);
			}
			ex = cloneFRDocumentationType(setFilterID);
		}
		if (ex == null) {
			setFilterID.clear();
			setFilterID.add(fundingRoundID);
			ex = cloneFRNarrativeGroup(setFilterID, newFR.Id);
		}
		if (ex == null) {
			setFilterID.clear();
			//lstFRNarrativeGroup populated in cloneFRNarrativeGroup
			for(Funding_Round_Narrative_Group__c oFRG : lstFRNarrativeGroup){
				setFilterID.add(oFRG.ID);
			}
			ex = cloneFRNarrativeAuthor(setFilterID);
		}
		if (ex == null) {
			//setFilterID.clear();
			//lstFRNarrativeAuthor populated in cloneFRNarrativeAuthor
			setFilterID.clear();
			for(Funding_Round_Narrative_Author__c oFRA : lstFRNarrativeAuthor){
				setFilterID.add(oFRA.ID);
			}
			ex = cloneFundingRoundNarrative(setFilterID, newFR.Id);
		}
		System.debug('CLONE TEST FRCLI LINK ex: ' + ex);
		if (ex == null) {
			setFilterID.clear();
			//lstFRCLI populated in cloneFundingRoundCLI
			for(Funding_Round_Checklist_Item__c oFRCLIL : lstFRCLI){
				setFilterID.add(oFRCLIL.ID);
			}
			System.debug('CLONE TEST FRCLI LINK setFilterID: ' + setFilterID);
			ex = cloneFundingRoundCLILink(setFilterID);
		}

		if (ex == null) {
			//open newly cloned funding round
			PageReference clonedFR = new ApexPages.StandardController(newFR).view();
	        clonedFR.setRedirect(true);
	        return clonedFR;
		} else {
        	Database.rollback(svpt);
            ApexPages.addMessages(ex);
            return null;		
		}
	}

	private List<sObject> fetchRecordsToClone(Set<ID> recordId, String obj, String filter, String lookup){
		/*
		*	function compiling objects attributes/data to be cloned
		*	leverage metadata api per object to build list of fields to be queried ensuring new/updated attributes are captured
		*/

		Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
		Schema.SObjectType sobjType = gd.get(obj); 
		Schema.DescribeSObjectResult describeResult = sobjType.getDescribe(); 
		List<String> fieldNames = new List<String>(describeResult.fields.getMap().keySet());

		//account for custom lookup fields in query not compiled in fieldnames
		if(lookup != null){
			fieldNames.add(lookup);
		}

		String query =
		  ' SELECT ' +
		      String.join(fieldNames, ',') +
		  ' FROM ' +
		      obj +
		  ' WHERE ' +
		      filter + ' IN :recordId ';

		system.debug('fetch query: ' + query);
		List<SObject> records = Database.query(query);

		return records;
	}

/*************************************************
** Added by EBG on 20190614 
** Routines to clone individual sObject types
*************************************************/

	/*******************************************************
	** clone manually shared groups shared with funding round
	********************************************************/
	private Exception cloneFundingRoundShare(Id fundingRoundID, Id newFRId) {
		System.debug('Inside cloneFundingRoundShare');
		Exception retVal = null;

		List<Funding_Round__Share> lstFRShareClone = new List<Funding_Round__Share>();
		List<Funding_Round__Share> lstFRShareInsert = new List<Funding_Round__Share>();

		//-build list of related funding_round__share records
		for(Funding_Round__Share frs : [SELECT AccessLevel
		                         				, UserOrGroupId
		                         				, RowCause
		                      			FROM Funding_Round__Share
		                      			WHERE ParentId = :fundingRoundID]){
		    String strUserOrGroupId = frs.UserOrGroupId;
			//add manually shared entities with 00G prefix DO NOT add custom object sharing rule
		    if(strUserOrGroupId.startswith('00G') && frs.RowCause == 'Manual'){  
				lstFRShareClone.add(frs);
		    }      
		}

		//-build list of new funding round shares to insert
		if(lstFRShareClone.size() > 0){
			for(Funding_Round__Share frs : lstFRShareClone){
		        Funding_Round__Share newFRShare = new Funding_Round__Share();
		        newFRShare.parentid = newFRId;
		        newFRShare.AccessLevel = 'Read';
		        newFRShare.UserOrGroupId = frs.UserOrGroupId;
		        newFRShare.RowCause = 'Manual';
			    lstFRShareInsert.add(newFRShare);
		    }
			try {
				insert lstFRShareInsert;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;		
	}

	/*******************************************************
	** clone funding round checklist items
	********************************************************/
	private Exception cloneFundingRoundCLI(Set<ID> setFilterID, Id newFRId) {
		System.debug('Inside cloneFundingRoundCLI');
		Exception retVal = null;

		lstFRCLI = fetchRecordsToClone(setFilterID, 
										'Funding_Round_Checklist_Item__c', 
										'Funding_Round__c', 
										null);
		List<Funding_Round_Checklist_Item__c> cloneFRCLI = new List<Funding_Round_Checklist_Item__c>();

		if(lstFRCLI.size() > 0){
			//loop through source funding round checklist items
			for(Funding_Round_Checklist_Item__c frcli : lstFRCLI){ 
				Funding_Round_Checklist_Item__c newFRCLI = frcli.clone();
				newFRCLI.Funding_Round__c = newFRId;
				newFRCLI.Record_Cloned_From__c = frcli.Id;
				cloneFRCLI.add(newFRCLI);
			}

			try {
				insert cloneFRCLI;

				for(Funding_Round_Checklist_Item__c frcli : cloneFRCLI){
					String keyVal = frcli.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, frcli.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}		
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round phase
	********************************************************/
	private Exception cloneFundingRoundPhase(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside cloneFundingRoundPhase');
		Exception retVal = null;

		lstFRPhase = fetchRecordsToClone(setFilterID, 
											'Phase__c', 
											'Funding_Round__c', 
											null);
		List<Phase__c> cloneFRPhase = new List<Phase__c>();
			
		if(lstFRPhase.size() > 0){
			//loop through source phases
			for(Phase__c p : lstFRPhase){ 
				Phase__c newPhase = p.clone();
				newPhase.Funding_Round__c = newFRId;
				newPhase.Record_Cloned_From__c = p.Id;
				System.debug('FR CLONE newPhase: ' + newPhase);
				cloneFRPhase.add(newPhase);
			}
			try {
				insert cloneFRPhase;
				
				for(Phase__c p : cloneFRPhase){
					String keyVal = p.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, p.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round checklists (associated to phase)
	********************************************************/
	private Exception cloneFundingRoundChecklist(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside cloneFundingRoundChecklist');
		Exception retVal = null;

		lstFRPhaseChecklist = fetchRecordsToClone(setFilterID,
													'Phase_Checklist__c',
													'Funding_Round__c',
													null);
		List<Phase_Checklist__c> cloneFRPhaseCL = new List<Phase_Checklist__c>();

		if(lstFRPhaseChecklist.size() > 0){
			//loop through source phase checklists
			for(Phase_Checklist__c frpcl : lstFRPhaseChecklist){ 
				Phase_Checklist__c newPhaseCL = frpcl.clone();
				newPhaseCL.Funding_Round__c = newFRId;
				String pKey = frpcl.Funding_Round_Phase__c;
				newPhaseCL.Funding_Round_Phase__c = mapClonedIds.get(pKey);
				newPhaseCL.Record_Cloned_From__c = frpcl.Id;
				cloneFRPhaseCL.add(newPhaseCL);
			}

			try {
				insert cloneFRPhaseCL;
				
				for(Phase_Checklist__c pcl : cloneFRPhaseCL){
					String keyVal = pcl.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, pcl.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round phase stages (sub phases)
	********************************************************/
	private Exception cloneFundingRoundPhaseStage(Set<Id> setFilterID) {
		System.debug('Inside cloneFundingRoundPhaseStage');
		Exception retVal = null;

		lstFRPhaseStage = fetchRecordsToClone(setFilterID, 
											'Phase__c', 
											'Parent_Phase__c', 
											'Parent_Phase__r.Phase__c');
		List<Phase__c> cloneFRPhaseStage = new List<Phase__c>();
			
		if(lstFRPhaseStage.size() > 0){
			//loop through source stages
			for(Phase__c s : lstFRPhaseStage){ 
				Phase__c newStage = s.clone();
				String pKey = s.Parent_Phase__c;
				newStage.Parent_Phase__c = mapClonedIds.get(pKey);
				newStage.Record_Cloned_From__c = s.Id;
				System.debug('FR CLONE newStage: ' + newStage);
				cloneFRPhaseStage.add(newStage);
			}

			try {
				insert cloneFRPhaseStage;

				for(Phase__c s : cloneFRPhaseStage){
					String keyVal = s.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, s.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}		
		}
		return retVal;
	}

	/*******************************************************
	** clone phase checklist items
	********************************************************/
	private Exception clonePhaseChecklistItem(Set<Id> setFilterID) {
		System.debug('Inside clonePhaseChecklistItem');
		Exception retVal = null; 

		lstFRPCLI = fetchRecordsToClone(setFilterID,
										'Phase_Checklist_Item__c',
										'Funding_Round_Stage__c',
										null);
		List<Phase_Checklist_Item__c> cloneFRPCLI = new List<Phase_Checklist_Item__c>();
		System.debug('FR Clone setFilterID: ' + setFilterID);
		System.debug('FR Clone lstFRPCLI: ' + lstFRPCLI);
		if(lstFRPCLI.size() > 0){ 
			//loop through source phase checklist items
			for(Phase_Checklist_Item__c frpcli : lstFRPCLI){ 
				Phase_Checklist_Item__c newFRPCLI = frpcli.clone();
				String fcliKey = frpcli.Funding_Round_Checklist_Item__c;
				System.debug('FR Clone fcliKey: ' + fcliKey);
				newFRPCLI.Funding_Round_Checklist_Item__c = mapClonedIds.get(fcliKey);
				String sKey = frpcli.Funding_Round_Stage__c;
				newFRPCLI.Funding_Round_Stage__c = mapClonedIds.get(sKey);
				String pclKey = frpcli.Phase_Checklist__c;
				newFRPCLI.Phase_Checklist__c = mapClonedIds.get(pclKey);
				newFRPCLI.Record_Cloned_From__c = frpcli.Id;
				cloneFRPCLI.add(newFRPCLI);
			}
			try {			
				insert cloneFRPCLI;

				for(Phase_Checklist_Item__c pcli : cloneFRPCLI){
					String keyVal = pcli.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, pcli.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone phase characteristic group (trigger group)
	********************************************************/
	private Exception clonePhaseCharacteristicGroup(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside clonePhaseCharacteristicGroup');
		Exception retVal = null;

		lstFRTriggerGroup = fetchRecordsToClone(setFilterID, 
												'Trigger_Group__c',
												'Funding_Round__c',
												null);
		List<Trigger_Group__c> cloneFRTriggerGroup = new List<Trigger_Group__c>();

		if(lstFRTriggerGroup.size() > 0){
			//loop through source trigger groups
			for(Trigger_Group__c tg : lstFRTriggerGroup){ 
				Trigger_Group__c newTG = tg.clone();
				newTG.Funding_Round__c = newFRId;
				newTG.Record_Cloned_From__c = tg.Id;
				cloneFRTriggerGroup.add(newTG);
			}
			try {
				insert cloneFRTriggerGroup;

				for(Trigger_Group__c tg : cloneFRTriggerGroup){
					String keyVal = tg.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, tg.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone phase characteristics (triggers)
	********************************************************/
	private Exception clonePhaseCharacteristic(Set<Id> setFilterID) {
		System.debug('Inside clonePhaseCharacteristic');
		Exception retVal = null;

		lstFRTrigger = fetchRecordsToClone(setFilterID, 
											'Funding_Round_Trigger__c', 
											'Trigger_Group__c',
											null);
		List<Funding_Round_Trigger__c> cloneFRTrigger = new List<Funding_Round_Trigger__c>();

		if(lstFRTrigger.size() > 0){
			//loop through source triggers
			for(Funding_Round_Trigger__c t : lstFRTrigger){ 
				Funding_Round_Trigger__c newFRT = t.clone();
				String tgKey = t.Trigger_Group__c;
				newFRT.Trigger_Group__c = mapClonedIds.get(tgKey);
				newFRT.Record_Cloned_From__c = t.Id;
				cloneFRTrigger.add(newFRT);
			}
			try {
				insert cloneFRTrigger;

				for(Funding_Round_Trigger__c frt : cloneFRTrigger){
					String keyVal = frt.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, frt.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}			
		}
		return retVal;
	}

	/*******************************************************
	** clone FRCLI Characteristics (triggers)
	********************************************************/
	private Exception cloneFRCLICharacteristic(Set<Id> setFilterID) {
		System.debug('Inside cloneFRCLICharacteristic');
		Exception retVal = null;

		lstFRCLITrigger = fetchRecordsToClone(setFilterID, 
											    'Funding_Round_Checklist_Item_Trigger__c', 
											    'Funding_Round_Checklist_Item__r.Funding_Round__c', 
											    'Funding_Round_Checklist_Item__r.Name, Funding_Round_Trigger__r.Name');
		List<Funding_Round_Checklist_Item_Trigger__c> cloneFRCLITrigger = new List<Funding_Round_Checklist_Item_Trigger__c>();

		if(lstFRCLITrigger.size() > 0){
			//loop through source funding round checklist item triggers
			for(Funding_Round_Checklist_Item_Trigger__c frclit : lstFRCLITrigger){ 
				Funding_Round_Checklist_Item_Trigger__c newFRCLIT = frclit.clone();
				String fcliKey = frclit.Funding_Round_Checklist_Item__c;
				newFRCLIT.Funding_Round_Checklist_Item__c = mapClonedIds.get(fcliKey);
				String tgKey = frclit.Funding_Round_Trigger__c;
				newFRCLIT.Funding_Round_Trigger__c = mapClonedIds.get(tgKey);
				newFRCLIT.Record_Cloned_From__c = frclit.Id;
				cloneFRCLITrigger.add(newFRCLIT);
			}
			try {
				insert cloneFRCLITrigger;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}


	/*******************************************************
	** clone FRCLI Stages (Sub-Phases)
	********************************************************/
	private Exception cloneFRCLIStage(Set<Id> setFilterID) {
		System.debug('Inside cloneFRCLIStage');
		Exception retVal = null;

		lstFRCLISubPhase = fetchRecordsToClone(setFilterID, 
												'Funding_Round_Checklist_Item_SubPhase__c', 
												'Funding_Round_Checklist_Item__r.Funding_Round__c', 
												'Funding_Round_Checklist_Item__r.Name, SubPhase__r.Sub_Phase__c');
		List<Funding_Round_Checklist_Item_SubPhase__c> cloneFRCLISubPhase = new List<Funding_Round_Checklist_Item_SubPhase__c>();

		if(lstFRCLISubPhase.size() > 0){
			//loop through source funding round checklist item sub phases
			for(Funding_Round_Checklist_Item_SubPhase__c frclisp : lstFRCLISubPhase){ 
				Funding_Round_Checklist_Item_SubPhase__c newFRCLISB = frclisp.clone();
				String fcliKey = frclisp.Funding_Round_Checklist_Item__c;
				newFRCLISB.Funding_Round_Checklist_Item__c = mapClonedIds.get(fcliKey);
				String sKey = frclisp.SubPhase__c;
				newFRCLISB.SubPhase__c = mapClonedIds.get(sKey);
				cloneFRCLISubPhase.add(newFRCLISB);
			}
			try {
				insert cloneFRCLISubPhase;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round tasks
	********************************************************/
	private Exception cloneFundingRoundTask(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside cloneFundingRoundTask');
		Exception retVal = null;

		lstFRTask = fetchRecordsToClone(setFilterID, 
										'Funding_Round_Task__c', 
										'Funding_Round__c', 
										null);
		List<Funding_Round_Task__c> cloneFRTask = new List<Funding_Round_Task__c>();

		if(lstFRTask.size() > 0){
			Map<Id, Funding_Round_Task__c> mapOrigTask = new Map<Id, Funding_Round_Task__c>();
			//loop through source funding round tasks
			for(Funding_Round_Task__c frt : lstFRTask){ 
				Funding_Round_Task__c newFRTask = frt.clone();
				newFRTask.Record_Cloned_From__c = frt.Id;
			
				cloneFRTask.add(newFRTask);
				mapOrigTask.put(frt.Id, frt);
			}

			try {
				insert cloneFRTask;			
			} catch (Exception ex) {
				retVal = ex;
				System.debug('Error\n' + ex);
				for (Funding_Round_Task__c oTaskError : cloneFRTask) {
					System.debug('oTaskError INSERT: ' + oTaskError);
				}
			}	
		
			if (retVal == null) {
				for(Funding_Round_Task__c t : cloneFRTask){
					String keyVal = t.Record_Cloned_From__c;
					mapClonedIds.put(keyVal, t.Id);
				}
				//loop through the map of new tasks to assign new values - post insert assignment helps meet validation rules / dependent picklist requirements
				List<Funding_Round_Task__c> lstUpdatePrecedingTask = new List<Funding_Round_Task__c>();
				for (Funding_Round_Task__c tskNew: cloneFRTask) {
					tskNew.Funding_Round__c = newFRId;
					tskNew.Checklist__c = mapClonedIds.get(mapOrigTask.get(tskNew.Record_Cloned_From__c).Checklist__c);					
					if (mapOrigTask.get(tskNew.Record_Cloned_From__c).Preceding_Task__c != null) {
						Id origPrecedingId = mapOrigTask.get(tskNew.Record_Cloned_From__c).Preceding_Task__c;
						tskNew.Preceding_Task__c = 	mapClonedIds.get(origPrecedingId);
					}
					tskNew.Due_Date_Baseline__c = mapOrigTask.get(tskNew.Record_Cloned_From__c).Due_Date_Baseline__c;
					tskNew.Funding_Round_Due_Date__c = mapOrigTask.get(tskNew.Record_Cloned_From__c).Funding_Round_Due_Date__c;
					tskNew.Phase__c = mapOrigTask.get(tskNew.Record_Cloned_From__c).Phase__c;
					tskNew.Stage__c = mapOrigTask.get(tskNew.Record_Cloned_From__c).Stage__c;
					tskNew.Stage_Lookup__c = mapClonedIds.get(mapOrigTask.get(tskNew.Record_Cloned_From__c).Stage_Lookup__c);
					lstUpdatePrecedingTask.add(tskNew);
				}
				if (!lstUpdatePrecedingTask.isEmpty()) {
					try {
						update lstUpdatePrecedingTask;
					} catch (Exception ex) {
						retVal = ex;
						System.debug('Error\n' + ex);
						for (Funding_Round_Task__c oTaskError : lstUpdatePrecedingTask) {
							System.debug('oTaskError UPDATE: ' + oTaskError);
						}
					}
				}
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round task characteristics
	********************************************************/
	private Exception cloneFRTaskCharacteristic(Set<Id> setFilterID) {
		System.debug('Inside cloneFRTaskCharacteristic');
		Exception retVal = null;

		lstFRTaskCharacteristic = fetchRecordsToClone(setFilterID, 
									                    'Funding_Round_Task_Characteristic__c', 
										                'Funding_Round_Task__c', 
											            null);
		List<Funding_Round_Task_Characteristic__c> cloneFRTaskCharacteristics = new List<Funding_Round_Task_Characteristic__c>();

		if(lstFRTaskCharacteristic.size() > 0){
			//loop through source funding round task characteristics
			for(Funding_Round_Task_Characteristic__c frtc : lstFRTaskCharacteristic){ 
				Funding_Round_Task_Characteristic__c newFRTaskCharacteristic = frtc.clone();
				String tKey = frtc.Funding_Round_Task__c;
				newFRTaskCharacteristic.Funding_Round_Task__c = mapClonedIds.get(tKey);
				String tgKey = frtc.Funding_Round_Characteristic__c;
				newFRTaskCharacteristic.Funding_Round_Characteristic__c = mapClonedIds.get(tgKey);
				newFRTaskCharacteristic.Record_Cloned_From__c = frtc.Id;
				cloneFRTaskCharacteristics.add(newFRTaskCharacteristic);
			}
			try {
				insert cloneFRTaskCharacteristics;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round team members
	********************************************************/
	private Exception cloneFundingRoundTeamMember(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside cloneFundingRoundTeamMember');
		Exception retVal = null;

		lstFRTeamMember = fetchRecordsToClone(setFilterID, 
										        'Funding_Round_Team_Member__c', 
										        'Funding_Round__c', 
										        null);
		List<Funding_Round_Team_Member__c> cloneFRTeamMember = new List<Funding_Round_Team_Member__c>();

		if(lstFRTeamMember.size() > 0){
			for(Funding_Round_Team_Member__c frtm : lstFRTeamMember){ //loop through source funding round team members
				Funding_Round_Team_Member__c newFRTM = frtm.clone();
				newFRTM.Funding_Round__c = newFRId;
				cloneFRTeamMember.add(newFRTM);
			}
			try {
				insert cloneFRTeamMember;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone documentation
	********************************************************/
	private Exception cloneFundingRoundDocumentation(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside cloneFundingRoundDocumentation');
		Exception retVal = null;
			
		lstFRDocumentation = fetchRecordsToClone(setFilterID, 
											    'Funding_Round_Documentation__c', 
											    'Funding_Round__c', 
											    null);
		List<Funding_Round_Documentation__c> cloneFRDocumentation = new List<Funding_Round_Documentation__c>();

		if(lstFRDocumentation.size() > 0){
			//loop through source documentation
			for(Funding_Round_Documentation__c d : lstFRDocumentation){ 
				Funding_Round_Documentation__c newDocumentation = d.clone();
				newDocumentation.Funding_Round__c = newFRId;
				cloneFRDocumentation.add(newDocumentation);
			}
			try {
				insert cloneFRDocumentation;
				
				for(Funding_Round_Documentation__c d : cloneFRDocumentation){
					mapClonedIds.put(d.Bucket__c, d.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone documentation type
	********************************************************/
	private Exception cloneFRDocumentationType(Set<Id> setFilterID) {
		System.debug('Inside cloneFRDocumentationType');
		Exception retVal = null;

		lstFRDocumentationType = fetchRecordsToClone(setFilterID, 
												        'Funding_Round_Documentation_Type__c', 
												        'Funding_Round_Documentation__c', 
												        null);
		List<Funding_Round_Documentation_Type__c> cloneFRDocumentationFunction = new List<Funding_Round_Documentation_Type__c>();

		if(lstFRDocumentationType.size() > 0){
			//loop through source subphases
			for(Funding_Round_Documentation_Type__c dt : lstFRDocumentationType){ 
				Funding_Round_Documentation_Type__c newDocumentationType = dt.clone();
				newDocumentationType.Funding_Round_Documentation__c = mapClonedIds.get(dt.Bucket__c);
				cloneFRDocumentationFunction.add(newDocumentationType);
			}
			try {		
				insert cloneFRDocumentationFunction;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round narrative groups
	********************************************************/
	private Exception cloneFRNarrativeGroup(Set<Id> setFilterID, Id newFRId) {
		System.debug('Inside cloneFRNarrativeGroup');
		Exception retVal = null;

		lstFRNarrativeGroup = fetchRecordsToClone(setFilterID, 
											        'Funding_Round_Narrative_Group__c', 
											        'Funding_Round__c', 
											        null);
		List<Funding_Round_Narrative_Group__c> cloneFRNarratvieGroup = new List<Funding_Round_Narrative_Group__c>();

		if(lstFRNarrativeGroup.size() > 0){
			//loop through source narrative groups
			for(Funding_Round_Narrative_Group__c frng : lstFRNarrativeGroup){ 
				Funding_Round_Narrative_Group__c newFRNG = frng.clone();
				newFRNG.Funding_Round__c = newFRId;
				cloneFRNarratvieGroup.add(newFRNG);
			}
			try {
				insert cloneFRNarratvieGroup;
				
				for(Funding_Round_Narrative_Group__c frng : cloneFRNarratvieGroup){					
					mapClonedIds.put(frng.Narrative_Group__c, frng.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round narrative authors
	********************************************************/
	private Exception cloneFRNarrativeAuthor(Set<Id> setFilterID) {
		System.debug('Inside cloneFRNarrativeAuthor');
		Exception retVal = null;

		lstFRNarrativeAuthor = fetchRecordsToClone(setFilterID, 
												    'Funding_Round_Narrative_Author__c', 
												    'Funding_Round_Narrative_Group__c', 
												    null);
		List<Funding_Round_Narrative_Author__c> cloneFRNarratvieAuthors = new List<Funding_Round_Narrative_Author__c>();

		if(lstFRNarrativeAuthor.size() > 0){
			//loop through source narrative authors
			for(Funding_Round_Narrative_Author__c frna : lstFRNarrativeAuthor){ 
				Funding_Round_Narrative_Author__c newFRNA = frna.clone();
				newFRNA.Funding_Round_Narrative_Group__c = mapClonedIds.get(frna.Narrative_Group__c);
				cloneFRNarratvieAuthors.add(newFRNA);
			}
			try {
				insert cloneFRNarratvieAuthors;
			
				for(Funding_Round_Narrative_Author__c frna : cloneFRNarratvieAuthors){
					String keyVal = frna.Narrative_Group__c + frna.Narrative_Author__c;
					mapClonedIds.put(keyVal, frna.Id);
				}
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round narratives
	********************************************************/
	private Exception cloneFundingRoundNarrative(Set<Id> setFilterID, Id newFRId) {
		//System.debug('Inside cloneFundingRoundNarrative');
		Exception retVal = null;

		lstFRNarrative = fetchRecordsToClone(setFilterID, 
											 'Funding_Round_Narrative__c', 
										     'Funding_Round_Narrative_Author__c', 
										      null);
		List<Funding_Round_Narrative__c> cloneFRNarrative = new List<Funding_Round_Narrative__c>();

		if(lstFRNarrative.size() > 0){
			//loop through source funding round narratives
			for(Funding_Round_Narrative__c frn : lstFRNarrative){ 
				Funding_Round_Narrative__c newFRN = frn.clone();
				newFRN.Funding_Round__c = newFRId;
				String frnaKey = frn.Narrative_Group__c + frn.Author__c;
				newFRN.Funding_Round_Narrative_Author__c = mapClonedIds.get(frnaKey);				
				cloneFRNarrative.add(newFRN);
			}			
			try {
				insert cloneFRNarrative;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}

	/*******************************************************
	** clone funding round checklist item links
	********************************************************/
	private Exception cloneFundingRoundCLILink(Set<Id> setFilterID) {
		System.debug('Inside cloneFundingRoundCLILink');
		Exception retVal = null;

		lstFRCLILink = fetchRecordsToClone(setFilterID, 
									       'Funding_Round_Checklist_Item_Link__c', 
										   'Funding_Round_Checklist_Item__c', 
										    null);
		List<Funding_Round_Checklist_Item_Link__c> cloneFRChecklistItemLink = new List<Funding_Round_Checklist_Item_Link__c>();

		if(lstFRCLILink.size() > 0){
			//loop through source funding round checklist item link records
			for(Funding_Round_Checklist_Item_Link__c frclilink : lstFRCLILink){ 
				Funding_Round_Checklist_Item_Link__c newFRCLILink = frclilink.clone();
				newFRCLILink.Funding_Round_Checklist_Item__c = mapClonedIds.get(frclilink.Funding_Round_Checklist_Item__c);
				cloneFRChecklistItemLink.add(newFRCLILink);
			}
			try {		
				insert cloneFRChecklistItemLink;
			} catch (Exception ex) {
				retVal = ex;
			}
		}
		return retVal;
	}	
}