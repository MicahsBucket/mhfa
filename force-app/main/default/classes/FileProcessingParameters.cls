/*
*   {Purpose}  	Manages which methods are executed during processing of an uploaded/updated File
*				Primary function is to reduce the number of SOQL queries being run when Files are uploaded/processed
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   06/19/18	Kevin Johnson DCS		Created
*   =============================================================================
*/

public class FileProcessingParameters  {
	private static Boolean blnRunFileValidation = true;
	private static Boolean blnRunShareFileNonOpptyTeamMembers = true;
	private static Boolean blnRunShareFileOpptyTeamMembers = true;
	private static Boolean blnRunEmailNotification = true;
	private static Boolean blnRunUpdateFileParentAttributes = true;

	public static void assignRunValue(Boolean blnRunTriggerFileValidation,
									  Boolean blnRunTriggerShareFileNonOpptyTeamMembers,
									  Boolean blnRunTriggerShareFileOpptyTeamMembers,
									  Boolean blnRunTriggerEmailNotification,
									  Boolean blnRunTriggerUpdateFileParentAttributes){
		blnRunFileValidation = blnRunTriggerFileValidation;
		blnRunShareFileNonOpptyTeamMembers = blnRunTriggerShareFileNonOpptyTeamMembers;
		blnRunShareFileOpptyTeamMembers = blnRunTriggerShareFileOpptyTeamMembers;
		blnRunEmailNotification = blnRunTriggerEmailNotification;
		blnRunUpdateFileParentAttributes = blnRunTriggerUpdateFileParentAttributes;
	}

	public static Boolean runFileValidation(){
		return blnRunFileValidation;
	}

	public static Boolean runShareFileNonOpptyTeamMembers(){
		return blnRunShareFileNonOpptyTeamMembers;
	}

	public static Boolean runShareFileOpptyTeamMembers(){
		return blnRunShareFileOpptyTeamMembers;
	}

	public static Boolean runEmailNotification(){
		return blnRunEmailNotification;
	}

	public static Boolean runUpdateFileParentAttributes(){
		return blnRunUpdateFileParentAttributes;
	}
}