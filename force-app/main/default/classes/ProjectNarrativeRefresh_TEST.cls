@isTest
private class ProjectNarrativeRefresh_TEST {
	@isTest static void testProjectNarrativeRefresh() {
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

         
        //create funding round group
        Funding_Round_Narrative_Group__c frng = TestDataUtility.createFundingRoundNarrativeGroup(fr.Id);

        //create funding round author
        Funding_Round_Narrative_Author__c frna = TestDataUtility.createFundingRoundNarrativeAuthor(frng.Id);

        //create funding round narrative
        Funding_Round_Narrative__c frn = TestDataUtility.createFundingRoundNarrative(fr.Id,
                                                                                     frna.Id); 
            
        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //create project narrative
        //-did not stage narrative author record being status of staged narrative was not updated from 'In Process' 
        // status change fires parent narrative author update
        Project_Narrative__c pn = TestDataUtility.createProjectNarrative(p.Id, 
        	 															 frn.Id,
                                                                         'Architect',
                                                                         'Architecture and Construction',
                                                                         'Term Letter',
                                                                         'In Process');		
        
        //set page
        PageReference refreshPage = Page.ProjectNarrative_Refresh;
        Test.setCurrentPage(refreshPage); 
        //set url parameters
        ApexPages.currentPage().getParameters().put('pnID',pn.Id);
        ApexPages.currentPage().getParameters().put('opptyID',p.Id);
        ApexPages.currentPage().getParameters().put('narrativeGroup', 'Selection Narratives');
        ApexPages.currentPage().getParameters().put('narrativeAuthor', 'Architect');
        //set controller
        ProjectNarrativeRefresh_Controller ext = new ProjectNarrativeRefresh_Controller();

        //run tests
        Test.startTest();
            PageReference pageRefRefresh = ext.refreshNarrative();
            PageReference pageRefReturn = ext.returnToNarrativeListPage();
        Test.stopTest();
	}
}