/*
  *   {Purpose}   Manage functionality tied to Project Narrative editing                              
  *
  *   CHANGE  HISTORY
  *   =============================================================================
  *   Date        Name                    Description
  *   02/06/18    Kevin Johnson DCS       Created
  *	  08/06/18	  Kevin Johnson DCS		  Updated updateNarrative() method to better trap DML errors
  *   =============================================================================
*/

public with sharing class ProjectNarrativeEdit_Controller {
	public Id opptyID { get; set; }
	public Id pnID { get; set; }
	public String narrativeGroup { get; set; }
	public String narrativeAuthor { get; set; }
	public String narrativeAuthorName { get; set; }
	public String projectName { get; set; }
	public String fundingRoundName { get; set; }
	public String fundingRoundID { get; set; }
	public String projectNarrativeAuthorID { get; set; }
	public String narrativeAuthorStatus { get; set; }
	public Date narrativeAuthorDueDate { get; set; }
	public Date narrativeAuthorFinalDate { get; set; }
	public Id narrativeID { get; set; }
	public List<Project_Narrative__c> lstPN { get; set; }

	//constructor
	public ProjectNarrativeEdit_Controller() {
		opptyID = ApexPages.currentPage().getParameters().get('opptyid');
		narrativeGroup = ApexPages.currentPage().getParameters().get('group');
		narrativeAuthor = ApexPages.currentPage().getParameters().get('author');

		//obtain list of narratives
		lstPN = new List<Project_Narrative__c> ();

		lstPN = [SELECT Id
		         , Name
		         , Status__c
		         , Narrative__c
		         , Help_Text__c
		         , Narrative_Group__c
		         , Author__c
		         , Project__c
		         , Sort_Order__c
		         , Project__r.Name
		         , Project__r.Funding_Round__r.Name
		         , Project__r.Funding_Round__r.Id
		         , Funding_Round_Narrative__c
		         , Project_Narrative_Author__c
		         , Project_Narrative_Author__r.Status__c
		         , Project_Narrative_Author__r.Narrative_Due_Date__c
				 , Project_Narrative_Author__r.Narrative_Final_Date__c
		         FROM Project_Narrative__c
		         WHERE Project__c = :opptyID
		         AND toLabel(Narrative_Group__c) = :narrativeGroup //add toLabel() to ensure we utilize the GUI picklist value
		         AND toLabel(Author__c) = :narrativeAuthor //add toLabel() to ensure we utilize the GUI picklist value
		         ORDER BY Sort_Order__c];

		projectName = lstPN[0].Project__r.Name;
		fundingRoundName = lstPN[0].Project__r.Funding_Round__r.Name;
		fundingRoundID = lstPN[0].Project__r.Funding_Round__r.Id;
		projectNarrativeAuthorID = lstPN[0].Project_Narrative_Author__c;
		narrativeAuthorStatus = lstPN[0].Project_Narrative_Author__r.Status__c;
		narrativeAuthorDueDate = lstPN[0].Project_Narrative_Author__r.Narrative_Due_Date__c;
		narrativeAuthorFinalDate = lstPN[0].Project_Narrative_Author__r.Narrative_Final_Date__c;
		narrativeId = lstPN[0].Id;

		//find the name of the narrative author based on the provided role and project (oppty)
		//Assumption is only one team member will be assigned the provided role so we just return the
		//first one located
		for (OpportunityTeamMember otm :[SELECT User.FirstName
												 , User.LastName
										FROM OpportunityTeamMember
										Where toLabel(TeamMemberRole) = :narrativeAuthor //add toLabel() to ensure we utilize the GUI picklist value
											And OpportunityId = :opptyID
										LIMIT 1]) {
			narrativeAuthorName = otm.User.LastName;
			if (otm.User.FirstName != null) {
				narrativeAuthorName = otm.User.FirstName + ' ' + narrativeAuthorName;
			}
		}
	}

	public PageReference updateNarrative() {
		Boolean isError = false;
		
		for (Project_Narrative__c pn : lstPN) {
			if (pn.Id == pnID) {
				try{
					update pn;
					//re-query data for updated page display
					lstPN = [SELECT Id
						 , Name
						 , Status__c
						 , Narrative__c
						 , Help_Text__c
						 , Narrative_Group__c
						 , Author__c
						 , Project__c
						 , Sort_Order__c
						 , Project__r.Name
						 , Project__r.Funding_Round__r.Name
						 , Project__r.Funding_Round__r.Id
						 , Funding_Round_Narrative__c
						 , Project_Narrative_Author__c
						 , Project_Narrative_Author__r.Status__c
						 , Project_Narrative_Author__r.Narrative_Due_Date__c
						 , Project_Narrative_Author__r.Narrative_Final_Date__c
						 FROM Project_Narrative__c
						 WHERE Project__c = :opptyID
						 AND toLabel(Narrative_Group__c) = :narrativeGroup //add toLabel() to ensure we utilize the GUI picklist value
						 AND toLabel(Author__c) = :narrativeAuthor //add toLabel() to ensure we utilize the GUI picklist value
						 ORDER BY Sort_Order__c];

					projectName = lstPN[0].Project__r.Name;
					fundingRoundName = lstPN[0].Project__r.Funding_Round__r.Name;
					fundingRoundID = lstPN[0].Project__r.Funding_Round__r.Id;
					projectNarrativeAuthorID = lstPN[0].Project_Narrative_Author__c;
					narrativeAuthorStatus = lstPN[0].Project_Narrative_Author__r.Status__c;
					narrativeAuthorDueDate = lstPN[0].Project_Narrative_Author__r.Narrative_Due_Date__c;
					narrativeAuthorFinalDate = lstPN[0].Project_Narrative_Author__r.Narrative_Final_Date__c;
					narrativeId = lstPN[0].Id;
				} catch (DMLException e){
					isError = true;
				}				
				break;
			}
		}

		if(isError == false){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Narrative has been updated.');
			ApexPages.addMessage(myMsg);
		} else if(isError == true){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Updates were not saved.  Ensure you have permission to update the project\'s narratives.');
			ApexPages.addMessage(myMsg);
		}
		
		return null;
	}

	public PageReference refreshNarrative() {
		PageReference pg = new Pagereference('/apex/ProjectNarrative_Refresh?pnid=' + pnID + '&opptyid=' + opptyID + '&group=' + narrativeGroup + '&author=' + narrativeAuthor);
		pg.setRedirect(true);
		return pg;
	}

	public PageReference returnToManagePage() {
		/*
		  //save prior to leaving page
		  for(Project_Narrative__c pn : lstPN){
		  if(pn.Id == pnID){
		  update pn;
		  //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Narrative updated'));
		  break;
		  }
		  }
		 */
		PageReference pg = new Pagereference('/apex/ProjectNarrative_Manage?id=' + opptyID);
		pg.setRedirect(true);
		return pg;
	}

	public PageReference returnToNarrativeListPage() {
		PageReference pg = new Pagereference('/apex/ProjectNarrative_Edit?opptyid=' + opptyID + '&group=' + narrativeGroup + '&author=' + narrativeAuthor);
		pg.setRedirect(true);
		return pg;
	}
}