/*
*   {purpose}  	Manage the functionality tied to Funding Round Phase Checklist Item automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    		Name             		Description
*	08/07/19		Kevin Johnson DCS		Created
*   =============================================================================
*/	

public with sharing class FundingRoundPhaseCLIUtility  {
	public static string processApprovalRoles(List<Phase_Checklist_Item__c> lstFRPCLI){
		/*
		*{purpose}		Cascades updated approver roles to related Projects Project Checklist Item records
		*
		*{function}		Fetches related Project Checklist Item records.  The returned Project Checklist Item records are updated with the  
		*				the related Project Checklist Item Phase records with the
		*				appropriate approver roles.
		*				
		*{trigger}  	FundingRoundPhaseCLIEventListener: after update
		*				
		*/

		Set<Id> setFRPCLIid = new Set<Id>();
		Map<Id, Phase_Checklist_Item__c> mapFRPCLI = new Map<Id, Phase_Checklist_Item__c>();
		List<Project_Checklist_Item__c> lstPCLIUpdate = new List<Project_Checklist_Item__c>();
		String errorMsg = null; 

		for(Phase_Checklist_Item__c oFRPCLI : lstFRPCLI){
			setFRPCLIid.add(oFRPCLI.Id);
			mapFRPCLI.put(oFRPCLI.Id, oFRPCLI);
		}

		for(Project_Checklist_Item__c oPCLI : [SELECT Id
													, Approval_Role_First__c
													, Approval_Role_Second__c
													, Approval_Role_Third__c
													, Funding_Round_Phase_Checklist_Item__c
											  FROM Project_Checklist_Item__c
											  WHERE Funding_Round_Phase_Checklist_Item__c IN :setFRPCLIid]){

			Project_Checklist_Item__c pcliToUpdate = new Project_Checklist_Item__c(Id = oPCLI.Id,
													                               Approval_Role_First__c = mapFRPCLI.get(oPCLI.Funding_Round_Phase_Checklist_Item__c).Approval_First__c,                           
													                               Approval_Role_Second__c = mapFRPCLI.get(oPCLI.Funding_Round_Phase_Checklist_Item__c).Approval_Second__c,
													                               Approval_Role_Third__c = mapFRPCLI.get(oPCLI.Funding_Round_Phase_Checklist_Item__c).Approval_Third__c);
			
			lstPCLIUpdate.add(pcliToUpdate);		
		}

		if(!lstPCLIUpdate.isEmpty()){
			Database.SaveResult[] updResult = Database.update(lstPCLIUpdate, false);
			
			for(Database.SaveResult sr : updResult){
				if(!sr.isSuccess()){
					for(Database.Error err : sr.getErrors()){
						errorMsg = 'Error updating approver roles: ' + err.getStatusCode() + ' - ' + err.getMessage();
					}
				}
			}
		}
		return errorMsg;
	}
}