@isTest
private class ProjectNarrativeUtility_TEST {
	@isTest static void testProjectNarrativeUtility() {
		//stage custom settings
		List<Project_Narrative_Status_Logic__mdt> csAuthorStatus = [SELECT Status__c
																		 , Status_Condition__c
						  		       	   							FROM Project_Narrative_Status_Logic__mdt
						  		       	   							WHERE Applies_To_Narrative_Author__c = TRUE];

		List<Project_Narrative_Status_Logic__mdt> csGroupStatus = [SELECT Status__c
																		, Status_Condition__c
						  		       	   						   FROM Project_Narrative_Status_Logic__mdt
						  		       	   						   WHERE Applies_To_Narrative_Group__c = TRUE];

		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create funding round group
        Funding_Round_Narrative_Group__c frng = TestDataUtility.createFundingRoundNarrativeGroup(fr.Id);

        //create funding round author
        Funding_Round_Narrative_Author__c frna = TestDataUtility.createFundingRoundNarrativeAuthor(frng.Id);

        //create funding round narrative
        Funding_Round_Narrative__c frn = TestDataUtility.createFundingRoundNarrative(fr.Id,
                                                                                     frna.Id); 

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //create project narrative group
        Project_Narrative_Group__c png = TestDataUtility.createProjectNarrativeGroup(p.Id, 
        	                                                                         'Selection Narratives', 
        	                                                                         'In Process', 
        	                                                                         frng.Id);

        Project_Narrative_Author__c pna = TestDataUtility.createProjectNarrativeAuthor(png.Id, 
        	                                                                           'Architect', 
        	                                                                           'In Process', 
        	                                                                           frna.Id);

        Project_Narrative__c pn = TestDataUtility.createProjectNarrative(p.Id, 
        	                                                             frn.Id, 
        	                                                             'Architect', 
        	                                                             'Architecture and Construction', 
        	                                                             'Selection Narratives', 
        	                                                             'In Process');
			//todo: add parameter to account for pna
			pn.Project_Narrative_Author__c = pna.Id;
			update pn;

		//run tests
        Test.startTest();
        	pn.Status__c = 'Changes Required';
        	update pn;
            pn.Status__c = 'NA';
            update pn;
            pn.Status__c = 'In Process';
            update pn;
        Test.stopTest();
	}
}