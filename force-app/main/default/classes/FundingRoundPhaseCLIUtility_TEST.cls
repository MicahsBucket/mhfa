@isTest
public class FundingRoundPhaseCLIUtility_TEST  {
	@isTest static void testFundingRoundPhaseCLIUtility(){
	//SUPPORTING DATA=======================================================================================================
    //custom metadata type
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       		     FROM Enable_Triggers__mdt
					  		       		     WHERE MasterLabel = 'FundingRoundPhaseChecklistItem'];
    
	//FUNIDNG ROUND FRAMEWORK===============================================================================================
	Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

	//PROJECT FRAMEWORK=====================================================================================================
	//create project
    Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

	//fetch required data
	Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																  FROM Funding_Round_Checklist_Item__c
																  WHERE Funding_Round__c = :oFundingRound.Id
																  And Name = 'Pre Selection Checklist Item'];
	
	Phase_Checklist_Item__c oFundingRoundPhaseChecklistItem = [SELECT Id
															   FROM Phase_Checklist_Item__c
															   WHERE Funding_Round_Checklist_Item__c = :oFundingRoundChecklistItem.Id];

	Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
														  FROM Project_Checklist__c
														  WHERE Name = 'Application'
														  AND Project__c = :oProject.Id];

	Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
														   , Name
													  FROM Project_Sub_Phase__c
													  WHERE Name = 'Application Submittals'
													  AND Project_Phase__r.Name = 'Application'
													  AND Project__c = :oProject.Id];
	
	//create pcli (existing)===========================================
    Project_Checklist_Item__c pcli = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                oFundingRoundChecklistItem.Id,
                                                                                false,
                                                                                null,
                                                                                null,
																				oProjectStagePreSelection.Id,
																				oProjectChecklistPreSelection.Id);
		pcli.Funding_Round_Phase_Checklist_Item__c = oFundingRoundPhaseChecklistItem.Id;
		update pcli;

	Test.startTest();
		oFundingRoundPhaseChecklistItem.Approval_First__c = 'Architect';
		update oFundingRoundPhaseChecklistItem;
		oFundingRoundPhaseChecklistItem.Approval_First__c = null;
		update oFundingRoundPhaseChecklistItem;
		oFundingRoundPhaseChecklistItem.Approval_Second__c = 'Closer';
		update oFundingRoundPhaseChecklistItem;
		oFundingRoundPhaseChecklistItem.Approval_Second__c = null;
		update oFundingRoundPhaseChecklistItem;
		oFundingRoundPhaseChecklistItem.Approval_Third__c = 'HDO';
		update oFundingRoundPhaseChecklistItem;
		oFundingRoundPhaseChecklistItem.Approval_Third__c = null;
		update oFundingRoundPhaseChecklistItem;
	Test.stopTest();
	}
}