/*
*   {purpose}  	Manage the functionality tied to ContentVersion (Files) automation
*
*   {contact}   ...
*               
*                                
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   12/20/16	Kevin Johnson DCS		Created
*   =============================================================================
*/

public with sharing class ContentVersionUtility {
	/*
	*	ContentVersion triggers fire once as one version of a record is inserted whereas triggers on 
	*   ContentDocumentLink fire multiple times being multiple records reflecting 'n' number of 
	*   users shared with File are inserted to reflect specific users ContentDocumentLink share properties
	*/
	
	public static String validateFileParameters(List<ContentVersion> lstCV){
		/*
		*{purpose}		Manages validation of Content Files being updated at the ContentVersion object
		*
		*{function}		Builds list of related Project Checklist Items (PCLI) with related Content File data so to use in validating attributes of uploaded File.  
		*				If a File is being uploaded (updating existing) that does not meet PCLI paramaters an error is passed notifying user of flagged validation.
		*				
		*{trigger}  	ContentVersionEventListener: after insert
		*/

		Set<Id> setCDids = new Set<Id>();
		Map<String, String> mapCV = new Map<String, String>();
		
		for (ContentVersion cv : lstCV) {
			setCDids.add(cv.ContentDocumentId);
			//evaluate file extension at this point to ensure accurate results
			String strExtension = cv.PathOnClient;
			strExtension = strExtension.substringAfterLast('.');
			mapCV.put(cv.ContentDocumentId, strExtension);
		}
		
		String errorMsg = ContentFileHelper.validateContentFile(setCDids, mapCV);
	
		return errorMsg;
	}
}