@isTest 
private class ProjectTaskUtility_TEST {

	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());
		oFundingRound.Carryover_Due_Date__c = Date.today().addDays(10);
		update oFundingRound;

        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		System.debug('After buildProjectDataStructure.  Queries: ' + Limits.getQueries());

		Funding_Round_Task__c oFundingRoundTask1 = TestDataUtility.createFundingRoundTaskPreSelection(oFundingRound.Id, null);
		Funding_Round_Task__c oFundingRoundTask2 = TestDataUtility.createFundingRoundTaskPreSelection(oFundingRound.Id, null);
		System.debug('After createFundingRoundTask.  Queries: ' + Limits.getQueries());
	}

	@isTest
	private static void test_AssignDueDates() {
		Funding_Round__c oFundingRound = [Select Id
												, Name
												, (Select Id, Name, Due_Date_Offset__c, Due_Date_Baseline__c, Funding_Round_Due_Date__c
													 From Funding_Round_Tasks__r)
											From Funding_Round__c LIMIT 1];

		Opportunity oProject = [Select Id
									, Name
									, Funding_Round__c 
									, (Select Id, Name From Project_Phases__r)
									, (Select Id, Name, Project_Phase__c From Project_Sub_Phases__r)
								From Opportunity LIMIT 1];


		Funding_Round_Task__c oFRTask_Stage = oFundingRound.Funding_Round_Tasks__r[0];
		oFRTask_Stage.Due_Date_Baseline__c = 'Funding Round Due Date';
		oFRTask_Stage.Due_Date_Offset__c = 1;
		oFRTask_Stage.Funding_Round_Due_Date__c = 'Carryover Due Date';
		oFRTask_Stage.Expected_Duration_Days__c = 30;
		update oFRTask_Stage;
		Project_Task__c oProjTask_Stage = TestDataUtility.createProjectStageTask(oProject.Project_Sub_Phases__r[0].Id
																				, oProject.Id
																				, 'Stage Task'
																				, oProject.Project_Sub_Phases__r[0].Name
																				, oFRTask_Stage.Id);
		oProjTask_Stage.Expected_Duration_Days__c = 30;
		update oProjTask_Stage;

		Test.startTest();

		Funding_Round_Task__c oFRTask_Child = oFundingRound.Funding_Round_Tasks__r[1];
		oFRTask_Child.Due_Date_Baseline__c = 'Funding Round Due Date';
		oFRTask_Child.Preceding_Task__c = oFRTask_Stage.Id;
		oFRTask_Child.Expected_Duration_Days__c = 30;
		update oFRTask_Child;
		Project_Task__c oProjTask_Child = TestDataUtility.createProjectWorkflowTask(oProjTask_Stage.Id
																					, 'Child Task'
																					, oProject.Id
																					, oFRTask_Child.Id);
		oProjTask_Child.Expected_Duration_Days__c = 30;
		update oProjTask_Child;

		oProjTask_Stage.Task_Status__c = 'Complete';
		oProjTask_Stage.Closed_Notes__c = 'Complete Notes';
		update oProjTask_Stage;

		oFRTask_Stage.Due_Date_Baseline__c = 'Project Stage Due Date';
		oFRTask_Stage.Funding_Round_Due_Date__c = null;
		update oFRTask_Stage;

		oProjTask_Stage.Task_Status__c = 'In Process';
		oProjTask_Stage.Closed_Notes__c = null;
		update oProjTask_Stage;

		oProjTask_Stage.Task_Status__c = 'Complete';
		oProjTask_Stage.Closed_Notes__c = 'Complete Notes';
		update oProjTask_Stage;
        
        oProjTask_Stage.Funding_Round_Task__c = null;
        oProjTask_Stage.Task_Owner__c = UserInfo.getUserId();
		update oProjTask_Stage;       

		Test.stopTest();
	}

	@isTest
	private static void test_ChecklistTasks() {
		Funding_Round__c oFundingRound = [Select Id
												, Name
												, (Select Id, Name, Due_Date_Offset__c, Due_Date_Baseline__c, Funding_Round_Due_Date__c
													 From Funding_Round_Tasks__r)
											From Funding_Round__c LIMIT 1];

		Opportunity oProject = [Select Id
									, Name
									, Funding_Round__c 
									, (Select Id, Name From Project_Phases__r)
									, (Select Id, Name, Project_Phase__c From Project_Sub_Phases__r)
								From Opportunity LIMIT 1];

		//get Application checklist - automatically created when project is created
		Project_Checklist__c oProjChecklist = [Select Id
													, Name
													, Checklist_Status__c
												From Project_Checklist__c 
												Where Project__c = :oProject.Id 
													And Name = 'Application' LIMIT 1];

		Funding_Round_Task__c oFRTask_Checklist = oFundingRound.Funding_Round_Tasks__r[0];
		oFRTask_Checklist.Due_Date_Baseline__c = 'Checklist Submission Status';
		oFRTask_Checklist.Expected_Duration_Days__c = 30;
		update oFRTask_Checklist;
		Project_Task__c oProjTask_Checklist = TestDataUtility.createChecklistTask(oProjChecklist.Id
																				, 'Checklist Task'
																				, oProject.Id
																				, oFRTask_Checklist.Id);
		oProjTask_Checklist.Expected_Duration_Days__c = 30;
		oProjTask_Checklist.Task_Status__c = 'Not Ready';
		update oProjTask_Checklist;

		Test.startTest();

		oProjChecklist.Checklist_Status__c = 'Submitted';
		oProjChecklist.Submitted_By__c = UserInfo.getUserId();
		update oProjChecklist;

		Test.stopTest();
	}
}