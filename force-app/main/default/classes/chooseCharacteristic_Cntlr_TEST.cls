@isTest 
private class chooseCharacteristic_Cntlr_TEST {

	@isTest
	private static void test_chooseCharacteristic() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		Funding_Round_Checklist_Item_Trigger__c oCLIChar = [Select Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
															From Funding_Round_Checklist_Item_Trigger__c
															LIMIT 1];

		Test.startTest();

		chooseCharacteristic_Cntlr.getGroupings(oFundingRound.Id, oCLIChar.Funding_Round_Checklist_Item__c);
		List<chooseCharacteristic_Cntlr.checklistOption> lstOptions = chooseCharacteristic_Cntlr.getChecklistOptions(oFundingRound.Id
																													, oCLIChar.Funding_Round_Checklist_Item__c);
		
		List<String> lstCharIds = new List<String>();
		lstCharIds.add(oCLIChar.Funding_Round_Trigger__c);
		chooseCharacteristic_Cntlr.insertCliCharacteristics(oCLIChar.Funding_Round_Checklist_Item__c
															, lstCharIds
															, true);
		chooseCharacteristic_Cntlr.insertCliCharacteristics(oCLIChar.Funding_Round_Checklist_Item__c
															, lstCharIds
															, false);
		chooseCharacteristic_Cntlr.insertCliChecklists(oCLIChar.Funding_Round_Checklist_Item__c
														, lstOptions);
		Test.stopTest();


	}
}