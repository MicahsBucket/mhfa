@isTest
private class ProjectWorkbookVersionUtility_TEST {
    @isTest static void testProjectWorkbookVersionUtility() {
        //stage custom settings
        List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
                                                 FROM Enable_Triggers__mdt
                                                 WHERE MasterLabel = 'ProjectWorkbookVersion'];

        List<File_Sharing_Settings_Objects__mdt> csSharingRules = [SELECT MasterLabel
                                                                        , Object_Prefix__c
                                                                        , Fields_To_Query__c
                                                                        , Prohibit_File_Upload__c
                                                                        , Update_Parent_File_Count__c
                                                                        , Update_Parent_File_Version_Count__c
                                                                        , Update_Parent_Upload_Status__c
                                                                   FROM File_Sharing_Settings_Objects__mdt];

        //stage data
        //create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

		//fetch group to share
        List<Group> lstGroup = new List<Group>([SELECT Id
                                              FROM Group
                                              WHERE Type = 'Role'
                                              LIMIT 1]);

        //create funding round share
        Funding_Round__Share frShare = new Funding_Round__Share();
          frShare.ParentId = fr.Id;
          frShare.AccessLevel = 'Read';
          frShare.UserOrGroupId = lstGroup[0].Id;
          frShare.RowCause = 'Manual';

		insert frShare;

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);
        //01237000000TgAiAAK = project pre-selection recordtype id - id propagated through stack
        //01237000000TgAjAAK = project post selection recordtype id - id propagated through stack
        p.recordtypeid = '01237000000TgAiAAK';

        update p;

        Project_Workbook__c pwb = TestDataUtility.createProjectWorkbook(p.Id);

        User otmUser = TestDataUtility.createUser('Multifamily Standard User',
                                                  'testOTM@mhfatest.com');

        OpportunityTeamMember otm = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                                'Closer', 
                                                                                otmUser.Id);      

         //run tests
        Test.startTest();
            //insert
            Project_Workbook_Version__c pwbv1 = TestDataUtility.createProjectWorkbookVersion(pwb.Id,
                                                                                            'Selection Meeting');

            Project_Workbook_Version__c pwbv2 = TestDataUtility.createProjectWorkbookVersion(pwb.Id,
                                                                                            'Revised Board');
                        
            Project_Workbook_Version__c pwbv3 = new Project_Workbook_Version__c(Project_Workbook__c = pwb.Id,
                                                                                Workbook_Version__c = 'Closed', 
                                                                                Make_Public__c = TRUE);
            //test for pwbv made public on create
            insert pwbv3;
                        
            //update: make public - pre-selection
            pwbv1.Make_Public__c = TRUE;
            pwbv1.Number_of_Uploaded_Files__c = 1;
            update pwbv1;
            System.assertEquals(pwbv1.Make_Public__c, TRUE);

            //update: make private - pre-selection            
            pwbv1.Make_Public__c = FALSE;
            update pwbv1;
            System.assertEquals(pwbv1.Make_Public__c, FALSE);

            //set project to post selection recordtype
            p.recordtypeid = '01237000000TgAjAAK';

            update p;

            //update: make public - post selection
            pwbv1.Make_Public__c = TRUE;
            update pwbv1;
            System.assertEquals(pwbv1.Make_Public__c, TRUE);

            //update: make private - post selection
            pwbv1.Make_Public__c = FALSE;
            update pwbv1;
            System.assertEquals(pwbv1.Make_Public__c, FALSE);

            //test duplicate pwbv
            try{
                Project_Workbook_Version__c pwbv4 = TestDataUtility.createProjectWorkbookVersion(pwb.Id,
                                                                                                 'Selection Meeting');
            } catch (Exception e){  //'Version Selection Meeting already exists for this Project Workbook and thus cannot be added.'
                Boolean validationErrorRecord =  TRUE;
                System.assertEquals(validationErrorRecord, true, e.getMessage());
            } 

            delete pwbv1;
        Test.stopTest();
    }
}