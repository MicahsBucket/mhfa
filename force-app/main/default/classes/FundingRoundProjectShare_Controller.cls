/*
*   {Purpose}    Manage Funding Round sharing with related Project(s) functionality                         
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   05/24/17    Kevin Johnson DCS       Created
*   07/07/17    Kevin Johnson DCS       Added Project Documentation sharing/sharing deletion functionality
*   11/02/17    Kevin Johnson DCS       Added Project Workbook Version sharing deletion functionality
*   05/17/18    Kevin Johnson DCS       Removed Project Documentation sharing/sharing deletion functionality
*   06/17/19    Kevin Johnson DCS       -Added filter to sharing functionality filtering out test projects
*                                       -Added flag to Funding Round indicating if Funding Round Project's are shared
*   =============================================================================
*/

public with sharing class FundingRoundProjectShare_Controller {
    /*
    *{purpose}      Manages adding/removing share of users manually shared to funding round to related funding round projects.
    *               Supports FACP visibility process during pre-selection phase.
    *
    *{function}     Builds 1. list of users shared with funding round 2. list of projects related to funding round
    *               -if adding share: builds opportunityshare record per funding round/project/user 
    *                 and inserts said record thereby sharing project with user
    *                   -duplicate shares are not added
    *               -if removing share: builds list of opportunityshare and project_workbook_version__share records 
    *                 per funding round / project and removes said records thereby removing project and
    *                 project workbook version visibility from user(s)
    *               
    *{assumptions}  -users to share are of Group object prefix = '00G'
    *               -process add/removes:
    *                   -all MANUALLY shared users at the funding round
    *                   -share with userorgroup id that starts with '00G'
    *           
    *{trigger}      -'Share Related Projects' button on FundingRound_ProjectShareAdd page
    *               -'Unshare Related Projects' button on FundingRound_ProjectShareRemove page
    */

    private ApexPages.StandardController controller {get;set;}
    private final Funding_Round__c oFundingRound;
    public String fundingRoundID;
    private List<Funding_Round__c> lstFundingRoundToUpdate = new List<Funding_Round__c>();

    //constructor
    public FundingRoundProjectShare_Controller(ApexPages.StandardController controller){
        this.controller = controller;
        oFundingRound = (Funding_Round__c)controller.getRecord();
        fundingRoundID = oFundingRound.Id;
    }

    public void addProjectShare(){
        //adds funding rounds manual shares to funding round's related projects
        List<Funding_Round__Share> lstFRShare = new List<Funding_Round__Share>();
        List<ID> lstProjId = new List<ID>();
        //build list of related funding_round__share records
        for(Funding_Round__Share frs : [SELECT AccessLevel
                                             , UserOrGroupId
                                             , RowCause
                                        FROM Funding_Round__Share
                                        WHERE ParentId = :fundingRoundID]){
            String strUserOrGroupId = frs.UserOrGroupId;
            if(strUserOrGroupId.startswith('00G') && frs.RowCause == 'Manual'){  //add manually shared entities with 00G prefix DO NOT add custom object sharing rule
                lstFRShare.add(frs);
            }           
        }
        //build list of related project records
        system.debug('lstFRShare: ' + lstFRShare);
        for(Opportunity proj : [SELECT Id
                                FROM Opportunity
                                WHERE Funding_Round__c = :fundingRoundID
                                AND Test_Project__c = FALSE]){
            lstProjId.add(proj.Id);
        }
        system.debug('lstProjId: ' + lstProjId);
        
        //build list of opportunity share records to insert
        List<OpportunityShare> lstOpptyShareInsert = new List<OpportunityShare>();

        for(ID proj : lstProjId){
            for(Funding_Round__Share frs : lstFRShare){
                    OpportunityShare newProjShare = new OpportunityShare();
                    newProjShare.OpportunityId = proj;
                    newProjShare.OpportunityAccessLevel = 'Read';
                    newProjShare.UserOrGroupId = frs.UserOrGroupId;

                    lstOpptyShareInsert.add(newProjShare);
            }
        }
        system.debug('lstOpptyShareInsert: ' + lstOpptyShareInsert);

        String[] strErrMsgOpptyShare = null;

        //insert opportunity share records
        if(!lstOpptyShareInsert.isEmpty()){
            Database.SaveResult[] insOpptyShareResult = Database.insert(lstOpptyShareInsert, false);        
            //capture records that errored during process
            for(Database.SaveResult srOpptyShare : insOpptyShareResult){
                if(!srOpptyShare.isSuccess()){
                    for(Database.Error err : srOpptyShare.getErrors()){
                        strErrMsgOpptyShare.add('Error sharing Project: ' + err.getStatusCode() + ' - ' + err.getMessage() + '\r');
                    }
                } 
            }   
        }
        //pass result to page
        if(strErrMsgOpptyShare == null){
            oFundingRound.Are_Projects_Being_Shared__c = 'Yes';
            lstFundingRoundToUpdate.add(oFundingRound);
            update lstFundingRoundToUpdate;
            
            ApexPages.Message eMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Projects have been shared.');
            ApexPages.addMessage(eMsg);
        } else if(strErrMsgOpptyShare != null){
            ApexPages.Message eMsg = new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(strErrMsgOpptyShare));
            ApexPages.addMessage(eMsg);     
        }   
    }

    public void removeProjectShare(){
        //removes funding rounds manual shares from funding round's related projects and projects designated related records
        //-removes groups from object share NOT individual users.  groups will be shared pre-selection where users will be shared post selection
        List<ID> lstProjId = new List<ID>();
        //build list of related projects
        for(Opportunity proj : [SELECT Id
                                FROM Opportunity
                                WHERE Funding_Round__c = :fundingRoundID]){
            lstProjId.add(proj.Id);
        }
        system.debug('lstProjId: ' + lstProjId);

        List<ID> lstPWBVId = new List<ID>();
        //build list of related pwbv records
        for(Project_Workbook_Version__c pwbv : [SELECT Id
                                                FROM Project_Workbook_Version__c
                                                WHERE Opportunity_ID__c IN :lstProjId
                                                AND Make_Public__c = TRUE]){
            lstPWBVId.add(pwbv.Id);
        }
        system.debug('lstPWBVId: ' + lstPWBVId);

        //build list of opportunity share records to delete
        List<OpportunityShare> lstOpptyShareToDelete = new List<OpportunityShare>();
        for(OpportunityShare os : [SELECT Id
                                        , UserOrGroupId
                                        , RowCause
                                   FROM OpportunityShare
                                   WHERE OpportunityId IN :lstProjId]){
            String strUserOrGroupId = os.UserOrGroupId;
            if(strUserOrGroupId.startswith('00G') && os.RowCause == 'Manual'){  //remove manually shared entities with 00G prefix
                lstOpptyShareToDelete.add(os);
            }   
        }
        system.debug('lstOpptyShareToDelete: ' + lstOpptyShareToDelete);

        //build list of pwbv records to delete
        List<Project_Workbook_Version__Share> lstPWBVShareToDelete = new List<Project_Workbook_Version__Share>();
        for(Project_Workbook_Version__Share pwbvs : [SELECT Id
                                                          , UserOrGroupId
                                                          , RowCause
                                                    FROM Project_Workbook_Version__Share
                                                    WHERE ParentId IN :lstPWBVId]){
            String strUserOrGroupId = pwbvs.UserOrGroupId;
            if(strUserOrGroupId.startswith('00G') && pwbvs.RowCause == 'Public_Share__c'){  //remove manually shared entities with 00G prefix
                lstPWBVShareToDelete.add(pwbvs);
            }   
        }
        system.debug('lstPWBVShareToDelete: ' + lstPWBVShareToDelete);

        String[] strErrMsgOpptyShare = null;
        String[] strErrMsgPWBVShare = null;

        //delete opportunity share records
        if(!lstOpptyShareToDelete.isEmpty()){
            Database.DeleteResult[] delOpptyShareResult = Database.delete(lstOpptyShareToDelete, false);
            //capture records that errored during process
            for(Database.DeleteResult drOpptyShare : delOpptyShareResult){
                if(!drOpptyShare.isSuccess()){
                    for(Database.Error err : drOpptyShare.getErrors()){
                        strErrMsgOpptyShare.add('Error removing Project share: ' + err.getStatusCode() + ' - ' + err.getMessage() + '\r');
                    }
                } 
            }
        }

        //delete pwbv share records
        if(!lstPWBVShareToDelete.isEmpty()){
            Database.DeleteResult[] delPWBVShareResult = Database.delete(lstPWBVShareToDelete, false);
            //capture records that errored during process
            for(Database.DeleteResult drPWBVShare : delPWBVShareResult){
                if(!drPWBVShare.isSuccess()){
                    for(Database.Error err : drPWBVShare.getErrors()){
                        strErrMsgPWBVShare.add('Error removing Project Workbook Type share: ' + err.getStatusCode() + ' - ' + err.getMessage() + '\r');
                    }
                } 
            }
        }
        //process result and pass appropriate message
        if(strErrMsgOpptyShare == null && strErrMsgPWBVShare == null){
            oFundingRound.Are_Projects_Being_Shared__c = 'No';
            lstFundingRoundToUpdate.add(oFundingRound);
            update lstFundingRoundToUpdate;
            
            ApexPages.Message eMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Project / Project Workbook Type shares have been removed.');
            ApexPages.addMessage(eMsg);
        } else if(strErrMsgOpptyShare != null || strErrMsgPWBVShare != null){
            ApexPages.Message eMsg = new ApexPages.Message(ApexPages.Severity.ERROR, strErrMsgOpptyShare + '\r' + strErrMsgPWBVShare);
            ApexPages.addMessage(eMsg); 
        }
    }

    public PageReference returnToFundingRound(){
        //direct back to funding round
        Pagereference pageRef = new Pagereference('/'+fundingRoundID);
        pageRef.setRedirect(true);
        return pageRef;
    }
}