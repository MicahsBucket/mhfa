public without sharing class Project_Cntlr  {
   	/*  Look at FundingRound_Cntlr for more code ideas for displaying items in a tree format */

	public class phaseTree {
        //properties
        @AuraEnabled
        public Project_Phase__c oPhase {get;set;}
        @AuraEnabled
        public List<Project_Sub_Phase__c> lstStagesAndTasks {get;set;}
        @AuraEnabled
        public List<Project_Cntlr.stageDetails> lstStageDetailsAndTasks {get;set;}
        @AuraEnabled
        public List<Phase_Checklist__c> lstChecklistsAndItems {get;set;}

        //constructor
        public phaseTree() {
            this.oPhase = new Project_Phase__c();
            this.lstStagesAndTasks = new List<Project_Sub_Phase__c>();
            this.lstChecklistsAndItems = new List<Phase_Checklist__c>();
			this.lstStageDetailsAndTasks = new List<Project_Cntlr.stageDetails>();
        }
    }

	public class stageDetails {
		//properties
		@AuraEnabled
		public Project_Sub_Phase__c oStage {get;set;}
		@AuraEnabled
		public Date dueDate {get;set;}

		//constructor
		public stageDetails() {
			this.oStage = new Project_Sub_Phase__c();
		}
	}
    
    public class groupTree {
        //properties
        @AuraEnabled
        public Trigger_Group__c oGroup {get;set;}
        @AuraEnabled
        public List<Funding_Round_Trigger__c> lstCharacteristics {get;set;}
        
        //constructor
        public groupTree() {
            this.oGroup = new Trigger_Group__c();
            this.lstCharacteristics = new List<Funding_Round_Trigger__c>();
        }
    }

    @AuraEnabled
    public static List<Project_Phase__c> retrievePhasesAndStages(Id projectId) {
        System.debug('Inside retrievePhasesAndStages.  projectId: ' + projectId);
        List<Project_Phase__c> lstPhases = new List<Project_Phase__c>();
        for (Project_Phase__c oPhase: [Select Id
                                        , Name
                                        , Due_Date_Baseline__c
										, Project_Phase_Due_Date__c
                                        , Sort_Order__c
										, In_Process_Date__c
                                        , Project__r.CreatedDate
                                        , Is_Stage_Duration_Editable__c
                                        , (Select Id
                                                    , Name
													, Estimated_Days_to_Complete__c
                                                    //, Due_Date_Offset__c
													, Due_Date__c
													//, Base_Due_Date__c
													, Stage_Begin__c
													, Stage_Complete__c
													, Stage_Duration_Days__c
													, In_Process_Date__c
													, Sort_Order__c
                                            From Project_Sub_Phases__r
                                            Order By Sort_Order__c)
                                From Project_Phase__c
                                Where Project__c = :projectId
                                Order By Sort_Order__c]) {
            lstPhases.add(oPhase);
            System.debug('lstPhases:\n' + oPhase);
        }
        return lstPhases;
    }

    @AuraEnabled
    public static List<Project_Cntlr.phaseTree> retrievePhaseStageTree(Id projectId, String taskType, String taskOwner) {
        System.debug('Inside retrievePhaseStageTree.  projectId: ' + projectId);
        //retrieve the phases associated to the given project
        Map<Id, Project_Phase__c> mapPhases = new Map<Id, Project_Phase__c>([Select Id
                                                                    , Name
                                                                    , Is_Stage_Duration_Editable__c
                                                                    , Due_Date_Baseline__c
																	//, Base_Due_Date__c
																	, In_Process_Date__c
																	, Funding_Round_Phase_Due_Date__c
																	, Funding_Round_Phase__r.Start_Date__c
																	, Project_Phase_Due_Date__c
                                                                    , Sort_Order__c
																	, Phase_Status__c
																	, Project__r.CreatedDate
																	, LastModifiedDate
																	, LastModifiedBy.FirstName
																	, LastModifiedBy.LastName
                                                                From Project_Phase__c
                                                                Where Project__c = :projectId
                                                                Order By Sort_Order__c]);

        //retrieve Stages and Tasks associated to the phases
		if (taskType == null || taskType == '' || taskType == 'All Tasks') {
			taskType = '%';
		}
		String strUserId = '%';
		if (taskOwner == 'Current User') {
			strUserId = UserInfo.getUserId();
		}
        System.debug('Inside retrievePhaseStageTree.  taskOwner: ' + strUserId);
        Map<Id, Project_Cntlr.phaseTree> mapPhaseTree = new Map<Id, Project_Cntlr.phaseTree>();
		String soqlStmtFields = 'Select Id'
							+ ', Name'
							+ ', Due_Date__c'
							+ ', Estimated_Days_to_Complete__c'
							+ ', Sort_Order__c'
							+ ', Stage_Status__c'
							+ ', Project_Phase__c'
							+ ', In_Process_Date__c'
							+ ', LastModifiedDate'
							+ ', LastModifiedBy.FirstName'
							+ ', LastModifiedBy.LastName'
							+ ', Project_Phase__r.In_Process_Date__c'
							+ ', Project_Phase__r.Sort_Order__c'
							+ ', Project_Phase__r.Project_Phase_Due_Date__c'
							+ ', Project_Phase__r.Phase_Status__c'
							+ ', Project_Phase__r.LastModifiedDate'
							+ ', Project_Phase__r.LastModifiedBy.FirstName'
							+ ', Project_Phase__r.LastModifiedBy.LastName';

		String soqlStmtSubQuery = '(Select Id'
									+ ', Name'
									+ ', OwnerId'
									+ ', Owner.Username'
									+ ', Owner.FirstName'
									+ ', Owner.LastName'
									+ ', Due_Date__c'
									+ ', Task_Status__c'
									+ ', Expected_Duration_Days__c'
									+ ', In_Process_Date__c'
									+ ', LastModifiedDate'
									+ ', LastModifiedBy.FirstName'
									+ ', LastModifiedBy.LastName'
									+ ', Funding_Round_Task__r.Task_Type__c'
									+ ', Funding_Round_Task__r.Assignee_Type__c'
									+ ', Funding_Round_Task__r.Assignee_Role_Project_Team__c'
									+ ', Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c'
									+ ', Funding_Round_Task__r.Expected_Duration_Days__c'
								+ ' From Project_Stage_Tasks__r'
								+ ' Where Id != null';

		if (taskType != '%') {
			soqlStmtSubQuery += ' And Funding_Round_Task__r.Task_Type__c = :taskType';
		}
		if (taskOwner == 'Current User') {
			soqlStmtSubQuery += ' And OwnerId = \'' + strUserId + '\'';
		}
		soqlStmtSubQuery += ' Order By Due_Date__c, Id)';
		Set<Id> setPhaseIds = mapPhases.keySet();
		String soqlStmtWhereClause = ' From Project_Sub_Phase__c'
									+ ' Where Project_Phase__c in :setPhaseIds'
									+ ' Order By Project_Phase__r.Sort_Order__c, Sort_Order__c';

		String soqlStmt = soqlStmtFields 
							+ ', ' + soqlStmtSubQuery
							+ soqlStmtWhereClause;
		System.debug('soqlStmt: ' + soqlStmt);

		for (SObject sobj : Database.query(soqlStmt)) {
			Project_Sub_Phase__c oStage = (Project_Sub_Phase__c)sobj;
			System.debug('oStage.Project_Stage_Tasks__r: ' + oStage.Project_Stage_Tasks__r + ' (' + oStage.Project_Stage_Tasks__r.size() + ')');
			if (taskOwner == 'Current User' && (oStage.Project_Stage_Tasks__r == null || oStage.Project_Stage_Tasks__r.size() == 0)) {
				//if limiting tasks to the current owner, do not show phases or stages that do not have tasks under them
				System.debug('Skip to next loop item');
				continue;
			}
            Project_Cntlr.phaseTree oPhaseTree = new Project_Cntlr.phaseTree();
            if (mapPhaseTree.containsKey(oStage.Project_Phase__c)) {
                oPhaseTree = mapPhaseTree.get(oStage.Project_Phase__c);
            } else {
                oPhaseTree.oPhase = mapPhases.get(oStage.Project_Phase__c);
            }
            oPhaseTree.lstStagesAndTasks.add(oStage);

			Date stageDueDate = oStage.Due_Date__c;
			Project_Cntlr.stageDetails oHoldStage = new Project_Cntlr.stageDetails();
			oHoldStage.oStage = oStage;
			oHoldStage.dueDate = stageDueDate;
			System.debug('Stage: ' + oStage.Name 
							+ '; DueDate:  ' + stageDueDate 
							+ '; oHoldStage.dueDate: ' + oHoldStage.dueDate
							+ '; Days to Complete: ' + oStage.Estimated_Days_to_Complete__c 
							+ '; Phase Due Date: ' + oStage.Project_Phase__r.Project_Phase_Due_Date__c);
			oPhaseTree.lstStageDetailsAndTasks.add(oHoldStage);
            mapPhaseTree.put(oStage.Project_Phase__c, oPhaseTree);
        }
        return mapPhaseTree.values();
    }


    @AuraEnabled
    public static List<Project_Task__c> retrieveProjectTasks(Id projectId, String taskOwner) {
		Set<Id> setProjectId = new Set<Id>();
		setProjectId.add(projectId);
		String soqlStmt = 'Select Id'
									+ ', Name'
									+ ', OwnerId'
									+ ', Owner.Username'
									+ ', Owner.FirstName'
									+ ', Owner.LastName'
									+ ', Owner_Role__c'
									+ ', Phase__c'
									+ ', Phase_Stage__c'
									+ ', Task_Status__c'
									+ ', Due_Date__c'
									+ ', Project_Checklist__c'
									+ ', Project_Checklist__r.Name'
									+ ', toLabel(Funding_Round_Task__r.Assignee_Role_Project_Team__c)'
									+ ', toLabel(Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c)'
									+ ', Related_To_Text__c'
									+ ', LastModifiedDate'
									+ ', LastModifiedBy.FirstName'
									+ ', LastModifiedBy.LastName'
								+ ' From Project_Task__c'
								+ ' Where Project__c IN :setProjectId';
		if (taskOwner == 'Current User') {
			soqlStmt += ' And OwnerId = \'' + UserInfo.getUserId() + '\'';
		}
		soqlStmt += ' Order By Due_Date__c, Id';

		List<Project_Task__c> lstTasks = new List<Project_Task__c>();
		for (SObject sobj : Database.query(soqlStmt)) {
			lstTasks.add((Project_Task__c)sobj);
		}

		return lstTasks;
	}

	//save event for inline editing on the ProjectPhaseStageTree lightning component
	@AuraEnabled
	public static void saveChangedRecords_Duration(String jsonRecs) {
		System.debug('Inside saveChangedRecords_Duration: ' + jsonRecs);
		
		List<Object> lstChangedRecs = (List<Object>)JSON.deserializeUntyped(jsonRecs);
		List<SObject> lstUpdate = new List<SObject>();

		//get the ids of the records being edited.  We only want to update Stages
		Set<Id> setStageIds = new Set<Id>();
		for (Object oRec: lstChangedRecs) {
			Map<String, Object> mapRec = (Map<String, Object>)oRec;
			Id recId = (Id)mapRec.get('id');
			setStageIds.add(recId);
		}

		//Retrieve stage records - only allow editing of durations set at the project
		Map<Id, Project_Sub_Phase__c> mapStages = new Map<Id, Project_Sub_Phase__c>();
		for (Project_Sub_Phase__c oStage : [Select Id
												, Estimated_Days_to_Complete__c
												, Project_Phase__r.Due_Date_Baseline__c
											From Project_Sub_Phase__c
											Where Id in :setStageIds]) {
			mapStages.put(oStage.Id, oStage);
		}

		//If a stage record is updated, write the new value to the database
		for (Object oRec : lstChangedRecs) {
			Map<String, Object> mapRec = (Map<String, Object>)oRec;
			Id recId = (Id)mapRec.get('id');
			if (mapStages.containsKey(recId)) {
				Project_Sub_Phase__c oStageRec = mapStages.get(recId);
				for (String fieldName : mapRec.keySet()) {
					switch on fieldName {
						when  'duration' {
							try {
								//only allow duration to be changed if duration is controlled at the project level
								if (oStageRec.Project_Phase__r.Due_Date_Baseline__c == 'Stage Duration - Project') {
									oStageRec.Estimated_Days_to_Complete__c = Decimal.valueOf((String)mapRec.get(fieldName));
									lstUpdate.add(oStageRec);
								}
							} catch (Exception ex) {
								oStageRec.Estimated_Days_to_Complete__c = null;
							}
						}
					}
				}
			}			
		}
		System.debug('End of saveChangedRecords_Duration: ' + lstUpdate);
		update lstUpdate;
	}
		
}