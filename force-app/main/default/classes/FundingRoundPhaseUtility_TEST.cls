@isTest
private class FundingRoundPhaseUtility_TEST {

/*
	//Commented out by EBG/DC on 20200313 - phase are no longer submitted

	static void testFundingRoundPhaseUtility() {
		//stage custom settings
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		     FROM Enable_Triggers__mdt
						  		       		     WHERE MasterLabel = 'FundingRound'];
	
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create fr phase
        Phase__c frph = TestDataUtility.createFundingRoundPhase('Pre-Construction Due Diligence', 
                                                               system.Now(), 
                                                               system.Now().addMonths(6), 
                                                               1,
                                                               'phase',
                                                               fr.Id, 
                                                               null);

        //create fr sub-phase        
        Phase__c frsph = TestDataUtility.createFundingRoundPhase('Credit Approval', 
                                                               null, 
                                                               null, 
                                                               1, 
                                                               'subphase',
                                                               null, 
                                                               frph.Id);

        //run tests
        Test.startTest();
    	frph.Require_Developer_Submission__c = true;
    	update frph;

    	//create second sub-phase under a submission phase.  Save should fail
        //create fr sub-phase
        try {
        Phase__c frsph_fail = TestDataUtility.createFundingRoundPhase('File to Closing – Construction Loan', 
	                                                               null, 
	                                                               null, 
	                                                               1, 
	                                                               'subphase',
	                                                               null, 
	                                                               frph.Id);   	
        } catch (Exception ex) {
        	system.debug(ex);
        }

        //remove submission requirement, create a second sub-phase, and then h
        try {
	    	frph.Require_Developer_Submission__c = false;
	    	update frph; 

	        Phase__c frsp1_pass = TestDataUtility.createFundingRoundPhase('File to Closing – Construction Loan', 
	                                                               null, 
	                                                               null, 
	                                                               1,
	                                                               'subphase',
	                                                               null, 
	                                                               frph.Id);

	        frph.Require_Developer_Submission__c = true;
	        update frph;
        } catch (Exception ex) {
        	system.debug(ex);
        }        

        Test.stopTest();
	}
*/

	@isTest static void updateDueDates_test() {
		List<Phase__c> lstNewPhase = new List<Phase__c>();
		Map<Id, Phase__c> mapOldPhase = new Map<Id, Phase__c>();
		List<Phase__c> lstNewStage = new List<Phase__c>();
		Map<Id, Phase__c> mapOldStage = new Map<Id, Phase__c>();

		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		Funding_Round_Task__c oFRTask = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
		Funding_Round_Task__c oFRTask_Post = TestDataUtility.createFundingRoundStageTaskPostSelection(oFundingRound.Id);
		Phase__c oPhase = [Select Id
								, Due_Date_Baseline__c
								, Phase__c 
								, RecordTypeId
								, (Select Id
										, Due_Date_Offset__c
										, Estimated_Days_to_Complete__c
										, Parent_Phase__c
										, RecordTypeId
									From SubPhases__r)
							From Phase__c 
							Where Funding_Round__c = :oFundingRound.Id 
								And Phase__c = 'Application'
							LIMIT 1];

		oPhase.Due_Date_Baseline__c = 'Stage Duration - Funding Round';
		update oPhase;

		Test.startTest();

		mapOldPhase.put(oPhase.Id, oPhase);
		Phase__c oNewPhase = new Phase__c(Id = oPhase.Id
											, RecordTypeId = oPhase.RecordTypeId
											, Due_Date_Baseline__c = 'Stage Duration - Project'
											, Phase__c = oPhase.Phase__c);
		lstNewPhase.add(oNewPhase);
		FundingRoundPhaseUtility.updateDueDates_PhaseBaseline(lstNewPhase, mapOldPhase);

		Phase__c oStage = oPhase.SubPhases__r[0];
		oStage.Due_Date_Offset__c = 8;
		oStage.Estimated_Days_to_Complete__c = 8;
		update oStage;
		mapOldStage.put(oStage.Id, oStage);
		Phase__c oNewStage = new Phase__c(Id = oStage.Id
										, Due_Date_Offset__c = 10
										, Estimated_Days_to_Complete__c = oStage.Estimated_Days_to_Complete__c
										, Parent_Phase__c = oStage.Parent_Phase__c
										, RecordTypeId = oStage.RecordTypeId);
		lstNewStage.add(oNewStage);
		FundingRoundPhaseUtility.updateStageDueDate_StageChange(lstNewStage, mapOldStage);

		oNewStage.Estimated_Days_to_Complete__c = 10;
		lstNewStage.clear();
		lstNewStage.add(oNewStage);
		FundingRoundPhaseUtility.updateDueDates_StageDuration(lstNewStage, mapOldStage);

		oFundingRound.Open_Date_Time__c = Datetime.now().addDays(1);
		update oFundingRound;

		Test.stopTest();		
	}

}