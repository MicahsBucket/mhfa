@isTest
private class ProjectNarrativeManage_TEST {
	@isTest static void testProjectNarrativeManage() {
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create funding round group
        Funding_Round_Narrative_Group__c frng = TestDataUtility.createFundingRoundNarrativeGroup(fr.Id);

        //create funding round author
        Funding_Round_Narrative_Author__c frna = TestDataUtility.createFundingRoundNarrativeAuthor(frng.Id);

        //create funding round narrative
        Funding_Round_Narrative__c frn = TestDataUtility.createFundingRoundNarrative(fr.Id,
                                                                                     frna.Id); 

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //delete pn w/o attributes (group)
        List<Project_Narrative__c> lstPNToDelete = new List<Project_Narrative__c>();
        for(Project_Narrative__c pn : [SELECT toLabel(Narrative_Group__c)
                                       FROM Project_Narrative__c 
                                       WHERE Project__c = :p.Id]){
            system.debug('pn test class loop: ' + pn);
            lstPNToDelete.add(pn);
        }

        delete lstPNToDelete;

        //create project narrative
        Project_Narrative__c pn1 = TestDataUtility.createProjectNarrative(p.Id,
        	 															 frn.Id,
        	 															 'Architect',
        	 															 'Architecture and Construction',
        	 															 'Selection Narratives',
        	 															 'In Process');        
        Project_Narrative__c pn2 = TestDataUtility.createProjectNarrative(p.Id,
                                                                         frn.Id,
                                                                         'SHO',
                                                                         'Selection Conditions',
                                                                         'Selection Narratives',
                                                                         'In Process');
        Project_Narrative__c pn3 = TestDataUtility.createProjectNarrative(p.Id,
                                                                         frn.Id,
                                                                         'HDO',
                                                                         'Funding Recommendation',
                                                                         'Selection Narratives',
                                                                         'In Process');

        //stage filter records.  not staged in data utility as this data is specific to this test and is not needed for reuse in other tests.
        Project_Narrative_Selected_Filter__c filterValueGroup = new Project_Narrative_Selected_Filter__c();
            filterValueGroup.Filter__c = 'Narrative_Group__c';
            filterValueGroup.Filter_Value__c = 'Selection Narratives';
            filterValueGroup.OwnerId = UserInfo.getUserId();
            filterValueGroup.Project__c = p.Id;
            
            insert filterValueGroup;

        Project_Narrative_Selected_Filter__c filterValueAuthor = new Project_Narrative_Selected_Filter__c();
            filterValueAuthor.Filter__c = 'Author__c';
            filterValueAuthor.Filter_Value__c = 'Architect';
            filterValueAuthor.OwnerId = UserInfo.getUserId();
            filterValueAuthor.Project__c = p.Id;
            
            insert filterValueAuthor;

        Project_Narrative_Selected_Filter__c filterValueStatus = new Project_Narrative_Selected_Filter__c();
            filterValueStatus.Filter__c = 'Status__c';
            filterValueStatus.Filter_Value__c = 'In Process';
            filterValueStatus.OwnerId = UserInfo.getUserId();
            filterValueStatus.Project__c = p.Id;
            
            insert filterValueStatus;

        //run tests
        Test.startTest();
        	Test.setCurrentPage(Page.ProjectNarrative_Manage);
            ProjectNarrativeManage_Controller ext = new ProjectNarrativeManage_Controller(new ApexPages.StandardController(p));

          	List<String> lstGroupValues = ext.lstGroupValues;
     	    List<String> lstAuthorValues = ext.lstAuthorValues;
    	    List<String> lstStatusValues = ext.lstStatusValues;

            ext.strSelectedAuthor = 'Architect';
            ext.strSelectedGroup = 'Selection Narratives';
            ext.strSelectedStatus = 'In Process';

            ext.manageFilters();

            List<Project_Narrative__c> lstpn = ext.getNarratives();
            PageReference pageRef = ext.pgNarrative();
        Test.stopTest();
	}
}