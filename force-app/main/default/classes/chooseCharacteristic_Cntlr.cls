public class chooseCharacteristic_Cntlr  {

	//wrapper classes
	public class groupCharacteristics {
		//properties
		@AuraEnabled
		public Trigger_Group__c oGroup {get;set;}
		@AuraEnabled
		public List<Funding_Round_Trigger__c> oGrpCharacteristics {get;set;}
		@AuraEnabled
		public List<Id> selectedChars {get;set;}

		//constructor
		public groupCharacteristics() {
			this.oGroup = new Trigger_Group__c();
			this.oGrpCharacteristics = new List<Funding_Round_Trigger__c>();
			this.selectedChars = new List<Id>();
		}
	}

	public class checklistOption {
		//properties
		@AuraEnabled
		public Boolean isSelected {get;set;}
		@AuraEnabled
		public Phase_Checklist__c oChecklist {get;set;}
		@AuraEnabled
		public Phase_Checklist_Item__c oChecklistItem {get;set;}
		@AuraEnabled
		public List<chooseCharacteristic_Cntlr.picklistOption> stageOptions {get;set;}
		@AuraEnabled
		public List<chooseCharacteristic_Cntlr.picklistOption> approverOptions {get;set;}

		//constructor
		public checklistOption() {
			this.isSelected = false;
			this.oChecklist = new Phase_Checklist__c();
			this.oChecklistItem = new Phase_Checklist_Item__c();
			this.stageOptions = new List<chooseCharacteristic_Cntlr.picklistOption>();
			this.approverOptions = new List<chooseCharacteristic_Cntlr.picklistOption>();
		}
	}

	public class picklistOption {
		//properties
		@AuraEnabled
		public String itemLabel {get;set;}
		@AuraEnabled
		public String itemValue {get;set;}

		//constructor
		public picklistOption(String label, String value) {
			this.itemLabel = label;
			this.itemValue = value;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and associated characteristics for funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<chooseCharacteristic_Cntlr.groupCharacteristics> getGroupings(Id frId, Id frcliId) {

		List<chooseCharacteristic_Cntlr.groupCharacteristics> retVal = new List<chooseCharacteristic_Cntlr.groupCharacteristics>();
		Map<String, Set<Id>> mapGrpSelectedValues = new Map<String, Set<Id>>();
		for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT : [Select Id
																	, Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
																	, Funding_Round_Trigger__r.Trigger_Group__c
																	, Funding_Round_Trigger__r.Trigger_Group__r.Name
																From Funding_Round_Checklist_Item_Trigger__c
																Where Funding_Round_Checklist_Item__c = :frcliId]) {
			Set<Id> setSelectedValues = new Set<Id>();
			if (mapGrpSelectedValues.containsKey(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c)) {
				setSelectedValues = mapGrpSelectedValues.get(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c);
			}
			setSelectedValues.add(oFRCLIT.Funding_Round_Trigger__c);
			mapGrpSelectedValues.put(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c, setSelectedValues);
		}

		Map<String, List<Funding_Round_Trigger__c>> mapCharsByGroup = new Map<String, List<Funding_Round_Trigger__c>>();
		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
											And Active__c = true
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				chooseCharacteristic_Cntlr.groupCharacteristics oGrpChars = new chooseCharacteristic_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				if (mapGrpSelectedValues.containsKey(oGrp.Id)) {
					oGrpChars.selectedChars.addAll(mapGrpSelectedValues.get(oGrp.Id));
				}
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getGroupings.\n' + retVal);
		return retVal;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//find the checklists associated to the given funding round and build the helper variable
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<chooseCharacteristic_Cntlr.checklistOption> getChecklistOptions(Id frId, Id frcliId) {
		System.debug('Inside getChecklistOptions.\nfrId: ' + frId + '\nfrcliId: ' + frcliId);
		Map<Id, chooseCharacteristic_Cntlr.checklistOption> mapOptions = new Map<Id, chooseCharacteristic_Cntlr.checklistOption>();

		//find all the stages available for each phase in the funding round
		Map<Id, List<chooseCharacteristic_Cntlr.picklistOption>> mapChecklistStages = chooseCharacteristic_Cntlr.retrieveFundingRoundStages(frId);
		System.debug('mapChecklistStages:\n' + mapChecklistStages);

		//find approver options to populate picklists
		List<chooseCharacteristic_Cntlr.picklistOption> approverOptions = chooseCharacteristic_Cntlr.retrievePicklistValues(new Phase_Checklist_Item__c()
																															, 'Approval_First__c');

		for (Phase_Checklist__c oChecklist : [Select Id
													, Name
													, Active__c
													, Sort_Order__c
													, Funding_Round_Phase__c
													, Funding_Round_Phase__r.Name
													, Funding_Round_Phase__r.End_Date__c
												From Phase_Checklist__c
												Where Funding_Round__c = :frId
													And Active__c = true
												Order By Sort_Order__c]) {
			chooseCharacteristic_Cntlr.checklistOption oChecklistOption = new chooseCharacteristic_Cntlr.checklistOption();
			oChecklistOption.oChecklist = oChecklist;
			oChecklistOption.oChecklistItem.Phase_Checklist__c = oChecklist.Id;
			oChecklistOption.oChecklistItem.Funding_Round_Checklist_Item__c = frcliId;
			if (mapChecklistStages.containsKey(oChecklist.Funding_Round_Phase__c)) {
				oChecklistOption.stageOptions = mapChecklistStages.get(oChecklist.Funding_Round_Phase__c);
			}
			oChecklistOption.approverOptions = approverOptions;
			mapOptions.put(oChecklist.Id, oChecklistOption);
		}


		//find checklist items already associated to this funding round and pre-select the checkbox
		for (Phase_Checklist_Item__c oChkCLI : [Select Id
																		, Phase_Checklist__c
																		, Funding_Round_Checklist_Item__c
																		, Funding_Round_Stage__c
																		, Approval_First__c
																		, Approval_Second__c
																		, Approval_Third__c
																	From Phase_Checklist_Item__c
																	Where Funding_Round_Checklist_Item__c = :frcliId]) {
			if (mapOptions.containsKey(oChkCLI.Phase_Checklist__c)) {
				chooseCharacteristic_Cntlr.checklistOption oOption = mapOptions.get(oChkCLI.Phase_Checklist__c);
				oOption.isSelected = true;
				oOption.oChecklistItem = oChkCLI;
				mapOptions.put(oChkCLI.Phase_Checklist__c, oOption);
			}
		}
		//return the list of checklist items
		System.debug('mapOptions:\n' + mapOptions);
		return mapOptions.values();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//save characteristics and checklists with the given funding round checklist item
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static void saveCliConfiguration(Id frcliId, List<String> lstCharIds, List<String> lstJsonChecklists) {
		System.debug('Inside saveCliConfiguration.'
						+ '\nfrcliId: ' + frcliId
						+ '\nlstCharIds: ' + lstCharIds
						+ '\nlstJsonChecklists: ' + lstJsonChecklists);
		
		chooseCharacteristic_Cntlr.insertCliCharacteristics(frcliId, lstCharIds, true);
		List<chooseCharacteristic_Cntlr.checklistOption> lstChecklists = new List<chooseCharacteristic_Cntlr.checklistOption>();
		for (String oJsonChecklist : lstJsonChecklists) {
			lstChecklists.add(
				(chooseCharacteristic_Cntlr.checklistOption) JSON.deserialize(oJsonChecklist, chooseCharacteristic_Cntlr.checklistOption.class)
			);
		}
		chooseCharacteristic_Cntlr.insertCliChecklists(frcliId, lstChecklists);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// insert checklist item characteristics for the given funding round checklist item
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static void insertCliCharacteristics(Id frcliId, List<String> lstCharIds, Boolean blnClearExisting) {
		System.debug('Inside insertCliCharacteristics'
						+ '\nfrcliId: ' + frcliId
						+ '\nlstCharIds: ' + lstCharIds
						+ '\nblnClearExisting: '+ blnClearExisting);

		//delete existing values for the given checklist item if told to do so
		if (blnClearExisting == true) {
			List<Funding_Round_Checklist_Item_Trigger__c> delRecs = new List<Funding_Round_Checklist_Item_Trigger__c>();
			for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT :  [Select Id 
															From Funding_Round_Checklist_Item_Trigger__c 
															Where Funding_Round_Checklist_Item__c = :frcliId]) {
				delRecs.add(oFRCLIT);
			}
			if (delRecs.size() > 0) {
				System.debug('Deleting ' + delRecs);
				delete delRecs;
			}
		}

		//insert the selected items
		List<Funding_Round_Checklist_Item_Trigger__c> lstInsertRecs = new List<Funding_Round_Checklist_Item_Trigger__c>();
		List<Id> lstIds = new List<Id>();
		//the provided list of ids might have commas, so we need to split those entries to get the individual ids
		System.debug('lstCharIds.size: ' + lstCharIds.size());
		for (Integer i=0; i<lstCharIds.size(); i++) {
			System.debug('sItems: ' + lstCharIds[i]);
			lstIds.add((Id)lstCharIds[i]);
		}
		System.debug('lstIds: ' + lstIds);

		for (Id charId : lstIds) {
			lstInsertRecs.add(new Funding_Round_Checklist_Item_Trigger__c(
										Funding_Round_Checklist_Item__c = frcliId
										, Funding_Round_Trigger__c = charId));
		}
		if (lstInsertRecs.size() > 0) {
			System.debug('Inserting ' + lstInsertRecs);
			insert lstInsertRecs;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// insert checklist checklist item
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static void insertCliChecklists(Id frcliId, List<chooseCharacteristic_Cntlr.checklistOption> lstChecklists) {
		List<Phase_Checklist_Item__c> lstDelete = new List<Phase_Checklist_Item__c>();
		List<Phase_Checklist_Item__c> lstUpsert = new List<Phase_Checklist_Item__c>();

		if (lstChecklists != null) {
			for (chooseCharacteristic_Cntlr.checklistOption oChecklistOption : lstChecklists) {
				if (oChecklistOption.isSelected == true) {
					lstUpsert.add(oChecklistOption.oChecklistItem);
				} else if (oChecklistOption.oChecklistItem.Id != null) {
					lstDelete.add(oChecklistOption.oChecklistItem);
				}
			}
		}

		if (lstDelete.size() > 0) {
			delete lstDelete;
		}
		if (lstUpsert.size() > 0) {
			upsert lstUpsert;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// retrieve a map of phase ids and associated stages in the given funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<Id, List<chooseCharacteristic_Cntlr.picklistOption>> retrieveFundingRoundStages(Id frId) {
		Map<Id, List<chooseCharacteristic_Cntlr.picklistOption>> retVal = new Map<Id, List<chooseCharacteristic_Cntlr.picklistOption>>();
		for (Phase__c oPhase : [Select Id
									, Name
									, Sort_Order__c
									, (Select Id
											, Name
											, Sort_Order__c 
										From SubPhases__r
										Order By Sort_Order__c)
								From Phase__c
								Where Funding_Round__c = :frId
								Order By Sort_Order__c]) {
			List<chooseCharacteristic_Cntlr.picklistOption> lstStageOptions = new List<chooseCharacteristic_Cntlr.picklistOption>();
			for (Phase__c oSubPhase : oPhase.SubPhases__r) {
				lstStageOptions.add(new chooseCharacteristic_Cntlr.picklistOption(oSubPhase.Name, oSubPhase.Id));
			}
			if (lstStageOptions.size() > 0) {
				retVal.put(oPhase.Id, lstStageOptions);
			}
		}
		return retVal;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Use the getPicklistValues utility to find picklist values for a field and then convert them
	// into the picklist structure that the lighnting component can iterate
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static List<chooseCharacteristic_Cntlr.picklistOption> retrievePicklistValues(Sobject objectName, String fieldName) {
		List<selectOption> lstOptions = utilityMethods_DCS.getPickValues(objectName, fieldName, null);
		List<chooseCharacteristic_Cntlr.picklistOption> retVal = new List<chooseCharacteristic_Cntlr.picklistOption>();
		for (SelectOption oOpt : lstOptions) {			
			retVal.add(new chooseCharacteristic_Cntlr.picklistOption(oOpt.getLabel(), oOpt.getValue()));
		}
		return retVal;		
	}

}