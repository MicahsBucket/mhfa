@isTest 
private class SK_LightningTreeCmpController_TEST {

	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());
		oFundingRound.Carryover_Due_Date__c = Date.today().addDays(10);
		update oFundingRound;

		Funding_Round_Task__c oFundingRoundTask1 = TestDataUtility.createFundingRoundTaskPreSelection(oFundingRound.Id, null);
		Funding_Round_Task__c oFundingRoundTask2 = TestDataUtility.createFundingRoundTaskPreSelection(oFundingRound.Id, null);
		oFundingRoundTask2.Preceding_Task__c = oFundingRoundTask1.Id;
		update oFundingRoundTask2;
		Funding_Round_Task__c oFundingRoundTask3 = TestDataUtility.createFundingRoundTaskPreSelection(oFundingRound.Id, null);
		oFundingRoundTask3.Preceding_Task__c = oFundingRoundTask2.Id;
		update oFundingRoundTask3;
		System.debug('After createFundingRoundTask.  Queries: ' + Limits.getQueries());
	}

	@isTest
	private static void testName() {
		Funding_Round__c oFundingRound = [Select Id
												, Name
												, (Select Id
														, Name
														, Preceding_Task__c
														, Related_To_Text__c
														, Target_Due_Date__c
														, Assignee_Type__c
														, toLabel(Assignee_Role_Project_Team__c)
														, toLabel(Assignee_Role_Funding_Round_Team__c)
													 From Funding_Round_Tasks__r)
											From Funding_Round__c LIMIT 1];

		SK_LightningTreeCmpController.findHierarchyData((String)oFundingRound.Id
														, 'Funding_Round_Task__c'
														, 'Funding_Round__c'
														, 'Preceding_Task__c'
														, 'Funding_Round_Tasks__r'
														, 'Name'
														, 'Related_To_Text__c:Parent Record:text,Target_Due_Date__c:Target Due Date:date-local,Assignee_Type__c:Assignee Type:text,toLabel(Assignee_Role_Project_Team__c):Assignee Role (Project):text,toLabel(Assignee_Role_Funding_Round_Team__c):Assignee Role (Funding Round):text');
	}
}