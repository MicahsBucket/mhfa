public without sharing class configureTask_Cntlr  {

	//wrapper classes
	public class groupCharacteristics {
		//properties
		@AuraEnabled
		public Trigger_Group__c oGroup {get;set;}
		@AuraEnabled
		public List<Funding_Round_Trigger__c> oGrpCharacteristics {get;set;}
		@AuraEnabled
		public List<Id> selectedChars {get;set;}
		@AuraEnabled
		public List<Id> inheritedChars {get;set;}

		//constructor
		public groupCharacteristics() {
			this.oGroup = new Trigger_Group__c();
			this.oGrpCharacteristics = new List<Funding_Round_Trigger__c>();
			this.selectedChars = new List<Id>();
			this.inheritedChars = new List<Id>();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and associated characteristics for funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureTask_Cntlr.groupCharacteristics> getGroupings(Id frId, Id frTaskId, String selectionType) {

		List<configureTask_Cntlr.groupCharacteristics> retVal = new List<configureTask_Cntlr.groupCharacteristics>();
		Map<String, Set<Id>> mapGrpSelectedValues = new Map<String, Set<Id>>();
		for (Funding_Round_Task_Characteristic__c oFRTC : [Select Id
																	, Funding_Round_Task__c
																	, Funding_Round_Characteristic__c
																	, Funding_Round_Characteristic__r.Trigger_Group__c
																	, Funding_Round_Characteristic__r.Trigger_Group__r.Name
																	, Inherited_From_Task__c
																From Funding_Round_Task_Characteristic__c
																Where Funding_Round_Task__c = :frTaskId]) {
			Set<Id> setSelectedValues = new Set<Id>();
			if (mapGrpSelectedValues.containsKey(oFRTC.Funding_Round_Characteristic__r.Trigger_Group__c)) {
				setSelectedValues = mapGrpSelectedValues.get(oFRTC.Funding_Round_Characteristic__r.Trigger_Group__c);
			}
			setSelectedValues.add(oFRTC.Funding_Round_Characteristic__c);
			mapGrpSelectedValues.put(oFRTC.Funding_Round_Characteristic__r.Trigger_Group__c, setSelectedValues);
		}

		Map<String, List<Funding_Round_Trigger__c>> mapCharsByGroup = new Map<String, List<Funding_Round_Trigger__c>>();
		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureTask_Cntlr.groupCharacteristics oGrpChars = new configureTask_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				if (mapGrpSelectedValues.containsKey(oGrp.Id)) {
					oGrpChars.selectedChars.addAll(mapGrpSelectedValues.get(oGrp.Id));
				}
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getGroupings.\n' + retVal);
		return retVal;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and associated characteristics for funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureTask_Cntlr.groupCharacteristics> getAvailableCharacteristics(Id frId, Id frTaskId, String selectionType) {

		List<configureTask_Cntlr.groupCharacteristics> retVal = new List<configureTask_Cntlr.groupCharacteristics>();
		Map<String, Set<Id>> mapGrpSelectedValues = new Map<String, Set<Id>>();
		Set<Id> setSelectedValues = new Set<Id>();
		for (Funding_Round_Task_Characteristic__c oFRTC : [Select Id
																	, Funding_Round_Task__c
																	, Funding_Round_Characteristic__c
																	, Funding_Round_Characteristic__r.Trigger_Group__c
																	, Funding_Round_Characteristic__r.Trigger_Group__r.Name
																	, Inherited_From_Task__c
																From Funding_Round_Task_Characteristic__c
																Where Funding_Round_Task__c = :frTaskId]) {
			setSelectedValues.add(oFRTC.Funding_Round_Characteristic__c);
		}

		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
                                               	Where Id not in :setSelectedValues
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureTask_Cntlr.groupCharacteristics oGrpChars = new configureTask_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				if (mapGrpSelectedValues.containsKey(oGrp.Id)) {
					oGrpChars.selectedChars.addAll(mapGrpSelectedValues.get(oGrp.Id));
				}
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getGroupings.\n' + retVal);
		return retVal;
	}
    
    //////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and characteristics already associated to the given item
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureTask_Cntlr.groupCharacteristics> getAssignedCharacteristics(Id frId, Id frTaskId, String selectionType) {

		List<configureTask_Cntlr.groupCharacteristics> retVal = new List<configureTask_Cntlr.groupCharacteristics>();
		Set<Id> setSelectedGroups = new Set<Id>();
		Set<Id> setSelectedTriggers = new Set<Id>();
		Set<Id> setInheritedChars = new Set<Id>();
        
		for (Funding_Round_Task_Characteristic__c oFRTC : [Select Id
																	, Funding_Round_Task__c
																	, Funding_Round_Characteristic__c
																	, Funding_Round_Characteristic__r.Trigger_Group__c
																	, Funding_Round_Characteristic__r.Trigger_Group__r.Name
																	, Inherited_From_Task__c
																	, Inherited_From_Task__r.Name
																From Funding_Round_Task_Characteristic__c
																Where Funding_Round_Task__c = :frTaskId]) {
			setSelectedGroups.add(oFRTC.Funding_Round_Characteristic__r.Trigger_Group__c);
			setSelectedTriggers.add(oFRTC.Funding_Round_Characteristic__c);
			if (oFRTC.Inherited_From_Task__c != null 
					&& oFRTC.Inherited_From_Task__c != oFRTC.Funding_Round_Task__c) {
				setInheritedChars.add(oFRTC.Funding_Round_Characteristic__c);
			}

		}

		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
                                               	Where Id in :setSelectedTriggers
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
                                      		And Id in :setSelectedGroups
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureTask_Cntlr.groupCharacteristics oGrpChars = new configureTask_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				List<Id> lstInheritedChars = new List<Id>();
				lstInheritedChars.addAll(setInheritedChars);
				oGrpChars.inheritedChars = lstInheritedChars;
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getAssigned.\n' + retVal);
		return retVal;
	}      

	//////////////////////////////////////////////////////////////////////////////////////////////
	// insert task characteristics for the given funding round task
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static void insertTaskCharacteristics(Id frTaskId, List<String> lstCharIds, Boolean blnClearExisting) {
		System.debug('Inside insertTaskCharacteristics'
						+ '\nfrTaskId: ' + frTaskId
						+ '\nlstCharIds: ' + lstCharIds
						+ '\nblnClearExisting: '+ blnClearExisting);

		//delete existing values for the given task if told to do so
		if (blnClearExisting == true) {
			List<Funding_Round_Task_Characteristic__c> delRecs = new List<Funding_Round_Task_Characteristic__c>();
			for (Funding_Round_Task_Characteristic__c oFRTC :  [Select Id 
                                                                     From Funding_Round_Task_Characteristic__c 
                                                                     Where Funding_Round_Task__c = :frTaskId]) {
				delRecs.add(oFRTC);
			}
			if (delRecs.size() > 0) {
				System.debug('Deleting ' + delRecs);
				delete delRecs;
			}
		}

		//insert the selected items
		List<Funding_Round_Task_Characteristic__c> lstInsertRecs = new List<Funding_Round_Task_Characteristic__c>();
		List<Id> lstIds = new List<Id>();
		//the provided list of ids might have commas, so we need to split those entries to get the individual ids
		System.debug('lstCharIds.size: ' + lstCharIds.size());
		for (Integer i=0; i<lstCharIds.size(); i++) {
			System.debug('sItems: ' + lstCharIds[i]);
			lstIds.add((Id)lstCharIds[i]);
		}
		System.debug('lstIds: ' + lstIds);

		for (Id charId : lstIds) {
			lstInsertRecs.add(new Funding_Round_Task_Characteristic__c(
										Funding_Round_Task__c = frTaskId
										, Funding_Round_Characteristic__c = charId));
		}
		if (lstInsertRecs.size() > 0) {
			System.debug('Inserting ' + lstInsertRecs);
			insert lstInsertRecs;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// delete characteristics
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
    public static void removeTaskCharacteristics(Id frTaskId, List<String> lstCharIds) {
		//delete given values
        List<Funding_Round_Task_Characteristic__c> delRecs = new List<Funding_Round_Task_Characteristic__c>();
        for (Funding_Round_Task_Characteristic__c oFRTC :  [Select Id 
                                                                 From Funding_Round_Task_Characteristic__c 
                                                                 Where (Funding_Round_Task__c = :frTaskId
																			OR Inherited_From_Task__c = :frTaskId)
                                                                	And Funding_Round_Characteristic__c in :lstCharIds]) {
        	delRecs.add(oFRTC);
        }
        if (delRecs.size() > 0) {
            System.debug('Deleting ' + delRecs);
            delete delRecs;
        }
    }

	///////////////////////////////////////////////////////////////////
	// As characteristics are associated to a funding round task, associate
	// those characteristics to the children tasks 
	///////////////////////////////////////////////////////////////////
	@future
	public static void assignChildTaskCharacteristics_Future(Set<Id> setFrTaskCharIds) {
		configureTask_Cntlr.assignChildTaskCharacteristics(setFrTaskCharIds);
	}

	public static void assignChildTaskCharacteristics(Set<Id> setFrTaskCharIds) {
		//find Task Ids associated to the given Funding Round Task Characteristics
		Set<Id> setParentTaskIds = new Set<Id>();
		for (Funding_Round_Task_Characteristic__c oFRTC : [Select Funding_Round_Task__c
															From Funding_Round_Task_Characteristic__c
															Where Id in :setFrTaskCharIds]) {
			setParentTaskIds.add(oFRTC.Funding_Round_Task__c);
		}
		System.debug('setParentTaskIds: ' + setParentTaskIds);

		//Find all characteristics and child tasks associated to the tasks found above
		Map<Id, List<Funding_Round_Task_Characteristic__c>> mapTaskCharacteristics = new Map<Id, List<Funding_Round_Task_Characteristic__c>>();
		List<Funding_Round_Task_Characteristic__c> lstNewRecs = new List<Funding_Round_Task_Characteristic__c>();
		for (Funding_Round_Task__c oFRT : [Select Id
												, (Select Id, Name
													From Funding_Round_Tasks__r)
												, (Select Id
														, Funding_Round_Characteristic__c
														, Inherited_From_Task__c
													From Funding_Round_Task_Attributes__r)
											From Funding_Round_Task__c
											Where Id in :setParentTaskIds]) {
			//for each child task, create task characteristic records from the parent task
			for (Funding_Round_Task__c oChildTask : oFRT.Funding_Round_Tasks__r) {
				for (Funding_Round_Task_Characteristic__c oTaskChar : oFRT.Funding_Round_Task_Attributes__r) {
					Funding_Round_Task_Characteristic__c oNewRec = new Funding_Round_Task_Characteristic__c();
					oNewRec.Funding_Round_Task__c = oChildTask.Id;
					oNewRec.Funding_Round_Characteristic__c = oTaskChar.Funding_Round_Characteristic__c;
					if (oTaskChar.Inherited_From_Task__c != null) {
						oNewRec.Inherited_From_Task__c = oTaskChar.Inherited_From_Task__c;
					} else {
						oNewRec.Inherited_From_Task__c = oFRT.Id;
					}
					lstNewRecs.add(oNewRec);
				}
			}
		}

		//insert recs
		if (lstNewRecs.size() > 0) {
			insert lstNewRecs;
		}
	}


	///////////////////////////////////////////////////////////////////
	// As funding round tasks are linked to a parent task, associate
	// the parent's characteristics to the newly created children tasks 
	///////////////////////////////////////////////////////////////////
	@future
	public static void inheritParentTaskCharacteristics(Set<Id> setFRTaskIds) {
		//find Task Ids associated to the given Funding Round Task Characteristics
		Set<Id> setParentTaskIds = new Set<Id>();
		Set<Id> setChildTaskIds = new Set<Id>();
		for (Funding_Round_Task__c oFRT : [Select Id, Preceding_Task__c
											From Funding_Round_Task__c
											Where Id in :setFRTaskIds
												And Preceding_Task__c != null]) {
			setParentTaskIds.add(oFRT.Preceding_Task__c);
			setChildTaskIds.add(oFRT.Id);
		}

		//Find all characteristics and child tasks associated to the tasks found above
		Map<Id, List<Funding_Round_Task_Characteristic__c>> mapTaskCharacteristics = new Map<Id, List<Funding_Round_Task_Characteristic__c>>();
		List<Funding_Round_Task_Characteristic__c> lstNewRecs = new List<Funding_Round_Task_Characteristic__c>();
		for (Funding_Round_Task__c oFRT : [Select Id
												, (Select Id, Name
													From Funding_Round_Tasks__r
													Where Id in :setChildTaskIds)
												, (Select Id
														, Funding_Round_Characteristic__c
														, Inherited_From_Task__c
													From Funding_Round_Task_Attributes__r)
											From Funding_Round_Task__c
											Where Id in :setParentTaskIds]) {
			//for each child task, create task characteristic records from the parent task
			for (Funding_Round_Task__c oChildTask : oFRT.Funding_Round_Tasks__r) {
				for (Funding_Round_Task_Characteristic__c oTaskChar : oFRT.Funding_Round_Task_Attributes__r) {
					Funding_Round_Task_Characteristic__c oNewRec = new Funding_Round_Task_Characteristic__c();
					oNewRec.Funding_Round_Task__c = oChildTask.Id;
					oNewRec.Funding_Round_Characteristic__c = oTaskChar.Funding_Round_Characteristic__c;
					if (oTaskChar.Inherited_From_Task__c != null) {
						oNewRec.Inherited_From_Task__c = oTaskChar.Inherited_From_Task__c;
					} else {
						oNewRec.Inherited_From_Task__c = oFRT.Id;
					}
					lstNewRecs.add(oNewRec);
				}
			}
		}

		//insert recs
		if (lstNewRecs.size() > 0) {
			insert lstNewRecs;
		}
	}

}