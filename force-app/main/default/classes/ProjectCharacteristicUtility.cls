/*
*   {Purpose}  	Manage the functionality tied to Project Characteristic / Project Checklist Item & Task / Project Checklist Item & Task Characteristic automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   05/10/19	Kevin Johnson DCS		Created
*	02/12/20	Kevin Johnson DCS		Refactored how child workflow tasks are processed
*	07/22/20	Kevin Johnson DCS		Added capability to assign Funding Round Team members as Project Task
*										owners to createProjectTask method
*   =============================================================================
*/

public class ProjectCharacteristicUtility  {
	public static void createProjectChecklists(List<Project_Characteristic__c> lstProjChar){
		/*
		*{purpose}		Manages Project Checklist creation
		*
		*{function}		-Fetches Funding Round Checklist Items relative to passed Funding Round Characteristics
		*				-Fetches affected Project Checklsits/Project Checklist Items relative to passed Project and Funding Round Characteristic
		*				-Inserts Project Checklist Items with corresponding Project Checklist Item Characteristic relative
		*				 to related Funding Round Checklist Item(s) and related Funding Round Characteristics
		*		
		*{assumptions}	Project phase/stage/checklists are spun up on project creation and thus exist when code is run
		*		
		*{trigger}  	ProjecCharacteristicEventListener: after insert
		*/

		Set<Id> setFRCharID = new Set<Id>();
		Set<Id> setProjID = new Set<Id>();
		Set<Id> setFRCLIID = new Set<Id>();
		Set<Id> setFRTID = new Set<Id>();
		Map<Id, Funding_Round_Checklist_Item_Trigger__c> mapFRCLIidFRCLIChar = new Map<Id, Funding_Round_Checklist_Item_Trigger__c>();
		List<Funding_Round_Checklist_Item_Trigger__c> lstFRCLIChar = new List<Funding_Round_Checklist_Item_Trigger__c>();	
		List<Funding_Round_Task_Characteristic__c> lstFRTChar = new List<Funding_Round_Task_Characteristic__c>();
		List<Phase_Checklist_Item__c> lstFundingRoundPhaseCLI = new List<Phase_Checklist_Item__c>();		
		List<Funding_Round_Task__c> lstStageFundingRoundTasks = new List<Funding_Round_Task__c>();
		List<Funding_Round_Task__c> lstChecklistFundingRoundTasks = new List<Funding_Round_Task__c>();
		List<Funding_Round_Task__c> lstWorkflowOrUnassociatedFundingRoundTasks = new List<Funding_Round_Task__c>();	
		Map<Id, Project_Task__c> mapFRTaskIDExistingProjectTasks = new Map<Id, Project_Task__c>();
		Map<Id, Project_Task__c> mapFRTaskIDProjTask = new Map<Id, Project_Task__c>();		
		List<Project_Task__c> lstProjTaskToCreate = new List<Project_Task__c>();
		List<Project_Task__c> lstProjTaskCharToProcess = new List<Project_Task__c>();
		List<Project_Task_Characteristic__c> lstProjTaskCharToCreate = new List<Project_Task_Characteristic__c>();
		List<Project_Task__c> lstTaskToUpdate = new List<Project_Task__c>();
		Set<Id> setNewTaskIDs = new set<Id>();
		List<Project_Task__c> lstNewTasks = new List<Project_Task__c>();
		List<Project_Checklist__c> lstPCL = new List<Project_Checklist__c>();
		List<Project_Checklist_Item__c> lstPCLI = new List<Project_Checklist_Item__c>();		
		Map<String, Project_Checklist_Item__c> mapFRChecklistItemIDProjChecklistItem = new Map<String, Project_Checklist_Item__c>();
		List<Project_Checklist_Item__c> lstPCLIToProcess = new List<Project_Checklist_Item__c>();
		List<Project_Checklist_Item__c> lstProjChecklistItemToCreate = new List<Project_Checklist_Item__c>();
		List<Project_Checklist_Item_Characteristic__c> lstProjChecklistItemCharToCreate = new List<Project_Checklist_Item_Characteristic__c>();
		Map<Id, Map<Id, Project_Sub_Phase__c>> mapProjectStages = new Map<Id, Map<Id, Project_Sub_Phase__c>>();
		
		for(Project_Characteristic__c pc : lstProjChar){
			setFRCharID.add(pc.Funding_Round_Characteristic__c);
			setProjID.add(pc.Project__c);
		}
		for(Project_Sub_Phase__c oPS : [SELECT Id
											    , Name
											    , Funding_Round_Sub_Phase__c
											    , Project__c
											    , Project_Phase__c
										FROM Project_Sub_Phase__c  //stages
										WHERE Project__c IN :setProjID]) {

			Map<Id, Project_Sub_Phase__c> mapFRStage = new Map<Id, Project_Sub_Phase__c>();
			if (mapProjectStages.containsKey(oPS.Project__c)) {
				mapFRStage = mapProjectStages.get(oPS.Project__c);
			}
			mapFRStage.put(oPS.Funding_Round_Sub_Phase__c, oPS);
			mapProjectStages.put(oPS.Project__c, mapFRStage);
		}

		//==========================================================================================================
		//FUNDING ROUND
		//==========================================================================================================
		//FR CHECKLIST ITEM
		//==========================================================================================================
		//fetch frclichar data
		for(Funding_Round_Checklist_Item_Trigger__c frclichar : [SELECT Funding_Round_Checklist_Item__c
																	  , Funding_Round_Trigger__c
																	  , Selection_Type__c
															     FROM Funding_Round_Checklist_Item_Trigger__c
																 WHERE Funding_Round_Trigger__c IN :setFRCharID]){
			setFRCLIID.add(frclichar.Funding_Round_Checklist_Item__c);
			System.debug('PROJ CHAR frclichar: ' + frclichar);
			lstFRCLIChar.add(frclichar);
			mapFRCLIidFRCLIChar.put(frclichar.Funding_Round_Checklist_Item__c, frclichar);
		}

		//fetch frcli relative to returned frchar
		for(Phase_Checklist_Item__c frcli : [SELECT Id
												  , Funding_Round_Checklist_Item__c
												  , Funding_Round_Stage__c
												  , Phase_Checklist__c
												  , Phase_Checklist__r.Selection_Type__c
												  , Funding_Round_Checklist_Item__r.Name
												  , Approval_First__c
												  , Approval_Second__c
												  , Approval_Third__c
												  , Funding_Round_Checklist_Item__r.Maximum_Number_of_Attachments__c
												  , Funding_Round_Checklist_Item__r.Required_File_Extension__c
											 FROM Phase_Checklist_Item__c
											 WHERE Funding_Round_Checklist_Item__c IN :setFRCLIID]){
			System.debug('PROJ CHAR lstFundingRoundPhaseCLI ADD frcli: ' + frcli);
			lstFundingRoundPhaseCLI.add(frcli);
		}

		//==========================================================================================================
		//FR TASKS
		//==========================================================================================================
		//fetch frtchar data relative to passed selected char
		Set<Id> setFundingRoundIds = new Set<Id>();
		for(Funding_Round_Task_Characteristic__c frtchar : [SELECT Funding_Round_Task__c
																, Funding_Round_Task__r.Funding_Round__c
																, Funding_Round_Characteristic__c
																, Inherited_From_Task__c
															FROM Funding_Round_Task_Characteristic__c
														    WHERE Funding_Round_Characteristic__c IN :setFRCharID
															AND Funding_Round_Task__r.Funding_Round__c != null]){
			setFRTID.add(frtchar.Funding_Round_Task__c);
			System.debug('TASK CHAR frtchar: ' + frtchar);
			lstFRTChar.add(frtchar);
			setFundingRoundIds.add(frtchar.Funding_Round_Task__r.Funding_Round__c);
		}

		//fetch frtask relative to returned frchar
		for(Funding_Round_Task__c frt : [SELECT Id
											  , Name
											  , Initial_Task__c
											  , Preceding_Task__c
											  , Stage_Lookup__c
											  , Checklist__c
											  , Description__c
											  , Target_Due_Date__c
											  , Stage__c
											  , Assignee_Type__c
											  , Funding_Round__r.OwnerId                   
											  , Expected_Duration_Days__c
											  , Assignee_Role_Project_Team__c
											  , Assignee_Role_Funding_Round_Team__c
										 FROM Funding_Round_Task__c
										 WHERE Id IN :setFRTID
										 AND Funding_Round__r.OwnerId != null]){  //ensure task is associated to a funding round			
			//segment types of tasks
			if(frt.Stage_Lookup__c != null){  //stage tasks
				System.debug('TASK CHAR lstStageFundingRoundTasks ADD frt: ' + frt);
				lstStageFundingRoundTasks.add(frt);
			}
			if(frt.Checklist__c != null){  //checklist tasks
				System.debug('TASK CHAR lstChecklistFundingRoundTasks ADD frt: ' + frt);
				lstChecklistFundingRoundTasks.add(frt);
			}
			if(frt.Preceding_Task__c != null){  //child workflow tasks
				System.debug('TASK CHAR lstWorkflowOrUnassociatedFundingRoundTasks ADD frt: ' + frt);
				lstWorkflowOrUnassociatedFundingRoundTasks.add(frt);
			}
			if(frt.Stage_Lookup__c == null && frt.Checklist__c == null && frt.Preceding_Task__c == null){  //unassociated to phase/stage, checklist, workflow tasks
				System.debug('TASK CHAR lstWorkflowOrUnassociatedFundingRoundTasks (Unassociated) ADD frt: ' + frt);
				lstWorkflowOrUnassociatedFundingRoundTasks.add(frt);
			}
		}
		
		//==========================================================================================================
		//PROJECT
		//==========================================================================================================
		//FETCH EXISTING PROJECT TASKS
		//==========================================================================================================
		Map<ID, Map<String, ID>> mapOTMRoles = new Map<ID, Map<String, ID>>();
		for(Project_Task__c pst : [SELECT Id
										, Funding_Round_Task__c
										, Project__c
										, Preceding_Task__c
										, Project_Checklist__c
										, Project_Stage__c
										, Expected_Duration_Days__c
										, OwnerId
								   FROM Project_Task__c
								   WHERE Project__c IN :setProjID]){
			mapFRTaskIDExistingProjectTasks.put(pst.Funding_Round_Task__c, pst);
		}

		//fetch project team member/roles
		mapOTMRoles = OpportunityTeamMemberUtility.retrieveOTMRoles(setProjID);

		//==========================================================================================================
		//PROCESS STAGE TASKS
		//==========================================================================================================
		Set<Project_Task__c> setPTtoUpdate = new Set<Project_Task__c>();  //ensure unique objects - used to populate list to update project task
		List<Project_Task__c> lstPTtoUpdate = new List<Project_Task__c>();  //list used to update project task

		if(lstStageFundingRoundTasks.size() > 0){
			for(Project_Sub_Phase__c oPS : [SELECT Id
											     , Name
											     , Funding_Round_Sub_Phase__c
											     , Project__c
											     , Project_Phase__c
												 , Project__r.OwnerId
												 , Project__r.Funding_Round__c
											FROM Project_Sub_Phase__c  //stages
											WHERE Project__c IN :setProjID]){
				System.debug('TASK CHAR oPS: ' + oPS);
				for(Funding_Round_Task__c oFRT : lstStageFundingRoundTasks){  //process funding round tasks resulting from new characteristics			
					if(!mapFRTaskIDExistingProjectTasks.containsKey(oFRT.Id) && //frt does not exist
					   oPS.Funding_Round_Sub_Phase__c == oFRT.Stage_Lookup__c){ //stage = frt stage then create task
					    Project_Task__c newStageTask = createProjectTask(oFRT
						                                               , oPS
																	   , null
																	   , null);
						System.debug('TASK CHAR newStageTask: ' + newStageTask);
						lstProjTaskToCreate.add(newStageTask);
					} else if(mapFRTaskIDExistingProjectTasks.containsKey(oFRT.Id) && //frt already exists - create project task characteristic
					          oPS.Funding_Round_Sub_Phase__c == oFRT.Stage_Lookup__c){  
								lstProjTaskCharToProcess.add(mapFRTaskIDExistingProjectTasks.get(oFRT.Id));
								//update affected project task: is active = true
								/* TODO: FINISH OWNER LOGIC FOR EXISTING TASKS KDJ 3/13/20
								if(!mapOTMRoles.isEmpty() && mapOTMRoles.get(oPS.Project__c).containsKey(oFRTask.Assignee_Role_Project_Team__c)){
									taskOwnerID = mapOTMRoles.get(oPS.Project__c).get(oFRTask.Assignee_Role_Project_Team__c);
								} else {

								}
								*/
								Project_Task__c ptToUpdate = new Project_Task__c(Id = mapFRTaskIDExistingProjectTasks.get(oFRT.Id).Id,
																				 Is_Active__c = TRUE);
								System.debug('TASK CHAR FLAG EXISTING STAGE: ' + ptToUpdate);
								setPTtoUpdate.add(ptToUpdate);	
					}
				}			
			}		
		}

		//==========================================================================================================
		//PROCESS CHECKLIST TASKS
		//==========================================================================================================
		if(lstChecklistFundingRoundTasks.size() > 0){
			for(Project_Checklist__c oPCL : [SELECT Id
											      , Project__c											      
											      , Project_Phase__c
												  , Funding_Round_Checklist__c
												  , Project__r.OwnerId
												  , Project__r.Funding_Round__c
									         FROM Project_Checklist__c
										     WHERE Project__c IN :setProjID]){
				System.debug('TASK CHAR oPCL: ' + oPCL);
				for(Funding_Round_Task__c oFRT : lstChecklistFundingRoundTasks){  //process funding round tasks resulting from new characteristics	
					if(!mapFRTaskIDExistingProjectTasks.containsKey(oFRT.Id) && //frt does not exist
					   oPCL.Funding_Round_Checklist__c == oFRT.Checklist__c){ //checklist = frt checklist then create task)){
		                Project_Task__c newCLTask = createProjectTask(oFRT
						                                            , null
																	, oPCL
																	, null);						
						System.debug('TASK CHAR newCLTask: ' + newCLTask);
						lstProjTaskToCreate.add(newCLTask);
					} else if(mapFRTaskIDExistingProjectTasks.containsKey(oFRT.Id) && //frt already exists - create project task characteristic
					          oPCL.Funding_Round_Checklist__c == oFRT.Checklist__c){  
								lstProjTaskCharToProcess.add(mapFRTaskIDExistingProjectTasks.get(oFRT.Id));
								//update affected project task: is active = true								
								Project_Task__c ptToUpdate = new Project_Task__c(Id = mapFRTaskIDExistingProjectTasks.get(oFRT.Id).Id,
																				 Is_Active__c = TRUE);
								System.debug('TASK CHAR FLAG EXISTING CHECKLIST: ' + ptToUpdate);
								setPTtoUpdate.add(ptToUpdate);	
					}
				}			
			}		
		}
		
		//==========================================================================================================
		//PROCESS CHILD WORKFLOW TASKS
		//==========================================================================================================
		if(lstWorkflowOrUnassociatedFundingRoundTasks.size() > 0){
			for(Opportunity oP : [SELECT Id
									   , Funding_Round__c
									   , OwnerId
 					              FROM Opportunity
								  WHERE Id IN :setProjID]){
				for(Funding_Round_Task__c oFRT : lstWorkflowOrUnassociatedFundingRoundTasks){  //process funding round tasks resulting from new characteristics
					if(!mapFRTaskIDExistingProjectTasks.containsKey(oFRT.Id)){  //frt does not exist	
					   Project_Task__c newWFTask = createProjectTask(oFRT
						                                           , null
																   , null
																   , oP);						
						System.debug('TASK CHAR newWFTask: ' + newWFTask);
						lstProjTaskToCreate.add(newWFTask);
					} else if(mapFRTaskIDExistingProjectTasks.containsKey(oFRT.Id)){  //frt already exists - create project task characteristic
						lstProjTaskCharToProcess.add(mapFRTaskIDExistingProjectTasks.get(oFRT.Id));
								//update affected project task: is active = true
								Project_Task__c ptToUpdate = new Project_Task__c(Id = mapFRTaskIDExistingProjectTasks.get(oFRT.Id).Id,
																				 Is_Active__c = TRUE);
								System.debug('TASK CHAR FLAG EXISTING WORKFLOW: ' + ptToUpdate);
								setPTtoUpdate.add(ptToUpdate);	
					}
				}			
			}		
		}
		
		System.debug('TASK CHAR lstProjTaskToCreate.size(): ' + lstProjTaskToCreate.size());
		if(lstProjTaskToCreate.size() > 0){
			insert lstProjTaskToCreate;
		    
			//LINK CHILD WORKFLOW TASKS
			//query inserted tasks so as to fetch related data
			for(Project_Task__c pt : lstProjTaskToCreate){
				setNewTaskIDs.add(pt.Id);
			}
		
			for(Project_Task__c oPT : [SELECT Id
											, Funding_Round_Task__c
											, Funding_Round_Task__r.Preceding_Task__c
											, Expected_Duration_Days__c
									   FROM Project_Task__c
									   WHERE Id IN :setNewTaskIDs]){
				System.debug('TASK CHAR LINK frt id, project task: ' + oPT.Funding_Round_Task__c + ' - ' + oPT);
				mapFRTaskIDProjTask.put(oPT.Funding_Round_Task__c, oPT);				
				lstNewTasks.add(oPT);
			}
			System.debug('TASK CHAR LINK lstNewTasks: ' + lstNewTasks);
			for(Project_Task__c pt : lstNewTasks){
				if(mapFRTaskIDProjTask.containsKey(pt.Funding_Round_Task__r.Preceding_Task__c)){
					System.debug('TASK CHAR LINK frt id, project task id being processed: ' + pt.Funding_Round_Task__c + ' - ' + pt.id);
					Project_Task__c updateProjTask = new Project_Task__c(Id = pt.Id
																	   , Preceding_Task__c = mapFRTaskIDProjTask.get(pt.Funding_Round_Task__r.Preceding_Task__c).Id);
					System.debug('TASK CHAR LINK updateProjTask: ' + updateProjTask);
					lstTaskToUpdate.add(updateProjTask);
				}
			}	
			System.debug('TASK CHAR LINK lstTaskToUpdate.size(): ' + lstTaskToUpdate.size());
			if(lstTaskToUpdate.size() > 0){
				update lstTaskToUpdate;
			}
		}
		
		//==========================================================================================================
		//PROCESS PROJECT TASK CHAR
		//==========================================================================================================
		//add all pt (existing / new) list for ptchar processing
		lstProjTaskCharToProcess.addAll(lstProjTaskToCreate);
		//System.debug('TASK CHAR lstProjTaskCharToProcess.size(): ' + lstProjTaskCharToProcess.size());
		//System.debug('TASK CHAR lstFRTChar.size(): ' + lstFRTChar.size());			
		for(Funding_Round_Task_Characteristic__c frtchar : lstFRTChar){			
			for(Project_Task__c pt : lstProjTaskCharToProcess){
				if(pt.Funding_Round_Task__c == frtchar.Funding_Round_Task__c){
					Project_Task_Characteristic__c newPTChar = new Project_Task_Characteristic__c(Funding_Round_Characteristic__c = frtchar.Funding_Round_Characteristic__c
																								, Project_Task__c = pt.Id);
					System.debug('PROJ CHAR newPTChar New PT: ' + newPTChar);
					lstProjTaskCharToCreate.add(newPTChar);					
				}
			}
		}
			
		System.debug('PROJ CHAR lstProjTaskCharToCreate.size(): ' + lstProjTaskCharToCreate.size());
		//insert new ptchar
		if(lstProjTaskCharToCreate.size() > 0){
			insert lstProjTaskCharToCreate;
		}
		System.debug('TASK CHAR FLAG EXISTING lstPTtoUpdate.size(): ' + lstPTtoUpdate.size());
		//update existing pt
		if(setPTtoUpdate.size() > 0){
			lstPTtoUpdate.addAll(setPTtoUpdate);  //cast set to list
			update lstPTtoUpdate;   //update affected project tasks: is active = true
		}				

		//==========================================================================================================
		//CHECKLIST ITEMS
		//==========================================================================================================
        //stage exising pcl/pcli relative to new characteristics
		//return existing pcl and related pcli relative to passed project and stage
		//for downstream processing
        for (Project_Checklist__c oPCL : [SELECT Id
                                               , Project__c  //link to project
											   , Project_Phase__c  //link to project phase
                                               , Funding_Round_Checklist__c  //link to fr phase checklist
											   , Funding_Round_Checklist__r.Selection_Type__c  //checklist selection type (pre/post)
                                               , (SELECT Id
                                                       , Funding_Round_Checklist_Item__c  //link to fr checklist item
                                                       , Project__c  //link to project
													   , Project_Checklist__c  //link to project checklist
													   , Project_Checklist__r.Funding_Round_Checklist__c  //link to funding round checklist
                                                  FROM Project_Checklist_Items__r)
                                          FROM Project_Checklist__c 
                                          WHERE Project__c IN :setProjID]) {
				lstPCL.add(oPCL);

				for (Project_Checklist_Item__c oPCLI : oPCL.Project_Checklist_Items__r) {
					lstPCLI.add(oPCLI);
					String mapId = String.valueOf(oPCLI.Funding_Round_Checklist_Item__c) + String.valueOf(oPCLI.Project_Checklist__r.Funding_Round_Checklist__c);
					mapFRChecklistItemIDProjChecklistItem.put(mapId, oPCLI);
				}					       
        }
		System.debug('PROJ CHAR lstFundingRoundPhaseCLI: ' + lstFundingRoundPhaseCLI.size());
		System.debug('PROJ CHAR mapFRChecklistItemIDProjChecklistItem: ' + mapFRChecklistItemIDProjChecklistItem);
		System.debug('PROJ CHAR lstPCLI: ' + lstPCLI);
		System.debug('PROJ CHAR lstPCL: ' + lstPCL);

		//loop through returned frcli/pcl/pcli to determine:
		//-if existing pcli needs pclichar record inserted (for existing pcli)
		//-if new pcli and its corresponding pclichar records need to be inserted (for new pcli)
		for(Phase_Checklist_Item__c frcli : lstFundingRoundPhaseCLI){  //fr phase cli relative to char selected
			if(frcli.Funding_Round_Checklist_Item__c == mapFRCLIidFRCLIChar.get(frcli.Funding_Round_Checklist_Item__c).Funding_Round_Checklist_Item__c){
				String frclipcliId = String.valueOf(frcli.Funding_Round_Checklist_Item__c) + String.valueOf(frcli.Phase_Checklist__c);
				//System.debug('PROJ CHAR frclipcliId: ' + frclipcliId);
				for(Project_Checklist__c pcl: lstPCL){  //all proj checklists
					if(frcli.Phase_Checklist__c == pcl.Funding_Round_Checklist__c &&  //match checklist
						pcl.Funding_Round_Checklist__r.Selection_Type__c == mapFRCLIidFRCLIChar.get(frcli.Funding_Round_Checklist_Item__c).Selection_Type__c){ //match selection type as frcli can be both pre/post selection			
						if(mapFRChecklistItemIDProjChecklistItem.size() > 0 &&																											
							mapFRChecklistItemIDProjChecklistItem.containsKey(frclipcliId)){  //check for existing pcli that need new pclichar inserted
								lstPCLIToProcess.add(mapFRChecklistItemIDProjChecklistItem.get(frclipcliId));						
						} else {  //create new pcli
							Id projectStageId = null;
							System.debug('PROJ CHAR newPCLI mapProjectStages: ' + mapProjectStages);
							if (mapProjectStages.containsKey(pcl.Project__c)) {
								if (mapProjectStages.get(pcl.Project__c).containsKey(frcli.Funding_Round_Stage__c)) {
									projectStageId = mapProjectStages.get(pcl.Project__c).get(frcli.Funding_Round_Stage__c).Id;
								}
							}						
							//System.debug('PROJ CHAR newPCLI (pcl) id: ' + pcl.id);
							//System.debug('PROJ CHAR newPCLI projectStageId: ' + projectStageId);						
							//System.debug('PROJ CHAR newPCLI (frcli) frcli id: ' + frcli.Funding_Round_Checklist_Item__c);
							//System.debug('PROJ CHAR newPCLI (frcli) frphcli id: ' + frcli.id);
							Project_Checklist_Item__c newPCLI = new Project_Checklist_Item__c(Project__c = pcl.Project__c
																							, Project_Checklist__c = pcl.Id
																							, Project_Stage__c = projectStageId																						
																							, Funding_Round_Checklist_Item__c = frcli.Funding_Round_Checklist_Item__c
																							, Funding_Round_Phase_Checklist_Item__c = frcli.Id
																							, Name = frcli.Funding_Round_Checklist_Item__r.Name									
																							, Approval_Role_First__c = frcli.Approval_First__c
																							, Approval_Role_Second__c = frcli.Approval_Second__c
																							, Approval_Role_Third__c = frcli.Approval_Third__c
																							, Maximum_Number_of_Attachments__c = frcli.Funding_Round_Checklist_Item__r.Maximum_Number_of_Attachments__c
																							, Required_File_Extension__c = frcli.Funding_Round_Checklist_Item__r.Required_File_Extension__c);
							System.debug('PROJ CHAR newPCLI: ' + newPCLI);
							lstProjChecklistItemToCreate.add(newPCLI);
						}
					}
				}
			}
		}		
		System.debug('PROJ CHAR lstProjChecklistItemToCreate: ' + lstProjChecklistItemToCreate);
		//insert new pcli
		if(lstProjChecklistItemToCreate.size() > 0){
			insert lstProjChecklistItemToCreate;
		}

		//==========================================================================================================
		//PROCESS PCLI CHAR
		//==========================================================================================================
		//add all pcli (existing / new) list for pclichar processing
		lstPCLIToProcess.addAll(lstProjChecklistItemToCreate);
		System.debug('PROJ CHAR lstPCLIToProcess size: ' + lstPCLIToProcess.size());
		System.debug('PROJ CHAR lstPCLIToProcess: ' + lstPCLIToProcess);
		//stage pclichar records for insert
		for(Funding_Round_Checklist_Item_Trigger__c frclichar : lstFRCLIChar){
			for(Project_Checklist__c pcl: lstPCL){
				for(Project_Checklist_Item__c pcli : lstPCLIToProcess){
					if(pcli.Funding_Round_Checklist_Item__c == frclichar.Funding_Round_Checklist_Item__c
							&& pcli.Project_Checklist__c == pcl.Id){
						Project_Checklist_Item_Characteristic__c newPCLIChar = new Project_Checklist_Item_Characteristic__c(Funding_Round_Characteristic__c = frclichar.Funding_Round_Trigger__c
																														  , Funding_Round_CLI_Characteristic__c = frclichar.Id
																														  , Funding_Round_Phase_Checklist__c = pcl.Funding_Round_Checklist__c
																														  , Project_Checklist__c = pcl.Id
																														  , Project_Checklist_Item__c = pcli.Id);
						System.debug('PROJ CHAR newPCLIChar New PCLI: ' + newPCLIChar);
						lstProjChecklistItemCharToCreate.add(newPCLIChar);
					}
				}		
			}
		}
		System.debug('PROJ CHAR lstProjChecklistItemCharToCreate: ' + lstProjChecklistItemCharToCreate);
		//insert new pclichar
		if(lstProjChecklistItemCharToCreate.size() > 0){
			insert lstProjChecklistItemCharToCreate;
		}
	}

	public static void deleteProjectChecklistAndTaskCharacteristics(List<Project_Characteristic__c> lstProjChar){
		/*
		*{purpose}		Manages Project Checklist Item Characteristic & Project Task Characteristic deletion
		*
		*{function}		-Fetches Project Checklist Item Characteristic & Project Task Characteristic records relative to passed 
		*				Funding Round Characteristics and Project
		*				-Deletes returned Project Checklist Item Characteristic records
		*				
		*{trigger}  	ProjectCharacteristicEventListener: after delete
		*/
		
		Set<Id> setFRCharID = new Set<Id>();
		Set<Id> setProjID = new Set<Id>();

		for(Project_Characteristic__c pc : lstProjChar){
			setFRCharID.add(pc.Funding_Round_Characteristic__c);
			setProjID.add(pc.Project__c);
		}
		//fetch pcli char to delete
		List<Project_Checklist_Item_Characteristic__c> lstPCLICtoDelete = [SELECT Id 
																		   FROM Project_Checklist_Item_Characteristic__c 
																		   WHERE Funding_Round_Characteristic__c IN :setFRCharID 
																		   AND Project_Checklist_Item__r.Project__c IN :setProjID];

		System.debug('PROJ CHAR lstPCLICtoDelete: ' + lstPCLICtoDelete);
		//fetch pt char to delete
		/*
		List<Project_Task_Characteristic__c> lstPTCtoDelete = [SELECT Id 
															  FROM Project_Task_Characteristic__c 
															  WHERE Funding_Round_Characteristic__c IN :setFRCharID 
															  AND Project_Task__r.Project__c IN :setProjID];
		*/
		List<Project_Task_Characteristic__c> lstPTCtoDelete = new List<Project_Task_Characteristic__c>();
		Set<Project_Task__c> setPTtoUpdate = new Set<Project_Task__c>();  //ensure unique objects - used to populate list to update project task
		List<Project_Task__c> lstPTtoUpdate = new List<Project_Task__c>();  //list used to update project task

		for(Project_Task_Characteristic__c oPTC : [SELECT Id
														, Project_Task__c
												  FROM Project_Task_Characteristic__c 
												  WHERE Funding_Round_Characteristic__c IN :setFRCharID 
												  AND Project_Task__r.Project__c IN :setProjID]){
			lstPTCtoDelete.add(oPTC);

			//update affected project task: is active = false
			Project_Task__c ptToUpdate = new Project_Task__c(Id = oPTC.Project_Task__c,
															 Is_Active__c = FALSE);
			setPTtoUpdate.add(ptToUpdate);												  
		}
		//cast set to list
		if(setPTtoUpdate.size() > 0){
			lstPTtoUpdate.addAll(setPTtoUpdate);
		}
		
		System.debug('PROJ CHAR lstPTCtoDelete: ' + lstPTCtoDelete);
        if (lstPCLICtoDelete.size() > 0) {
            delete lstPCLICtoDelete;  //delete project checklist item characteristic(s)
        }
		System.debug('TASK CHAR FLAG REMOVE lstPTtoUpdate.size(): ' + lstPTtoUpdate.size());
		if (lstPTCtoDelete.size() > 0) {
            delete lstPTCtoDelete;  //delete project task characteristic(s)
			update lstPTtoUpdate;  //update affected project tasks: is active = false
        }
	}

	Public static Project_Task__c createProjectTask(Funding_Round_Task__c oFRTask
                                                  , Project_Sub_Phase__c oPStage
												  , Project_Checklist__c oPChecklist
												  , Opportunity oProj){
	    /*
		*{purpose}		Manages Project Task creation
		*
		*{function}		Accepts Project Task parent object relative to the type of task is being created.
		*				Based on the type of task parent a Project Task is created and returned.
		*		
		*{trigger}		createProjectChecklists
		*/		
		
		ID projStageID = null;
		String projStageName = null;
		ID projChecklistID = null;
		ID projID = null;
		ID frID = null;
		ID taskOwnerID = null;

		Set<Id> setProjID = new Set<Id>();
		Set<Id> setFundingRoundIds = new Set<Id>();
		if(oPStage != null){
			setProjID.add(oPStage.Project__c);
			setFundingRoundIds.add(oPStage.Project__r.Funding_Round__c);
		}
		if(oPChecklist != null){
			setProjID.add(oPChecklist.Project__c);
			setFundingRoundIds.add(oPChecklist.Project__r.Funding_Round__c);
		}		
		if(oProj != null){
			setProjID.add(oProj.Id);
			setFundingRoundIds.add(oProj.Funding_Round__c);
		}
		System.debug('TASK CREATE SETPROJID: ' + setProjID);
		//fetch project team member/roles
		Map<ID, Map<String, ID>> mapOTMRoles = OpportunityTeamMemberUtility.retrieveOTMRoles(setProjID);
		//fetch funding round team member/roles
		Map<Id, Map<String, ID>> mapFRTeamRoles = FundingRoundTeamMemberUtility.retrieveFRTeamRoles(setFundingRoundIds);
		System.debug('TASK CREATE MAPOTMROLES: ' + mapOTMRoles);
		System.debug('TASK CREATE MAPFRTEAMMEMBERROLES: ' + mapFRTeamRoles);
		//stage task
		if(oPStage != null){
			projStageID = oPStage.Id;
			projStageName = oFRTask.Stage__c;
			projID = oPStage.Project__c;
			frID = oFRTask.Funding_Round__c;

			System.debug('TASK CREATE STAGE TYPE: ' + oFRTask.Assignee_Type__c);
			System.debug('TASK CREATE STAGE ROLE: ' + oFRTask.Assignee_Role_Project_Team__c);
			
			if(oFRTask.Assignee_Type__c == 'Internal Task'){
				//process project team roles to determine task owner
				if(!mapOTMRoles.isEmpty() && 
					oFRTask.Assignee_Role_Project_Team__c != null &&
				    mapOTMRoles.get(projID).containsKey(oFRTask.Assignee_Role_Project_Team__c)){
					taskOwnerID = mapOTMRoles.get(projID).get(oFRTask.Assignee_Role_Project_Team__c);
				//process funding round team roles to determine task owner
				} else if(!mapFRTeamRoles.isEmpty() &&
						  oFRTask.Assignee_Role_Funding_Round_Team__c != null &&
						  mapFRTeamRoles.get(frID).containsKey(oFRTask.Assignee_Role_Funding_Round_Team__c)){
						  taskOwnerID = mapFRTeamRoles.get(frID).get(oFRTask.Assignee_Role_Funding_Round_Team__c);
				} else {
					taskOwnerID = oFRTask.Funding_Round__r.OwnerId;
				}
			} else if(oFRTask.Assignee_Type__c == 'External Task'){
				taskOwnerID = oPStage.Project__r.OwnerId;
			} else {
				taskOwnerID = oFRTask.Funding_Round__r.OwnerId;  //set to funding round mgr so as to be assigned
			}
		}
		//checklist task
		if(oPChecklist != null){
			projChecklistID = oPChecklist.Id;
			projID = oPChecklist.Project__c;
			frID = oFRTask.Funding_Round__c;

			System.debug('TASK CREATE CHECKLIST TYPE: ' + oFRTask.Assignee_Type__c);
			System.debug('TASK CREATE CHECKLIST ROLE: ' + oFRTask.Assignee_Role_Project_Team__c);
			
			if(oFRTask.Assignee_Type__c == 'Internal Task'){
				//process project team roles to determine task owner
				if(!mapOTMRoles.isEmpty() && 
				    oFRTask.Assignee_Role_Project_Team__c != null &&
				    mapOTMRoles.get(projID).containsKey(oFRTask.Assignee_Role_Project_Team__c)){
					taskOwnerID = mapOTMRoles.get(projID).get(oFRTask.Assignee_Role_Project_Team__c);
				//process funding round team roles to determine task owner
				} else if(!mapFRTeamRoles.isEmpty() &&
						  oFRTask.Assignee_Role_Funding_Round_Team__c != null &&
						  mapFRTeamRoles.get(frID).containsKey(oFRTask.Assignee_Role_Funding_Round_Team__c)){
						  taskOwnerID = mapFRTeamRoles.get(frID).get(oFRTask.Assignee_Role_Funding_Round_Team__c);
				} else {
					taskOwnerID = oFRTask.Funding_Round__r.OwnerId;
				}
			} else if(oFRTask.Assignee_Type__c == 'External Task'){
				taskOwnerID = oPChecklist.Project__r.OwnerId;
			} else {
				taskOwnerID = oFRTask.Funding_Round__r.OwnerId;  //set to funding round mgr so as to be assigned
			}
		}		
		//workflow task
		if(oProj != null){
			projID = oProj.Id;
			frID = oFRTask.Funding_Round__c;

			System.debug('TASK CREATE WORKFLOW TYPE: ' + oFRTask.Assignee_Type__c);
			System.debug('TASK CREATE WORKFLOW ROLE: ' + oFRTask.Assignee_Role_Project_Team__c);

			if(oFRTask.Assignee_Type__c == 'Internal Task'){
				//process project team roles to determine task owner
				if(!mapOTMRoles.isEmpty() && 
				    oFRTask.Assignee_Role_Project_Team__c != null &&
				    mapOTMRoles.get(projID).containsKey(oFRTask.Assignee_Role_Project_Team__c)){
					taskOwnerID = mapOTMRoles.get(projID).get(oFRTask.Assignee_Role_Project_Team__c);
				//process funding round team roles to determine task owner
				} else if(!mapFRTeamRoles.isEmpty() &&
						  oFRTask.Assignee_Role_Funding_Round_Team__c != null &&
						  mapFRTeamRoles.get(frID).containsKey(oFRTask.Assignee_Role_Funding_Round_Team__c)){
						  taskOwnerID = mapFRTeamRoles.get(frID).get(oFRTask.Assignee_Role_Funding_Round_Team__c);
	 			} else {	
					taskOwnerID = oFRTask.Funding_Round__r.OwnerId;
				}
			} else if(oFRTask.Assignee_Type__c == 'External Task'){
				taskOwnerID = oProj.OwnerId;
			} else {
				taskOwnerID = oFRTask.Funding_Round__r.OwnerId;  //set to funding round mgr so as to be assigned
			}
		}
		
        Project_Task__c newProjectTask = new Project_Task__c(Funding_Round_Task__c = oFRTask.Id
														   , Project__c = projID
														   , Project_Stage__c = projStageID
														   , Phase_Stage__c = projStageName
												 	       , Project_Checklist__c = projChecklistID
														   , Name = oFRTask.Name														   
														   , Due_Date__c = oFRTask.Target_Due_Date__c
														   , Description__c = oFRTask.Description__c
					   									   , Expected_Duration_Days__c = oFRTask.Expected_Duration_Days__c
														   , OwnerId = taskOwnerID); 
		system.debug('Creating task: ' + newProjectTask);
        return newProjectTask;
    }
}