@istest
private class LightningFileUploadHandler_Test {
    static testMethod void TestFileUpload(){
        Account acc = new Account(name = 'Test Account');
        insert acc;
        Set<Id> setCVId = new Set<Id>();
        ContentVersion cvABC =new ContentVersion(Title = 'cvABC',
                                                    PathOnClient= '/Test.abc',
                                                    VersionData = Blob.valueOf('Test Body'),
                                                    Origin = 'H');
        insert cvABC;
        setCVId.add(cvABC.Id);

        Id contentDocId;
        for(ContentVersion cv : [SELECT Id
                                        , Title
                                        , ContentDocumentId
                                    FROM ContentVersion
                                    WHERE Id IN :setCVId]){
            contentDocId = cv.ContentDocumentId;
        }
        ContentDocumentLink cdlFile = new ContentDocumentLink(LinkedEntityId = acc.Id,
                                                                                 ContentDocumentId = contentDocId,
                                                                                 ShareType = 'V');
		insert cdlFile;
        test.startTest();
        LightningFileUploadHandler.getFiles(acc.id);
        LightningFileUploadHandler.deleteFile(contentDocId);
        test.stopTest();
    }
}