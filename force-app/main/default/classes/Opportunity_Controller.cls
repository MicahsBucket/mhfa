/*
*   {purpose}   Manage Project creation functionality                     
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   10/10/16    Kevin Johnson DCS       Created
*   10/05/17    Kevin Johnson DCS       Updated constructor functionality to prohibit application post Funding Round close date
*   11/28/17    Kevin Johnson DCS       Updated createMFProject to include dynamic project number assignment functionality
*   01/30/18    Kevin Johnson DCS       Updated createMFProject to reflect new field requirements
*   02/28/18    Kevin Johnson DCS       Updated call to lockProjectNumbers to pass new opptyid using opptySaveResult.getId()
*   12/06/19    Eric Gronholz DCS       Updated creation of opportunity so CloseDate = today; CloseDate repurposed to be Open Date
*	03/30/2020	Kevin Johnson DCS		Added new fields to support application process
*   =============================================================================
*/

public with sharing class Opportunity_Controller {
    /*
    *{purpose}      Manages custom opportunity creation interfaces
    *
    *{function}     Populates Opportunity fields based on requirements relative to user creating said Opportunity
    *               Parameters:
    *                   -Multifamily = Multifamily/Multifamily developer
    *                   -...
    *                   retURL = default view for Funding Round
    *
    *{trigger}      Funding Round Custom button (Apply For Funding)
    */

    public Opportunity oOppty {get;set;}
    public Id fundingRoundId {get;set;}
    public Boolean isOpen {get;set;}
    public Id acctId {get; set;}
    public Date closeDate {get;set;}
    public String result;
    public String retURL;
    public String errMsg;
    public integer intProjNumToCreate;

    //constructor
    public Opportunity_Controller(ApexPages.StandardController controller){     
        oOppty = new Opportunity();
        fundingRoundId = ApexPages.currentPage().getParameters().get('frid');
        system.debug('FRID: ' + fundingRoundId);
        //determine if funding round is open
        List<Funding_Round__c> fundingRound = [SELECT Open_Date_Time__c
                                                    , Close_Date_Time__c
                                                    , Number_of_Projects_Assigned__c  
                                                    FROM Funding_Round__c
                                                    WHERE Id = :fundingRoundId];
        system.debug('FROPENDATETIME: ' + fundingRound);
        intProjNumToCreate = integer.valueof(fundingRound[0].Number_of_Projects_Assigned__c);
        if(system.now() >= fundingRound[0].Open_Date_Time__c &&
           system.now() <= fundingRound[0].Close_Date_Time__c){  
            isOpen = TRUE;
        } else {
            isOpen = FALSE;
        } 
        system.debug('ISOPEN: ' + isOpen);          
    }

    public void createMFProject(Boolean multiCreate){
        /*
        *{purpose}      Creates Multifamily Developer Project
        *
        *{function}     -Obtains and sets needed data in order to create new Project. 
        *               -Upon successful Project creation user is re-directed to Project else error is displayed.   
        *               
        *{trigger}      Funding Round 'Apply For Funding' button
        */
        errMsg = null;
        String currentPageURL = ApexPages.currentPage().getURL();
        List<Externally_Generated_IDs__c> lstProjNums = returnProjectNumbers();
        Database.SaveResult opptySaveResult;

        if(lstProjNums != null && lstProjNums.size() == intProjNumToCreate){  //if project numbers returned create project
            oOppty.Name = oOppty.Name;
            oOppty.Names_Development_Formerly_Known_As__c = oOppty.Names_Development_Formerly_Known_As__c;
            oOppty.Primary_Development_Street_Address__c = oOppty.Primary_Development_Street_Address__c;
            oOppty.Primary_Development_City__c = oOppty.Primary_Development_City__c;
            oOppty.Primary_Development_ZIP_Code__c = oOppty.Primary_Development_ZIP_Code__c;
            oOppty.Primary_Development_County__c = oOppty.Primary_Development_County__c;
			oOppty.Latitude_Longitude__latitude__s = oOppty.Latitude_Longitude__latitude__s;
			oOppty.Latitude_Longitude__longitude__s = oOppty.Latitude_Longitude__longitude__s;
            oOppty.Previously_applied_received_funding__c = oOppty.Previously_applied_received_funding__c;
            oOppty.Previous_D_Number__c = oOppty.Previous_D_Number__c;
			oOppty.Number_of_Units__c = oOppty.Number_of_Units__c;
			oOppty.Estimated_Total_Construction_Costs__c = oOppty.Estimated_Total_Construction_Costs__c;
			oOppty.Estimated_Total_Development_Costs__c = oOppty.Estimated_Total_Development_Costs__c;
			oOppty.Estimated_Number_of_Vouchers_Requested__c = oOppty.Estimated_Number_of_Vouchers_Requested__c;
            oOppty.StageName = 'Active';
            
            //EBG 20191206 - change Close Date to be the Opened Date and set value to current date
            //oOppty.CloseDate = returnFRCloseDate('Multifamily');
            oOppty.CloseDate = Date.today();
            
            oOppty.AccountId = returnOwnerAccount('Multifamily');
            oOppty.Funding_Round__c = fundingRoundId;
            oOppty.Project_Number__c = lstProjNums[0].Name;  //always be at least one project number
            if(lstProjNums.size() == 2){
                oOppty.Secondary_Project_Number__c = lstProjNums[1].Name;
            }
            if(lstProjNums.size() == 3){
                oOppty.Secondary_Project_Number__c = lstProjNums[1].Name;
                oOppty.Third_Project_Number__c = lstProjNums[2].Name;
            }
            oOppty.Developer_Name__c = oOppty.Developer_Name__c;
            oOppty.Developer_Street_Address__c = oOppty.Developer_Street_Address__c;
            oOppty.Developer_City__c = oOppty.Developer_City__c;
            oOppty.Developer_ZIP_Code__c = oOppty.Developer_ZIP_Code__c;
            oOppty.Developer_Contact_First_Name__c = oOppty.Developer_Contact_First_Name__c;
            oOppty.Developer_Contact_Last_Name__c = oOppty.Developer_Contact_Last_Name__c;
            oOppty.Developer_Contact_Email__c = oOppty.Developer_Contact_Email__c;
            oOppty.Developer_Contact_Phone__c = oOppty.Developer_Contact_Phone__c;
            oOppty.Project_Owner_Company__c = oOppty.Project_Owner_Company__c;  //KDJ 1/30/18: potential to delete
            oOppty.Project_Contact_First_Name__c = oOppty.Project_Contact_First_Name__c;  //KDJ 1/30/18: potential to delete
            oOppty.Project_Contact_Last_Name__c = oOppty.Project_Contact_Last_Name__c;  //KDJ 1/30/18: potential to delete
            oOppty.Project_Sponsor_Company__c = oOppty.Project_Sponsor_Company__c;
            oOppty.Project_Sponsor_Contact_First_Name__c = oOppty.Project_Sponsor_Contact_First_Name__c;
            oOppty.Project_Sponsor_Contact_Last_Name__c = oOppty.Project_Sponsor_Contact_Last_Name__c;
            oOppty.Project_Sponsor_Contact_Email__c = oOppty.Project_Sponsor_Contact_Email__c;
            oOppty.Project_Sponsor_Contact_Phone__c = oOppty.Project_Sponsor_Contact_Phone__c;
            oOppty.Project_Sponsor_Street_Address__c = oOppty.Project_Sponsor_Street_Address__c;
            oOppty.Project_Sponsor_City__c = oOppty.Project_Sponsor_City__c;
            oOppty.Project_Sponsor_ZIP_Code__c = oOppty.Project_Sponsor_ZIP_Code__c;
            oOppty.Processing_Agent_Company__c = oOppty.Processing_Agent_Company__c;
            oOppty.Processing_Agent_Contact_First_Name__c = oOppty.Processing_Agent_Contact_First_Name__c;
            oOppty.Processing_Agent_Contact_Last_Name__c = oOppty.Processing_Agent_Contact_Last_Name__c;
            oOppty.Processing_Agent_Contact_Email__c = oOppty.Processing_Agent_Contact_Email__c;

            opptySaveResult = Database.insert(oOppty, false);
            
            if(!opptySaveResult.isSuccess()){  //if error prompt
                Database.Error eOSR = opptySaveResult.getErrors().get(0);  //designed to process one record - projects will never be inserted en massse with this method
                errMsg = 'Error in creating Project: ' + eOSR.getStatusCode();
                result = currentPageURL;
            } else if(multiCreate == True){  //re-direct to new edit page               
                oOppty = new Opportunity();
                result = '/apex/Opportunity_New?frid=' + fundingRoundId;
                lockProjectNumbers(lstProjNums, opptySaveResult.getId());   //lock externally generated id upon succesfull project creation
            } else {  //re-direct to new project
                result = '/' + oOppty.Id;
                system.debug('ONE PROJECT: ' + result);
                lockProjectNumbers(lstProjNums, opptySaveResult.getId());   //lock externally generated id upon succesfull project creation
            }
        } else {  //if error return to edit page            
            errMsg = 'Error in generating Project IDs.  Please contact mhfa.app@state.mn.us';
            result = currentPageURL;
        }
    }

    public PageReference createOneMFProject(){
        createMFProject(False);

        if(errMsg != null){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.WARNING, errMsg));
            return null;
        } else {
            PageReference resultPage = new PageReference(result);
            resultPage.setRedirect(true);
            return resultPage;
        }
    }

    public PageReference createMultipleMFProject(){     
        createMFProject(True);

        if(errMsg != null){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.WARNING, errMsg));
            return null;
        } else {
            PageReference resultPage = new PageReference(result);
            resultPage.setRedirect(true);
            return resultPage;
        }
    }

    public List<Externally_Generated_IDs__c> returnProjectNumbers(){
        /*
        *{purpose}      Returns and assigns Oracle project numbers to new Project
        *
        *{function}     Obtains next two available Oracle Ids from Externally Generated IDs object and assigns them to new Project.
        *               
        *{trigger}      'Apply For Funding' link
        */
        List<Externally_Generated_IDs__c> projNumbers = new List<Externally_Generated_IDs__c>();

        projNumbers = [SELECT Id
                            , Name
                            , Assigned__c
                       FROM Externally_Generated_IDs__c
                       WHERE Assigned__c = FALSE
                       ORDER BY Name ASC
                       LIMIT :intProjNumToCreate];
        system.debug('PROJNUMBERS: ' + projNumbers);           
        if(projNumbers.size() != intProjNumToCreate){  //if error prompt user
            return null;
        } else {
            return projNumbers;
        }
    }

    public void lockProjectNumbers(List<Externally_Generated_IDs__c> lstIDs, ID opptyID){
        /*
        *{purpose}      Locks returned Oracle project numbers upon successful Project assignment
        *
        *{function}     Flags newly assigned Project numbers respective Externally Generated Id records as 'Assigned'.  
        *               
        *{trigger}      'Apply For Funding' link
        */
        List<Externally_Generated_IDs__c> egidToUpdate = new List<Externally_Generated_IDs__c>();

        for(Externally_Generated_IDs__c egid : lstIDs){  //build list of external id records to update to assigned
            egid.Assigned__c = TRUE;
            egid.Project__c = opptyID;
            egidToUpdate.add(egid);
        }

        try{
            update egidToUpdate;
        }

        catch(DmlException eEGID) {
            for(Externally_Generated_IDs__c egid : egidToUpdate) {
                errMsg = ' External ID record ' + egid.Name + ' not updated to Assigned: ' + eEGID.getMessage();
            }
        }
    }

    public String returnOwnerAccount(String userFunction){
        /*
        *{purpose}      Returns user's contact parent account
        *
        *{function}     Obtains users Account to be linked to Project. 
        *               
        *{trigger}      'Apply For Funding' link
        */
        String userType = UserInfo.getUserType();
        String userId = UserInfo.getUserId();
        acctId = null;

        if(userFunction == 'Multifamily'){
            List<User> oUser = [SELECT User.Contact.AccountId 
                                FROM User 
                                WHERE Id = :userId];

        acctId = oUser[0].Contact.AccountId;
        }

        return acctId;
    }

    public Date returnFRCloseDate(String userFunction){
        /*
        *{purpose}      Returns related Funding Round's close date
        *
        *{function}     Obtains related Funcing Round's close date to be set on Project. 
        *               
        *{trigger}      'Apply For Funding' link
        */
        closeDate = null;
        
        if(userFunction == 'Multifamily'){
            List<Funding_Round__c> oFR = [SELECT Close_Date__c
                                          FROM Funding_Round__c
                                          WHERE Id = :fundingRoundId];
        system.debug('TEST FRID: ' + fundingRoundId);
        system.debug('TEST CLOSE DATE: ' + oFR[0].Close_Date__c);
        closeDate = oFR[0].Close_Date__c.addMonths(24);
        }

        return closeDate;
    }

    //cancel button
    public PageReference cancelProject() {
		Id frid = apexpages.currentpage().getparameters().get('frid');       
		String retURL = '/' + frid;
		PageReference returnPage = new PageReference(retURL);
		returnPage.setRedirect(true);

        return returnPage;
    }
}