public without sharing class utilityMethods_DCS {

	//return all picklist values for the given object and field
	public static List<selectOption> getPickValues(Sobject object_name, String field_name, String first_val) {
	      List<selectOption> options = new List<selectOption>();
	      //if an initial value is provided, add it to the list first
	      if (first_val != null) {
	         options.add(new selectOption(first_val, first_val));
	      }
	      //grab the sobject that was passed
	      Schema.sObjectType sobject_type = object_name.getSObjectType(); 
	      //describe the sobject
	      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
	      //get a map of fields for the passed sobject
	      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
	      //grab the list of picklist values for the passed field on the sobject
	      List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); 
	      //for all values in the picklist list
	      for (Schema.PicklistEntry a : pick_list_values) { 
	            //add the value and label to our final list
	            options.add(new selectOption(a.getValue(), a.getLabel())); 
	      }
	      return options;
	}

	//static variables for preventing recursive triggers
	public static boolean firstTriggerTime_FundingRoundPhaseEventListener = true;
	public static boolean firstTriggerTime_ProjectPhaseEventListener = true;
	public static boolean firstTriggerTime_ProjectStageEventListener = true;

}