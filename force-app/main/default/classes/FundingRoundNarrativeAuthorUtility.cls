/*
*   {Purpose}  	Manage the functionality tied to Funding Round Narrative Author automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   03/21/18	Kevin Johnson DCS		Created
*	08/10/18	Kevin Johnson DCS		Added Final_Due_Date__c to logic
*   =============================================================================
*/

public with sharing class FundingRoundNarrativeAuthorUtility {
	public static String updateRelatedProjectNarrativeAuthors(List<Funding_Round_Narrative_Author__c> lstFRNA){
		/*
		*{purpose}		Manages data updates to related Poject Narrative Author records
		*
		*{function}		Fetches Project Narrative Author records related to updated Funding Round Narrative Author record.
		*				Returned Project Narrative Author records are updated to reflect the releated Funding Round Narrative Author 
		*				data update.
		*			
		*{trigger}  	FundingRoundNarrativeAuthorEventListener: after update
		*/
		
		Set<ID> setFRNAid = new Set<ID>();
		Map<Id, Funding_Round_Narrative_Author__c> mapFRNA = new Map<Id, Funding_Round_Narrative_Author__c>();
		List<Project_Narrative_Author__c> lstPNA = new List<Project_Narrative_Author__c>();
		List<Project_Narrative_Author__c> lstPNAtoUpdate = new List<Project_Narrative_Author__c>();
		String errorMsg = null;

		for(Funding_Round_Narrative_Author__c frna : lstFRNA){
			setFRNAid.add(frna.Id);
			mapFRNA.put(frna.Id, frna);
		}

		//fetch related project narrative author records and process update
		for(Project_Narrative_Author__c pna : [SELECT Id
													, Narrative_Due_Date__c
													, Narrative_Final_Date__c
													, Funding_Round_Narrative_Author__c
											   FROM Project_Narrative_Author__c
											   WHERE Funding_Round_Narrative_Author__c IN :setFRNAid]){

						Project_Narrative_Author__c pnaUpdate = new Project_Narrative_Author__c(Id = pna.Id,
				                                                                                Narrative_Due_Date__c = mapFRNA.get(pna.Funding_Round_Narrative_Author__c).Due_Date__c,
																								Narrative_Final_Date__c = mapFRNA.get(pna.Funding_Round_Narrative_Author__c).Final_Due_Date__c);

			lstPNAtoUpdate.add(pnaUpdate);
		}
		system.debug('lstPNAtoUpdate: ' + lstPNAtoUpdate);
		//update Project Narrative Author
		if(!lstPNAToUpdate.isEmpty()){
			Database.SaveResult[] updResult = Database.update(lstPNAToUpdate, false);				
			
			for(Database.SaveResult sr : updResult){
				if(!sr.isSuccess()){
					for(Database.Error err : sr.getErrors()){
						errorMsg = 'Error updating Narrative Author due date: ' + err.getStatusCode() + ' - ' + err.getMessage();
					}
				}
			}
		}
		return errorMsg;		
	}
}