@isTest
private class FundingRoundCLI_Controller_TEST {

	@isTest static void test_GeneralItems() {

		Funding_Round__c oFR = TestDataUtility.buildFundingRoundStructure();
		Funding_Round_Checklist_Item__c oFrCLI = [Select Id, Funding_Round__c 
													From Funding_Round_Checklist_Item__c 
													Where Funding_Round__c = :oFR.Id LIMIT 1];
		Test.setCurrentPage(Page.FundingRoundCLI_Edit);
		Test.startTest();											
   		FundingRoundCLI_Controller ext = new FundingRoundCLI_Controller(new ApexPages.StandardController(oFrCLI));
   		ext.getPhaseTabs();
   		ext.saveChanges();		
   		Test.stopTest();
	}

}