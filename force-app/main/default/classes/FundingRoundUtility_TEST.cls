@isTest
private class FundingRoundUtility_TEST {
	@isTest static void testFundingRoundUtility() {
		//stage custom settings
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		     FROM Enable_Triggers__mdt
						  		       		     WHERE MasterLabel = 'FundingRound'];
	
		//FUNDING ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

		//fetch & stage required funding round data
		Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																      FROM Funding_Round_Checklist_Item__c
																      WHERE Funding_Round__c = :oFundingRound.Id
																      AND Name = 'Pre Selection Checklist Item'];

		Funding_Round_Checklist_Item_Link__c oFundingRoundCheckistItemLink = TestDataUtility.createFundingRoundChecklistItemLink(oFundingRoundChecklistItem.Id);

		//PROJECT FRAMEWORK=====================================================================================================
		//create project
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

		//fetch & stage required data
		Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
															  FROM Project_Checklist__c
															  WHERE Name = 'Application'
															  AND Project__c = :oProject.Id];

		Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
															   , Name
														  FROM Project_Sub_Phase__c
														  WHERE Name = 'Application Submittals'
														  AND Project_Phase__r.Name = 'Application'
														  AND Project__c = :oProject.Id];		
		
		Project_Checklist_Item__c oProjectChecklistItem = TestDataUtility.createProjectChecklistItem(oProject.Id, 
																										oFundingRoundChecklistItem.Id,
																										false,
																										null,
																										null,
																										oProjectStagePreSelection.Id,
																										oProjectChecklistPreSelection.Id);
		
        List<Opportunity> lstOpptyUpdate = new List<Opportunity>();
        lstOpptyUpdate.add(oProject);
        
        //run tests
        Test.startTest();
        	oFundingRound.Show_Selection__c = TRUE;
        	update oFundingRound;

        	oFundingRound.Show_Selection__c = FALSE;
        	update oFundingRound;

			oProject.Project_Status__c = 'Select';
			update oProject;

			oFundingRound.Show_Selection__c = TRUE;
        	update oFundingRound;
        Test.stopTest();
	}
}