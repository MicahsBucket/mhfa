public class SK_LightningTreeCmpController {
	////////////////////////////////////////////////////
	// Code copied from https://gist.github.com/Sunil02kumar/c5110dd1448e80850aa55cd351b4c48b
	// Displays hierarchy for any sobject
	////////////////////////////////////////////////////
/*
	@AuraEnabled
    public static List<HierarchyData> findHierarchyData_Orig(string recId
																, string sObjectName
																, string parentFieldAPIname
																, string labelFieldAPIName
																, string additionalFields){
        List<HierarchyData> returnValue = new List<HierarchyData>();
        string queryString = 'select id, '+labelFieldAPIName+ ' ,' + parentFieldAPIname;

		if (additionalFields != null) {
			queryString = queryString + ',' + additionalFields;
		}
		queryString = queryString + ' from ' + sObjectName;
		System.debug('queryString: ' + queryString);
        //Section to get all child account details from ultimate parent starts-------------------------
        List<String> currentParent = new List<String>{};
        Integer level = 0;
        Boolean endOfStructure = false;
        //method to find ultimate parent of account
        string topMostparent = GetUltimateParentId(recId, sObjectName, parentFieldAPIname );
        currentParent.add(topMostparent);
        system.debug('**********topMostparent:'+ currentParent);
        //Loop though all children
        string finalQueryString = '';
        List<sObject> queryOutput = new List<sObject>();
        while ( !endOfStructure ){  
            if( level == 0 ){
                finalQueryString = queryString + ' where id IN : CurrentParent ORDER BY '+parentFieldAPIname +'  Limit 1000';
            } 
            else {
                finalQueryString = queryString + ' where ' + parentFieldAPIname + ' IN : CurrentParent ORDER BY ' + parentFieldAPIname + ' Limit 1000';
            }
            system.debug('********finalQueryString:'+finalQueryString);
            if(finalQueryString != null && finalQueryString !=''){
                try{
                    if(Limits.getLimitQueries()-Limits.getQueries()>0){
                        queryOutput = database.query(finalQueryString);
                    }else{
                        endOfStructure = true;
                    }
                }catch(exception ex){ 
                    endOfStructure = true;
                }
            }
            if( queryOutput.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                //iterating through query output
                for ( Integer i = 0 ; i < queryOutput.size(); i++ ){
                    sobject sb= queryOutput[i];
                    currentParent.add(string.valueof(sb.get('id')) );
                    HierarchyData ss = new HierarchyData();
                    if(sb.get('id') == recId || level == 0){
                        ss.expanded = true;
                    }else{
                        ss.expanded = false;
                    }
                    ss.rec = sb;
                    returnValue.add(ss);
                 }
            }
            level++;
        }
        system.debug('**********returnValue:'+returnValue);
        return returnValue;
    }
*/
	@AuraEnabled
    public static List<HierarchyData> findHierarchyData(string recId
														, string sObjectName
														, string parentFieldAPIname
														, string hierarchyFieldAPIname
														, string childRelationshipAPIname
														, string labelFieldAPIName
														, string additionalFieldDetails) {
		//the additionalFieldsDetails parameter is a comma-separated list of values, where each value is a colon-separated list of values
		// detailing a field in this format -> fieldApiName:displayText:displayStyle
		// Examples: 
		// * text_field__c:Text Field Label:text
		// * url_field__c:Link Name:url
		// * date_field__c:Start Date:date-local
		System.debug('recId: ' + recId
						+ '\nsObjectName: ' + sObjectName
						+ '\nparentFieldAPIname: ' + parentFieldAPIname
						+ '\nhierarchyFieldAPIname: ' + hierarchyFieldAPIname
						+ '\nchildRelationshipAPIname: ' + childRelationshipAPIname
						+ '\nlabelFieldAPIName: ' + labelFieldAPIName
						+ '\nadditionalFieldDetails: ' + additionalFieldDetails);
		string additionalFields = '';
		for (string fldDetails : additionalFieldDetails.split(',')) {
			List<String> fldParts = fldDetails.split(':');
			if (fldParts.size() > 0) {
				additionalFields += fldParts[0] + ',';
			}
		}
		additionalFields = additionalFields.removeEnd(',');

        List<HierarchyData> returnValue = new List<HierarchyData>();
		System.debug('parentFieldAPIname: ' + parentFieldAPIname + '  hierarchyFieldAPIname: ' + hierarchyFieldAPIname);
		if (parentFieldAPIname != null && parentFieldAPIname != '') {
			returnValue = SK_LightningTreeCmpController.findHierarchyDataForParent(recId
																					, sObjectName
																					, parentFieldAPIname
																					, hierarchyFieldAPIname
																					, childRelationshipAPIname
																					, labelFieldAPIName
																					, additionalFields);
			return returnValue;
		}

        string queryString = 'select id, ' + labelFieldAPIName+ ' ,' + hierarchyFieldAPIname;

		if (additionalFields != null) {
			queryString = queryString + ',' + additionalFields;
		}
		queryString = queryString + ' from ' + sObjectName;
		System.debug('queryString: ' + queryString);
        //Section to get all child account details from ultimate parent starts-------------------------
        List<String> currentParent = new List<String>{};
        Integer level = 0;
        Boolean endOfStructure = false;
        //method to find ultimate parent of account
        string topMostparent = GetUltimateParentId(recId, sObjectName, hierarchyFieldAPIname );
        currentParent.add(topMostparent);
        system.debug('**********topMostparent:'+ currentParent);
        //Loop through all children
        string finalQueryString = '';
        List<sObject> queryOutput = new List<sObject>();
        while ( !endOfStructure ){  
            if( level == 0 ){
                finalQueryString = queryString + ' where id IN : CurrentParent ORDER BY '+hierarchyFieldAPIname +'  Limit 1000';
            } 
            else {
                finalQueryString = queryString + ' where ' + hierarchyFieldAPIname + ' IN : CurrentParent ORDER BY ' + hierarchyFieldAPIname + ' Limit 1000';
            }
            system.debug('********finalQueryString:'+finalQueryString);
            if(finalQueryString != null && finalQueryString !=''){
                try{
                    if(Limits.getLimitQueries()-Limits.getQueries()>0){
                        queryOutput = database.query(finalQueryString);
						System.debug('queryOutput:\n' + queryOutput);
                    }else{
                        endOfStructure = true;
                    }
                }catch(exception ex){ 
                    endOfStructure = true;
                }
            }
            if( queryOutput.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                //iterating through query output
                for ( Integer i = 0 ; i < queryOutput.size(); i++ ){
                    sobject sb= queryOutput[i];
					System.debug('sb: ' + sb);
                    currentParent.add(string.valueof(sb.get('id')) );
                    HierarchyData ss = new HierarchyData();
                    if(sb.get('id') == recId || level == 0){
                        ss.expanded = true;
                    }else{
                        ss.expanded = false;
                    }
                    ss.rec = sb;
					if (additionalFields != null) {
						String strMetaText = '';
						for (String fieldName : additionalFields.split(',')) {
							fieldName = fieldName.trim();
							//strip "toLabel(...)" from the field name if it exists
							if (fieldName.contains('toLabel(')) {
								fieldName = fieldName.right(fieldName.length()-8);
								fieldName = fieldName.left(fieldName.length()-1);
							}
							strMetaText += sb.get(fieldName) + '; ';
						}
						strMetaText = strMetaText.removeEnd('; ');
						ss.metatext = strMetaText;
					}
                    returnValue.add(ss);
                 }
            }
            level++;
        }
        system.debug('**********returnValue:'+returnValue);
        return returnValue;
    }
    
	// Find the top most element in Hierarchy  
    // @return objId
    public static String GetUltimateParentId( string recId, string sObjectName, string parentFieldAPIname ){
        Boolean top = false;
        while ( !top ) {
            string queryString = 'select id , ' +parentFieldAPIname+ ' from '+sObjectName + ' where Id =:recId LIMIT 1';
            system.debug('**********queryString GetUltimateParentId:'+queryString);
            sobject sb = database.query(queryString);
            
            if ( sb.get(parentFieldAPIname) != null ) {
                recId = string.valueof(sb.get(parentFieldAPIname));
            }else {
                top = true;
            }
        }
        return recId ;
    }

    public class HierarchyData{
        @AuraEnabled
        public sObject rec{get;set;}
        @AuraEnabled
        public boolean expanded{get;set;}
		@AuraEnabled
		public string metatext{get;set;}
        public HierarchyData(){
            expanded = false;
        }
    }

	//find top level child records under the given parent object
	public static List<HierarchyData> findHierarchyDataForParent(Id parentRecId
																, string sObjectName
																, string parentFieldAPIname
																, string hierarchyFieldAPIname
																, string childRelationshipAPIname
																, string labelFieldAPIName
																, string additionalFields) {
        List<HierarchyData> returnValue = new List<HierarchyData>();
        string queryString = 'select id';
		if (childRelationshipAPIname != null) {
			queryString += ', (select id from ' + childRelationshipAPIname + ') ';
		}		
		queryString += ' from ' + sObjectName + ' where ' + hierarchyFieldAPIname + ' = null AND ' + parentFieldAPIname + ' = :parentRecId';
		System.debug('findHierarchyDataForParent.  queryString: ' + queryString);

		for (SObject sobj : database.query(queryString)) {
			System.debug('Processing id: ' + (Id)sobj.get('id'));
			//only retrieve records that are part of a hierarchy
			if (childRelationshipAPIname != null && sobj.getSObjects(childRelationshipAPIname) != null) {
				List<HierarchyData> lstRecs = SK_LightningTreeCmpController.findHierarchyData((Id)sobj.get('id')
																								, sObjectName
																								, null
																								, hierarchyFieldAPIname
																								, childRelationshipAPIname
																								, labelFieldAPIName
																								, additionalFields);
				returnValue.addAll(lstRecs);
			}
		}
		return returnValue;
	}
}