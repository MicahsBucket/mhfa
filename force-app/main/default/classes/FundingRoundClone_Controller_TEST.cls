@isTest
private class FundingRoundClone_Controller_TEST {
    
    @isTest static void testFundingRoundClone() {
		//FUNIDNG ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
			
		//SUPPORTING DATA=======================================================================================================
		//test user
        User oStandardUser = TestDataUtility.createUser('Multifamily Standard User',
                                                        'testUser@mhfatest.com');
		
		//funding round share: fetch role to share
        List<Group> oUserGroup = [SELECT Id
                                  FROM Group 
                                  WHERE Type = 'RoleAndSubordinates' 
                                  LIMIT 1];

        Funding_Round__Share newFRShare = new Funding_Round__Share();
            newFRShare.parentid = oFundingRound.Id;
            newFRShare.AccessLevel = 'Read';
            newFRShare.UserOrGroupId = oUserGroup[0].Id;
            newFRShare.RowCause = 'Manual';

        insert newFRShare;

		//funding round team member
        Funding_Round_Team_Member__c oFundingRoundTeamMember = TestDataUtility.createFundingRoundTeamMember(oFundingRound.Id, 
																											'LeadArchitect', 
																											 oStandardUser.Id);
		//funding round checklist item link
		Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																      FROM Funding_Round_Checklist_Item__c
																      WHERE Funding_Round__c =: oFundingRound.Id
																      LIMIT 1];

		Funding_Round_Checklist_Item_Link__c oFundingRoundChecklistItemLink = TestDataUtility.createFundingRoundChecklistItemLink(oFundingRoundChecklistItem.Id);

        Test.setCurrentPage(Page.FundingRound_Clone);
        FundingRoundClone_Controller ext = new FundingRoundClone_Controller(new ApexPages.StandardController(oFundingRound));

        //run tests
        Test.startTest();
            PageReference pageRef = ext.cloneFundingRound();
        Test.stopTest();
    }
}