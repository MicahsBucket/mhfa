/*
*   {purpose}  	Manage the functionality tied to Project Document: Customer automation                
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   05/29/18	Kevin Johnson DCS		Created
*   =============================================================================
*/

public without sharing class ProjectDocumentCustomerUtility  {
	//without sharing on class to ensure users have capability to insert sharing records
	public static String validateRecordParameters(List<Project_Documents_Customer__c> lstPDC){
		/*
		*{purpose}		Manages validation of Project Document: Customer records being added
		*
		*{function}		Builds list of Project Document: Customer (PDC) records per parent Project for use in validating attributes of new PDC record.  
		*				If a PDC record (inserted new/updated) does not meet requirements an error is passed notifying user of flagged validation.
		*				
		*{trigger}  	ProjectDocumentCustomerEventListener: before insert, before update
		*/

		Set<ID> setProjectIds = new Set<ID>();
		Set<ID> setPDCIds = new Set<ID>();
		for(Project_Documents_Customer__c pdc : lstPDC){
			setPDCIds.add(pdc.Id);
			setProjectIds.add(pdc.Project__c);
		}

		Map<Id, List<String>> mapPDC = new Map<Id, List<String>>();

		//compile existing pdc records
		for(Project_Documents_Customer__c oPDC : [SELECT Project__c
													   , Type__c
											     FROM Project_Documents_Customer__c
												 WHERE Project__c IN :setProjectIds
											     AND Id NOT IN :setPDCIds]) {
					if(mapPDC.containsKey(oPDC.Project__c)) {
						List<String> custDoc = mapPDC.get(oPDC.Project__c);
						custDoc.add(oPDC.Type__c);
						mapPDC.put(oPDC.Project__c, custDoc);
					} else {
						mapPDC.put(oPDC.Project__c, new List<String> {oPDC.Type__c});
					}
		}
		system.debug('MAPPDC: ' + mapPDC);

		String errorMsg = null;
		for(Project_Documents_Customer__c pdc : lstPDC){
			//validate if project document: customer record already exists for parent project
			List<String> lstExistingDocuments = mapPDC.get(pdc.Project__c);
			system.debug('LSTEXISTINGDOCUMENT: ' + lstExistingDocuments);
			Set<String> setExistingDocuments = new Set<String>();
			if(lstExistingDocuments != null){
				setExistingDocuments.addAll(lstExistingDocuments);
				
				String addedDocument = pdc.Type__c;
				if(setExistingDocuments.contains(addedDocument)){
					errorMsg = 'Document ' + addedDocument + ' already exists for this Project and thus cannot be added.';
					break;
				}
			}			
		}
		return errorMsg;		
	}
}