@isTest 
private class characteristicTaskLookup_Cntrl_TEST {

	@isTest static void test_characteristicTaskLookup() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
		for (Funding_Round_Trigger__c oChar : [Select Id 
												From Funding_Round_Trigger__c 
												Where Trigger_Group__r.Funding_Round__c = :oFundingRound.Id]) {
			characteristicTaskLookup_Cntrl.getCharacteristicTasks(oChar.Id);
		}
	}
	
}