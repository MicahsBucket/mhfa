public without sharing class FundingRoundPhaseUtility {
/*
	//Commented out by EBG/DC on 20200313 - phase are no longer submitted

	public static void validateSubmissionRules(List<Phase__c> lstNewPhases, Map<Id, Phase__c> mapOldPhases) {

		Set<Id> setPhaseId_SubmitRequired = new Set<Id>();
		Set<Id> setParentIds = new Set<Id>();
		Map<Id, String> mapPhaseErrorReason = new Map<Id, String>();

		for (Phase__c oPhase : lstNewPhases) {
			//Phase changed from not requiring submission to requiring submission
			if (oPhase.Parent_Phase__c == null
					&& mapOldPhases != null
					&& oPhase.Require_Developer_Submission__c == true) {
				setPhaseId_SubmitRequired.add(oPhase.Id);
			}

			//Sub-phase added so hold parent id to determine if the parent has other sub-phases
			if (oPhase.Parent_Phase__c != null
					&& mapOldPhases == null) {
				setParentIds.add(oPhase.Parent_Phase__c);
			}
		}

		system.debug('setPhaseId_SubmitRequired: ' + setPhaseId_SubmitRequired);
		system.debug('setParentIds: ' + setParentIds);


		//if a phase now requires developer submission, make sure it has no more than one sub-phase
		if (setPhaseId_SubmitRequired.size() > 0) {
			system.debug('Checking phase submission');
			for (Phase__c oParentPhase : [Select Id
												, (Select Id From SubPhases__r) 
											From Phase__c 
											Where Id in :setPhaseId_SubmitRequired]) {
				if (oParentPhase.SubPhases__r.size() > 1) {
					mapPhaseErrorReason.put(oParentPhase.Id, 'Enable Phase Submission Error');
				}
			}

			for (Phase__c oUpdatePhase : lstNewPhases) {
				if (mapPhaseErrorReason.containsKey(oUpdatePhase.Id)) {
					oUpdatePhase.Sub_Phase_Error_Reason__c = mapPhaseErrorReason.get(oUpdatePhase.Id);
				}
			}
		}

		//if a sub-phase is added to a phase requiring developer submission, make sure its parent does not already have sub-phases
		if (setParentIds.size() > 0) {
			system.debug('Processing sub-phase insert');
			for (Phase__c oParentPhase : [Select Id
												, (Select Id From SubPhases__r) 
											From Phase__c 
											Where Id in :setParentIds
												And Require_Developer_Submission__c = true]) {
				if (oParentPhase.SubPhases__r.size() > 0) {
					system.debug('Subphases: ' + oParentPhase.SubPhases__r.size());
					mapPhaseErrorReason.put(oParentPhase.Id, 'Add Multiple Sub-Phase Error');
				}
			}
			system.debug('mapPhaseErrorReason: ' + mapPhaseErrorReason);
			for (Phase__c oUpdatePhase : lstNewPhases) {
				if (mapPhaseErrorReason.containsKey(oUpdatePhase.Parent_Phase__c)) {
					oUpdatePhase.Sub_Phase_Error_Reason__c = mapPhaseErrorReason.get(oUpdatePhase.Parent_Phase__c);
				}
			}			
		}
	}
*/

/*
	//Commented out by EBG/DC on 20200313 - phase due dates calculated via updateDueDates_PhaseBaseline and updateDueDates_StageDuration
	
	//////////////////////////////////
	// 20190729 EBG - if a phase's due date changes, update the start date of the phase immediately after it
	//					to match the new due date of the prior phase
	//////////////////////////////////
	public static void updatePhaseStartDates(List<Phase__c> lstPhases, Map<Id, Phase__c> mapOldPhases) {
		Id recTypeId_Phase = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Phase').getRecordTypeId();
		Map<Id, Map<Integer, Datetime>> mapFrPhaseStartDate = new Map<Id, Map<Integer, Datetime>>();
		for (Phase__c oPhase : lstPhases) {
			if (oPhase.RecordTypeId == recTypeId_Phase
					&& oPhase.End_Date__c != mapOldPhases.get(oPhase.Id).End_Date__c && oPhase.Sort_Order__c != null) {
				Map<Integer, Datetime> mapPhaseStartDate = new Map<Integer, Datetime>();
				if (mapFrPhaseStartDate.containsKey(oPhase.Funding_Round__c)) {
					mapPhaseStartDate = mapFrPhaseStartDate.get(oPhase.Funding_Round__c);
				}
				//hold the start date for the next phase in the sort order
				mapPhaseStartDate.put(Integer.valueOf(oPhase.Sort_Order__c + 1), oPhase.End_Date__c);
				mapFrPhaseStartDate.put(oPhase.Funding_Round__c, mapPhaseStartDate);
			}
		}
		if (mapFrPhaseStartDate.size() == 0) {
			return;
		}

		System.debug('mapFrPhaseStartDate: ' + mapFrPhaseStartDate);

		//find the phases under the funding rounds in the map and update the start date for the phases
		// with the same sort order as specified above
		List<Phase__c> lstUpdate = new List<Phase__c>();
		for (Phase__c oPhaseUpdate : [Select Id, Sort_Order__c, Start_Date__c, End_Date__c, Funding_Round__c
										From Phase__c 
										Where Funding_Round__c in :mapFrPhaseStartDate.keySet()
											And RecordType.Name = 'Phase'
										Order By Sort_Order__c]) {
			Map<Integer, Datetime> mapSortOrderStartDate = mapFrPhaseStartDate.get(oPhaseUpdate.Funding_Round__c);
			System.debug('mapSortOrderStartDate: ' + mapSortOrderStartDate + '\nCurrent Phase: ' + oPhaseUpdate);
			if (mapSortOrderStartDate.containsKey(Integer.valueOf(Integer.valueOf(oPhaseUpdate.Sort_Order__c)))) {
				oPhaseUpdate.Start_Date__c = mapSortOrderStartDate.get(Integer.valueOf(oPhaseUpdate.Sort_Order__c));
				lstUpdate.add(oPhaseUpdate);
			}
		}
		System.debug('lstUpdate: ' + lstUpdate);
		if (lstUpdate.size() > 0) {
			update lstUpdate;
		}
	}
*/	
	//////////////////////////////////
	// 20190502 EBG - update task dates if phase due date changes
	//////////////////////////////////
	public static void updateTaskDueDates(List<Phase__c> lstPhases, Map<Id, Phase__c> mapOldPhases) {
		List<String> lstJsonTasks = new List<String>();
		Set<Id> setPhaseIds = new Set<Id>();
		for (Phase__c oPhase : lstPhases) {
			if (oPhase.Parent_Phase__c == NULL 
					&& oPhase.End_Date__c != mapOldPhases.get(oPhase.Id).End_Date__c) {
				setPhaseIds.add(oPhase.Id);
			}
		}

		if (setPhaseIds.size() > 0) {
			for (Funding_Round_Task__c oFRT : [Select Id
													, Name
													, Stage_Lookup__c
													, Phase__c, Stage__c
													, Funding_Round__c
													, Task_Item__c
													, Due_Date_Baseline__c
													, Target_Due_Date__c
													, Expected_Duration_Days__c
											From Funding_Round_Task__c
											Where Stage_Lookup__r.Parent_Phase__c IN :setPhaseIds]) {
				lstJsonTasks.add(JSON.serialize(oFRT));
			}
		}
		if (lstJsonTasks.size() > 0) {
			FundingRoundPhaseUtility.updateTasks_Future(lstJsonTasks);
		}
	}
	
	///////////////////////////////////////////////////////////
	// update tasks in a future method so we can query the
	// phase/stage associated to the task which was updated.
	///////////////////////////////////////////////////////////
	@future
	public static void updateTasks_Future(List<String> lstJsonTasks) {
		List<Funding_Round_Task__c> lstTasks = new List<Funding_Round_Task__c>();
		for (String strTask : lstJsonTasks) {
			lstTasks.add((Funding_Round_Task__c)JSON.deserialize(strTask, Funding_Round_Task__c.class));
		}
		if (lstTasks.size() > 0) {
			update lstTasks;
		}
	}

	///////////////////////////////////////////////////////////
	// When a checklist is created, find the Funding Round Phase
	//  record matching the selected phase name
	///////////////////////////////////////////////////////////
	public static void assignPhaseLookup(List<Phase_Checklist__c> lstChecklists) {
		Set<String> setPhaseNames = new Set<String>();
		Set<Id> setFRIds = new Set<Id>();
		for (Phase_Checklist__c oChecklist : lstChecklists) {
			setFRIds.add(oChecklist.Funding_Round__c);
			setPhaseNames.add(oChecklist.Phase__c);	
			if (oChecklist.Phase__c == null) {
				oChecklist.Funding_Round_Phase__c = null;
			}		
		}

		for (Phase__c oPhase : [Select Id, Funding_Round__c, Phase__c
								From Phase__c
								Where RecordType.Name = 'Phase'
									And Funding_Round__c in :setFRIds
									And Phase__c in :setPhaseNames]) {
			for (Phase_Checklist__c oChecklist : lstChecklists) {
				if (oChecklist.Phase__c == oPhase.Phase__c
						&& oChecklist.Funding_Round__c == oChecklist.Funding_Round__c) {
					oChecklist.Funding_Round_Phase__c = oPhase.Id;
				}
			}
		}
	}

/*
	//Commented out by EBG/DC on 20200313 - phase due dates calculated via updateDueDates_PhaseBaseline and updateDueDates_StageDuration

	///////////////////////////////////////////////////////////
	//20191016 EBG - These routines ensure the resulting due date does not land on a holiday or weekend
	//				When a change is made to phase's Due Date, recalculate stage due dates for all child stages
	//				When a change is made to a stage's Days Offset, recalculate the due date
	///////////////////////////////////////////////////////////

	//called from after update trigger on Phase
	public static void updateStageDueDates_PhaseChange(List<Phase__c> lstNewRecs, map<Id, Phase__c> mapOldRecs) {
		Id recTypeId_Phase = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Phase').getRecordTypeId();
		Map<Id, Date> mapPhaseDueDate = new Map<Id, Date>();

		for (Phase__c oRec : lstNewRecs) {
			//if Phase due date changed, hold the phase record so we can update all stage records
			if (oRec.RecordTypeId == recTypeId_Phase
					&& mapOldRecs != null
					&& mapOldRecs.get(oRec.Id).End_Date__c != oRec.End_Date__c) {
				mapPhaseDueDate.put(oRec.Id, Date.valueOf(oRec.End_Date__c));
			}
		}

		//retrieve the stages for the phases updated
		List<Phase__c> lstStageUpdates = new List<Phase__c>();
		for (Phase__c oStage : [Select Id
										, Parent_Phase__c
										, End_Date__c
										, Due_Date_Offset__c
										, Not_Applicable__c
										, RecordType.Name 
								From Phase__c 
								Where RecordType.Name = 'Stage'
									And Parent_Phase__c in :mapPhaseDueDate.keySet()]) {
			Date dtDueDate = Date.valueOf(mapPhaseDueDate.get(oStage.Parent_Phase__c));
			if (oStage.Due_Date_Offset__c != null && dtDueDate != null) {
				dtDueDate = dtDueDate.addDays(Integer.valueOf(oStage.Due_Date_Offset__c) * -1);
				//ensure the end date doesn't land on a weekend or holiday
				if (DateUtility.isBusinessDay(dtDueDate) == false) {
					dtDueDate = DateUtility.subtractBusinessDaysFromDate(dtDueDate, 1);
				}
			}
			//convert back to a datetime
			Datetime dttPhaseDueDate = mapPhaseDueDate.get(oStage.Parent_Phase__c);
			if (dttPhaseDueDate == null || oStage.Not_Applicable__c) {
				oStage.End_Date__c = null;
			} else {
				oStage.End_Date__c = Datetime.newInstance(dtDueDate.year()
															, dtDueDate.month()
															, dtDueDate.day()
															, dttPhaseDueDate.hour()
															, dttPhaseDueDate.minute()
															, dttPhaseDueDate.second());
			}
			lstStageUpdates.add(oStage);
		}

		if (lstStageUpdates.size() > 0) {
			update lstStageUpdates;
		}
	}
*/

	//called from before insert/update trigger on phase (stage)
	public static void updateStageDueDate_StageChange(List<Phase__c> lstNewRecs, map<Id, Phase__c> mapOldRecs) {
		Id recTypeId_Stage = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Stage').getRecordTypeId();
		Map<Id, Phase__c> mapStageUpdates = new Map<Id, Phase__c>();

		Set<Id> setPhaseIds = new Set<Id>();
		for (Phase__c oStage : lstNewRecs) {
			if (oStage.RecordTypeId == recTypeId_Stage
					&& oStage.Parent_Phase__c != null) {
				setPhaseIds.add(oStage.Parent_Phase__c);
			}
		}

		//fetch the parent phase due date
		Map<Id, Datetime> mapPhaseDueDate = new Map<Id, Datetime>();
		for (Phase__c oPhase : [Select Id, End_Date__c From Phase__c Where Id in :setPhaseIds]) {
			mapPhaseDueDate.put(oPhase.Id, oPhase.End_Date__c);
		}

		for (Phase__c oRec : lstNewRecs) {
			//if Stage is created or offset changed, set the due date based on the parent phase's due date
			if (oRec.RecordTypeId == recTypeId_Stage
					&& oRec.Parent_Phase__c != null
					&& mapPhaseDueDate.containsKey(oRec.Parent_Phase__c)
					&& (mapOldRecs == null
						|| (mapOldRecs.get(oRec.Id).Due_Date_Offset__c != oRec.Due_Date_Offset__c))) {
				Date dtDueDate = Date.valueOf(mapPhaseDueDate.get(oRec.Parent_Phase__c));
				if (oRec.Due_Date_Offset__c != null && dtDueDate != null) {
					dtDueDate = dtDueDate.addDays(Integer.valueOf(oRec.Due_Date_Offset__c) * -1);
					//ensure the end date doesn't land on a weekend or holiday
					if (DateUtility.isBusinessDay(dtDueDate) == false) {
						dtDueDate = DateUtility.subtractBusinessDaysFromDate(dtDueDate, 1);
					}
				}
				//convert back to a datetime
				Datetime dttPhaseDueDate = mapPhaseDueDate.get(oRec.Parent_Phase__c);
				if (dttPhaseDueDate == null || oRec.Not_Applicable__c) {
					oRec.End_Date__c = null;
				} else {
					oRec.End_Date__c = Datetime.newInstance(dtDueDate.year()
															, dtDueDate.month()
															, dtDueDate.day()
															, dttPhaseDueDate.hour()
															, dttPhaseDueDate.minute()
															, dttPhaseDueDate.second());
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 20191217 EBG DC - reset FR phase and stage start and due dates when the FR Open Date changes
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void updateDueDates_FROpenDate(List<Funding_Round__c> lstNewFRs, Map<Id, Funding_Round__c> mapOldFRs) {
		if (mapOldFRs != null) {
			for (Funding_Round__c oFR : lstNewFRs) {
				if (oFR.Open_Date_Time__c != mapOldFRs.get(oFR.Id).Open_Date_Time__c) {
					//reset all phase and stage dates based on the new FR Open Date
					FundingRoundUtility.assignPhaseStageDueDates(oFR.Id, 1, 1);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 20191217 EBG DC - reset FR phase and stage start and due dates when the Phase Due Date Baseline changes
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void updateDueDates_PhaseBaseline(List<Phase__c> lstNewPhases, Map<Id, Phase__c> mapOldPhases) {		
		if (mapOldPhases != null) {
			Id recTypeId_Phase = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Phase').getRecordTypeId();
			for (Phase__c oPhase : lstNewPhases) {
				if (oPhase.RecordTypeId == recTypeId_Phase
						&& oPhase.Due_Date_Baseline__c != mapOldPhases.get(oPhase.Id).Due_Date_Baseline__c) {
					//reset phase and stage dates based on the current and subsequent phases
					FundingRoundUtility.assignPhaseStageDueDates(oPhase.Funding_Round__c, oPhase.Sort_Order__c, 1);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 20191217 EBG DC - reset FR phase and stage start and due dates when the Stage Estimated Days to Complete changes
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void updateDueDates_StageDuration(List<Phase__c> lstNewStages, Map<Id, Phase__c> mapOldStages) {
		if (mapOldStages != null) {
			Set<Id> setStageIds = new Set<Id>();
			Id recTypeId_Stage = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Stage').getRecordTypeId();
			for (Phase__c oStage : lstNewStages) {
				if (oStage.RecordTypeId == recTypeId_Stage
						&& (oStage.Estimated_Days_to_Complete__c != mapOldStages.get(oStage.Id).Estimated_Days_to_Complete__c
							|| oStage.Not_Applicable__c != mapOldStages.get(oStage.Id).Not_Applicable__c)) {
					setStageIds.add(oStage.Id);
				}
			}
			//requery the data so we get the phase and funding round info
			for (Phase__c oLookupStage : [Select Id
												, Sort_Order__c
												, Parent_Phase__c
												, Parent_Phase__r.Sort_Order__c
												, Parent_Phase__r.Funding_Round__c
												, Not_Applicable__c
											From Phase__c
											Where Id in :setStageIds
											Order By Parent_Phase__r.Sort_Order__c, Sort_Order__c]) {
				//reset phase and stage dates based on the current and subsequent stages
				FundingRoundUtility.assignPhaseStageDueDates(oLookupStage.Parent_Phase__r.Funding_Round__c
															, oLookupStage.Parent_Phase__r.Sort_Order__c
															, oLookupStage.Sort_Order__c);

			}
		}
	}
}