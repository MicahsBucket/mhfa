/*
*   {purpose}   Manage APEX Sharing functionality accross objects                   
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   2/21/17     Kevin Johnson DCS       Created
*   2/20/20     Kevin Johnson DCS       Added 'Role (Additional)' 2 to sharing framework
*   =============================================================================
*/

public without sharing class APEXSharingUtility {
    //without sharing enabled being users require access to insert sharing records against records they may not have full access to
    public static string processAPEXSharing(List<SObject> lstObj, Set<ID> setOpptyID, Map<ID, ID> mapObjIDOpptyID, String objectToProcess){     
        /*
        *{purpose}      Manages APEX sharing - called when record is inserted against a project
        *
        *{function}     Builds list of opportunity team members and builds APEX sharing records to insert respective to objects detailed by custom setting.
        *               
        *{trigger}      ProjectChecklistItemEventListener: afterinsert
        *               ProjectPhaseDecisionEventListener: afterinsert
        *               ProjectWorkbookVersionEventListener: afterinsert   
        */
        List<APEX_Sharing_Rule__mdt> csSharingRules = [SELECT MasterLabel
                                                            , Object__c
                                                            , Role__c
                                                            , Role_Additional__c
															, Role_Additional_2__c
                                                            , Access_level__c
                                                            , Relationship_To_Project__c
                                                      FROM APEX_Sharing_Rule__mdt
                                                      WHERE Object__c = :objectToProcess];
        system.debug('CUSTOM SETTING: ' + csSharingRules);
        String projectRelationship = csSharingRules[0].Relationship_To_Project__c;
        Set<ID> setUserID = new Set<ID>();

        //system.debug('OPPTYIDS: ' + setOpptyID);

        //return team members related to project that record is linked to
        List<OpportunityTeamMember> lstOTM = new List<OpportunityTeamMember>();
        for(OpportunityTeamMember otm : [SELECT OpportunityId
                                              , UserId
                                              , TeamMemberRole                                         
                                        FROM OpportunityTeamMember 
                                        WHERE OpportunityId = :setOpptyID]){
            setUserID.add(otm.UserId);
            lstOTM.add(otm);
        }   
        //system.debug('SETUSERID: ' + setUserID);
        //system.debug('LSTOTM: ' + lstOTM);

        List<sObject> lstShareInsert = new List<sObject>();
        String errorMsg = null;

        String dataObject = csSharingRules[0].Object__c + '__c';
        String shareObject = csSharingRules[0].Object__c + '__Share';
        String shareAccess = csSharingRules[0].Access_Level__c;                 
        Set<String> setRoles = new Set<String>();  //cannot use string contains as it will return fuzzy matches
        String[] splitrole = csSharingRules[0].Role__c.split(',');           
        //accommodate if additional space is needed for roles
        if(csSharingRules[0].Role_Additional__c != null){
            String[] splitroleadditional = csSharingRules[0].Role_Additional__c.split(',');
            splitrole.addall(splitroleadditional);
        }
		if(csSharingRules[0].Role_Additional_2__c != null){
            String[] splitroleadditional = csSharingRules[0].Role_Additional_2__c.split(',');
            splitrole.addall(splitroleadditional);
        }

        for(String r : splitrole){
            setRoles.add(r);
        }
        //system.debug('LSTOBJ: ' + lstObj);
        sObject sobjShareRecordMain = Schema.getGlobalDescribe().get(shareObject).newSObject();                     
        for(SObject oData : lstObj){  //process records passed from trigger
            for(OpportunityTeamMember otm : lstOTM){  //process team members
                system.debug('OTM USERID: ' + otm.UserId);
                system.debug('ODATA OWNERID: ' + oData.get('OwnerId'));
                system.debug('OTM TEAM MEMBER ROLE: ' + otm.TeamMemberRole);
                system.debug('OTM SET ROLES: ' + setRoles);
                system.debug('ROLE CONTAINS OTM ROLE: ' + setRoles.contains(otm.TeamMemberRole));
                if(setRoles.contains(otm.TeamMemberRole) && 
                                     otm.OpportunityId == mapObjIDOpptyID.get(string.valueof(oData.get('Id'))) &&                                                
                                     otm.UserId != oData.get('OwnerId')){                       
					sObject sobjShareRecord = sobjShareRecordMain.clone();
                    sobjShareRecord.put('ParentId', oData.Id);
                    sobjShareRecord.put('AccessLevel', shareAccess);
                    sobjShareRecord.put('UserOrGroupId', otm.UserId);
                    sobjShareRecord.put('RowCause', 'Project_Team__c');

                    lstShareInsert.add(sobjShareRecord);    
                }
            }                           
        }
        system.debug('LSTSHARETOINSERT: ' + lstShareInsert);
        if(!lstShareInsert.isEmpty()){
            Database.SaveResult[] insResult = Database.insert(lstShareInsert, false);
            
            for(Database.SaveResult sr : insResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding share: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
    }
}