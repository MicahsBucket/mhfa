@isTest
private class  ProjectPhaseDecisionUtility_TEST {
	@isTest static void testProjectPhaseDecisionUtility() {
		//stage custom settings
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		     FROM Enable_Triggers__mdt
						  		       		     WHERE MasterLabel = 'ProjectPhaseDecision'];
	
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //read only user
        User roUser = TestDataUtility.createUser('Read Only',
                                                'testCDLU@mhfatest.com');

        //run tests
        Test.startTest();
            Project_Phase_Decision__c pdd1 = TestDataUtility.createProjectPhaseDecision(p.Id
                                                                                     , '811'
                                                                                     , 'Eligible');
  
            pdd1.Decision__c = 'Not Applicable';
            update pdd1;

            Project_Phase_Decision__c pdd2 = TestDataUtility.createProjectPhaseDecision(p.Id
                                                                                     , 'Preservation'
                                                                                     , 'Eligible');

        	pdd2.Decision__c = 'Not Applicable';
        	update pdd2;
            
            

            try{ 
                Project_Phase_Decision__c pdd4 = TestDataUtility.createProjectPhaseDecision(p.Id
                                                                                           , '811'
                                                                                           , 'Eligible');                               
                List<Project_Phase_Decision__c> lstPPDToInsert = new List<Project_Phase_Decision__c>();
                lstPPDToInsert.add(pdd4);
                Database.SaveResult[] insResult = Database.Insert(lstPPDToInsert, true);                
            } catch(Exception e){
                system.assert(e.getMessage().contains('Type 811 already exists for this Project and thus cannot be added'),e.getMessage());
            }

            //negative dml test case:
            System.runAs(roUser){                
                try{ 
                    Project_Phase_Decision__c pdd3 = TestDataUtility.createProjectPhaseDecision(p.Id
                                                                                               , '811'
                                                                                               , 'Eligible');                               
                    pdd3.Decision_Type__c = 'Preservation';
                    pdd3.Decision__c = 'Eligible';
                    List<Project_Phase_Decision__c> lstPPDToUpdate = new List<Project_Phase_Decision__c>();
                    lstPPDToUpdate.add(pdd3);
                    Database.SaveResult[] updResult = Database.Update(lstPPDToUpdate, true);                
                } catch(Exception e){
                    system.assert(e.getMessage().contains('insufficient access rights on cross-reference id'),e.getMessage());
                }
            }
        Test.stopTest();
	}
}