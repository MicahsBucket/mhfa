@isTest
public class ProjectChecklistItemPhaseUtility_TEST {
	@isTest static void testProjectChecklistItemPhaseUtility() {
    //stage custom settings
    //insert RunTrigger
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       		     FROM Enable_Triggers__mdt
					  		       		     WHERE MasterLabel = 'ProjectChecklistItemPhase'];

    //stage data
    //create funding round
    Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                             system.Now().addMonths(9));                     
    //create fr phase
    Phase__c frp = TestDataUtility.createFundingRoundPhase('Pre-Construction Due Diligence', 
                                                           system.Now(), 
                                                           system.Now().addMonths(6), 
                                                           1, 
                                                           'phase',
                                                           fr.Id, 
                                                           null);
    //create fr sub phase
    Phase__c frsp = TestDataUtility.createFundingRoundPhase('Credit Approval',
                                                             system.Now(), 
                                                             system.Now().addMonths(6),                                               
                                                              1, 
                                                              'subphase',
                                                              fr.Id, 
                                                              frp.Id);
    //create project
    Opportunity p = TestDataUtility.createProject(fr.Id);

    //create user for otm
    User otmUser1 = TestDataUtility.createUser('Multifamily Standard User',
                                                    'testOTMPCLI1@mhfatest.com');
    User otmUser2 = TestDataUtility.createUser('Multifamily Standard User',
                                                    'testOTMPCLI2@mhfatest.com');
    User otmUser3 = TestDataUtility.createUser('Multifamily Standard User',
                                                    'testOTMPCLI3@mhfatest.com');
    //create otm    
    OpportunityTeamMember otm1 = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                             'Loan Processor', 
                                                                              otmUser1.Id);
        
    OpportunityTeamMember otm2 = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                             'Architect', 
        																	  otmUser2.Id);
    OpportunityTeamMember otm3 = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                             'HDO',
        																	  otmUser3.Id);
	//create pcli
    Project_Checklist_Item__c pcli = TestDataUtility.createProjectChecklistItem(p.id, 
                                                                                null,
                                                                                false,
                                                                                null,
                                                                                null);
      
    //create pclip
    Project_Checklist_Item_Phase__c pclip = TestDataUtility.createProjectChecklistItemPhase(pcli.id,
                                                											frp.Id,
                                                											false);

    //run tests                                                        
    Test.startTest();
		//create pclisp - test roles that are represented on project team
		Project_Checklist_Item_Phase__c pclisp = TestDataUtility.createProjectChecklistItemSubPhase(pcli.id, 
                                                  													frp.Id,
                                                  													frsp.Id,
																									FALSE,
                                                  													'LoanProcessor',
                                                  													'Architect',
                                                  													'HDO');
		//delete otm - test roles that are not represented on project team
		delete otm1;
		delete otm2;
		delete otm3;
		//update pclisp
		pclisp.Approval_Role_First__c = 'HDO';
		pclisp.Approval_Role_Second__c = 'LegalTech';
		pclisp.Approval_Role_Third__c = 'Closer';
		update pclisp;		 
	Test.stopTest();
	}
}