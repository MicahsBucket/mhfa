/*
*   {Purpose}   Manage functionality tied to Project Narrative filtering/management                              
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   01/28/17    Kevin Johnson DCS       Created
*   06/05/17    Kevin Johnson DCS       Added filter save functionality
*   02/05/18    Kevin Johnson DCS       -Updated interface manage interface to display narratives by group / author
*                                       -Deprecated fetching picklist value data via aggregateresult. Replaced with fetching values to
*                                        a set using toLabel() converting returned data to GUI picklist values.  Picklist API values 
*                                        are returned using aggregateresult where toLabel() does not function.
*   =============================================================================
*/

public with sharing class ProjectNarrativeManage_Controller {
    public Id opptyID {get;set;}
    public Set<String> setGroupValues = new Set<String>();  //compile unique values
    public List<String> lstGroupValues = new List<String>();  //sort values
    public Set<String> setAuthorValues = new Set<String>();
    public List<String> lstAuthorValues = new List<String>();
    public Set<String> setStatusValues = new Set<String>();
    public List<String> lstStatusValues = new List<String>();
    public List<SelectOption> lstFilterGroupOptions {get;set;}
    public String strSelectedGroup {get;set;}
    public List<SelectOption> lstFilterAuthorOptions {get;set;}
    public String strSelectedAuthor {get;set;}
    public List<SelectOption> lstFilterStatusOptions {get;set;}
    public String strSelectedStatus {get;set;}
	public Map<String, String> mapAuthorNames {get;set;}

    //constructor
    public ProjectNarrativeManage_Controller(ApexPages.StandardController Opportunity) {
        opptyID = Opportunity.getId();

        //build narrative filter selection options and initial list of narratives
        lstFilterGroupOptions = new List<SelectOption>();
        lstFilterAuthorOptions = new List<SelectOption>();
        lstFilterStatusOptions = new List<SelectOption>();

        for(Project_Narrative__c pn : [SELECT toLabel(Narrative_Group__c)
                                       FROM Project_Narrative__c 
                                       WHERE Project__c = :opptyID]){
            setGroupValues.add(pn.Narrative_Group__c);
        }
        lstGroupValues.addAll(setGroupValues);
        lstGroupValues.sort();

        for(Project_Narrative__c pn : [SELECT toLabel(Author__c)
                                       FROM Project_Narrative__c 
                                       WHERE Project__c = :opptyID]){
            setAuthorValues.add(pn.Author__c);
        }
        lstAuthorValues.addAll(setAuthorValues);
        lstAuthorValues.sort();

        for(Project_Narrative__c pn : [SELECT Project_Narrative_Author_Status__c
                                       FROM Project_Narrative__c 
                                       WHERE Project__c = :opptyID]){
            setStatusValues.add(pn.Project_Narrative_Author_Status__c);
        }
        lstStatusValues.addAll(setStatusValues);
        lstStatusValues.sort();

        lstFilterGroupOptions.add(new SelectOption('NONE', '--ALL GROUPS--'));
        lstFilterAuthorOptions.add(new SelectOption('NONE', '--ALL AUTHORS--'));
        lstFilterStatusOptions.add(new SelectOption('NONE', '--ALL STATUSES--'));

        for(String pn : lstGroupValues){            
            lstFilterGroupOptions.add(new SelectOption(string.valueof(pn), string.valueof(pn)));    
        }

        for(String pn : lstAuthorValues){
            lstFilterAuthorOptions.add(new SelectOption(string.valueof(pn), string.valueof(pn)));    
        }

        for(String pn : lstStatusValues){
            lstFilterStatusOptions.add(new SelectOption(string.valueof(pn), string.valueof(pn)));    
        }

        //query and apply filter value to respective filter
        for(Project_Narrative_Selected_Filter__c oSelectedFilter : [SELECT Filter__c
                                                                         , Filter_Value__c
                                                                    FROM Project_Narrative_Selected_Filter__c
                                                                    WHERE Project__c = :opptyID
                                                                    AND OwnerId = :UserInfo.getUserId()]){
            if(oSelectedFilter.Filter__c == 'Narrative_Group__c'){
                strSelectedGroup = oSelectedFilter.Filter_Value__c;
            } 

            if(oSelectedFilter.Filter__c == 'Author__c'){
                strSelectedAuthor = oSelectedFilter.Filter_Value__c;
            } 

            if(oSelectedFilter.Filter__c == 'Project_Narrative_Author_Status__c'){
                strSelectedStatus = oSelectedFilter.Filter_Value__c;
            }            
        }
    }

    public ApexPages.StandardSetController setCon {
        get{
            String queryAll = 'SELECT Project__c, Project_Narrative_Author_Status__c,   Project_Narrative_Author_Due_Date__c, Project_Narrative_Author_Final_Date__c, toLabel(Author__c), toLabel(Narrative_Group__c)' +
                              ' FROM Project_Narrative__c';                  
            String queryFilter = ' WHERE Project__c = :opptyID';

            if((setCon == null && strSelectedGroup == null //no filter selections made: initial page load / post page load
                 && strSelectedAuthor == null && strSelectedStatus == null) 
                || (setCon != null && strSelectedGroup == 'NONE'
                 && strSelectedAuthor == 'NONE' && strSelectedStatus == 'NONE')){
                system.debug('ALL QUERY: ' + queryAll + queryFilter);
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryAll + queryFilter));
                setCon.setPageSize(2000);  //default return set is 20 - need to declare size to ensure full set returned
            } else if(setCon != null){  //setcon already instantiated meaning not selected filter values == NONE
                if(strSelectedGroup != 'NONE'){
                    queryFilter = queryFilter + ' AND toLabel(Narrative_Group__c) = :strSelectedGroup';
                }
                if(strSelectedAuthor != 'NONE'){
                    queryFilter = queryFilter +' AND toLabel(Author__c) = :strSelectedAuthor';
                }
                if(strSelectedStatus != 'NONE'){
                    queryFilter = queryFilter +' AND Project_Narrative_Author_Status__c = :strSelectedStatus';
                }
            } else if(setCon == null){  //setcon not instantiated meaning not selected filter values == null
                if(strSelectedGroup != null){
                    queryFilter = queryFilter + ' AND toLabel(Narrative_Group__c) = :strSelectedGroup';
                }
                if(strSelectedAuthor != null){
                    queryFilter = queryFilter +' AND toLabel(Author__c) = :strSelectedAuthor';
                }
                if(strSelectedStatus != null){
                    queryFilter = queryFilter +' AND Project_Narrative_Author_Status__c = :strSelectedStatus';
                } 
            }
            system.debug('FILTER QUERY: ' + queryAll + queryFilter);
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryAll + queryFilter));
            setCon.setPageSize(2000);  //default return set is 20 - need to declare size to ensure full set returned
            return setCon;
        }
        set;
    }

	//Initialize setCon and return a list of records
	public List<Project_Narrative__c> getNarratives() {
		Set<String> setPN = new Set<String>();
		List<Project_Narrative__c> lstPN = new List<Project_Narrative__c>();

		//retrieve project team members and store the names in a map where the key is the role
		mapAuthorNames = new Map<String, String>();
		for (OpportunityTeamMember otm : [SELECT toLabel(TeamMemberRole)
												, User.FirstName
												, User.LastName 
											FROM OpportunityTeamMember
											Where  OpportunityId = :opptyID]) {
			String displayName = otm.User.LastName;
			if (otm.User.FirstName != null) {
				displayName = otm.User.FirstName + ' ' + displayName;
			}
			mapAuthorNames.put(otm.TeamMemberRole, displayName);
		}
		     
		for(Project_Narrative__c pn : (List<Project_Narrative__c>)setCon.getRecords()){
			if(!setPN.contains(pn.Author__c + pn.Narrative_Group__c)){
				setPN.add(pn.Author__c + pn.Narrative_Group__c); 
				lstPN.add(pn);
			}
			if (!mapAuthorNames.containsKey(pn.Author__c)) {
				mapAuthorNames.put(pn.Author__c, '');
			}			              
		}
		return lstPN;
	}

    public void manageFilters(){
        //delete prior project narrative filter records
        deleteFilterRecords();
        //insert currently selected filter records
        List<Project_Narrative_Selected_Filter__c> lstFilterValues = new List<Project_Narrative_Selected_Filter__c>();

        if(strSelectedGroup != 'NONE'){
            Project_Narrative_Selected_Filter__c oFilterValue = new Project_Narrative_Selected_Filter__c();
            oFilterValue.Filter__c = 'Narrative_Group__c';
            oFilterValue.Filter_Value__c = strSelectedGroup;
            oFilterValue.OwnerId = UserInfo.getUserId();
            oFilterValue.Project__c = opptyID;
            lstFilterValues.add(oFilterValue);
        }
        if(strSelectedAuthor != 'NONE'){
            Project_Narrative_Selected_Filter__c oFilterValue = new Project_Narrative_Selected_Filter__c();
            oFilterValue.Filter__c = 'Author__c';
            oFilterValue.Filter_Value__c = strSelectedAuthor;
            oFilterValue.OwnerId = UserInfo.getUserId();
            oFilterValue.Project__c = opptyID;
            lstFilterValues.add(oFilterValue);
        }
        if(strSelectedStatus != 'NONE'){
            Project_Narrative_Selected_Filter__c oFilterValue = new Project_Narrative_Selected_Filter__c();
            oFilterValue.Filter__c = 'Project_Narrative_Author_Status__c';
            oFilterValue.Filter_Value__c = strSelectedStatus;
            oFilterValue.OwnerId = UserInfo.getUserId();
            oFilterValue.Project__c = opptyID;
            lstFilterValues.add(oFilterValue);
        }

        if(lstFilterValues.size() > 0) {
            insert lstFilterValues;
        }
    }

    private void deleteFilterRecords(){
        //delete prior project narrative filer records if exist
        List<Project_Narrative_Selected_Filter__c> lstDeleteFilter = new List<Project_Narrative_Selected_Filter__c>();
        for(Project_Narrative_Selected_Filter__c oSelectedFilter : [SELECT Id
                                                                    FROM Project_Narrative_Selected_Filter__c
                                                                    WHERE Project__c = :opptyID
                                                                    AND OwnerId = :UserInfo.getUserId()]) {
            lstDeleteFilter.add(oSelectedFilter);
        }
        if (lstDeleteFilter.size() > 0) {
            delete lstDeleteFilter;
        }
    }

    public PageReference pgNarrative(){
        getNarratives();
        return null;
    }
}