/*
*   {purpose}  	Manage the functionality tied to Funding Round Documentation Type automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    		Name             		Description
*	02/12/19		Kevin Johnson DCS		Created
*   =============================================================================
*/	

public with sharing class FundingRoundDocumentTypeUtility  {
	public static string processNewFRDType(List<Funding_Round_Documentation_Type__c> lstFRDT){
		/*
		*{purpose}		Creates new Project Document Type: Internal record for related Projects when new Funding Round Document Type is created
		*
		*{function}		Fetches related Project Document: Internal records.  The returned records provide the data needed to create the related 
		*				Project Document Type: Internal records with the appropriate relationships and attributes.
		*				
		*{trigger}  	FundingRoundDocumentationTypeEventListener: after insert
		*/

		Set<Id> setFRDid = new Set<Id>();
		Map<Id, Funding_Round_Documentation_Type__c> mapFRDT = new Map<Id, Funding_Round_Documentation_Type__c>();
		List<Project_Documentation_Type__c> lstPDTInsert = new List<Project_Documentation_Type__c>();
		String errorMsg = null; 

		for(Funding_Round_Documentation_Type__c oFRDT : lstFRDT){
			setFRDid.add(oFRDT.Funding_Round_Documentation__c);
			mapFRDT.put(oFRDT.Funding_Round_Documentation__c, oFRDT);			
		}
		System.debug('setFRDid: ' + setFRDid);
		System.debug('mapFRDT: ' + mapFRDT);

		for(Project_Documentation__c oPD : [SELECT Id
		                                         , Name
		                                         , Funding_Round_Documentation__c
											FROM Project_Documentation__c
											WHERE Funding_Round_Documentation__c IN :setFRDid]){
			System.debug('oPD: ' + oPD);
			Project_Documentation_Type__c pdt = new Project_Documentation_Type__c(Project_Documentation__c = oPD.Id
																				 , Funding_Round_Documentation_Type__c = mapFRDT.get(oPD.Funding_Round_Documentation__c).Id
																				 , Name = mapFRDT.get(oPD.Funding_Round_Documentation__c).Type__c
																				 , Bucket__c = oPD.Name
																				 , Type__c = mapFRDT.get(oPD.Funding_Round_Documentation__c).Type__c);
		    System.debug('pdt: ' + pdt);
			lstPDTInsert.add(pdt);			
		}
		System.debug('lstPDTInsert size: ' + lstPDTInsert.size());
		System.debug('lstPDTInsert: ' + lstPDTInsert);
		
		if(!lstPDTInsert.isEmpty()){
			Database.SaveResult[] insertResult = Database.insert(lstPDTInsert, false);
			
			for(Database.SaveResult sr : insertResult){
				if(!sr.isSuccess()){
					for(Database.Error err : sr.getErrors()){
						errorMsg = 'Error inserting Project Document Type: Internal record: ' + err.getStatusCode() + ' - ' + err.getMessage();
					}
				}
			}
		}
		return errorMsg;
	}
}