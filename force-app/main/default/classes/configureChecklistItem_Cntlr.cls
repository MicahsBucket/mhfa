public without sharing class configureChecklistItem_Cntlr  {

	//wrapper classes
	public class groupCharacteristics {
		//properties
		@AuraEnabled
		public Trigger_Group__c oGroup {get;set;}
		@AuraEnabled
		public List<Funding_Round_Trigger__c> oGrpCharacteristics {get;set;}
		@AuraEnabled
		public List<Id> selectedChars {get;set;}

		//constructor
		public groupCharacteristics() {
			this.oGroup = new Trigger_Group__c();
			this.oGrpCharacteristics = new List<Funding_Round_Trigger__c>();
			this.selectedChars = new List<Id>();
		}
	}

	public class checklistOption {
		//properties
		@AuraEnabled
		public Boolean isSelected {get;set;}
		@AuraEnabled
		public Boolean isChanged {get;set;}
		@AuraEnabled
		public Phase_Checklist__c oChecklist {get;set;}
		@AuraEnabled
		public Phase_Checklist_Item__c oChecklistItem {get;set;}
		@AuraEnabled
		public String sDueDate {get;set;}
		@AuraEnabled
		public List<configureChecklistItem_Cntlr.picklistOption> stageOptions {get;set;}
		@AuraEnabled
		public List<configureChecklistItem_Cntlr.picklistOption> closingTypeOptions {get;set;}
		@AuraEnabled
		public List<configureChecklistItem_Cntlr.picklistOption> dueDateOffsetOptions {get;set;}
		@AuraEnabled
		public List<configureChecklistItem_Cntlr.picklistOption> approverOptions {get;set;}

		//constructor
		public checklistOption() {
			this.isSelected = false;
			this.isChanged = false;
			this.oChecklist = new Phase_Checklist__c();
			this.oChecklistItem = new Phase_Checklist_Item__c();
			this.stageOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
			this.closingTypeOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
			this.dueDateOffsetOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
			this.approverOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
		}
	}

	public class picklistOption {
		//properties
		@AuraEnabled
		public String itemLabel {get;set;}
		@AuraEnabled
		public String itemValue {get;set;}

		//constructor
		public picklistOption(String label, String value) {
			this.itemLabel = label;
			this.itemValue = value;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and associated characteristics for funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureChecklistItem_Cntlr.groupCharacteristics> getGroupings(Id frId, Id frcliId, String selectionType) {

		List<configureChecklistItem_Cntlr.groupCharacteristics> retVal = new List<configureChecklistItem_Cntlr.groupCharacteristics>();
		Map<String, Set<Id>> mapGrpSelectedValues = new Map<String, Set<Id>>();
		for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT : [Select Id
																	, Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
																	, Funding_Round_Trigger__r.Trigger_Group__c
																	, Funding_Round_Trigger__r.Trigger_Group__r.Name
																From Funding_Round_Checklist_Item_Trigger__c
																Where Funding_Round_Checklist_Item__c = :frcliId
																	And Funding_Round_Trigger__c != null]) {
			Set<Id> setSelectedValues = new Set<Id>();
			if (mapGrpSelectedValues.containsKey(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c)) {
				setSelectedValues = mapGrpSelectedValues.get(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c);
			}
			setSelectedValues.add(oFRCLIT.Funding_Round_Trigger__c);
			mapGrpSelectedValues.put(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c, setSelectedValues);
		}

		Map<String, List<Funding_Round_Trigger__c>> mapCharsByGroup = new Map<String, List<Funding_Round_Trigger__c>>();
		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureChecklistItem_Cntlr.groupCharacteristics oGrpChars = new configureChecklistItem_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				if (mapGrpSelectedValues.containsKey(oGrp.Id)) {
					oGrpChars.selectedChars.addAll(mapGrpSelectedValues.get(oGrp.Id));
				}
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getGroupings.\n' + retVal);
		return retVal;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and associated characteristics for funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureChecklistItem_Cntlr.groupCharacteristics> getAvailableCharacteristics(Id frId, Id frcliId, String selectionType) {

		List<configureChecklistItem_Cntlr.groupCharacteristics> retVal = new List<configureChecklistItem_Cntlr.groupCharacteristics>();
		Map<String, Set<Id>> mapGrpSelectedValues = new Map<String, Set<Id>>();
		Set<Id> setSelectedValues = new Set<Id>();
		for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT : [Select Id
																	, Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
																	, Funding_Round_Trigger__r.Trigger_Group__c
																	, Funding_Round_Trigger__r.Trigger_Group__r.Name
																From Funding_Round_Checklist_Item_Trigger__c
																Where Funding_Round_Checklist_Item__c = :frcliId
																	And Funding_Round_Trigger__c != null]) {
			setSelectedValues.add(oFRCLIT.Funding_Round_Trigger__c);
		}

		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
                                               	Where Id not in :setSelectedValues
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureChecklistItem_Cntlr.groupCharacteristics oGrpChars = new configureChecklistItem_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				if (mapGrpSelectedValues.containsKey(oGrp.Id)) {
					oGrpChars.selectedChars.addAll(mapGrpSelectedValues.get(oGrp.Id));
				}
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getGroupings.\n' + retVal);
		return retVal;
	}
    
    //////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and characteristics already associated to the given item
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureChecklistItem_Cntlr.groupCharacteristics> getAssignedCharacteristics(Id frId, Id frcliId, String selectionType) {

		List<configureChecklistItem_Cntlr.groupCharacteristics> retVal = new List<configureChecklistItem_Cntlr.groupCharacteristics>();
		Set<Id> setSelectedGroups = new Set<Id>();
		Set<Id> setSelectedTriggers = new Set<Id>();
        
		for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT : [Select Id
																	, Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
																	, Funding_Round_Trigger__r.Trigger_Group__c
																	, Funding_Round_Trigger__r.Trigger_Group__r.Name
																From Funding_Round_Checklist_Item_Trigger__c
																Where Funding_Round_Checklist_Item__c = :frcliId]) {
			setSelectedGroups.add(oFRCLIT.Funding_Round_Trigger__r.Trigger_Group__c);
			setSelectedTriggers.add(oFRCLIT.Funding_Round_Trigger__c);
		}

		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
                                               	Where Id in :setSelectedTriggers
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
                                      		And Id in :setSelectedGroups
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureChecklistItem_Cntlr.groupCharacteristics oGrpChars = new configureChecklistItem_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getAssigned.\n' + retVal);
		return retVal;
	}    
        
	//////////////////////////////////////////////////////////////////////////////////////////////
	//find the checklists associated to the given funding round and build the helper variable
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureChecklistItem_Cntlr.checklistOption> getChecklistOptions(Id frId, Id frcliId, String selectionType) {
		System.debug('Inside getChecklistOptions.\nfrId: ' + frId + '\nfrcliId: ' + frcliId);
		Map<Id, configureChecklistItem_Cntlr.checklistOption> mapOptions = new Map<Id, configureChecklistItem_Cntlr.checklistOption>();

		//find all the stages available for each phase in the funding round
		Map<Id, List<configureChecklistItem_Cntlr.picklistOption>> mapChecklistStages = configureChecklistItem_Cntlr.retrieveFundingRoundStages(frId);
		System.debug('mapChecklistStages:\n' + mapChecklistStages);

		//find closing type options to populate picklists
		List<configureChecklistItem_Cntlr.picklistOption> closingTypeOptions = configureChecklistItem_Cntlr.retrievePicklistValues(new Phase_Checklist_Item__c()
																																	, 'Closing_Types__c');

		//find due date options to populate picklists
		List<configureChecklistItem_Cntlr.picklistOption> dueDateOffsetOptions = configureChecklistItem_Cntlr.retrievePicklistValues(new Phase_Checklist_Item__c()
																																	, 'Due_Date_Offset_Type__c');

		//find approver options to populate picklists
		List<configureChecklistItem_Cntlr.picklistOption> approverOptions = configureChecklistItem_Cntlr.retrievePicklistValues(new Phase_Checklist_Item__c()
																																, 'Approval_First__c');

		for (Phase_Checklist__c oChecklist : [Select Id
													, Name
													, Active__c
													, Sort_Order__c
													, Closing_Types__c
													, Funding_Round_Phase__c
													, Funding_Round_Phase__r.Name
													, Funding_Round_Phase__r.End_Date__c
												From Phase_Checklist__c
												Where Funding_Round__c = :frId
													And Active__c = true
													And Selection_Type__c = :selectionType
												Order By Sort_Order__c]) {
			configureChecklistItem_Cntlr.checklistOption oChecklistOption = new configureChecklistItem_Cntlr.checklistOption();
			oChecklistOption.oChecklist = oChecklist;
			oChecklistOption.oChecklistItem.Phase_Checklist__c = oChecklist.Id;
			oChecklistOption.oChecklistItem.Funding_Round_Checklist_Item__c = frcliId;
			if (mapChecklistStages.containsKey(oChecklist.Funding_Round_Phase__c)) {
				oChecklistOption.stageOptions = mapChecklistStages.get(oChecklist.Funding_Round_Phase__c);
			}
			oChecklistOption.closingTypeOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
			if (oChecklist.Closing_Types__c != null) {
				oChecklistOption.closingTypeOptions = configureChecklistItem_Cntlr.buildPicklistValues(oChecklist.Closing_Types__c);
			}
			oChecklistOption.approverOptions = approverOptions;
			oChecklistOption.dueDateOffsetOptions = dueDateOffsetOptions;
			//if no phase is associated to the checklist, overwrite the dueDateOffsetOptions by only allowing 'Equal to Due Date'
			if (oChecklist.Funding_Round_Phase__c == null) {
				oChecklistOption.dueDateOffsetOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
				oChecklistOption.dueDateOffsetOptions.add(new configureChecklistItem_Cntlr.picklistOption('Equal to Due Date', 'Equal to Due Date'));
			}

			mapOptions.put(oChecklist.Id, oChecklistOption);
		}

		//find checklist items already associated to this funding round and pre-select the checkbox
		for (Phase_Checklist_Item__c oChkCLI : [Select Id
                                                        , Phase_Checklist__c
                                                        , Funding_Round_Checklist_Item__c
                                                        , Funding_Round_Stage__c
														, Closing_Types__c
														, Due_Date_Offset__c
														, Due_Date_Offset_Type__c
														, Due_Date__c
                                                        , Approval_First__c
                                                        , Approval_Second__c
                                                        , Approval_Third__c
												From Phase_Checklist_Item__c
                                                Where Funding_Round_Checklist_Item__c = :frcliId]) {
			if (mapOptions.containsKey(oChkCLI.Phase_Checklist__c)) {
				configureChecklistItem_Cntlr.checklistOption oOption = mapOptions.get(oChkCLI.Phase_Checklist__c);
				oOption.isSelected = true;
				oOption.oChecklistItem = oChkCLI;
				oOption.sDueDate = String.valueOf(oChkCLI.Due_Date__c);
				mapOptions.put(oChkCLI.Phase_Checklist__c, oOption);
			}
		}
		//return the list of checklist items
		System.debug('mapOptions:\n' + mapOptions);
		return mapOptions.values();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//save characteristics and checklists with the given funding round checklist item
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static void saveCliChecklists(Id frcliId, List<String> lstJsonChecklists) {
		System.debug('Inside saveCliConfiguration.'
						+ '\nfrcliId: ' + frcliId
						+ '\nlstJsonChecklists: ' + lstJsonChecklists);
		
		List<configureChecklistItem_Cntlr.checklistOption> lstChecklists = new List<configureChecklistItem_Cntlr.checklistOption>();
		for (String oJsonChecklist : lstJsonChecklists) {
			lstChecklists.add(
				(configureChecklistItem_Cntlr.checklistOption) JSON.deserialize(oJsonChecklist, configureChecklistItem_Cntlr.checklistOption.class)
			);
		}
		configureChecklistItem_Cntlr.insertCliChecklists(frcliId, lstChecklists);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// insert checklist item characteristics for the given funding round checklist item
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static void insertCliCharacteristics(Id frcliId, List<String> lstCharIds, Boolean blnClearExisting) {
		System.debug('Inside insertCliCharacteristics'
						+ '\nfrcliId: ' + frcliId
						+ '\nlstCharIds: ' + lstCharIds
						+ '\nblnClearExisting: '+ blnClearExisting);

		//delete existing values for the given checklist item if told to do so
		if (blnClearExisting == true) {
			List<Funding_Round_Checklist_Item_Trigger__c> delRecs = new List<Funding_Round_Checklist_Item_Trigger__c>();
			for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT :  [Select Id 
                                                                     From Funding_Round_Checklist_Item_Trigger__c 
                                                                     Where Funding_Round_Checklist_Item__c = :frcliId]) {
				delRecs.add(oFRCLIT);
			}
			if (delRecs.size() > 0) {
				System.debug('Deleting ' + delRecs);
				delete delRecs;
			}
		}

		//insert the selected items
		List<Funding_Round_Checklist_Item_Trigger__c> lstInsertRecs = new List<Funding_Round_Checklist_Item_Trigger__c>();
		List<Id> lstIds = new List<Id>();
		//the provided list of ids might have commas, so we need to split those entries to get the individual ids
		System.debug('lstCharIds.size: ' + lstCharIds.size());
		for (Integer i=0; i<lstCharIds.size(); i++) {
			System.debug('sItems: ' + lstCharIds[i]);
			lstIds.add((Id)lstCharIds[i]);
		}
		System.debug('lstIds: ' + lstIds);

		for (Id charId : lstIds) {
			lstInsertRecs.add(new Funding_Round_Checklist_Item_Trigger__c(
										Funding_Round_Checklist_Item__c = frcliId
										, Funding_Round_Trigger__c = charId));
		}
		if (lstInsertRecs.size() > 0) {
			System.debug('Inserting ' + lstInsertRecs);
			insert lstInsertRecs;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// delete characteristics
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
    public static void removeCliCharacteristics(Id frcliId, List<String> lstCharIds) {
		//delete given values
        List<Funding_Round_Checklist_Item_Trigger__c> delRecs = new List<Funding_Round_Checklist_Item_Trigger__c>();
        for (Funding_Round_Checklist_Item_Trigger__c oFRCLIT :  [Select Id 
                                                                 From Funding_Round_Checklist_Item_Trigger__c 
                                                                 Where Funding_Round_Checklist_Item__c = :frcliId
                                                                	And (Funding_Round_Trigger__c = null
																			OR Funding_Round_Trigger__c in :lstCharIds)]) {
        	delRecs.add(oFRCLIT);
        }
        if (delRecs.size() > 0) {
            System.debug('Deleting ' + delRecs);
            delete delRecs;
        }
    }
    
	//////////////////////////////////////////////////////////////////////////////////////////////
	// insert checklist checklist item
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static void insertCliChecklists(Id frcliId, List<configureChecklistItem_Cntlr.checklistOption> lstChecklists) {
		List<Phase_Checklist_Item__c> lstDelete = new List<Phase_Checklist_Item__c>();
		List<Phase_Checklist_Item__c> lstUpsert = new List<Phase_Checklist_Item__c>();

		System.debug('Inside insertCliChecklists.  frcliId: ' + frcliId + '\nlstChecklists: ' + lstChecklists);

		if (lstChecklists != null) {
			for (configureChecklistItem_Cntlr.checklistOption oChecklistOption : lstChecklists) {
				if (oChecklistOption.isSelected == true) {
					//need to handle date fields separately because the Lightning component treats them as strings
					if (String.isNotBlank(oChecklistOption.sDueDate)) {
						oChecklistOption.oChecklistItem.Due_Date__c = Date.valueOf(oChecklistOption.sDueDate);
					}
					lstUpsert.add(oChecklistOption.oChecklistItem);
				} else if (oChecklistOption.oChecklistItem.Id != null) {
					lstDelete.add(oChecklistOption.oChecklistItem);
				}
			}
		}

		String errMsg = '';
		if (lstDelete.size() > 0) {
			try {
				delete lstDelete;
			} catch(DmlException e) {
				//Get All DML Messages
				for (Integer i = 0; i < e.getNumDml(); i++) {
					//Get Validation Rule & Trigger Error Messages
					errMsg =+ e.getDmlMessage(i) + '\n';
				}				
			} catch(Exception e) {
				errMsg = e.getMessage() + '\n';
			}
		}

		if (lstUpsert.size() > 0) {
			try {
				upsert lstUpsert;
			} catch(DmlException e) {
				System.debug('DmlException: ' + e);
				//Get All DML Messages
				for (Integer i = 0; i < e.getNumDml(); i++) {
					//Get Validation Rule & Trigger Error Messages
					errMsg =+ e.getDmlMessage(i) +  '\n' ;
				}				
			} catch(Exception e) {
				errMsg =+ e.getMessage();
			}
		}

		if (errMsg != '') {
			System.debug('errMsg: ' + errMsg);
			throw new AuraHandledException(errMsg);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// retrieve a map of phase ids and associated stages in the given funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static Map<Id, List<configureChecklistItem_Cntlr.picklistOption>> retrieveFundingRoundStages(Id frId) {
		Map<Id, List<configureChecklistItem_Cntlr.picklistOption>> retVal = new Map<Id, List<configureChecklistItem_Cntlr.picklistOption>>();
		for (Phase__c oPhase : [Select Id
									, Name
									, Sort_Order__c
									, (Select Id
											, Name
											, Sort_Order__c 
										From SubPhases__r
										Order By Sort_Order__c)
								From Phase__c
								Where Funding_Round__c = :frId
								Order By Sort_Order__c]) {
			List<configureChecklistItem_Cntlr.picklistOption> lstStageOptions = new List<configureChecklistItem_Cntlr.picklistOption>();
			for (Phase__c oSubPhase : oPhase.SubPhases__r) {
				lstStageOptions.add(new configureChecklistItem_Cntlr.picklistOption(oSubPhase.Name, oSubPhase.Id));
			}
			if (lstStageOptions.size() > 0) {
				retVal.put(oPhase.Id, lstStageOptions);
			}
		}
		return retVal;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Use the getPicklistValues utility to find picklist values for a field and then convert them
	// into the picklist structure that the lighnting component can iterate
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static List<configureChecklistItem_Cntlr.picklistOption> retrievePicklistValues(Sobject objectName, String fieldName) {
		List<selectOption> lstOptions = utilityMethods_DCS.getPickValues(objectName, fieldName, null);
		List<configureChecklistItem_Cntlr.picklistOption> retVal = new List<configureChecklistItem_Cntlr.picklistOption>();
		for (SelectOption oOpt : lstOptions) {			
			retVal.add(new configureChecklistItem_Cntlr.picklistOption(oOpt.getLabel(), oOpt.getValue()));
		}
		System.debug('Inside retrievePicklistValues for ' + objectName + '.' + fieldName + '\n' + retVal);
		return retVal;		
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Build a picklist structure that the lighnting component can iterate from a semi-colon 
	//  separated list of strings
	//////////////////////////////////////////////////////////////////////////////////////////////
	public static List<configureChecklistItem_Cntlr.picklistOption> buildPicklistValues(String fieldValues) {
		List<configureChecklistItem_Cntlr.picklistOption> retVal = new List<configureChecklistItem_Cntlr.picklistOption>();
		for (String strValue : fieldValues.split(';')) {			
			retVal.add(new configureChecklistItem_Cntlr.picklistOption(strValue, strValue));
		}
		System.debug('Inside buildPicklistValues for ' + fieldValues + '\n' + retVal);
		return retVal;		
	}

}