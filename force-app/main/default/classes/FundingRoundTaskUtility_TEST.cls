@isTest
public class FundingRoundTaskUtility_TEST  {
	@isTest static void testFundingRoundTaskUtility() {
		//be careful staging tasks as there are many cascading events that fire which can lead to too many soql queries
		//SUPPORTING DATA=======================================================================================================
		//custom metadata types
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       					FROM Enable_Triggers__mdt
					  		       					WHERE MasterLabel = 'FundingRoundTask'];
		
		//create test users
		User oTestUser = TestDataUtility.createUser('Multifamily Standard User',
	                                                 'testUser@mhfatest.com');

		//FUNDING ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		
		//PROJECT FRAMEWORK=====================================================================================================
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		
		//fetch/stage required data		
		Funding_Round_Task__c oFundingRoundTaskPreSelection = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);

		Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
															   , Name
														  FROM Project_Sub_Phase__c
														  WHERE Name = 'Application Submittals'
														  AND Project_Phase__r.Name = 'Application'
														  AND Project__c = :oProject.Id];	
			
		//run tests                                                        
		Test.startTest();
			Funding_Round_Team_Member__c oFundingRoundTeamMember= TestDataUtility.createFundingRoundTeamMember(oFundingRound.Id, 
                                                                                                           'ChiefUnderwriter', 
                                                                                                            oTestUser.Id);
		
			OpportunityTeamMember oProjectTeamMember = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
	                                                                                           'Closer', 
	                                                                                            oTestUser.Id);
			
			Phase__c oFundingRoundPhasePreSelection = [SELECT Id
		                                                , End_Date__c
												   FROM Phase__c
												   WHERE Name = 'Application'
												   AND Funding_Round__c = :oFundingRound.Id];
			oFundingRoundPhasePreSelection.End_Date__c =  DateTime.Now().AddDays(30);
			update oFundingRoundPhasePreSelection;

			//project stage task
			Project_Task__c oProjectStageTaskPreSelection = TestDataUtility.createProjectStageTask(oProjectStagePreSelection.Id,
																						           oProject.Id,
																						           'Review Pre-Application Documents',
																						           'Project Launch',
			   																			           oFundingRoundTaskPreSelection.Id);
			//test cascading owner change
			oFundingRoundTaskPreSelection.Assignee_Type__c = 'Internal Task';
			oFundingRoundTaskPreSelection.Assignee_Role_Project_Team__c = 'Closer';
			update oFundingRoundTaskPreSelection;
			
			oFundingRoundTaskPreSelection.Assignee_Role_Project_Team__c = null;
			oFundingRoundTaskPreSelection.Assignee_Role_Funding_Round_Team__c = 'ChiefUnderwriter';
			update oFundingRoundTaskPreSelection;
			
			oFundingRoundTaskPreSelection.Assignee_Type__c = 'External Task';
			oFundingRoundTaskPreSelection.Assignee_Role_Funding_Round_Team__c = null;
			update oFundingRoundTaskPreSelection;

			FundingRoundTaskUtility.findInitialTask(oProjectStageTaskPreSelection.Id);
		Test.stopTest();	
	}
}