@isTest
public class FundingRoundCLILinkUtility_TEST  {
	@isTest static void testFundingRoundCLILinkUtility() {
	//SUPPORTING DATA=======================================================================================================
    //custom metadata type
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       		     FROM Enable_Triggers__mdt
					  		       		     WHERE MasterLabel = 'FundingRoundChecklistItemLink'];

	//FUNDING ROUND FRAMEWORK===============================================================================================
	Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

	//PROJECT FRAMEWORK=====================================================================================================
	//create project
    Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

	//fetch required data
	Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																  FROM Funding_Round_Checklist_Item__c
																  WHERE Funding_Round__c = :oFundingRound.Id
																  AND Name = 'Pre Selection Checklist Item'];
	 
	Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
														  FROM Project_Checklist__c
														  WHERE Name = 'Application'
														  AND Project__c = :oProject.Id];
	
	Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
														   , Name
													  FROM Project_Sub_Phase__c
													  WHERE Name = 'Application Submittals'
													  AND Project_Phase__r.Name = 'Application'
													  AND Project__c = :oProject.Id];

	//create pcli===========================================
    Project_Checklist_Item__c pcli = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                oFundingRoundChecklistItem.Id,
                                                                                false,
                                                                                null,
                                                                                null,
																				oProjectStagePreSelection.Id,
																				oProjectChecklistPreSelection.Id);
	//run tests                                                        
    Test.startTest();
		Funding_Round_Checklist_Item_Link__c oFundingRoundCheckistItemLink = TestDataUtility.createFundingRoundChecklistItemLink(oFundingRoundChecklistItem.Id);
	Test.stopTest();
	}
}