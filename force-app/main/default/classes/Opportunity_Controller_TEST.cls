@isTest
private class Opportunity_Controller_TEST {
	@isTest static void testOpportunity_Controller() {
        //stage data
        //create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9)); 

        //create externally generated ids
        List<Externally_Generated_IDs__c> egid = TestDataUtility.createExternalGeneratedIDs();

        //run tests                                                        
        Test.startTest();
	        PageReference pageRef = new PageReference('Opportunity_New');		    
		    pageRef.getParameters().put('frid', fr.Id);

	        Test.setCurrentPage(pageRef);

	        Opportunity_Controller ext = new Opportunity_Controller(new ApexPages.StandardController(new Opportunity()));	   		

            ext.oOppty.Name = 'Test Development';
 			ext.oOppty.Primary_Development_Street_Address__c = 'Test Street Address';
			ext.oOppty.Primary_Development_City__c = 'Test City';
			ext.oOppty.Primary_Development_ZIP_Code__c = '11111';
			ext.oOppty.Primary_Development_County__c = 'Anoka';
			ext.oOppty.Previously_applied_received_funding__c = 'No';
            ext.oOppty.StageName = 'Submitted';
			ext.oOppty.Funding_Round__c = fr.Id;

			ext.createOneMFProject();
			ext.createMultipleMFProject();			
			ext.cancelProject();
        Test.stopTest();
	}
}