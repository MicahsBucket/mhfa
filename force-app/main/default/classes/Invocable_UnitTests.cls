/*
*   CHANGE  HISTORY
*   =============================================================================
*   Date		Name				Description
*   09/27/17	EBG DCS				Created
*	06/27/18	Kevin Johnson DCS	Added test for Public Groups parameter
*   =============================================================================
*/
@isTest
private class Invocable_UnitTests {	
	@isTest static void test_SendEmail() {
        //stage custom settings
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'OpportunityTeamMember'];

        List<File_Sharing_Settings_Objects__mdt> csSharingRules = [SELECT MasterLabel
                                                                        , Object_Prefix__c
                                                                        , Fields_To_Query__c
                                                                        , Parent_Lookup__c
                                                                   FROM File_Sharing_Settings_Objects__mdt];

        //stage data
        //create test user
        User testUser = TestDataUtility.createUser('Multifamily Standard User',
	                                               'testFRTM@mhfatest.com');
		
		//create public group
		Group testPublicGroup = TestDataUtility.createPublicGroup('Test Public Group');

		//create public group member
		//received MIXED_DML_OPERATION when inserting groupmember.  removed creation of group member - test obtaining 93% coverage w/o team member
				
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));   

        //create funding round team member
        Funding_Round_Team_Member__c frtm = TestDataUtility.createFundingRoundTeamMember(fr.Id, 
        	                                                                             'LeadArchitect', 
        	                                                                             testUser.Id);

        //create project
        Opportunity p = TestDataUtility.createProject(fr.Id);
        
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		EmailTemplate validEmailTemplate = new EmailTemplate();
		System.runAs(thisUser) {
	        //create email template - pulled from https://salesforce.stackexchange.com/questions/12395/how-can-email-template-be-created-in-unit-tests
			validEmailTemplate.isActive = true;
			validEmailTemplate.Name = 'TemplateName';
			validEmailTemplate.DeveloperName = 'TemplateDeveloperName';
			validEmailTemplate.TemplateType = 'text';
			validEmailTemplate.FolderId = UserInfo.getUserId();

			insert validEmailTemplate;

	        User otmUser = TestDataUtility.createUser('Multifamily Standard User',
	                                                  'testOTM3@mhfatest.com');

	        OpportunityTeamMember otm = TestDataUtility.createOpportunityTeamMember(p.Id, 
	                                                                                'HDO', 
	                                                                                otmUser.Id);
			otm.Email_Notification__c = true;
			update otm;
		}

        Test.startTest();
			Invocable_SendEmail.EmailRequest request = new Invocable_SendEmail.EmailRequest();

			request.parentRecordId = p.Id;
			request.recipientUserId = UserInfo.getUserId();
			request.projectId = p.Id;
			request.emailTemplateId = validEmailTemplate.Id;
			request.blnIndividualTeamMemberNotifyFlag = true;
			request.blnAllInternalTeamMemberNotifyFlag = true;
			request.strTeamRoles = 'HDO';
			request.strAdditionalAddresses = 'test@test.com';
			request.strFundingRoundTeamMemberRole = 'LeadArchitect';
			request.strPublicGroup = 'Test Public Group';

			List<Invocable_SendEmail.EmailRequest> lstRequests = new List<Invocable_SendEmail.EmailRequest>();
			lstRequests.add(request);
			Invocable_SendEmail.SendEmail(lstRequests);
        Test.stopTest();
	}
}