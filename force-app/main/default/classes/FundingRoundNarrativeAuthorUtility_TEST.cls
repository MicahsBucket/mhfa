@isTest
private class FundingRoundNarrativeAuthorUtility_TEST {
	@isTest static void testFundingRoundNarrativeAuthorUpdate(){
		//stage custom settings
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		     FROM Enable_Triggers__mdt
						  		       		     WHERE MasterLabel = 'FundingRoundNarrativeAuthor'];

		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create funding round group
        Funding_Round_Narrative_Group__c frng = TestDataUtility.createFundingRoundNarrativeGroup(fr.Id);

        //create funding round author
        Funding_Round_Narrative_Author__c frna = TestDataUtility.createFundingRoundNarrativeAuthor(frng.Id);

        //create funding round narrative
        Funding_Round_Narrative__c frn = TestDataUtility.createFundingRoundNarrative(fr.Id,
                                                                                     frna.Id); 

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //create project narrative group
        Project_Narrative_Group__c png = TestDataUtility.createProjectNarrativeGroup(p.Id, 
        	                                                                         'Selection Narratives', 
        	                                                                         'In Process', 
        	                                                                         frng.Id);

        Project_Narrative_Author__c pna = TestDataUtility.createProjectNarrativeAuthor(png.Id, 
        	                                                                           'Architect', 
        	                                                                           'In Process', 
        	                                                                           frna.Id);

        Project_Narrative__c pn = TestDataUtility.createProjectNarrative(p.Id, 
        	                                                             frn.Id, 
        	                                                             'Architect', 
        	                                                             'Architecture and Construction', 
        	                                                             'Selection Narratives', 
        	                                                             'In Process');
			//todo: add parameter to account for pna
			pn.Project_Narrative_Author__c = pna.Id;
			update pn;
	

		//run tests
        Test.startTest();
        	frna.Due_Date__c = system.today();
        	update frna;
        Test.stopTest();
	}
}