@isTest 
public class FundingRoundDocumentTypeUtility_TEST  {
	@isTest
	private static void testFundingRoundDocumentTypeUtility() {
		//stage custom settings
		//insert RunTrigger
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       				 FROM Enable_Triggers__mdt
					  		       				 WHERE MasterLabel = 'FundingRoundDocumentType'];

		//stage data	
		//create funding round
		Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9)); 

		//create funding round document
		Funding_Round_Documentation__c frd = TestDataUtility.createFundingRoundDocumentation(fr.Id);

		//create project
		Opportunity p = TestDataUtility.createProject(fr.Id);

		//create project document: internal
		Project_Documentation__c pd = TestDataUtility.createProjectDocumentation(p.Id,
                                                                                'Approvals',
                                                                                 null,
                                                                                 null);
		
		//run tests                                                        
		Test.startTest();
			//insert new funding round documentation type
			Funding_Round_Documentation_Type__c frdt = TestDataUtility.createFundingRoundDocumentationType(frd.Id);
		Test.stopTest();
	}
}