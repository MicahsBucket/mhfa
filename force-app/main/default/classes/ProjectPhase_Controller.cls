/*
*   {Purpose}   Checklist Management screen   
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date         Name                    Description
*   07/11/2016	Eric Gronholz DCS       Adapted to new structures
*	06/28/2017	Eric Gronholz DCS 		Refactor to decrease page viewstate: combine datasets (configure and manage) and load on demand
*	07/08/2019	Kevin Johnson DCS		Refactor to leverage new framework
*   =============================================================================
*/
public without sharing class ProjectPhase_Controller {
	public Opportunity oProject {get;set;}
	public Id projectId {get;set;}
	public Id fundingRoundId {get;set;}
	public typeFundingRoundPhase oPhaseDetails_Data {get;set;}
	public Set<Id> setSelectedSubPhaseIds {get;set;}
	public Id phaseId {get;set;}
	public Id checklistId {get;set;}
	public String activeTab {get;set;}
	public String previousTab {get;set;}

	//////////////////////////////////////////////////////////////////////
	//constructor - standard controller (detail button)
	//////////////////////////////////////////////////////////////////////	
	public ProjectPhase_Controller(ApexPages.StandardController stdController) {		
		oProject = (Opportunity)stdController.getRecord();
		projectId = oProject.Id;
		fundingRoundId = oProject.Funding_Round__c;
		if(ApexPages.currentPage().getParameters().get('phaseId') != null && //account for checklists not associated to a phase
			ApexPages.currentPage().getParameters().get('phaseId') != '' &&
            ApexPages.currentPage().getParameters().get('phaseId') != 'null'){
			phaseId = (Id)ApexPages.currentPage().getParameters().get('phaseId');
		}		
		checklistId = (Id)ApexPages.currentPage().getParameters().get('checklistid');

		oPhaseDetails_Data = new typeFundingRoundPhase(phaseId, projectId, checklistId, 'Manage');
	}

	//////////////////////////////////////////////////////////////////////
	//build the tabs for configuration and management pages
	//////////////////////////////////////////////////////////////////////	
	public Component.Apex.Outputpanel getPhaseTabs() {		
		Component.Apex.Outputpanel tabPanel = new Component.Apex.Outputpanel();
		Component.Apex.Outputpanel tabManage = new Component.Apex.Outputpanel();

		oPhaseDetails_Data = null;
		oPhaseDetails_Data = new typeFundingRoundPhase(phaseId, projectId, checklistId, 'Manage');		
																					
		Component.ProjectPhaseManage comManageCLI = new Component.ProjectPhaseManage(oPhaseDetails=oPhaseDetails_Data
																						   , oProject=oProject);
		tabManage.childComponents.add(comManageCLI);
		tabPanel.childComponents.add(tabManage);

		return tabPanel;
	}

	////////////////////////////////////////////////////////////////////////
	// Set the Opt Out value on the Phase Object record
	////////////////////////////////////////////////////////////////////////
	public PageReference optOutChange() {
		/*
		Project_Phase__c oProjectPhase = new Project_Phase__c(Id=oPhaseDetails_Data.oProjectPhase.Id
															, Opt_Out__c=oPhaseDetails_Data.oProjectPhase.Opt_Out__c);
		update oProjectPhase;

		PageReference oPageRef = new PageReference('/apex/ProjectPhase?id=' + projectId + '&phaseId=' + oPhaseDetails_Data.oFRPhase.Id + '&checklistId=' + oPhaseDetails_Data.oFRPhaseChecklist.Id);
		oPageRef.setRedirect(true);
		return oPageRef;
		*/
		Project_Checklist__c oProjectChecklist = new Project_Checklist__c(Id=oPhaseDetails_Data.oProjectChecklist.Id
															            , Opt_Out__c=oPhaseDetails_Data.oProjectChecklist.Opt_Out__c);
		update oProjectChecklist;

		PageReference oPageRef = new PageReference('/apex/ProjectPhase?id=' + projectId + '&phaseId=' + oPhaseDetails_Data.oFRPhase.Id + '&checklistId=' + oPhaseDetails_Data.oFRPhaseChecklist.Id);
		oPageRef.setRedirect(true);
		return oPageRef;		
	}

	////////////////////////////////////////////////////////////////////////
	// Reload the page with the Manage tab selected
	////////////////////////////////////////////////////////////////////////
	public PageReference loadManageTab() {
		activeTab = 'tab_Manage';
		return null;
	}	
}