@isTest
public class ProjectChecklistItemUtility_TEST {
	@isTest static void testProjectChecklistItemUtility() {
		//SUPPORTING DATA=======================================================================================================
		//custom metadata types
		Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ProjectChecklistItem'];

		//create test users
		User oTestUser = TestDataUtility.createUser('Multifamily Standard User',
	                                                'testOTM1@mhfatest.com');

		//FUNDING ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();

		//fetch & stage required funding round data
		Funding_Round_Checklist_Item__c oFundingRoundChecklistItem = [SELECT Id
																      FROM Funding_Round_Checklist_Item__c
																      WHERE Funding_Round__c = :oFundingRound.Id
																      AND Name = 'Pre Selection Checklist Item'];

		Funding_Round_Checklist_Item_Link__c oFundingRoundCheckistItemLink = TestDataUtility.createFundingRoundChecklistItemLink(oFundingRoundChecklistItem.Id);

		//PROJECT FRAMEWORK=====================================================================================================
		//create project
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);

		//fetch & stage required data
		Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
															  FROM Project_Checklist__c
															  WHERE Name = 'Application'
															  AND Project__c = :oProject.Id];

		Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
															   , Name
														  FROM Project_Sub_Phase__c
														  WHERE Name = 'Application Submittals'
														  AND Project_Phase__r.Name = 'Application'
														  AND Project__c = :oProject.Id];		

		//run tests                                                        
		Test.startTest();
			//create project checklist item
			Project_Checklist_Item__c oProjectChecklistItem = TestDataUtility.createProjectChecklistItem(oProject.Id, 
																										oFundingRoundChecklistItem.Id,
																										false,
																										null,
																										null,
																										oProjectStagePreSelection.Id,
																										oProjectChecklistPreSelection.Id);
			//process approver role updates w/o project team member
			//need to have all approvar roles populated to fulfill validation rule
			oProjectChecklistItem.Approval_Role_First__c = 'HDO';
			update oProjectChecklistItem;
			oProjectChecklistItem.Approval_Role_First__c = 'Architect';
			oProjectChecklistItem.Approval_Role_Second__c = 'HDO';
			update oProjectChecklistItem;
			oProjectChecklistItem.Approval_Role_Second__c = 'HMO';
			oProjectChecklistItem.Approval_Role_Third__c = 'HDO';
			update oProjectChecklistItem;

			OpportunityTeamMember oProjectTeamMember1 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
	                                                                                               'HDO', 
	                                                                                                oTestUser.Id);
			
			//process approver role updates w/ project team member
			oProjectChecklistItem.Approval_Role_First__c = 'HDO';
			update oProjectChecklistItem;
			//process one additional cycle to hit second approver role = HDO
			oProjectChecklistItem.Approval_Role_First__c = 'Architect'; 
			oProjectChecklistItem.Approval_Role_Second__c = 'HDO';
			oProjectChecklistItem.Approval_Role_Third__c = 'HMO';
			update oProjectChecklistItem;
		Test.stopTest();
	}
}