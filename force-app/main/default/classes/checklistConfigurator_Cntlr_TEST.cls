@isTest 
private class checklistConfigurator_Cntlr_TEST {

	@isTest
	private static void test_checklistConfigurator() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		Funding_Round_Task__c oFRTask = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);
		
		Test.startTest();

		checklistConfigurator_Cntlr.getItems(oFundingRound.Id);
		checklistConfigurator_Cntlr.getApprovers();

		Test.stopTest();

	}
}