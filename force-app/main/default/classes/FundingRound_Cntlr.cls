public without sharing class FundingRound_Cntlr  {
    public class phaseTree {
        //properties
        @AuraEnabled
        public Phase__c oPhase {get;set;}
        @AuraEnabled
        public List<Phase__c> lstStagesAndTasks {get;set;}
        @AuraEnabled
        public List<FundingRound_Cntlr.stageDetails> lstStageDetailsAndTasks {get;set;}
        @AuraEnabled
        public List<Phase_Checklist__c> lstChecklistsAndItems {get;set;}

        //constructor
        public phaseTree() {
            this.oPhase = new Phase__c();
            this.lstStagesAndTasks = new List<Phase__c>();
            this.lstChecklistsAndItems = new List<Phase_Checklist__c>();
			this.lstStageDetailsAndTasks = new List<FundingRound_Cntlr.stageDetails>();
        }
    }

	public class stageDetails {
		//properties
		@AuraEnabled
		public Phase__c oStage {get;set;}
		@AuraEnabled
		public Date dueDate {get;set;}

		//constructor
		public stageDetails() {
			this.oStage = new Phase__c();
		}
	}
    
    public class groupTree {
        //properties
        @AuraEnabled
        public Trigger_Group__c oGroup {get;set;}
        @AuraEnabled
        public List<Funding_Round_Trigger__c> lstCharacteristics {get;set;}
        
        //constructor
        public groupTree() {
            this.oGroup = new Trigger_Group__c();
            this.lstCharacteristics = new List<Funding_Round_Trigger__c>();
        }
    }

    @AuraEnabled
    public static List<Phase__c> retrievePhasesAndStages(Id frId) {
        System.debug('Inside retrievePhasesAndStages.  frId: ' + frId);
        List<Phase__c> lstPhases = new List<Phase__c>();
        for (Phase__c oPhase: [Select Id
                                        , Name
                                        , Phase__c
                                        , Sub_Phase__c
                                        , Start_Date__c
                                        , End_Date__c
                                        , Due_Date_Baseline__c
                                        , Sort_Order__c
                                        , RecordType.Name
										, Funding_Round__r.Close_Date__c
										, Funding_Round__r.Target_Project_End_Date__c
                                        , (Select Id
                                                    , Name
                                                    , Sub_Phase__c
													, Start_Date__c
													, Estimated_Days_to_Complete__c
													, End_Date__c
                                                    , Offset_Type__c
                                                    , Due_Date_Offset__c
													, Not_Applicable__c
                                                    , Sort_Order__c
                                            From SubPhases__r
                                            Order By Sort_Order__c)
                                From Phase__c
                                Where Funding_Round__c = :frId
                                    And RecordType.Name = 'Phase'
                                Order By Sort_Order__c]) {
            lstPhases.add(oPhase);
            System.debug('lstPhases:\n' + oPhase);
        }
        return lstPhases;
    }

    @AuraEnabled
    public static List<FundingRound_Cntlr.phaseTree> retrievePhaseStageTree(Id frId, String taskType) {
        System.debug('Inside retrievePhaseStageTree.  frId: ' + frId);
		if (taskType == null || taskType == '' || taskType == 'All Tasks') {
			taskType = '%';
		}
        //retrieve the phases associated to the given funding round
        Map<Id, Phase__c> mapPhases = new Map<Id, Phase__c>([Select Id
                                                                    , Name
                                                                    , Phase__c
                                                                    , Sub_Phase__c
                                                                    , Start_Date__c
                                                                    , End_Date__c
                                                                    , Due_Date_Baseline__c
                                                                    , Sort_Order__c
                                                                    , RecordType.Name
																	, Funding_Round__r.Close_Date__c
																	, Funding_Round__r.Target_Project_End_Date__c
                                                                From Phase__c
                                                                Where Funding_Round__c = :frId
                                                                    And RecordType.Name = 'Phase'
                                                                Order By Sort_Order__c]);

        //retrieve Stages and Tasks associated to the phases
        Map<Id, FundingRound_Cntlr.phaseTree> mapPhaseTree = new Map<Id, FundingRound_Cntlr.phaseTree>();
        for (Phase__c oStage: [Select Id
                                    , Name
                                    , Sub_Phase__c
                                    , Offset_Type__c
                                    , Due_Date_Offset__c
                                    , Sort_Order__c
									, Start_Date__c
									, Estimated_Days_to_Complete__c
									, End_Date__c
									, Not_Applicable__c
                                    , Parent_Phase__c
                                    , Parent_Phase__r.Sort_Order__c
									, Parent_Phase__r.End_Date__c
									, Parent_Phase__r.Funding_Round__r.Close_Date__c
									, Parent_Phase__r.Funding_Round__r.Target_Project_End_Date__c
                                    , (Select Id
                                            , Name
                                            , Due_Date_Baseline__c
                                            , Task_Item__r.Name
                                       		, Target_Due_Date__c
											, Task_Type__c
											, Assignee_Type__c
											, Expected_Duration_Days__c
											, toLabel(Assignee_Role_Funding_Round_Team__c)
											, toLabel(Assignee_Role_Project_Team__c)
                                        From Funding_Round_Tasks__r
										Where Task_Type__c like :taskType
                                      	Order By Target_Due_Date__c, Id)
                                From Phase__c
                                Where Parent_Phase__c in :mapPhases.keySet()
                                    And RecordType.Name = 'Stage'
                                Order By Parent_Phase__r.Sort_Order__c, Sort_Order__c]) {
            FundingRound_Cntlr.phaseTree oPhaseTree = new FundingRound_Cntlr.phaseTree();
            if (mapPhaseTree.containsKey(oStage.Parent_Phase__c)) {
                oPhaseTree = mapPhaseTree.get(oStage.Parent_Phase__c);
            } else {
                oPhaseTree.oPhase = mapPhases.get(oStage.Parent_Phase__c);
            }
            oPhaseTree.lstStagesAndTasks.add(oStage);
			Date stageDueDate = DateUtility.findBusinessDate(Date.valueOf(oStage.Parent_Phase__r.End_Date__c)
																, oStage.Offset_Type__c
																, Integer.valueOf(oStage.Due_Date_Offset__c));
			FundingRound_Cntlr.stageDetails oHoldStage = new FundingRound_Cntlr.stageDetails();
			oHoldStage.oStage = oStage;
			oHoldStage.dueDate = stageDueDate;
			System.debug('Stage: ' + oStage.Name 
							+ '; DueDate:  ' + stageDueDate 
							+ '; oHoldStage.dueDate: ' + oHoldStage.dueDate
							+ '; Offset Type: ' + oStage.Offset_Type__c 
							+ '; Offset: ' + oStage.Due_Date_Offset__c 
							+ '; Phase End Date: ' + oStage.Parent_Phase__r.End_Date__c);
			oPhaseTree.lstStageDetailsAndTasks.add(oHoldStage);
            mapPhaseTree.put(oStage.Parent_Phase__c, oPhaseTree);
        }
        return mapPhaseTree.values();
    }

    @AuraEnabled
    public static List<FundingRound_Cntlr.phaseTree> retrievePhaseChecklistTree(Id frId) {
        System.debug('Inside retrievePhaseChecklistTree.  frId: ' + frId);

        //retrieve the phases associated to the given funding round
        Map<Id, Phase__c> mapPhases = new Map<Id, Phase__c>([Select Id
                                                                    , Name
                                                                    , Phase__c
                                                                    , Sub_Phase__c
                                                                    , Start_Date__c
                                                                    , End_Date__c
                                                                    , Due_Date_Baseline__c
                                                                    , Sort_Order__c
                                                                    , RecordType.Name
																	, Funding_Round__r.Close_Date__c
																	, Funding_Round__r.Target_Project_End_Date__c
                                                                From Phase__c
                                                                Where Funding_Round__c = :frId
                                                                    And RecordType.Name = 'Phase'
                                                                Order By Sort_Order__c]);
        Map<Id, FundingRound_Cntlr.phaseTree> mapPhaseTree = new Map<Id, FundingRound_Cntlr.phaseTree>();
        for (Phase__c oPhase : mapPhases.values()) {
            FundingRound_Cntlr.phaseTree oPhaseTree = new FundingRound_Cntlr.phaseTree();
            oPhaseTree.oPhase = oPhase;
            mapPhaseTree.put(oPhase.Id, oPhaseTree);
        }

        //retrieve Checklist and Items associated to the phases
        for (Phase_Checklist__c oChecklist: [Select Id
                                                    , Name
                                                    , Selection_Type__c
                                                    , Active__c
                                                    , Sort_Order__c
                                                    , Funding_Round_Phase__c
                                                    , Funding_Round_Phase__r.Sort_Order__c
                                                    , (Select Id
                                                            , Name
                                                            , Funding_Round_Stage__c
                                                            , Funding_Round_Stage__r.Name
                                                            , Funding_Round_Checklist_Item__c
                                                            , Funding_Round_Checklist_Item__r.Name
                                                            , toLabel(Approval_First__c)
                                                            , toLabel(Approval_Second__c)
                                                            , toLabel(Approval_Third__c)
                                                        From Phase_Checklist_Items__r)
                                                From Phase_Checklist__c
                                                Where Funding_Round_Phase__c in :mapPhases.keySet()
                                                Order By Funding_Round_Phase__r.Sort_Order__c, Sort_Order__c]) {
            FundingRound_Cntlr.phaseTree oPhaseTree = new FundingRound_Cntlr.phaseTree();
            if (mapPhaseTree.containsKey(oChecklist.Funding_Round_Phase__c)) {
                oPhaseTree = mapPhaseTree.get(oChecklist.Funding_Round_Phase__c);
            } else {
                oPhaseTree.oPhase = mapPhases.get(oChecklist.Funding_Round_Phase__c);
            }
            oPhaseTree.lstChecklistsAndItems.add(oChecklist);
            mapPhaseTree.put(oChecklist.Funding_Round_Phase__c, oPhaseTree);
        }

        return mapPhaseTree.values();
    }

    @AuraEnabled
    public static List<FundingRound_Cntlr.groupTree> retrieveGroupCharacteristicTree(Id frId) {
        System.debug('Inside retrieveGroupCharacteristicTree.  frId: ' + frId);

        //retrieve the groups associated to the given funding round
        Map<Id, Trigger_Group__c> mapGroups = new Map<Id, Trigger_Group__c>([Select Id
                                                                                , Name
                                                                                , Trigger_Group_Name__c
                                                                                , Selection_Type__c
                                                                                , Description__c
                                                                                , Active__c
                                                                                , Characteristic_Count__c
                                                                                , Sort_Order__c
                                                                            From Trigger_Group__c
                                                                            Where Funding_Round__c = :frId
                                                                            Order By Sort_Order__c]);

        //retrieve characteristics associated to the groups
        Map<Id, FundingRound_Cntlr.groupTree> mapGroupTree = new Map<Id, FundingRound_Cntlr.groupTree>();
		for (Id grpId : mapGroups.keySet()) {
            FundingRound_Cntlr.groupTree oGroupTree = new FundingRound_Cntlr.groupTree();
			oGroupTree.oGroup = mapGroups.get(grpId);
			mapGroupTree.put(grpId, oGroupTree);
		}
        for (Funding_Round_Trigger__c oCharacteristic: [Select Id
                                                            , Name
                                                            , Trigger__c
                                                            , Trigger_Group__c
                                                            , Trigger_Group__r.Sort_Order__c
                                                            , Sort_Order__c
															, Sort_Order_in_Phase__c
															, Help_Text__c
															, Required_for_All__c
                                                        From Funding_Round_Trigger__c
                                                        Where Trigger_Group__c in :mapGroups.keySet()
                                                        Order By Trigger_Group__r.Sort_Order__c, Name]) {
            FundingRound_Cntlr.groupTree oGroupTree = new FundingRound_Cntlr.groupTree();
            if (mapGroupTree.containsKey(oCharacteristic.Trigger_Group__c)) {
                oGroupTree = mapGroupTree.get(oCharacteristic.Trigger_Group__c);
            } else {
                oGroupTree.oGroup = mapGroups.get(oCharacteristic.Trigger_Group__c);
            }
            oGroupTree.lstCharacteristics.add(oCharacteristic);
            mapGroupTree.put(oCharacteristic.Trigger_Group__c, oGroupTree);
        }

        return mapGroupTree.values();
    }

    @AuraEnabled
    public static List<Id> retrieveFundingRoundBaseTasks(Id frId) {
		Map<Id, Funding_Round_Task__c> mapBaseTasks	= new Map<Id, Funding_Round_Task__c>([Select Id
																								, Name
																								, RecordTypeId
																								, RecordType.Name
																								, Funding_Round__c
																								, Funding_Round__r.OwnerId
																								, OwnerId
																								, Preceding_Task__c
																								, Stage__c
																								, Phase__c
																								, Checklist__c
																								, Expected_Duration_Days__c
																								, Due_Date_Baseline__c
																								, Related_To__c
																								, Initial_Task__c
																								, Assignee_Type__c
																								, toLabel(Assignee_Role_Funding_Round_Team__c)
																								, toLabel(Assignee_Role_Project_Team__c)
																							From Funding_Round_Task__c
																							Where Funding_Round__c = :frId
																								And Preceding_Task__c = null]);
		if (mapBaseTasks.size() > 0) {
			List<Id> retVal = new List<Id>();
			retVal.addAll(mapBaseTasks.keySet());
			return retVal;
		} else {
			return null;
		}
	}

    ///////////////////////////////////////////////////////////////////////
	// Update project phase and stage due dates based on the changes made
	//  to the given funding round
    ///////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static String cascadeStageChanges(Id frId) {
        batchResetStageDates job = new batchResetStageDates(frId);
		ID batchId = Database.executeBatch(job, 5);
		return (String)batchId;      
	}
	
    ///////////////////////////////////////////////////////////////////////
	// Return the number of phases under the given funding round where
	//  the Due Date Baseline is 'Stage Duration - Funding Round'
    ///////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static Integer fundingRoundDurationCount(Id frId) {
		Integer recCount = 0;
        for (Funding_Round__c oFR : [Select Id
                                            , Name
                                            , (Select Id From Phases__r Where Due_Date_Baseline__c = 'Stage Duration - Funding Round')
                                    From Funding_Round__c Where Id = :frId]) {
            recCount = oFR.Phases__r.size();
		}
		return recCount;
	}

/* 
-- Commented out on 20200311 by EBG/DC because the CharacteristicTree component is not used.  
-- Need to create test class if wish to implement this feature 

    public class clsCharTable {
		public string id {get;set;}
		public string selectionType {get;set;}
		public string description {get;set;}
		public decimal sortOrder {get;set;}
		public boolean requiredForAll {get;set;}
	}

	//save event for inline editing on the CharacteristicTree lightning component
	@AuraEnabled
	public static void saveChangedRecords_CharTree(String jsonRecs) {
		System.debug('Inside saveChangedRecords_CharTree: ' + jsonRecs);
		//Map<String,String> keys = new Map<String,String>();
		Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
		//for(String s:describe.keyset()) {
		//	keys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
		//}
		
		List<Object> lstChangedRecs = (List<Object>)JSON.deserializeUntyped(jsonRecs);
		List<Trigger_Group__c> lstUpdate_Groups = new List<Trigger_Group__c>();
		List<Funding_Round_Trigger__c> lstUpdate_Chars = new List<Funding_Round_Trigger__c>();
		List<SObject> lstUpdate = new List<SObject>();
		for (Object oRec : lstChangedRecs) {
			Map<String, Object> mapRec = (Map<String, Object>)oRec;
			Id recId = (Id)mapRec.get('id');
			//determine the type of object being updated based on the id
			String sobjectName = recId.getSObjectType().getDescribe().getName();
			Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sobjectName);
			System.debug('sobjectType: ' + sobjectType);
			SObject oNewRec = sObjectType.newSObject(recId);
			for (String fieldName : mapRec.keySet()) {
				switch on fieldName {
					when 'description' {
						String fieldValue = (String)mapRec.get(fieldName);
						if (sobjectName == 'Trigger_Group__c') {
							oNewRec.put('Description__c', fieldValue);
						} 
					}
					when 'selectionType' {
						String fieldValue = (String)mapRec.get(fieldName);
						if (sobjectName == 'Trigger_Group__c') {
							oNewRec.put('Selection_Type__c', fieldValue);
						} 
					}
					when 'requiredForAll' {
						Boolean fieldValue = Boolean.valueOf(mapRec.get(fieldName));
						if (sobjectName == 'Funding_Round_Trigger__c') {
							oNewRec.put('Required_for_All__c', fieldValue);
						}
					}
				}
					
			}
			lstUpdate.add(oNewRec);
		}

		if (lstUpdate.size() > 0) {
			update lstUpdate;
		}
	}
*/

	//save event for inline editing on the PhaseStageTree lightning component
	@AuraEnabled
	public static void saveChangedRecords_Duration(String jsonRecs) {
		System.debug('Inside saveChangedRecords_StageOffset: ' + jsonRecs);
		
		List<Object> lstChangedRecs = (List<Object>)JSON.deserializeUntyped(jsonRecs);
		List<SObject> lstUpdate = new List<SObject>();

		//get the ids of the records being edited.  We only want to update Stages
		Set<Id> setStageIds = new Set<Id>();
		for (Object oRec: lstChangedRecs) {
			Map<String, Object> mapRec = (Map<String, Object>)oRec;
			Id recId = (Id)mapRec.get('id');
			setStageIds.add(recId);
		}

		//Retrieve stage records
		Map<Id, Phase__c> mapStages = new Map<Id, Phase__c>();
		for (Phase__c oStage : [Select Id, RecordType.Name, Estimated_Days_to_Complete__c
								From Phase__c
								Where RecordType.Name = 'Stage'
									And Id in :setStageIds]) {
			mapStages.put(oStage.Id, oStage);
		}

		//If a stage record is updated, write the new value to the database
		for (Object oRec : lstChangedRecs) {
			Map<String, Object> mapRec = (Map<String, Object>)oRec;
			Id recId = (Id)mapRec.get('id');
			if (mapStages.containsKey(recId)) {
				Phase__c oStageRec = mapStages.get(recId);
				Boolean blnUpdateStage = false;
				for (String fieldName : mapRec.keySet()) {
					switch on fieldName {
						when 'durationDays' {
							oStageRec.Estimated_Days_to_Complete__c = Decimal.valueOf((String)mapRec.get(fieldName));	
							blnUpdateStage = true;						
						}
						when 'notApplicable' {
							oStageRec.Not_Applicable__c = (Boolean)mapRec.get(fieldName);	
							blnUpdateStage = true;						
						}
					}
				}
				if (blnUpdateStage) {
					lstUpdate.add(oStageRec);
				}
			}			
		}

		if (lstUpdate.size() > 0) {
			update lstUpdate;
		}
	}

	@AuraEnabled
	public static void deleteRecord(Id recId, String recType) {
		System.debug('Inside FundingRound_Cntlr.deleteRecord.  recId: ' + recId + ' recType: ' + recType);
		List<Trigger_Group__c> lstDeleteGroup = new List<Trigger_Group__c>();
		List<Funding_Round_Trigger__c> lstDeleteChar = new List<Funding_Round_Trigger__c>();
		if (recType == 'Characteristic Group') {
			for (Trigger_Group__c oGroup : [Select Id From Trigger_Group__c Where Id = :recId]) {
				lstDeleteGroup.add(oGroup);
			}
		} else {
			for (Funding_Round_Trigger__c oChar : [Select Id From Funding_Round_Trigger__c Where Id = :recId]) {
				lstDeleteChar.add(oChar);
			}
		}

		delete lstDeleteGroup;
		delete lstDeleteChar;
	}
		
}