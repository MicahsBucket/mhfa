/*
*   {purpose}   Manage the functionality tied to Funding Round automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   02/27/17    Kevin Johnson DCS       Created
*   09/27/17    Kevin Johnson DCS       Modified Project/PCLIP query to only select Projects where Project Status = Select
*   11/27/17    Kevin Johnson DCS       Decoupled setting PCLIP record type from process
*   03/01/19    Eric Gronholz DCS       Added routine to automatically create phases and stages when a FR is created
*	07/27/20	Kevin Johnson DCS		Updated setProjectRecordTypes to account for new Project Status - Waitlist
*   =============================================================================
*/

public without sharing class FundingRoundUtility {
    public static string setProjectRecordTypes(List<Funding_Round__c> lstFR, Map<ID, String> mapFRSelectionStatus) {
        /*
        *{purpose}      Manages setting of RecordType for related Projects based on status of Show Selection flag.
        *
        *{function}     Fetches respective Opportunity RecordTypes to be applied to the Funding Rounds related Projects.
        *               If Show Selection = TRUE set all related Projects (Opportunities) to display selection elements by changing the Project
        *               RecordType.
        *               If Show Selection = FALSE set all related Projects (Opportunities) to not display selection elements by changing the
        *               Project RecordType.
        *
        *{assumption}   Function run after all selected Projects have set Project Status = Select
        *
        *{trigger}      FundingRoundEventListener: after update
        */

        Set<ID> setFRId = new Set<ID>();
        for(Funding_Round__c fr : lstFR){
            setFRId.add(fr.Id);
        }

        //fetch required recordtypes
        List<RecordType> lstPreSelectRecordTypeProject = [SELECT Id
                                                          FROM RecordType
                                                          WHERE Id = '01237000000TgAiAAK' //Project: Pre-Selection                                          
                                                          AND Sobjecttype = 'Opportunity'];

        List<RecordType> lstSelectRecordTypeProject = [SELECT Id
                                                       FROM RecordType
                                                       WHERE Id = '01237000000TgAjAAK' //Project: Selection
                                                       AND Sobjecttype = 'Opportunity'];

        //map funding round id to its selected recordtype id                                        
        Map<ID, ID> mapFRRecordTypeIdProject = new Map<ID, ID>();
        for(Id fr : mapFRSelectionStatus.keySet()){
            if(mapFRSelectionStatus.get(fr) == 'Select'){
                mapFRRecordTypeIdProject.put(fr, lstSelectRecordTypeProject[0].Id);
            } else {
                mapFRRecordTypeIdProject.put(fr, lstPreSelectRecordTypeProject[0].Id);
            }
        }

        //fetch funding round related projects and apply appropriate recordtype
        List<Opportunity> lstOpptyToUpdate = new List<Opportunity>();
        for(Opportunity o : [SELECT Id
                                  , RecordTypeId
                                  , Funding_Round__c
                             FROM Opportunity
                             WHERE Funding_Round__c = :setFRId
							 AND (Project_Status__c = 'Select'
							 OR Project_Status__c = 'Waitlist')]){

            Opportunity opptyToUpdate = new Opportunity(Id = o.Id,
                                                        RecordTypeId = mapFRRecordTypeIdProject.get(o.Funding_Round__c));

            lstOpptyToUpdate.add(opptyToUpdate);
        }
        system.debug('LSTOPPTYTOUPDATE: ' + lstOpptyToUpdate);

        String errorMsg = null;
        Boolean recordUpdateSuccessful = TRUE;
        //process project record type update
        if(!lstOpptyToUpdate.isEmpty()){
            Database.SaveResult[] updResultOppty = Database.update(lstOpptyToUpdate, false);                
            
            for(Database.SaveResult srOppty : updResultOppty){
                if(!srOppty.isSuccess()){
                    recordUpdateSuccessful = FALSE;
                    for(Database.Error err : srOppty.getErrors()){
                        errorMsg = 'Error updating Project RecordType: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
    }

	public static void loadPhasesAndStages(Map<Id, Funding_Round__c> mapFRs) {
        /*
        *{purpose}      Creates phase and stage records below the provided funding round records
        *
        *{function}     Fetches phase and stages defaults from custom metadata and creates the corresponding
		*				records under the given funding rounds
        *
        *{assumption}   Every funding round has the same phases and stages, and the defaults exist in the metadata.  Phase names are unique.
        *
        *{trigger}      FundingRoundEventListener: after insert
        */

		//this function only works when inserting a single funding round
		if (mapFRs.size() > 1) {
			return;
		}

		//retreive the default phases
		map<Id, Default_Phase__mdt> mapDefaultPhases = new map<Id, Default_Phase__mdt>([Select Id
																						, Label
																						, Due_Date_Baseline__c
																						, Sort_Order__c
																					From Default_Phase__mdt
																					Order By Sort_Order__c]);
		map<String, List<Default_Stage__mdt>> mapDefaultPhaseStages = new map<String, List<Default_Stage__mdt>>();
		Map<String, Default_Stage__mdt> mapDefaultStages = new Map<String, Default_Stage__mdt>();

		//retrieve the default stages for each phase.  Couldn't use a relationship query due to the need to
		// sort at both the phase and stage level.  soql statement was only returning one stage per phase as if
		// it was forcing a query iterator
		for (Default_Stage__mdt oStage : [Select Id
                                 				, Label
                                      			, Default_Phase__c
                                 				, Sort_Order__c
												, Default_Phase__r.Label
												, Default_Duration__c
                                 			From Default_Stage__mdt
											Where Default_Phase__c in :mapDefaultPhases.keySet()
                                  			Order By Default_Phase__c, Sort_Order__c]) {
			List<Default_Stage__mdt> lstStages = new List<Default_Stage__mdt>();
			if (mapDefaultPhaseStages.containsKey(oStage.Default_Phase__r.Label)) {
				lstStages = mapDefaultPhaseStages.get(oStage.Default_Phase__r.Label);
			}
			lstStages.add(oStage);
			mapDefaultPhaseStages.put(oStage.Default_Phase__r.Label, lstStages);
			mapDefaultStages.put(oStage.Label, oStage);
		}

		//create the phase records for the funding rounds
		List<Phase__c> lstPhases = new List<Phase__c>();
		Id fundingRoundId = null;
		for (Id idFR : mapFRs.keySet()) {
			fundingRoundId = idFR;
			for (Default_Phase__mdt oDefaultPhase : mapDefaultPhases.values()) {
				lstPhases.add(new Phase__c(Funding_Round__c = idFR
											, Name = oDefaultPhase.Label
											, Phase__c = oDefaultPhase.Label
											, Due_Date_Baseline__c = oDefaultPhase.Due_Date_Baseline__c
											, Sort_Order__c = oDefaultPhase.Sort_Order__c
											, RecordTypeId = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Phase').getRecordTypeId()));
			}
		}
		if (lstPhases.size() > 0) {
			insert lstPhases;

			//once the phases are created, create the stages below them
			List<Phase__c> lstStages = new List<Phase__c>();
			for (Phase__c oPhase : lstPhases) {
				if (mapDefaultPhaseStages.containsKey(oPhase.Phase__c)) {
					for (Default_Stage__mdt oDefaultStage : mapDefaultPhaseStages.get(oPhase.Phase__c)) {
						lstStages.add(new Phase__c(Name = oDefaultStage.Label
													, Parent_Phase__c = oPhase.Id
													, Phase__c = oPhase.Phase__c
													, Sub_Phase__c = oDefaultStage.Label
													, Sort_Order__c = oDefaultStage.Sort_Order__c
													, Estimated_Days_to_Complete__c = oDefaultStage.Default_Duration__c
													, RecordTypeId = Schema.SObjectType.Phase__c.getRecordTypeInfosByName().get('Stage').getRecordTypeId()));
					}
				}
			}

			if (lstStages.size() > 0) {
				insert lstStages;

				for (Id idFR : mapFRs.keySet()) {
					FundingRoundUtility.assignPhaseStageDueDates(idFR, 1, 1);
				}

			}
		}

	}

	public static void assignPhaseStageDueDates(Id idFR, Decimal phaseSortOrder, Decimal stageSortOrder) {
        /*
        *{purpose}      Calculates the due date for each stage of a funding round based on the funding round start date and the estimated days
		*				to complete for each stage
        *
        *{function}     Fetches phases and stages for a funding round and assigns the start and due dates
        *
        *{assumption}   Function called when phases and stages are first created, when the FR open date changes, or estimated days to complete changes on a stage
        */
		Map<Id, Phase__c> mapPhaseUpdates = new Map<Id, Phase__c>();
		Map<Id, Phase__c> mapStageUpdates = new Map<Id, Phase__c>();
		if (phaseSortOrder == null) {
			phaseSortOrder = 1;
		}
		if (stageSortOrder == null) {
			stageSortOrder = 1;
		}

		System.debug('Calculate dates beginning on phase position ' + phaseSortOrder + ' and stage position ' + stageSortOrder);

		Id prevPhaseId = null;
		for (Phase__c oPhase : [Select Id
									, Name
									, Funding_Round__r.Open_Date_Time__c
									, Funding_Round__r.Close_Date_Time__c
									, Start_Date__c
									, End_Date__c
									, Sort_Order__c
									, (Select Id
											, Name
											, Start_Date__c
											, End_Date__c
											, Estimated_Days_to_Complete__c
											, Not_Applicable__c
											, Sort_Order__c
										From SubPhases__r
										Order By Sort_Order__c)
								From Phase__c
								Where Funding_Round__c = :idFR
									And Funding_Round__r.Open_Date_Time__c != null
									And Sort_Order__c >= :phaseSortOrder
								Order By Sort_Order__c]) {
			//the start date of the first phase matches the open date of the funding round
			if (oPhase.Sort_Order__c == 1) {
				System.debug('FR Dates.  Start: ' + oPhase.Funding_Round__r.Open_Date_Time__c + '  End: ' + oPhase.Funding_Round__r.Close_Date_Time__c);
				mapPhaseUpdates.put(oPhase.Id
									, new Phase__c(Id = oPhase.Id
													, Start_Date__c = oPhase.Funding_Round__r.Open_Date_Time__c
													, End_Date__c = null));
			} else if (oPhase.Sort_Order__c == phaseSortOrder) {
				//if working with a specific phase (based on sort order), no need to change the start date
				mapPhaseUpdates.put(oPhase.Id
									, new Phase__c(Id = oPhase.Id
													, Start_Date__c = oPhase.Start_Date__c
													, End_Date__c = null));
			} else {
				//the start date of the next phase matches the end date of the previous phase
				mapPhaseUpdates.put(oPhase.Id
									, new Phase__c(Id = oPhase.Id
													, Start_Date__c = mapPhaseUpdates.get(prevPhaseId).End_Date__c
													, End_Date__c = null));
			}
			System.debug('oPhase: ' + oPhase.Sort_Order__c + '-' + oPhase.Name);
			Phase__c oUpdatedPhase = mapPhaseUpdates.get(oPhase.Id);
			System.debug('Phase Start Date: ' + oUpdatedPhase.Start_Date__c);
				
			Datetime dtLastStageEndDate = null;
			//loop through the stages under the current phase
			Id prevStageId = null;
			for (Phase__c oStage : oPhase.SubPhases__r) {
				System.debug('oStage: ' + oPhase.Sort_Order__c + '-' + oStage.Sort_Order__c + '-' + oStage.Name + ' (' + oStage.Estimated_Days_to_Complete__c + ')');
				
				//no need to process stages prior to the one being modified, so skip this stage
				if (oPhase.Sort_Order__c == phaseSortOrder && oStage.Sort_Order__c < stageSortOrder) {
					System.debug('Stage skipped');
					continue;
				}

				if (oStage.Sort_Order__c == 1) {
					//start date of first stage in a phase equals start date of the phase
					mapStageUpdates.put(oStage.Id, new Phase__c(Id = oStage.Id
																, Start_Date__c = oUpdatedPhase.Start_Date__c
																, End_Date__c = null));
					System.debug('Assigned Start Date: ' + oUpdatedPhase.Start_Date__c);
				} else if (oPhase.Sort_Order__c == phaseSortOrder && oStage.Sort_Order__c == stageSortOrder) {
					//if working with a specific stage (based on sort order), no need to change the start date
					mapStageUpdates.put(oStage.Id, new Phase__c(Id = oStage.Id
																, Start_Date__c = oStage.Start_Date__c
																, End_Date__c = null));
				} else {
					//start date of subsequent stages equals end date of previous stage
					mapStageUpdates.put(oStage.Id, new Phase__c(Id = oStage.Id
																, Start_Date__c = mapStageUpdates.get(prevStageId).End_Date__c
																, End_Date__c = null));
				}
				Phase__c oUpdatedStage = mapStageUpdates.get(oStage.Id);

				//add the duration to the start date to get the end date.  Ensure the end date does not land on a weekend or holiday
				if (oStage.Not_Applicable__c) {
					oUpdatedStage.Estimated_Days_to_Complete__c = 0;
					oUpdatedStage.End_Date__c = oUpdatedStage.Start_Date__c;
				} else {
					oUpdatedStage.End_Date__c = oUpdatedStage.Start_Date__c.addDays(Integer.valueOf(oStage.Estimated_Days_to_Complete__c));
				}
				System.debug('End date after adding days: ' + oUpdatedStage.End_Date__c);
				//set the end time to be the end time of the funding round due date; if it isn't populated, use the time of the open date
				Datetime dttEndTime = oPhase.Funding_Round__r.Open_Date_Time__c;
				if (oPhase.Funding_Round__r.Close_Date_Time__c != null) {
					dttEndTime = oPhase.Funding_Round__r.Close_Date_Time__c;
				}
				oUpdatedStage.End_Date__c = Datetime.newInstance(oUpdatedStage.End_Date__c.date()
																	, dttEndTime.time());

				while (DateUtility.isBusinessDay(oUpdatedStage.End_Date__c.date()) == false) {
					System.debug('End date inside weekend check: ' + oUpdatedStage.End_Date__c);
					oUpdatedStage.End_Date__c = oUpdatedStage.End_Date__c.addDays(1);
					System.debug('End date after adding 1 day: ' + oUpdatedStage.End_Date__c);
				}
				System.debug('End date after weekend check: ' + oUpdatedStage.End_Date__c);

				dtLastStageEndDate = oUpdatedStage.End_Date__c;
				System.debug('Start Date: ' + oUpdatedStage.Start_Date__c + '(' + mapStageUpdates.get(oStage.Id).Start_Date__c + ')  End Date: ' + oUpdatedStage.End_Date__c);
				mapStageUpdates.put(oStage.Id, oUpdatedStage);
				prevStageId = oStage.Id;
			}
			//end date for the phase is the due date of the last stage
			oUpdatedPhase.End_Date__c = dtLastStageEndDate;
			System.debug('Phase End Date: ' + oUpdatedPhase.End_Date__c);
			mapPhaseUpdates.put(oPhase.Id, oUpdatedPhase);
			prevPhaseId = oPhase.Id;
		}

		if (mapPhaseUpdates.size() > 0) {
			update mapPhaseUpdates.values();
		}
		if (mapStageUpdates.size() > 0) {
			update mapStageUpdates.values();
		}
	}
}