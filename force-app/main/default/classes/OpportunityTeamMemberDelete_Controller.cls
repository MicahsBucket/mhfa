public without sharing class OpportunityTeamMemberDelete_Controller {

	public final OpportunityTeamMember oppTeamMember;
	public final Boolean deleteAllowed {get;set;}

	public OpportunityTeamMemberDelete_Controller(ApexPages.StandardController stdController) {
		this.oppTeamMember = (OpportunityTeamMember)stdController.getRecord();
		deleteAllowed = OpportunityTeamMemberUtility.userHasTeamPermission(this.oppTeamMember.OpportunityId, (ID)UserInfo.getUserId(), 'Delete');
	}

	public PageReference deleteTeamMember() {
		PageReference retVal = null;

		if (deleteAllowed == true) {
			retVal = new PageReference('/' + this.oppTeamMember.OpportunityId);
			retVal.setRedirect(true);
			delete this.oppTeamMember;
		} else {
			this.oppTeamMember.addError('Delete permissions denied.');
		}

		return retVal;
	}

	public PageReference cancelDelete() {
		PageReference retVal = new ApexPages.StandardController(oppTeamMember).view();
		retVal.setRedirect(true);
		return retVal;
	}

}