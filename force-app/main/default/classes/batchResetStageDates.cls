global class batchResetStageDates implements Database.Batchable<SObject> {

	global final Id fundingRoundId;
    global final String soqlStmt;
    global final String fundingRoundName;
    global final Datetime dttJobStart;
    global final Integer intProjectCount;

    //constructor
	public batchResetStageDates(Id frId) {
        dttJobStart = Datetime.now();
        fundingRoundId = frId;
        for (Funding_Round__c oFR : [Select Id, Name, (Select Id From Opportunities__r) From Funding_Round__c Where Id = :frId]) {
            fundingRoundName = oFR.Name;
            intProjectCount = oFR.Opportunities__r.size();
        }

        
        system.debug('inside batchResetStageDates.  fundingRoundId: ' + fundingRoundId);

		soqlStmt = 'Select Id'
                    + ', Name '
                    + ', (Select Id'
                        + ', Name'
                        + ', Project__c'
                        + ', Estimated_Days_to_Complete__c'
                        + ', Stage_Begin__c'
                        + ', Due_Date__c'
                        + ', Project_Phase__r.Sort_Order__c'
                        + ', Sort_Order__c'
                        + ', Funding_Round_Sub_Phase__r.Start_Date__c'
                        + ', Funding_Round_Sub_Phase__r.Estimated_Days_to_Complete__c'
                        + ' From Project_Sub_Phases__r'
                        + ' Order By Project_Phase__r.Sort_Order__c, Sort_Order__c)'
                    + ' From Opportunity'
                + ' Where Funding_Round__c = :fundingRoundId'
                + ' Order By Id';
        system.debug('inside batchResetStageDates.  soql: ' + soqlStmt);

	}

    ////////////////////////////////////////////////
    //base method that runs when the class is instantiated.  Its return value is sent to the "execute" routine
    ////////////////////////////////////////////////
    global Database.QueryLocator start(Database.BatchableContext BC) {
	    system.debug('\n\n******Inside start.  soqlStmt:\n' + soqlStmt + '\n*********\n');
      
        return Database.getQueryLocator(soqlStmt);
    }

    ////////////////////////////////////////////////
    // EXECUTE
    ////////////////////////////////////////////////
    global void execute(Database.BatchableContext BC, List<SObject> jobRecords) {

        Map<Id, Project_Sub_Phase__c> mapProjectStages = new Map<Id, Project_Sub_Phase__c>();

		for (SObject oRec : jobRecords) {
            Opportunity oProject = (Opportunity)oRec;
            system.debug('Processing project ' + oProject.Name);
            if (oProject.Project_Sub_Phases__r.size() > 0) {
                for (Project_Sub_Phase__c oStage : oProject.Project_Sub_Phases__r) {
                    mapProjectStages.put(oStage.Id, oStage);
                }
            }
        }

        if (mapProjectStages.size() > 0) {
            system.debug('calling updateDueDates with \n' + mapProjectStages);
            ProjectStageUtility.updateDueDates(mapProjectStages, mapProjectStages);
        }

    }

    ////////////////////////////////////////////////
    // FINISH
    ////////////////////////////////////////////////
    global void finish(Database.BatchableContext BC) {
              
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [Select Id
                                , CompletedDate
                                , Status
                                , ExtendedStatus
                                , JobType
                                , NumberOfErrors
                                , JobItemsProcessed
                                , TotalJobItems 
                            from AsyncApexJob 
                            where Id = :BC.getJobId()];
        
        // Send an email to the user who initiated the process notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Reset Stage Dates for Funding Round \'' + fundingRoundName +'\' completed');
        String strBody = 'The routine to reset stage dates for funding round \'' + fundingRoundName +'\' has completed.' +
                        '\n\nStart Date/Time: ' + dttJobStart.format('MM/dd/yyyy h:mm a') +
                        '\nCompleted Date/Time: ' + a.CompletedDate.format('MM/dd/yyyy h:mm a') +
                        '\nJob Status: ' + a.Status +
                        '\nTotal Projects Processed: ' + intProjectCount +
                        '\nTotal Batches Processed: ' + a.TotalJobItems;
        if (a.NumberOfErrors > 0) {
            strBody += '\nProjects with Errors: ' + a.NumberOfErrors +
                        '\nError Details: ' + a.ExtendedStatus;
        }
                        
        mail.setPlainTextBody(strBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }    

}