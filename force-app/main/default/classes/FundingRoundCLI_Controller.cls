/*
*   {Purpose}   Enables the association of triggers to Funding Round Checklist Items
*
*   {Contact}   support@demandchainsystems.com
*               www.demandchainsystems.com
*               612-424-0032                  
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date         Name                    Description
*   20160912     Eric Gronholz DCS       Adapted to new structures
*   =============================================================================
*/
public without sharing class FundingRoundCLI_Controller {
    public FundingRoundCLI_Controller(ApexPages.StandardSetController controller) {

    }


	public Funding_Round_Checklist_Item__c oFRCLI {get;set;}
	public Id frcliID {get;set;}
	public Id fundingRoundId {get;set;}
	public List<typeFundingRoundPhase> lstPhaseDetails {get;set;}
	public Set<Id> setSelectedTriggerIds {get;set;}
	public Set<Id> setSelectedSubPhaseIds {get;set;}
	public Map<Id, Funding_Round_Checklist_Item_SubPhase__c> mapSelectedSubPhases {get;set;}

	//////////////////////////////////////////////////////////////////////
	//constructor - standard controller (detail button)
	//////////////////////////////////////////////////////////////////////	
	public FundingRoundCLI_Controller(ApexPages.StandardController stdController) {
		system.debug('StandardController: ' + stdController);
		oFRCLI = (Funding_Round_Checklist_Item__c)stdController.getRecord();
		system.debug('stdController constructor: ' + oFRCLI);
		frcliID = oFRCLI.Id;
		fundingRoundId = oFRCLI.Funding_Round__c;
		lstPhaseDetails = retrieveFundingRoundPhaseDetails(fundingRoundId, frcliID);
	}

	//////////////////////////////////////////////////////////////////////
	//find the phases, sub-phases, trigger groups, and triggers
	// associated to the given funding round
	//////////////////////////////////////////////////////////////////////	
	private List<typeFundingRoundPhase> retrieveFundingRoundPhaseDetails(Id frId, Id frcliID) {
		List<typeFundingRoundPhase> lstPhaseDetails = new List<typeFundingRoundPhase>();

		for (Phase__c oPhase : [Select Id
									, Name
									, Phase__c
									, Start_Date__c
									, End_Date__c
									, Internal_Only__c
									, Sort_Order__c
									, Description__c
									, Funding_Round__c
									, Funding_Round__r.Name
									, (Select Id
											, Name
											, Sub_Phase__c
											, Sort_Order__c
											, Description__c
											, Due_Date_Offset__c
											, Offset_Type__c
											, Required_for_All__c
										From SubPhases__r
										Order By Sort_Order__c)
									, (Select Id
											, Name
											, Trigger__c
											, Trigger__r.Name
											, Sort_Order__c
											, Sort_Order_in_Phase__c
											, Required_for_All__c
											, Trigger_Group__c
											, Trigger_Group__r.Name
											, Trigger_Group__r.Trigger_Group_Name__c
											, Trigger_Group__r.Description__c
											, Trigger_Group__r.Sort_Order__c
										From Funding_Round_Triggers__r
										Order By Sort_Order_in_Phase__c)
								From Phase__c
								Where Funding_Round__c = :frId
								Order By Sort_Order__c]) {

			typeFundingRoundPhase oPhaseDetails = new typeFundingRoundPhase();
			Phase__c oHoldPhase = new Phase__c(Id = oPhase.Id
												, Phase__c = oPhase.Phase__c
												, Start_Date__c = oPhase.Start_Date__c
												, End_Date__c = oPhase.End_Date__c
												, Internal_Only__c = oPhase.Internal_Only__c
												, Sort_Order__c = oPhase.Sort_Order__c
												, Funding_Round__c = oPhase.Funding_Round__c);
			oPhaseDetails.oPhase = oHoldPhase;
			oPhaseDetails.lstTriggerGroups = prepareTriggerGroupStructures(oPhase.Funding_Round_Triggers__r, frcliID);
			system.debug('Phase: ' + oHoldPhase.Phase__c + '\n' + oPhaseDetails.lstTriggerGroups);
			oPhaseDetails.mapGroupRows = prepareGroupRows(oPhaseDetails.lstTriggerGroups);
			oPhaseDetails.lstSubPhases = prepareSubPhaseStructures(oPhase.SubPhases__r, frcliID);
			oPhaseDetails.mapSubPhasePositions = prepareSubPhaseMap(oPhaseDetails.lstSubPhases);
			lstPhaseDetails.add(oPhaseDetails);
			system.debug('Processing phase ' + oHoldPhase.Phase__c);
		}
		return lstPhaseDetails;							
	}

	//////////////////////////////////////////////////////////////////////
	//build out the trigger group structure using the given trigger records
	//////////////////////////////////////////////////////////////////////	
	private List<typeFundingRoundPhase.FR_TriggerGroupDetails> prepareTriggerGroupStructures(List<Funding_Round_Trigger__c> lstTriggers, Id frcliId) {
		setSelectedTriggerIds = retrieveSelectedTriggerIds(frcliID);

		List<typeFundingRoundPhase.FR_TriggerGroupDetails> lstTGD = new List<typeFundingRoundPhase.FR_TriggerGroupDetails>();
		Set<Id> setTriggerGroupIds = new Set<Id>();
		typeFundingRoundPhase.FR_TriggerGroupDetails oTGD;
		List<typeFundingRoundPhase.FR_TriggerDetails> lstGroupTriggers = new List<typeFundingRoundPhase.FR_TriggerDetails>();

		for (Funding_Round_Trigger__c oTrigger : lstTriggers) {
			//the first time a trigger group is reached, add the triggers to the last group processed and 
			// and then create the next group detail class and list of triggers
			if (setTriggerGroupIds.contains(oTrigger.Trigger_Group__c) == false) {
				if (oTGD != null) {
					oTGD.lstTriggers = lstGroupTriggers;
					lstTGD.add(oTGD);
				}
				oTGD = new typeFundingRoundPhase.FR_TriggerGroupDetails();
				oTGD.oTriggerGroup = new Trigger_Group__c(Id = oTrigger.Trigger_Group__c
															, Trigger_Group_Name__c = oTrigger.Trigger_Group__r.Trigger_Group_Name__c
															, Description__c = oTrigger.Trigger_Group__r.Description__c
															, Sort_Order__c = oTrigger.Trigger_Group__r.Sort_Order__c);

				system.debug('Building new trigger group: ' + oTGD.oTriggerGroup.Trigger_Group_Name__c);
				lstGroupTriggers = new List<typeFundingRoundPhase.FR_TriggerDetails>();
				setTriggerGroupIds.add(oTrigger.Trigger_Group__c);
			}
			typeFundingRoundPhase.FR_TriggerDetails oTD = new typeFundingRoundPhase.FR_TriggerDetails();
			oTD.oTrigger = oTrigger;
			oTD.isChecked = setSelectedTriggerIds.contains(oTrigger.Id);
/*
			if (oTrigger.Required_for_All__c) {
				oTD.isChecked = true;
				oTD.isDisabled = true;
			}
*/			
			lstGroupTriggers.add(oTD);
		}
		//handle last trigger
		if (oTGD != null && oTGD.lstTriggers != null) {
			oTGD.lstTriggers = lstGroupTriggers;
			lstTGD.add(oTGD);
		}		
		return lstTGD;
	}

	//////////////////////////////////////////////////////////////////////
	//put the triggers groups and associated triggers into a map that
	// breaks the groups into rows and columns for display purposes
	//////////////////////////////////////////////////////////////////////	
	private Map<Integer, List<typeFundingRoundPhase.FR_TriggerGroupDetails>> prepareGroupRows(List<typeFundingRoundPhase.FR_TriggerGroupDetails> lstTriggerGroups) {
		//break the trigger groups into a set number of columns by storing them in a map
		//hard-coded # of columns; should add to a custom setting or save with the FR Phase
		Integer MAXCOLUMNS = 5;
		Integer rowCounter = 0;
		Integer groupCounter = 0;
		Map<Integer, List<typeFundingRoundPhase.FR_TriggerGroupDetails>> mapGroupRows = new Map<Integer, List<typeFundingRoundPhase.FR_TriggerGroupDetails>>();
		List<typeFundingRoundPhase.FR_TriggerGroupDetails> lstRowGroups = new List<typeFundingRoundPhase.FR_TriggerGroupDetails>();

		for (typeFundingRoundPhase.FR_TriggerGroupDetails trgGroup : lstTriggerGroups) {
			groupCounter++;
			if (groupCounter > MAXCOLUMNS) {
				mapGroupRows.put(rowCounter, lstRowGroups);
				rowCounter++;
				groupCounter = 1;
				lstRowGroups = new List<typeFundingRoundPhase.FR_TriggerGroupDetails>();
			}
			lstRowGroups.add(trgGroup);
		}
		mapGroupRows.put(rowCounter, lstRowGroups);
		return mapGroupRows;		
	}

	//////////////////////////////////////////////////////////////////////
	//retrieve the funding round trigger ids associated to the given 
	// funding round checklist item
	//////////////////////////////////////////////////////////////////////
	private Set<Id> retrieveSelectedTriggerIds (Id frcliID) {
		Set<Id> setTriggerIds = new Set<Id>();
		for (Funding_Round_Checklist_Item_Trigger__c oCliTrigger : [Select Funding_Round_Trigger__c
																	From Funding_Round_Checklist_Item_Trigger__c
																	Where Funding_Round_Checklist_Item__c = :frcliID]) {
			setTriggerIds.add(oCliTrigger.Funding_Round_Trigger__c);
		}
		return setTriggerIds;
	}

	//////////////////////////////////////////////////////////////////////
	//build out the sub-phase structure using the given sub-phase records
	//////////////////////////////////////////////////////////////////////	
	private List<typeFundingRoundPhase.FR_SubPhaseDetails> prepareSubPhaseStructures(List<Phase__c> lstSubPhases, Id frcliId) {
		setSelectedSubPhaseIds = retrieveSelectedSubPhaseIds(frcliID);
		List<typeFundingRoundPhase.FR_SubPhaseDetails> lstSubPhaseDetails = new List<typeFundingRoundPhase.FR_SubPhaseDetails>();

		for (Phase__c oSubPhase : lstSubPhases) {
			typeFundingRoundPhase.FR_SubPhaseDetails oSubPhaseDetails = new typeFundingRoundPhase.FR_SubPhaseDetails();
			oSubPhaseDetails.oSubPhase = oSubPhase;
			oSubPhaseDetails.isChecked = setSelectedSubPhaseIds.contains(oSubPhase.Id);
			oSubPhaseDetails.oHoldFrCliSubPhase = new Funding_Round_Checklist_Item_SubPhase__c();
			if (oSubPhaseDetails.isChecked) {
				oSubPhaseDetails.oHoldFrCliSubPhase.Offset_Days__c = mapSelectedSubPhases.get(oSubPhase.Id).Offset_Days__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Offset_Type__c = mapSelectedSubPhases.get(oSubPhase.Id).Offset_Type__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Priority_Level__c = mapSelectedSubPhases.get(oSubPhase.Id).Priority_Level__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Resubmit_Required__c = mapSelectedSubPhases.get(oSubPhase.Id).Resubmit_Required__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Reapproval_Required__c = mapSelectedSubPhases.get(oSubPhase.Id).Reapproval_Required__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Approval_First__c = mapSelectedSubPhases.get(oSubPhase.Id).Approval_First__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Approval_Second__c = mapSelectedSubPhases.get(oSubPhase.Id).Approval_Second__c;
				oSubPhaseDetails.oHoldFrCliSubPhase.Approval_Third__c = mapSelectedSubPhases.get(oSubPhase.Id).Approval_Third__c;
			}
			lstSubPhaseDetails.add(oSubPhaseDetails);
		}

		return lstSubPhaseDetails;
	}

	//////////////////////////////////////////////////////////////////////
	//retrieve the sub-phase ids associated to the given 
	// funding round checklist item
	//////////////////////////////////////////////////////////////////////
	private Set<Id> retrieveSelectedSubPhaseIds (Id frcliID) {
		mapSelectedSubPhases = new Map<Id, Funding_Round_Checklist_Item_SubPhase__c>();
		for (Funding_Round_Checklist_Item_SubPhase__c oCliSubPhase : [Select Id
																			, SubPhase__c
																			, Offset_Days__c
																			, Offset_Type__c
																			, Priority_Level__c
																			, Resubmit_Required__c
																			, Reapproval_Required__c
																			, Approval_First__c
																			, Approval_Second__c
																			, Approval_Third__c
																	From Funding_Round_Checklist_Item_SubPhase__c
																	Where Funding_Round_Checklist_Item__c = :frcliID]) {
			mapSelectedSubPhases.put(oCliSubPhase.SubPhase__c, oCliSubPhase);
		}
		return mapSelectedSubPhases.keySet();
	}

	//////////////////////////////////////////////////////////////////////
	//create a map of sub-phase ids and the position in the list where
	// that id is located.  use this approach because maps are not sorted
	// but lists are sorted
	//////////////////////////////////////////////////////////////////////
	private Map<Id, Integer> prepareSubPhaseMap (List<typeFundingRoundPhase.FR_SubPhaseDetails> lstSubPhaseDetails) {
		Map<Id, Integer> mapSubPhasePositions = new Map<Id, Integer>();
		Integer position = 0;
		for (typeFundingRoundPhase.FR_SubPhaseDetails oSubPhaseDetails : lstSubPhaseDetails) {
			mapSubPhasePositions.put(oSubPhaseDetails.oSubPhase.Id, position);
			position++;
		}
		return mapSubPhasePositions;

	}

	//////////////////////////////////////////////////////////////////////
	//build the tabs for each phase associated to the funding round
	//////////////////////////////////////////////////////////////////////	
	public Component.Apex.TabPanel getPhaseTabs() {
		Component.Apex.TabPanel tabPanel = new Component.Apex.TabPanel();
		for (typeFundingRoundPhase oPhaseDetails : lstPhaseDetails) {
			system.debug('Building tab for ' + oPhaseDetails.oPhase.Phase__c + '\n' + oPhaseDetails);
			Component.Apex.Tab newTab = new Component.Apex.Tab();
			newTab.Label = oPhaseDetails.oPhase.Phase__c;
			newTab.Name = 'tab_' + oPhaseDetails.oPhase.Id;
			newTab.Id = 'tab_' + oPhaseDetails.oPhase.Id;
			newTab.switchType = 'client';
			Component.PhaseTriggers comPhaseTriggers = new Component.PhaseTriggers(oPhaseDetails=oPhaseDetails, oFRCLI=oFRCLI);
			newTab.childComponents.add(comPhaseTriggers);
			tabPanel.childComponents.add(newTab);
		}
		return tabPanel;
	}

	//////////////////////////////////////////////////////////////////////	
	//delete triggers and sub-phases associated to the given funding 
	// round checklist item
	//////////////////////////////////////////////////////////////////////
	public PageReference saveChanges() {
		//delete triggers and sub-phases currently associated to the funding round checklist item
		savePhaseTriggers(frcliId);
		saveSubPhases(frcliId);
		return null;
	}

	//////////////////////////////////////////////////////////////////////	
	//delete triggers associated to the given funding round checklist 
	// item that are no longer selected and then insert the newly selected
	// items
	//////////////////////////////////////////////////////////////////////	
	public void savePhaseTriggers(Id frcliId) {
		setSelectedTriggerIds = retrieveSelectedTriggerIds(frcliID);

		Set<Id> setDeleteIds = new Set<Id>();
		List<Funding_Round_Checklist_Item_Trigger__c> lstInsertTriggers = new List<Funding_Round_Checklist_Item_Trigger__c>();

		for (typeFundingRoundPhase oCurrentPhaseDetails : lstPhaseDetails) {
			for (Integer rowNumber : oCurrentPhaseDetails.mapGroupRows.keySet()) {
				for (typeFundingRoundPhase.FR_TriggerGroupDetails oGroupDetails : oCurrentPhaseDetails.mapGroupRows.get(rowNumber)) {
					for (typeFundingRoundPhase.FR_TriggerDetails oTriggerDetails : oGroupDetails.lstTriggers) {
						if (oTriggerDetails.isChecked == false) {
							setDeleteIds.add(oTriggerDetails.oTrigger.Id);
						} else {
							//insert triggers that aren't already associated to the checklist item
							if (setSelectedTriggerIds.contains(oTriggerDetails.oTrigger.Id) == false) {
								Funding_Round_Checklist_Item_Trigger__c oRec = new Funding_Round_Checklist_Item_Trigger__c();
								oRec.Funding_Round_Checklist_Item__c = frcliId;
								oRec.Funding_Round_Trigger__c = oTriggerDetails.oTrigger.Id;
								lstInsertTriggers.add(oRec);
							}
						}
					}
				}
			}
		}

		List<Funding_Round_Checklist_Item_Trigger__c> lstDelete = [Select Id
																	From Funding_Round_Checklist_Item_Trigger__c
																	Where Funding_Round_Checklist_Item__c = :frcliId
																		And Funding_Round_Trigger__c in :setDeleteIds];
		if (lstDelete.size() > 0) {
			system.debug('Deleting ' + lstDelete.size() + ' Triggers');
			delete lstDelete;
		}

		if (lstInsertTriggers.size() > 0) {
			system.debug('Inserting ' + lstInsertTriggers.size() + ' Triggers');
			insert lstInsertTriggers;
		}

		setSelectedTriggerIds = retrieveSelectedTriggerIds(frcliID);		
	}

	//////////////////////////////////////////////////////////////////////	
	//delete sub-phases associated to the given funding round checklist 
	// item that are no longer selected and then insert the newly selected
	// items
	//////////////////////////////////////////////////////////////////////	
	private void saveSubPhases(Id frcliId) {
		setSelectedSubPhaseIds = retrieveSelectedSubPhaseIds(frcliID);

		Set<Id> setDeleteIds = new Set<Id>();
		List<Funding_Round_Checklist_Item_SubPhase__c> lstInsertSubPhases = new List<Funding_Round_Checklist_Item_SubPhase__c>();

		for (typeFundingRoundPhase oCurrentPhaseDetails : lstPhaseDetails) {
			for (typeFundingRoundPhase.FR_SubPhaseDetails oSubPhaseDetails : oCurrentPhaseDetails.lstSubPhases) {
				if (oSubPhaseDetails.isChecked == false) {
					setDeleteIds.add(oSubPhaseDetails.oSubPhase.Id);
				} else {
					//insert sub phases that aren't already associated to the checklist item, or update existing records
					Funding_Round_Checklist_Item_SubPhase__c oRec;
					if (setSelectedSubPhaseIds.contains(oSubPhaseDetails.oSubPhase.Id) == false) {
						oRec = new Funding_Round_Checklist_Item_SubPhase__c();
						oRec.Funding_Round_Checklist_Item__c = frcliId;
					} else {
						oRec = new Funding_Round_Checklist_Item_SubPhase__c(Id=mapSelectedSubPhases.get(oSubPhaseDetails.oSubPhase.Id).Id);
					}
					oRec.SubPhase__c = oSubPhaseDetails.oSubPhase.Id;
					oRec.Offset_Days__c = oSubPhaseDetails.oHoldFrCliSubPhase.Offset_Days__c;
					oRec.Offset_Type__c = oSubPhaseDetails.oHoldFrCliSubPhase.Offset_Type__c;
					oRec.Priority_Level__c = oSubPhaseDetails.oHoldFrCliSubPhase.Priority_Level__c;
					oRec.Resubmit_Required__c = oSubPhaseDetails.oHoldFrCliSubPhase.Resubmit_Required__c;
					oRec.Reapproval_Required__c = oSubPhaseDetails.oHoldFrCliSubPhase.Reapproval_Required__c;
					oRec.Approval_First__c = oSubPhaseDetails.oHoldFrCliSubPhase.Approval_First__c;
					oRec.Approval_Second__c = oSubPhaseDetails.oHoldFrCliSubPhase.Approval_Second__c;
					oRec.Approval_Third__c = oSubPhaseDetails.oHoldFrCliSubPhase.Approval_Third__c;

					lstInsertSubPhases.add(oRec);
				}
			}
		}

		List<Funding_Round_Checklist_Item_SubPhase__c> lstDelete = [Select Id
																	From Funding_Round_Checklist_Item_SubPhase__c
																	Where Funding_Round_Checklist_Item__c = :frcliId
																		And SubPhase__c in :setDeleteIds];
		if (lstDelete.size() > 0) {
			system.debug('Deleting ' + lstDelete.size() + ' Sub Phases');
			delete lstDelete;
		}

		if (lstInsertSubPhases.size() > 0) {
			system.debug('Inserting ' + lstInsertSubPhases.size() + ' Sub Phases');
			upsert lstInsertSubPhases;
		}
		setSelectedSubPhaseIds = retrieveSelectedSubPhaseIds(frcliID);
	}

}