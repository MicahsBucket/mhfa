@isTest
private class ProjectCLI_Methods_TEST {    

	@testSetup static void setupTestData() {
        //stage data
        Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('After buildFundingRoundDataStructure.  Queries: ' + Limits.getQueries());

        //create project
        Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		System.debug('After buildProjectDataStructure.  Queries: ' + Limits.getQueries());

/*
		for (Funding_Round_Trigger__c oFrChar : [Select Id
													, Name
													, Trigger_Group__r.Name
													, Selection_Type__c 
												From Funding_Round_Trigger__c 
												Where Trigger_Group__r.Funding_Round__c = :oFundingRound.Id]) {
			System.debug('oFrChar: ' + oFrChar);
			TestDataUtility.createProjectCharacteristic(oFrChar.Id, oProject.Id, oFrChar.Selection_Type__c);
		}
*/
		System.debug('After createProjectCharacteristic.  Queries: ' + Limits.getQueries());
		TestDataUtility.createOpportunityTeamMember(oProject.Id, 'Loan Processor', UserInfo.getUserId());

        //stage custom settings
        Org_URLs__c csORGurls = new Org_URLs__c();         
        csORGurls.External_URL__c = 'https://mnhousing.force.com/MultifamilyPortal/';        
        csORGurls.Internal_URL__c = 'https://mnhousing.my.salesforce.com/';           
        insert csORGurls;

		Org_Environment__c csORGenvironment = new Org_Environment__c();
		csORGenvironment.Environment__c = 'DEV';
		insert csORGenvironment;
	}

    @isTest static void test_CreatePWBV() {  
	
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];
	     
		//get Application checklist and an item linked to it
		Phase_Checklist_Item__c oAppChecklistItem = [Select Id
														, Name
														, Funding_Round_Checklist_Item__c
														, Phase_Checklist__c
														, Phase_Checklist__r.Name
														, Phase_Checklist__r.Funding_Round__c
														, Phase_Checklist__r.Create_Project_Workbook_Version__c
														, Phase_Checklist__r.Workbook_To_Create_Primary__c
														, Phase_Checklist__r.Workbook_To_Create_Secondary__c
														, Phase_Checklist__r.Workbook_Version_Parent_Primary__c
														, Phase_Checklist__r.Workbook_Version_Parent_Secondary__c
													From Phase_Checklist_Item__c 
													Where Phase_Checklist__r.Funding_Round__c = :oFundingRound.Id 
														And Phase_Checklist__r.Name = 'Application' LIMIT 1];
		System.debug('oAppChecklistItem: ' + oAppChecklistItem + '\nChecklist Name: ' + oAppChecklistItem.Phase_Checklist__r.Name);
		
        //add additional values specific for test
		Phase_Checklist__c oChecklist = new Phase_Checklist__c(Id=oAppChecklistItem.Phase_Checklist__c);
        oChecklist.Create_Project_Workbook_Version__c = TRUE; 
        oChecklist.Workbook_To_Create_Primary__c = 'Multifamily Workbook';
        oChecklist.Workbook_To_Create_Secondary__c = 'Dual Multifamily Workbook';
        oChecklist.Workbook_Version_Parent_Primary__c = 'MUPT Application';
        oChecklist.Workbook_Version_Parent_Secondary__c = 'MUPT Application';
        update oChecklist;
        system.debug('***Queries after update oChecklist: ' + Limits.getQueries());

        //create pclis
        Project_Checklist_Item__c pcliDualWB = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                          oAppChecklistItem.Funding_Round_Checklist_Item__c,
                                                                                          false,
                                                                                          null,
                                                                                          null,
																						  null,
																						  null);
        system.debug('***Queries after createProjectChecklistItem: ' + Limits.getQueries());

        //add additional values specific for test
        pcliDualWB.Name = 'Dual Multifamily Workbook';
		pcliDualWB.Funding_Round_Phase_Checklist_Item__c = oAppChecklistItem.Id;
        update pcliDualWB;
		System.debug('pcliDualWB: ' + pcliDualWB);
        system.debug('***Queries after update pcliDualWB to \'Dual Multifamily Workbook\': ' + Limits.getQueries());

        Project_Checklist_Item__c pcliWB = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                          oAppChecklistItem.Funding_Round_Checklist_Item__c,
                                                                                          false,
                                                                                          null,
                                                                                          null,
																						  null,
																						  null);
        system.debug('***Queries after createProjectChecklistItem: ' + Limits.getQueries());

        //add additional values specific for test
        pcliWB.Name = 'Multifamily Workbook';
		pcliWB.Funding_Round_Phase_Checklist_Item__c = oAppChecklistItem.Id;
        update pcliWB;
		System.debug('pcliWB: ' + pcliWB);

        system.debug('***Queries after update pcliDualWB to \'Multifamily Workbook\': ' + Limits.getQueries());

        //create pclips
        //Project_Checklist_Item_Phase__c pclipDualWB = TestDataUtility.createProjectChecklistItemPhase(pcliDualWB.id, 
        //                                                                                              frp.Id,
        //                                                                                              false);
        system.debug('***Queries after createProjectChecklistItemPhase: ' + Limits.getQueries());

        //Project_Checklist_Item_Phase__c pclipWB = TestDataUtility.createProjectChecklistItemPhase(pcliWB.id, 
        //                                                                                           frp.Id,
        //                                                                                           false);
        system.debug('***Queries after createProjectChecklistItemPhase: ' + Limits.getQueries());

        //create pwbs
        Project_Workbook__c pwbDualWB = TestDataUtility.createProjectWorkbook(oProject.Id);
        pwbDualWB.Workbook_Level__c = 'Secondary';
        update pwbDualWB;

        Project_Workbook__c pwbWB = TestDataUtility.createProjectWorkbook(oProject.Id);
        pwbWB.Workbook_Level__c = 'Primary';
        update pwbWB;  
        system.debug('***Queries after update pwbWB: ' + Limits.getQueries());

        Test.startTest();

        //create pwbvs
        Project_Workbook_Version__c pwbvDualWB = TestDataUtility.createProjectWorkbookVersion(pwbDualWB.Id,
                                                                                             'MUPT Application');
        system.debug('***Queries after createProjectWorkbookVersion: ' + Limits.getQueries());

        Project_Workbook_Version__c pwbvWB = TestDataUtility.createProjectWorkbookVersion(pwbWB.Id,
                                                                                          'MUPT Application');
        system.debug('***Queries after createProjectWorkbookVersion: ' + Limits.getQueries());

        system.debug('***Queries used prior to startTest: ' + Limits.getQueries());

                           
            //create contentversions (file)
            ContentVersion cvXLSMWB =new ContentVersion(Title = 'cvXLSMWB',
                                                      PathOnClient= '/Test.xlsm',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvXLSMWB;
            system.debug('***Queries after new ContentVersion: ' + Limits.getQueries());
            
            String cvIDWB = cvXLSMWB.Id;
            List<ContentVersion> cvWB = new List<ContentVersion>([SELECT Id
                                                                       , Title
                                                                       , ContentDocumentId 
                                                                FROM ContentVersion 
                                                                WHERE Id = :cvIDWB]);
        
            ContentDocumentLink cdlPCLIPWB = new ContentDocumentLink(LinkedEntityId = pcliWB.Id,
                                                                    ContentDocumentId = cvWB[0].ContentDocumentId,
                                                                    ShareType = 'V');
            insert cdlPCLIPWB;
            
            ProjectCLI_Methods.createPWBV('Multifamily Workbook', 
                                          'MUPT Application', 
                                          'Dual Multifamily Workbook', 
                                          'MUPT Application',
                                          'Application',
                                          oProject.Id);        
            /*
            ContentVersion cvXLSMDualWB =new ContentVersion(Title = 'cvXLSMDualWB',
                                                      PathOnClient= '/Test.xlsm',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvXLSMDualWB

            system.debug('***Queries after new ContentVersion: ' + Limits.getQueries());
            //return cv contentdocumentids
            String cvIDDualWB = cvXLSMDualWB.Id;
            List<ContentVersion> cvDualWB = new List<ContentVersion>([SELECT Id
                                                                           , Title
                                                                           , ContentDocumentId 
                                                                     FROM ContentVersion 
                                                                     WHERE Id = :cvIDDualWB]);          
                    
            //create contentdocumentlink records associating files to pclips
            ContentDocumentLink cdlPCLIPDualWB = new ContentDocumentLink(LinkedEntityId = pclipDualWB.Id,
                                                                        ContentDocumentId = cvDualWB[0].ContentDocumentId,
                                                                        ShareType = 'V');
            try {
              insert cdlPCLIPDualWB;
            } catch (Exception ex) {
              if (ex.getMessage().contains('You do not have access to upload Files to this Project')) {
                //add user to the project team
                TestDataUtility.createOpportunityTeamMember(p.Id, 'Loan Processor', UserInfo.getUserId());
              }
            }
            */
        Test.stopTest();
    }

    @isTest static void test_Retrieve() {
        //stage data
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];
      
        Test.startTest();

        Phase__c oPhase = [Select Id
                                , Funding_Round__c
                            From Phase__c
                            Where Funding_Round__c = :oProject.Funding_Round__c LIMIT 1];
        //typeFundingRoundPhase typeTest1 = new typeFundingRoundPhase(oPhase.Id);
        //typeFundingRoundPhase typeTest2 = new typeFundingRoundPhase(oPhase.Id, oProject.Id);

        Map<Id, Map<Id, Project_Checklist_Item_Attributes__c>> mapTriggers = ProjectCLI_Methods.retrievePCLIs_Trigger(oProject.Id);
        Map<Id, Map<Id, Project_Checklist_Item_Attributes__c>> mapSubPhases = ProjectCLI_Methods.retrievePCLIs_SubPhase(oProject.Id);

		ProjectCLI_Methods.deletePCLIs_CLIAs(oProject.Id, oPhase.Id);
        Test.stopTest();
    }
    

    @isTest static void test_DueDateChange() {
        //stage data
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];
      
        Set<Id> setProjectIds = new Set<Id>();
        setProjectIds.add(oProject.Id);
        Opportunity oUpdateProject = [Select Id, CloseDate, Funding_Round__c From Opportunity Where Id = :oProject.Id LIMIT 1];

        Map<Id, Project_Sub_Phase__c> mapPSP = new Map<Id, Project_Sub_Phase__c>();
        for (Project_Sub_Phase__c oPSP : [Select Id
                                                , Project__c
                                                , Funding_Round_Sub_Phase__c
                                                , Offset_Type__c
                                                , Due_Date_Offset__c
                                                , Due_Date__c
                                                , Base_Due_Date__c
                                                , Project__r.CloseDate
                                        From Project_Sub_Phase__c
                                        Where Project__c in :setProjectIds
                                            And Funding_Round_Sub_Phase__c != null
                                            And Offset_Type__c = null]) {
            //system.assert(1==2, '***oPSP: ' + oPSP);
            mapPSP.put(oPSP.Funding_Round_Sub_Phase__c, oPSP);
        }
        
        for (Integer recCount = 0; recCount < mapPSP.values().size(); recCount++) {
            if (mapPSP.values()[recCount].Due_Date__c == null) {
                mapPSP.values()[recCount].Due_Date__c = oUpdateProject.CloseDate;
            }
        }
        update mapPSP.values();

        Test.startTest();

        oUpdateProject.CloseDate = oUpdateProject.CloseDate.addDays(-12);
        update oUpdateProject;

        ProjectCLI_Methods.updateDueDates(oProject.Id, oUpdateProject.CloseDate, null, mapPSP);

        Test.stopTest();
    }


    @isTest static void test_DeleteRecs() {
        //stage data
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];
		Project_Sub_Phase__c oStage = [Select Id, Name From Project_Sub_Phase__c Where Project__c = :oProject.Id LIMIT 1];

        Project_Checklist_Item__c oPCLI1 = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                          null,
                                                                                          false,
                                                                                          null,
                                                                                          null,
																						  oStage.Id,
																						  null);
        Project_Checklist_Item__c oPCLI2 = TestDataUtility.createProjectChecklistItem(oProject.Id, 
                                                                                          null,
                                                                                          false,
                                                                                          null,
                                                                                          null,
																						  oStage.Id,
																						  null);


        Set<Id> setProjectIds = new Set<Id>();
        setProjectIds.add(oProject.Id);

        for (Project_Checklist_Item__c oPCLI : [Select Id
                                                    , Project__c 
                                                From Project_Checklist_Item__c
                                                Where Project__c in :setProjectIds
                                                LIMIT 1]) {
            oPCLI.Project__c = null;
            update oPCLI;
        }
        
		Project_Checklist_Item__c oPCLI = [Select Id
                                                , Approval_Status__c
                                                , Funding_Round_Phase__c
												, Funding_Round_Stage__c
                                                , Project_Stage__c
                                            From Project_Checklist_Item__c
                                            Where Project__c = :oProject.Id LIMIT 1];  

        Test.startTest();
        ProjectCLI_Methods.deleteOrphanedCLIs();
        ProjectCLI_Methods.deleteCLIs(setProjectIds);


        Set<Id> setDeleteIds = new Set<Id>();
        setDeleteIds.add(oPCLI.Project_Stage__c);
        ProjectCLI_Methods.removeUnselectedRecords(oProject.Id, setDeleteIds);
        Test.stopTest();

    }


    @isTest static void test_ApprovalStatus() {
        //stage data
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];

        Set<Id> setProjectIds = new Set<Id>();

        Test.startTest(); 
/*
        Project_Checklist_Item_Phase__c oPCLIP = [Select Id
                                                            , Approval_Status__c
                                                            , Funding_Round_Phase__c
                                                            , Funding_Round_Sub_Phase__c
                                                        From Project_Checklist_Item_Phase__c
                                                        Where Project_Checklist_Item__r.Project__c = :oProject.Id LIMIT 1];


/*
        ProjectCLI_Methods.assignPclipApprovalStatus(oProject.Id
                                                    , oPCLIP.Funding_Round_Phase__c
                                                    , oPCLIP.Funding_Round_Sub_Phase__c
                                                    , 'Submitted');

        ProjectCLI_Methods.countPCLIPsRequired(oProject.Id
                                                    , oPCLIP.Funding_Round_Phase__c
                                                    , oPCLIP.Funding_Round_Sub_Phase__c);
*/
        Test.stopTest();

    }


    @isTest static void test_ProjectPhases() {
        //stage data
		Funding_Round__c oFundingRound = [Select Id, Name From Funding_Round__c LIMIT 1];
		Opportunity oProject = [Select Id, Name, Funding_Round__c From Opportunity LIMIT 1];

        Project_Phase__c oPP = [Select Id
                                        , Funding_Round_Phase__c
                                        , Phase_Status__c
                                    From Project_Phase__c
                                    Where Project__c = :oProject.Id LIMIT 1];

        oPP.Phase_Status__c = 'Complete';
        update oPP;

        Test.startTest();
        oPP.Phase_Status__c = 'In Process';
        update oPP;
        Test.stopTest();
    }

	@isTest static void test_DeprecatedMethods() {
		ProjectCLI_Methods.buildPclipName(null);
		ProjectCLI_Methods.resetDueDates_ProjectPhase(null, null);
		ProjectCLI_Methods.resetDueDates_FundingRoundPhase(null, null);
	}
}