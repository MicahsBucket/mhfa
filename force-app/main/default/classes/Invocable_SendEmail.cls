/*
*   {purpose}   Manage email notifications triggered from record updates.
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name            Description
*   09/22/17    Eric Gronholz	Created
*   01/24/18    Kevin Johnson   Added check to see if addressee is populated (!lstRecipientIds.isEmpty()) before composing/sending email
*   03/14/18    Kevin Johnson   Added parameter Funding Round Team Member Role / function findFundingRoundTeamMemberIds
*   04/02/18    Eric Gronholz   Bulkified the send email process
*	06/27/18	Kevin Johnson	Added parameter Public Group / function findPublicGroupRecipients
*	07/30/18	Kevin Johnson	Added setOrgWideEmailAddressId attribute to message
*	11/26/18	Kevin Johnson	-Added parameters: blnAllInternalTeamMemberNotifyFlag, recipientUserId2
*								-Updated findProjectTeamRecipients to process blnIndividualTeamMemberNotifyFlag and blnAllInternalTeamMemberNotifyFlag		
*   =============================================================================
*/

public without sharing class Invocable_SendEmail {
/*
*   {purpose}   Manage email notifications triggered from record updates.  Needed to support requirement for dynamic email addressing across
*               objects.
*                
*   {function}  Class called from Process Builder jobs passing variables enabling class to build addressees mapped to a designated email template
*
*   {trigger}   Process Builder:
*                   -MF: Set Phase Submission Data
*                   -MF: PCLIP Changes Required notification
*					-MF: File Has Error notifications: PCLIP & PWBV
*					-MF: Make Public notifications: PWBV, Project Document: Customer, Project Document: Partner
*                   -MF: Project Narrative Author notification Process Builder jobs...detail once built 
*					-MF: Project Unacceptable Practice notification
*					-MF: Set Phase Submission Data     
*/
    //input variables
    public class EmailRequest {
    	//required parameters
        @InvocableVariable(label='Parent Record Id' 
        					description='The ID of the record containing data to be included in the email' 
        					required=true)
        public Id parentRecordId;
        
        @InvocableVariable(label='Project Id' 
        					description='The ID of the project associated to the parent record'
        					required=true)
        public Id projectId;

        @InvocableVariable(label='Email Template Id' 
        					description='The ID of the email template to use when sending the email'
        					required=true)
        public Id emailTemplateId;
        
        //optional parameters
        @InvocableVariable(label='Recipient User Id' 
                            description='The ID of the user receiving the email'
                            required=false)
        public Id recipientUserId;

		@InvocableVariable(label='Recipient User Id (2)' 
                            description='The ID of the user receiving the email'
                            required=false)
        public Id recipientUserId2;

        @InvocableVariable(label='Notify Individual Team Member via Notification Flag' 
        					description='If set to true, flag indicates that any project team member with the Email Notification flag checked will receive this email'
        					required=false)
        public Boolean blnIndividualTeamMemberNotifyFlag;

		@InvocableVariable(label='Notify All Internal Team Members' 
        					description='If set to true, all internal team members will receive this email'
        					required=false)
        public Boolean blnAllInternalTeamMemberNotifyFlag;

        @InvocableVariable(label='Team Roles' 
        					description='A semicolon-separated list of project team member roles to receive this email'
        					required=false)
        public String strTeamRoles;

        @InvocableVariable(label='Additional Email Addresses' 
        					description='A semicolon-separated list of non-Salesforce users email addresses to receive this email'
        					required=false)
        public String strAdditionalAddresses;

        @InvocableVariable(label='Funding Round Team Member Role' 
                            description='A semicolon-separated listing of funding round team member role(s) to receive this email'
                            required=false)
        public String strFundingRoundTeamMemberRole;

		@InvocableVariable(label='Public Group' 
                            description='A semicolon-separated listing of Public Group(s) to receive this email'
                            required=false)
        public String strPublicGroup;
    }

    //helper class holding potential email recipients per project
    public class hlcProjectRecipients {
        public Map<String, Id> mapProjectTeamRoleRecipients;
        public Set<Id> setProjectNotificationRecipients;
        public Map<String, Set<Id>> mapProjectFundingRoundRoleRecipients;
		public Set<Id> setPublicGroupNotificationRecipients;

        //constructor
        public hlcProjectRecipients() {
            mapProjectTeamRoleRecipients = new Map<String, Id>();
            setProjectNotificationRecipients = new Set<Id>();
            mapProjectFundingRoundRoleRecipients = new Map<String, Set<Id>>();
			setPublicGroupNotificationRecipients = new Set<Id>();
        }
    }

    /*--------------------------------------------------------
    -- Method called from Process Builder to calculate daily utilization
    --------------------------------------------------------*/
	@InvocableMethod(label='Send Email'
                     description='Send an email using the given template and other parameters')

    public static void SendEmail(List<EmailRequest> requests) {
		system.debug('requests:' + requests);
		List<Id> lstRecipientIds = new List<Id>();
		Set<String> setRoles = new Set<String>();
		List<String> lstCCAddresses = new List<String>();
		Id recordId;
		Id recipientId;
		Id recipientId2;
		Id projectId;
		Id templateId;
		Boolean blnAllNotifyFlag = false;
        Set<String> setFundingRoundRoles = new Set<String>();
		List<Id> lstGroupMemberIds = new List<Id>();

        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();

        //add temporary contact to send single email message to users (SFDC requirement)
        Contact tempContact = new Contact();
            tempContact.FirstName = 'Temp';
            tempContact.LastName = 'Contact';
            tempContact.Email = 'no-reply@mhfa.com';  
            insert tempContact;

        Map<Id, hlcProjectRecipients> mapProjectRecipients = loadProjectEmailRecipients(requests);
        system.debug('***mapProjectRecipients: ' + mapProjectRecipients);

    	for (EmailRequest oRequest : requests) {
            system.debug('EMAIL CLASS OREQUEST: '+ oRequest);    		
            recordId = oRequest.parentRecordId;
    		recipientId = oRequest.recipientUserId;
			recipientId2 = oRequest.recipientUserId2;
    		projectId = oRequest.projectId;
    		templateId = oRequest.emailTemplateId;
    		if (oRequest.strAdditionalAddresses != null) {
    			lstCCAddresses = oRequest.strAdditionalAddresses.split(';');
    		}
    	
            if (mapProjectRecipients.containsKey(projectId) || (recipientId != null || recipientId2 != null)) {

                lstRecipientIds = retrieveProjectEmailRecipients(oRequest, mapProjectRecipients.get(projectId));

				if(recipientId != null){
					lstRecipientIds.add(recipientId);
				}

				if(recipientId2 != null){
					lstRecipientIds.add(recipientId2);
				}

                system.debug('RECIPIENT IDS: ' + lstRecipientIds);
                if (lstRecipientIds.size() > 0) {
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    message.setWhatId(recordId);
                    message.setTargetObjectId(tempContact.Id);
                    message.setTemplateId(templateId);
                    message.setToAddresses(lstRecipientIds);
                    message.setCCAddresses(lstCCAddresses);
                    message.saveAsActivity = FALSE;
                    message.setTreatTargetObjectAsRecipient(FALSE);
					message.setOrgWideEmailAddressId('0D237000000TNgm');  //'minnesota housing' org wide address 
                    system.debug('MESSAGE: ' + message);
                    emailsToSend.add(message);
                }
            }
        }
		system.debug('LSTRECIPIENTIDS: ' + lstRecipientIds);
        system.debug('NUMBER OF EMAILS TO SEND: ' + emailsToSend.size());
		if(emailsToSend.size() > 0){
            Messaging.SendEmailResult[] results = Messaging.sendEmail(emailsToSend);  //send 5000 emails in a 24 hr period
                   
            if (results[0].success) {
                System.debug('Email(s) sent successfully.');
            } else {
                System.debug('Email(s) failed to send: ' + results[0].errors[0].message);
            }

            delete tempContact;  //delete temporary contact  
        }
    }

    /*********************************************************************
    * build a map of recipient ids per project based on team member roles
    * and funding round roles.  Return the results in a helper class.
    **********************************************************************/
    public static Map<Id, hlcProjectRecipients> loadProjectEmailRecipients(List<EmailRequest> lstRequests) {        
        Set<Id> setProjectIds = new Set<Id>();

        for (EmailRequest oRequest : lstRequests) {
            setProjectIds.add(oRequest.projectId);
        }

		Map<Id, hlcProjectRecipients> mapProjectRecipients = findProjectTeamRecipients(lstRequests);
        Map<Id, Map<String, Set<Id>>> mapProjectFundingRoundRoleRecipients = findProjectFundingRoundRoleRecipients(setProjectIds);
		Map<Id, Set<Id>> mapPublicGroupRecipients = findPublicGroupRecipients(lstRequests);

        for (Id projectId : setProjectIds) {
            hlcProjectRecipients oProjRecipients = new hlcProjectRecipients();
            if (mapProjectRecipients.containsKey(projectId)) {
                oProjRecipients = mapProjectRecipients.get(projectId);
            }
           	if(mapPublicGroupRecipients.containsKey(projectId)){
				oProjRecipients.setPublicGroupNotificationRecipients = mapPublicGroupRecipients.get(projectId);
			}
		    if (mapProjectFundingRoundRoleRecipients.containsKey(projectId)) {
                oProjRecipients.mapProjectFundingRoundRoleRecipients = mapProjectFundingRoundRoleRecipients.get(projectId);
            }
            mapProjectRecipients.put(projectId, oProjRecipients);
        }
        return mapProjectRecipients;
    }
	
    /*********************************************************************
    * Locate the user ids of email recipients using the map and helper
    * class structure loaded earlier
    **********************************************************************/
    public static List<Id> retrieveProjectEmailRecipients(EmailRequest oRequest, hlcProjectRecipients oProjRecipients) {
        Set<Id> setRecipientIds = new Set<Id>();

        //loop through the provided team roles and find the project team members to receive the email
        if (oRequest.strTeamRoles != null) {
            system.debug('*** Processing team roles: '  + oRequest.strTeamRoles);
            for (String strRole : oRequest.strTeamRoles.split(';')) {
				//system.debug('*** Processing role: '  + strRole);
				//system.debug('*** Processing oprojrecipients: '  + oProjRecipients.mapProjectTeamRoleRecipients);
				//system.debug('*** Processing map: '  + oProjRecipients.mapProjectTeamRoleRecipients.containsKey(strRole));
                if (oProjRecipients.mapProjectTeamRoleRecipients.containsKey(strRole)) {
                    setRecipientIds.add(oProjRecipients.mapProjectTeamRoleRecipients.get(strRole));
                }
            }
        }

		//retrieve all project team members who receive notices
        if (oRequest.blnAllInternalTeamMemberNotifyFlag == true 
                && oProjRecipients.setProjectNotificationRecipients.size() > 0) {
            setRecipientIds.addAll(oProjRecipients.setProjectNotificationRecipients);
        }

        //retrieve individual project team members who receive notices
        if (oRequest.blnIndividualTeamMemberNotifyFlag == true 
                && oProjRecipients.setProjectNotificationRecipients.size() > 0) {
            setRecipientIds.addAll(oProjRecipients.setProjectNotificationRecipients);
        }

        //loop through the provided funding round roles and find the funding round members to receive the email
        if (oRequest.strFundingRoundTeamMemberRole != null) {
            //system.debug('*** Processing funding round team roles: '  + oRequest.strFundingRoundTeamMemberRole);
            //system.debug('*** Project funding round roles: ' + oProjRecipients.mapProjectFundingRoundRoleRecipients);
            for (String strFundingRoundRole : oRequest.strFundingRoundTeamMemberRole.split(';')) {
                if (oProjRecipients.mapProjectFundingRoundRoleRecipients.containsKey(strFundingRoundRole)) {
                    setRecipientIds.addAll(oProjRecipients.mapProjectFundingRoundRoleRecipients.get(strFundingRoundRole));
                }
            }
        }

		//retrieve public group members
        if (oRequest.strPublicGroup != null
                && oProjRecipients.setPublicGroupNotificationRecipients.size() > 0) {
            setRecipientIds.addAll(oProjRecipients.setPublicGroupNotificationRecipients);
        }

        //convert the set of recipient ids to a list
        List<Id> lstRecipientIds = new List<Id>();
        lstRecipientIds.addAll(setRecipientIds);
        return lstRecipientIds;
    }

    /*********************************************************************
    * Locate the user ids of opportunity team members either matching the
    * provided roles or have the Project team member Email Notification flag set
    **********************************************************************/
    public static Map<Id, hlcProjectRecipients> findProjectTeamRecipients(List<EmailRequest> lstRequests) {
	//public static Map<Id, hlcProjectRecipients> findProjectTeamRecipients(Set<Id> setProjectIds) {
        Map<Id, hlcProjectRecipients> mapProjectRecipients = new Map<Id, hlcProjectRecipients>();

		for(EmailRequest oEmail : lstRequests){
			for (OpportunityTeamMember otm : [SELECT OpportunityId
												   , UserId
												   , TeamMemberRole
												   , Email_Notification__c
												   , User.UserType
											 FROM OpportunityTeamMember 
											 WHERE OpportunityId = :oEmail.projectId
											 ORDER BY OpportunityId]) {
				hlcProjectRecipients oProjectRecipients = new hlcProjectRecipients();
				if (mapProjectRecipients.containsKey(otm.OpportunityId)) {
					oProjectRecipients = mapProjectRecipients.get(otm.OpportunityId);
				}
				//assumption is only one user can be assigned to a role
				oProjectRecipients.mapProjectTeamRoleRecipients.put(otm.TeamMemberRole, otm.UserId);
				
				//if all team members receive notification email, store the value
				if (oEmail.blnAllInternalTeamMemberNotifyFlag == true &&
				    otm.User.UserType == 'Standard') {
					oProjectRecipients.setProjectNotificationRecipients.add(otm.UserId);
				}
				
				//if individual team member receives notification email, store the value
				if (oEmail.blnIndividualTeamMemberNotifyFlag == true &&
				    otm.Email_Notification__c == true) {
					oProjectRecipients.setProjectNotificationRecipients.add(otm.UserId);
				}

				mapProjectRecipients.put(otm.OpportunityId, oProjectRecipients);
			}
		}
        return mapProjectRecipients;
    }

    /*********************************************************************
    //build a map of funding round team members by role to include for each project
    *********************************************************************/
    public static Map<Id, Map<String, Set<Id>>> findProjectFundingRoundRoleRecipients(Set<Id> setProjectIds) {
        Map<Id, Map<String, Set<Id>>> mapProjectFundingRoundRoleRecipients = new Map<Id, Map<String, Set<Id>>>();

        Map<Id, Id> mapProjectFundingRounds = new Map<Id, Id>();
        for(Opportunity o : [SELECT Id
                                  , Funding_Round__c
                            FROM Opportunity
                            WHERE Id IN :setProjectIds]){
            mapProjectFundingRounds.put(o.Id, o.Funding_Round__c);
        }

        Map<Id, Map<String, Set<Id>>> mapFundingRoundRoleMembers = new Map<Id, Map<String, Set<Id>>>();
        for(Funding_Round_Team_Member__c frtm : [SELECT Funding_Round__c
                                                      , Team_Role__c
                                                      , Team_Member__c
                                                FROM Funding_Round_Team_Member__c
                                                WHERE Funding_Round__c IN :mapProjectFundingRounds.values()
                                                AND Team_Member__c != null
                                                Order By Funding_Round__c, Team_Role__c]){

            Map<String, Set<Id>> mapRoleRecipients = new Map<String, Set<Id>>();
            if (mapFundingRoundRoleMembers.containsKey(frtm.Funding_Round__c)) {
                mapRoleRecipients = mapFundingRoundRoleMembers.get(frtm.Funding_Round__c);
            }
            Set<Id> setRecipients = new Set<Id>();
            if (mapRoleRecipients.containsKey(frtm.Team_Role__c)) {
                setRecipients = mapRoleRecipients.get(frtm.Team_Role__c);
            }
            setRecipients.add(frtm.Team_Member__c);
            mapRoleRecipients.put(frtm.Team_Role__c, setRecipients);
            mapFundingRoundRoleMembers.put(frtm.Funding_Round__c, mapRoleRecipients);
        }

        //loop through all the projects and locate the funding round team members.  Store the values
        //in a map where the key is the project id
        for (Id projectId : mapProjectFundingRounds.keySet()) {
            Id fundingRoundId = mapProjectFundingRounds.get(projectId);
            if (mapFundingRoundRoleMembers.containsKey(fundingRoundId)) {
                mapProjectFundingRoundRoleRecipients.put(projectId, mapFundingRoundRoleMembers.get(fundingRoundId));
            }
        }

        return mapProjectFundingRoundRoleRecipients;
    }

	/*********************************************************************
    //build a map of public group members for each project
    *********************************************************************/
    //public static Map<Id, hlcProjectRecipients> findPublicGroupRecipients(List<EmailRequest> lstRequests) {
	public static Map<Id, Set<Id>> findPublicGroupRecipients(List<EmailRequest> lstRequests) {
		//assumes only users are represented in public group
		Set<String> setGroupNames = new Set<String>();
		Set<Id> setGroupIds = new Set<Id>();
		Map<String, Id> mapGroupNameProjectId = new Map<String, Id>();
		Map<Id, String> mapGroupIdGroupName = new Map<Id, String>();
		Map<Id, Set<Id>> mapGroupRecipients = new Map<Id, Set<Id>>();

		//parse passed public group names
		for(EmailRequest er : lstRequests){
			if(er.strPublicGroup != null){
				String[] splitGroup = er.strPublicGroup.split(';'); 
			
				for(String g : splitGroup){
					setGroupNames.add(g);
					mapGroupNameProjectId.put(g, er.projectId);
				}
			}
		}   
		System.debug('GROUP NAMES: ' + setGroupNames);
		System.debug('MAPGROUPNAMEPROJECTID: ' + mapGroupNameProjectId);
		//fetch public group id and name
		if(setGroupNames.size() > 0){
			for(Group g : [SELECT Id
								, Name
						  FROM Group
						  WHERE Name IN :setGroupNames]){
				setGroupIds.add(g.Id);
				mapGroupIdGroupName.put(g.Id, g.Name);
			}			
		    //System.debug('GROUP IDS: ' + setGroupIds);
		    //System.debug('MAPGROUPIDGROUPNAME: ' + mapGroupIdGroupName);
			//fetch group memebers (users) and populate object to return to address email
			for(GroupMember gm : [SELECT UserorgroupId
								       , GroupId
								  FROM GroupMember
								  WHERE GroupId IN :setGroupIds]){
				Set<Id> setPGRecipientID = new Set<Id>();

				if (mapGroupRecipients.containsKey(mapGroupNameProjectId.get(mapGroupIdGroupName.get(gm.GroupId)))) {
					setPGRecipientID = mapGroupRecipients.get(mapGroupNameProjectId.get(mapGroupIdGroupName.get(gm.GroupId)));
				}
				System.debug('MAPGROUPRECIPIENTS: ' + mapGroupRecipients);
				setPGRecipientID.add(gm.UserOrGroupId);
				mapGroupRecipients.put(mapGroupNameProjectId.get(mapGroupIdGroupName.get(gm.GroupId)), setPGRecipientID);
			}
		}
		return mapGroupRecipients;
	}	
}