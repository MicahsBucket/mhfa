public without sharing class FundingRoundCLI_Methods {

	/////////////////////////////////////////////////////////////////////////
	// If a Funding Round Checklist Item is being inserted or the associated
	// checklist item changed, set the internal and external help text
	// to the values stored on the checklist item.
	// 20171220 EBG/DCS - Added filter tag pre-population
	/////////////////////////////////////////////////////////////////////////
	public static void processFRCLIs(List<Funding_Round_Checklist_Item__c> lstFRCLIs
									, Map<Id, Funding_Round_Checklist_Item__c> mapOldFRCLIs)	{

		Set<Id> setCliIds = new Set<Id>();
		for (Funding_Round_Checklist_Item__c oFRCLI : lstFRCLIs) {
			if (oFRCLI.Checklist_Item__c != null) {
				setCliIds.add(oFRCLI.Checklist_Item__c);
			}
		}

		Map<Id, Checklist_Item__c> mapCLIs = new Map<Id, Checklist_Item__c>([Select Id
																					, Name
																					, Internal_Help_Text_RT__c
																					, External_Help_Text_RT__c
																					, Filter_Tags__c
																			From Checklist_Item__c
																			Where Id in :setCliIds]);

		for (Funding_Round_Checklist_Item__c oFRCLI : lstFRCLIs) {
			if (oFRCLI.Checklist_Item__c != null
					&& (mapOldFRCLIs == null 
							|| oFRCLI.Checklist_Item__c != mapOldFRCLIs.get(oFRCLI.Id).Checklist_Item__c)) {
				oFRCLI.Internal_Help_Text_RT__c = mapCLIs.get(oFRCLI.Checklist_Item__c).Internal_Help_Text_RT__c;
				oFRCLI.External_Help_Text_RT__c = mapCLIs.get(oFRCLI.Checklist_Item__c).External_Help_Text_RT__c;
				//update filter tags when checklist item changed or on an insert and the filter tags are not populated
				if ((oFRCLI.Filter_Tags__c == null && mapOldFRCLIs == null) || 
						(mapOldFRCLIs != null && mapOldFRCLIs.containsKey(oFRCLI.Id))) {
					oFRCLI.Filter_Tags__c = mapCLIs.get(oFRCLI.Checklist_Item__c).Filter_Tags__c;
				}
			}
		}
	}

}