/*
*   {purpose}   Performs a variety of functions involving dates           
*
*   CHANGE HISTORY
*   =============================================================================
*   Date        Name                    Description
*   04/29/19	Eric Gronholz DCS		Created
*   =============================================================================
*/
public without sharing class DateUtility  {

	public static Date findBusinessDate(Date baseDate, String offsetType, Integer offsetValue) {
		if (baseDate == null) {
			return null;
		}
		//Ensure we are working with a pure date and not a date/time field
		Date holdDate = Date.newInstance(baseDate.year(), baseDate.month(), baseDate.day());
		Date retVal = holdDate;
		if (offsetValue == null) {
			offsetValue = 0;
		}
		switch on offsetType {
			when 'Prior to Due Date' {
				retVal = DateUtility.subtractBusinessDaysFromDate(holdDate, offsetValue);
			}
			when 'Equal Due Date' {
				retVal = holdDate;
			}
			when 'After Due Date' {
				retVal = DateUtility.addBusinessDaysToDate(holdDate, offsetValue);
			}
		}

		return retVal;
	}

    ///////////////////////////////////////////
    // Determines if the provided date is a business day
    ///////////////////////////////////////////
    public static boolean isBusinessDay(Date dte) {
        boolean retVal = true;
        
        Integer iDOW = DateUtility.getDayOfWeek(dte);
		System.debug('Inside isBusinessDay: ' + dte + '  iDOW: ' + iDOW);
        if (iDOW == 0 || iDOW == 6) {
			//date falls on a weekend
            retVal = false;
        } else {
			Map<Date, Holiday> mapHoliday = DateUtility.getHolidaysForDates(dte, dte);
			if (mapHoliday.containsKey(dte)) {
				//date falls on a holiday
				retVal = false;
			}
		}
        
        return retVal;      
    }
    
    ///////////////////////////////////////////
    // Returns the date of the next business day in relation to the provided date
    ///////////////////////////////////////////
    public static Date getNextBusinessDateFromDate(Date dte
                                                    , set<Date> setHolidays) {
		if (dte == null) {
			return null;
		}
		//Ensure we are working with a pure date and not a date/time field
		Date holdDate = Date.newInstance(dte.year(), dte.month(), dte.day());
        Date retVal = holdDate.addDays(1);
        Integer iDOW = DateUtility.getDayOfWeek(retVal);

        //make sure the next day isn't Sunday (0) or Saturday (6)
        while (iDOW == 0 || iDOW == 6) {
            retVal = retVal.addDays(1);
            iDOW = getDayOfWeek(retVal);
        }
        
        //now determine if the day returned is a holiday and move past it if is
        boolean bIsHoliday = setHolidays.contains(retVal);
        
        while (bIsHoliday) {
			//System.debug('Holiday: ' + retVal);
            retVal = retVal.addDays(1);
			//ensure the day after the holiday isn't on a weekend
	        iDOW = DateUtility.getDayOfWeek(retVal);
			while (iDOW == 0 || iDOW == 6) {
				retVal = retVal.addDays(1);
				iDOW = getDayOfWeek(retVal);
			}
		    bIsHoliday = setHolidays.contains(retVal);
        }
        
        return retVal;
    }

    ///////////////////////////////////////////
    // Returns the date of the previous business day in relation to the provided date
    ///////////////////////////////////////////
    public static Date getPreviousBusinessDateFromDate(Date dte
                                                    , set<Date> setHolidays) {
		if (dte == null) {
			return null;
		}
		//Ensure we are working with a pure date and not a date/time field
		Date holdDate = Date.newInstance(dte.year(), dte.month(), dte.day());
        Date retVal = holdDate.addDays(-1);
        Integer iDOW = DateUtility.getDayOfWeek(retVal);

        //make sure the next day isn't Sunday (0) or Saturday (6)
        while (iDOW == 0 || iDOW == 6) {
            retVal = retVal.addDays(-1);
            iDOW = getDayOfWeek(retVal);
        }
        
        //now determine if the day returned is a holiday and move past it if is
        boolean bIsHoliday = setHolidays.contains(retVal);
        
        while (bIsHoliday) {
			//System.debug('Holiday: ' + retVal);
            retVal = retVal.addDays(-1);
			//ensure the day before the holiday isn't on a weekend
			iDOW = DateUtility.getDayOfWeek(retVal);
			while (iDOW == 0 || iDOW == 6) {
				retVal = retVal.addDays(-1);
				iDOW = getDayOfWeek(retVal);
			}
            bIsHoliday = setHolidays.contains(retVal);
        }
        
        return retVal;
    }
    
    ///////////////////////////////////////////
    // Returns the date for the number of business 
    //  days after from the provided date
    ///////////////////////////////////////////
    public static Date addBusinessDaysToDate(Date dte
                                            , Integer intDays) {
		if (dte == null) {
			return null;
		}
		//Ensure we are working with a pure date and not a date/time field
		Date holdDate = Date.newInstance(dte.year(), dte.month(), dte.day());

		//find all holidays starting with the given date through one year after the number of days provided.  This
		// ensures we retrieve enough holidays so they can be included when calculating business days. 
        Set<Date> setHolidays = new Set<Date>();
		Map<Date, Holiday> mapHolidays = DateUtility.getHolidaysForDates(holdDate, holdDate.addDays(intDays).addYears(1));
		if (mapHolidays.size() > 0) {
			setHolidays = mapHolidays.keySet();
		}
		//System.debug('Holidays to exclude for dates: ' + holdDate + ' through ' + holdDate.addDays(intDays).addYears(1) + '\n' + setHolidays);

        Date retVal = holdDate;
        for (Integer iCounter = 0; iCounter < intDays; iCounter++) {            
            retVal = getNextBusinessDateFromDate(retVal, setHolidays);
            //system.debug('\n\n*** iCounter: ' + iCounter + '\nNext Business Day: ' + retVal);
        } 
            
        //system.debug('\n\n*** Return date: ' + retVal);
        return retVal;
    }

    ///////////////////////////////////////////
    // Returns the date for the number of business 
    //  days prior to the provided date
    ///////////////////////////////////////////
    public static Date subtractBusinessDaysFromDate(Date dte
													, Integer intDays) {
		if (dte == null) {
			return null;
		}
		//Ensure we are working with a pure date and not a date/time field
		Date holdDate = Date.newInstance(dte.year(), dte.month(), dte.day());

		if (intDays > 0) {
			intDays = -1 * intDays;
		}
		//find all holidays one year prior to the given date minus the number of days provided.  This
		// ensures we retrieve enough holidays so they can be included when calculating business days. 
        Set<Date> setHolidays = new Set<Date>();
		Map<Date, Holiday> mapHolidays = DateUtility.getHolidaysForDates(holdDate.addDays(intDays).addYears(-1), holdDate);
		if (mapHolidays.size() > 0) {
			setHolidays = mapHolidays.keySet();
		}
		System.debug('Holidays to exclude for dates: ' + holdDate.addDays(intDays).addYears(-1) + ' through ' + holdDate + '\n' + setHolidays);

        Date retVal = holdDate;
        for (Integer iCounter = 0; iCounter > intDays; iCounter--) {            
            retVal = getPreviousBusinessDateFromDate(retVal, setHolidays);
            system.debug('\n\n*** iCounter: ' + iCounter + '\nNext Business Day: ' + retVal);
        } 
            
        system.debug('\n\n*** Return date: ' + retVal);
        return retVal;
    }
    
    ///////////////////////////////////////////
    // This method returns an Integer value of 0-6 for 
    //   the corresponding day of the week (0-based)
    ///////////////////////////////////////////
    public static Integer getDayOfWeek(Date dteTargetDate) {
        Integer intDayOfWeek;
        DateTime dtmTargetDate = dteTargetDate;
        
        if(dtmTargetDate.formatGmt('EEEE') == 'Sunday') {
            intDayOfWeek = 0;
        }
        if(dtmTargetDate.formatGmt('EEEE') == 'Monday') {
            intDayOfWeek = 1;
        }
        if(dtmTargetDate.formatGmt('EEEE') == 'Tuesday') {
            intDayOfWeek = 2;
        }
        if(dtmTargetDate.formatGmt('EEEE') == 'Wednesday') {
            intDayOfWeek = 3;
        }
        if(dtmTargetDate.formatGmt('EEEE') == 'Thursday') {
            intDayOfWeek = 4;
        }
        if(dtmTargetDate.formatGmt('EEEE') == 'Friday') {
            intDayOfWeek = 5;
        }
        if(dtmTargetDate.formatGmt('EEEE') == 'Saturday') {
            intDayOfWeek = 6;
        }
        
        //System.Debug('TESTING - Target Date = ' + dtmTargetDate + ' Day of Week = ' + intDayOfWeek);
        
        return intDayOfWeek;         
    }

	////////////////////////////////////////////////////////////////////////////////////////////////
	/* The following code handles all types of holidays, including recurring holidays
		Copied from https://salesforce.stackexchange.com/questions/158547/recurring-holidays-exclude-holidays-between-two-dates
	*/
	////////////////////////////////////////////////////////////////////////////////////////////////
    public static List<Integer> seperateBitMask(Integer bitMask){
        List<Integer> toReturn = new List<Integer>();
        Map<Integer, Integer> maskedDayMap = getBitMaskMap();
        LIst<Integer> bits = new List<Integer>{64, 32, 16, 8, 4, 2, 1};

        for (Integer bit : bits){
            if (bitMask >= bit){
                toReturn.add(bit);
                bitMask = bitMask - bit;
            }
        }
        return toReturn;
    }

    public static Map<Integer, Integer> getBitMaskMap(){
        return new Map<Integer, Integer>{1 => 1, 2 => 2, 4 => 3, 8 => 4, 16 => 5, 32 => 6, 64 => 7};//1 =  1 - Sunday, 2 = 2 Monday, 4 = 3 Tuesday, /8 = 4 Wednesday, 16 = 5 Thursday, 32 = 6 Friday, 64 = 7 Saturday
    }

    public static Map<String, Integer> getMonthMap(){
        return new Map<String, Integer>{'January' => 1, 'February' => 2, 'March' => 3, 'April' => 4, 'May' => 5, 'June' => 6, 'July' => 7, 'August' => 8, 'September' => 9,
        'October' => 10, 'November' => 11, 'December' => 12};
    }

    public static Map<String, Integer> getWeekMap(){return new Map<String, Integer>{'First' => 1, 'Second' => 2, 'Third' => 3, 'Fourth' => 4, 'Fifth' => 5};}

    public static Map<Date, Holiday> getHolidaysForDates(Date startDate, date endDate){
        Map<Date, Holiday> toReturn = new Map<Date, holiday>();
        List<Date> dates = new List<Date>();
        List<Holiday> holidays = [select id, name, ActivityDate, Description, EndTimeInMinutes, IsAllDay, IsRecurrence, RecurrenceDayOfMonth, RecurrenceDayOfWeekMask, RecurrenceEndDateOnly, 
                                    RecurrenceInstance, RecurrenceInterval, RecurrenceMonthOfYear, RecurrenceStartDate, RecurrenceType, StartTimeInMinutes FROM Holiday
                                    WHERE IsRecurrence = true OR (ActivityDate >= :startDate AND ActivityDate <= :endDate)];

        for (Holiday h : holidays){
            system.debug('Holiday: ' + h);
            if (h.RecurrenceType == 'RecursYearlyNth')
                dates = getYearlyNthHoliday(h, startDate, endDate);
            else if (h.RecurrenceType == 'RecursYearly')
                dates = getYearlyHoliday(h, startDate, endDate);
            else if (h.RecurrenceType == 'RecursMonthlyNth')
                dates = getMonthlyNthHoliday(h, startDate, endDate);
            else if (h.RecurrenceType == 'RecursMonthly')
                dates = getMonthlyHoliday(h, startDate, endDate);
            else if (h.RecurrenceType == 'RecursWeekly')
                dates = getWeeklyHoliday(h, startDate, endDate);
            else if (h.RecurrenceType == 'RecursDaily')
                dates = getDailyHoliday(h, startDate, endDate);
            for (Date d : dates)
                toReturn.put(d, h);
        }
        return toReturn;
    }

    public static List<Date> getYearlyHoliday(Holiday h, date startDate, date endDate){//recurs on set days in a year, like Christmas, New Years 
        List<Date> toReturn = new List<Date>();
        Map<String, Integer> monthMap = getMonthMap();
        for (Integer year : getYears(startDate, EndDate)){
            Integer month = monthMap.get(h.RecurrenceMonthOfYear);
            Date d = date.newInstance(year, month, h.RecurrenceDayOfMonth);
            if (d > startDate && d < endDate)
                toReturn.add(d);
        }
        return toReturn;
    }

    public static List<Date> getYearlyNthHoliday(Holiday h, date startDate, date endDate){//recurs on something like 2nd tuesday of march - Thanksgiving, MLK day, Columbus Day

        List<Date> toReturn = new List<Date>();

        Map<Integer, Integer> maskedDayMap = getBitMaskMap();
        Map<String, Integer> monthMap = getMonthMap();
        Map<String, Integer> weekMap = getWeekMap();

        Integer day = maskedDayMap.get(h.RecurrenceDayOfWeekMask);
        Integer month = monthMap.get(h.RecurrenceMonthOfYear);
		//System.debug('Inside getYearlyNthHoliday:\n' + h);

        for (Integer year : getYears(startDate, EndDate)){
            Date d;
			if (h.RecurrenceInstance == 'Last') {
                d = getLastXOfMonth(day, month, year);
            } else {
                Integer week = weekMap.get(h.RecurrenceInstance);
				//System.debug('Week: ' + week + ' Day: ' + day + ' Month: ' + month);
                d = getXDayOfMonth(week, day, date.newInstance(year, month, 2));
            }
            if (d > startDate && d < endDate) {
				//System.debug('Calculated Date: ' + d);
				//toReturn.add(d);
                toReturn.add(Date.newInstance(d.year(), d.month(), d.day()));
			}
        }
        return toReturn;
    }

    public static List<Date> getMonthlyHoliday(Holiday h, date startDate, date endDate){//recurs on set day of every month - something like 5th of every month
        List<Date> toReturn = new List<Date>();
        for (Date d : getMonths(startDate, endDate)){
            Date da = date.newInstance(d.year(), d.month(), h.RecurrenceDayOfMonth);
            if (da > startDate && da < endDate)
                //toReturn.add(da);
                toReturn.add(Date.newInstance(da.year(), da.month(), da.day()));
        }
        return toReturn;
    }

    public static List<Date> getMonthlyNthHoliday(Holiday h, date startDate, date endDate){//recurs monthly - something like every second sunday
        List<Date> toReturn = new List<Date>();

        List<Date> months = getMonths(startDate, endDate);
        Integer day = getBitMaskMap().get(h.RecurrenceDayOfWeekMask);

        Integer week = getWeekMap().get(h.RecurrenceInstance);
        for (date month : months){
            Date d;
             if (h.RecurrenceInstance == 'Last')
                    d = getLastXOfMonth(day, month.month(), month.year());
            else
                d = getXDayOfMonth(week, day, date.newInstance(month.year(), month.month(), 1));
            if (d > startDate && d < endDate)
                //toReturn.add(d);
                toReturn.add(Date.newInstance(d.year(), d.month(), d.day()));
        }
       return toReturn;
    }

    public static List<Date> getWeeklyHoliday(Holiday h, date startDate, date endDate){//something like every sat, sun or tues
        List<Date> toReturn = new List<Date>();
        Integer day = getBitMaskMap().get(h.RecurrenceDayOfWeekMask);
        Map<String, Integer> wMap = createDayOfWeekMap();
        DateTime d = datetime.newInstance(startDate.year(), startdate.month(), startDate.day(), 0, 0, 0);//valueOf(startDate);
        while (wMap.get(d.format('EEEE')) != day)
            d = d.addDays(1);
        while (d < endDate){
            if (d > startDate && d < endDate)
                toReturn.add(date.valueOf(d));
            d = d.addDays(7);
        }
        return toReturn;
    }

    public static List<Date> getDailyHoliday(Holiday h, date startDate, date endDate){//something like every 15 days from start
        List<Date> toReturn = new List<Date>();
        Integer remainder;
        Date dailyDate = startDate.addDays(-1);

        while (remainder != 0){
            dailyDate = dailyDate.addDays(1);
            remainder = math.mod(h.RecurrenceStartDate.daysBetween(dailyDate), h.RecurrenceInterval);
        }
        toReturn.add(dailyDate);
        while (dailyDate < endDate){
            dailyDate = dailyDate.addDays(h.RecurrenceInterval);
            if (dailyDate > startDate && dailyDate < endDate)
                toReturn.add(dailyDate);
        }
        return toReturn;
    }


    public static Date getLastXOfMonth(Integer dayOfWeek, Integer month, Integer year){
        Map<String, Integer> DayOfWeekMap = createDayofWeekMap();
        DateTime lastMonthDate = DateTime.newInstance(year, month + 1, 0, 0, 0, 0);
        Integer day = DayOfWeekMap.get(lastMonthDate.format('EEEE'));

        while(day != dayOfWeek){
            lastMonthDate = lastMonthDate.addDays(-1);
            day = DayOfWeekMap.get(lastMonthDate.format('EEEE'));
        }
        return Date.valueOf(lastMonthDate);
    }

    public static Date getXDayOfMonth(Integer week, Integer day, date monthIn){
        Map<String, Integer> DayOfWeekMap = createDayofWeekMap();
        DateTime firstMonthDate = DateTime.newInstance(monthIn.year(), monthIn.month(), 1);
        Integer dayName = DayOfWeekMap.get(firstMonthDate.format('EEEE'));
		//System.debug('Inside getXDayOfMonth. firstMonthDate: ' + firstMonthDate + ' - ' + firstMonthDate.format('EEEE') + ' dayName: ' + dayName + ' day: ' + day);  
        while(dayName != day){
			firstMonthDate = Datetime.newInstance(firstMonthDate.year(), firstMonthDate.month(), firstMonthDate.day() + 1);
            //firstMonthDate = firstMonthDate.addDays(1);
            dayName = DayOfWeekMap.get(firstMonthDate.format('EEEE'));
			//System.debug('firstMonthDate: ' + firstMonthDate + ' - ' + firstMonthDate.format('EEEE') + ' dayName: ' + dayName + ' day: ' + day);  
        }
        if (week == 1) {
            return date.valueOf(firstMonthDate);
        } else {
            return date.valueOf(firstMonthDate.addDays(7 * (week - 1)));
		}
    }

	public static Map<String, Integer> createDayOfWeekMap(){
		return new Map<String, Integer>{'Sunday' => 1, 'Monday' => 2, 'Tuesday' => 3, 'Wednesday' => 4, 'Thursday' => 5, 'Friday' => 6, 'Saturday' => 7};
	}

	public static List<Date> getMonths(Date startDate, Date endDate){
        List<Date> months = new List<Date>();
        Date m = date.newInstance(startDate.year(), startDate.month(), 1);
        months.add(m);
        if (startDate.month() != endDate.month() || startDate.year() != endDate.year()){
            while (m.monthsBetween(endDate) != 0){
                m = m.addMonths(1);
                months.add(m);
            }
        }
        return months;
    }

	public static List<Integer> getYears(date startDate, date endDate){
        List<Integer> years = new List<Integer>();
        Integer y = startDate.year();
        years.add(y);
        if (startDate.year() != endDate.year()){
            while (y != endDate.year()){
                y = y + 1;
                years.add(y);
            }
        }
        return years;
    }

}