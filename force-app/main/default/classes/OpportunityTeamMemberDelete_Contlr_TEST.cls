@isTest
public class OpportunityTeamMemberDelete_Contlr_TEST {
	@isTest static void testDeleteTeamMember_AllowDelete(){
        //stage data
        //create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));   

        //create project
        Opportunity p = TestDataUtility.createProject(fr.Id);


        OpportunityTeamMember otm = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                                'Closer', 
                                                                                UserInfo.getUserId());

		PageReference pageRef = Page.OpportunityTeamMemberDelete;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('Id', String.valueOf(otm.Id));
		ApexPages.StandardController sc = new ApexPages.StandardController(otm);
		OpportunityTeamMemberDelete_Controller controller = new OpportunityTeamMemberDelete_Controller(sc);

		controller.deleteTeamMember();
		controller.cancelDelete();
	}

	@isTest static void testDeleteTeamMember_PreventDelete(){
        //stage data
        //create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create project
        Opportunity p = TestDataUtility.createProject(fr.Id);


        OpportunityTeamMember otm = TestDataUtility.createOpportunityTeamMember(p.Id, 
                                                                                'Bad Value', 
                                                                                UserInfo.getUserId());

		PageReference pageRef = Page.OpportunityTeamMemberDelete;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('Id', String.valueOf(otm.Id));
		ApexPages.StandardController sc = new ApexPages.StandardController(otm);
		OpportunityTeamMemberDelete_Controller controller = new OpportunityTeamMemberDelete_Controller(sc);

		controller.deleteTeamMember();
		controller.cancelDelete();
	}
}