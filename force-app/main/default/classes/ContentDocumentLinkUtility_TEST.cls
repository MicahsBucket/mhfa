@isTest
private class ContentDocumentLinkUtility_TEST { 
    @isTest static void testContentDocumentLinkPCLI() {        
        //STAGE METADATA TYPES==================================================================================================
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ContentDocumentLink'];
		
		Approved_Attachment_Extension__mdt[] ApprovedAttachment = [SELECT MasterLabel
                                                                   FROM Approved_Attachment_Extension__mdt];

        List<File_Sharing_Settings_Objects__mdt> mdtSharingRules = [SELECT MasterLabel
                                                                         , Object_Prefix__c
                                                                         , Fields_To_Query__c
                                                                         , Prohibit_File_Upload__c
																	     , Update_Parent_Upload_Status__c
                                                                         , Update_Parent_File_Count__c 
                                                                    FROM File_Sharing_Settings_Objects__mdt];

        File_Sharing_Settings_Users__mdt[] fileSharingSetting = [SELECT MasterLabel
                                                                      , LinkedEntityId__c
                                                                      , ShareType__c
                                                                      , Visibility__c
                                                                FROM File_Sharing_Settings_Users__mdt];
        
		File_Sharing_Settings_Users__mdt [] FundingAppCommunityID = [SELECT MasterLabel
                                                                         , LinkedEntityId__c
                                                                      FROM File_Sharing_Settings_Users__mdt
                                                                      WHERE MasterLabel = 'Multifamily Customer Portal'];
		                                                            
        String idFundingAppCommunityID = FundingAppCommunityID[0].LinkedEntityId__c;
		
		Org_URLs__c csORGurls = new Org_URLs__c();         
        csORGurls.External_URL__c = 'https://mnhousing.force.com/MultifamilyPortal/';        
        csORGurls.Internal_URL__c = 'https://mnhousing.my.salesforce.com/';           
        insert csORGurls;

		Org_Environment__c csORGenvironment = new Org_Environment__c();
		csORGenvironment.Environment__c = 'ORG';
		insert csORGenvironment;

		//SUPPORTING DATA=======================================================================================================
		//create test users
		User oROUser = TestDataUtility.createUser('Multifamily Standard User',
													 'testRO@mhfatest.com');
		
		User oTestUser1 = TestDataUtility.createUser('Multifamily Standard User',
	                                                 'testOTM1@mhfatest.com');

		User oTestUser2 = TestDataUtility.createUser('Multifamily Standard User',
													'testOTM2@mhfatest.com');

		User oTestUser3 = TestDataUtility.createUser('Multifamily Standard User',
													'testOTM3@mhfatest.com');
		
		//FUNIDNG ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		oFundingRound.Roles_To_Notify__c = 'LoanProcessor';
        update oFundingRound;

		//PROJECT FRAMEWORK=====================================================================================================
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		                                                                   
        //fetch required data		
		Phase_Checklist__c oFundingRoundChecklistPreSelection = [SELECT Id
		                                                              , Send_Upload_Notification__c
																 FROM Phase_Checklist__c
																 WHERE Funding_Round__c = :oFundingRound.Id
																 AND Name = 'Application'];		
			oFundingRoundChecklistPreSelection.Send_Upload_Notification__c = true;
			update oFundingRoundChecklistPreSelection;

		Funding_Round_Checklist_Item__c oFundingRoundChecklistItemPreSelection = [SELECT Id
																				 FROM Funding_Round_Checklist_Item__c
																				 WHERE Funding_Round__c = :oFundingRound.Id
																				 AND Name = 'Pre Selection Checklist Item'];
		
		Project_Checklist__c oProjectChecklistPreSelection = [SELECT Id
															  FROM Project_Checklist__c
															  WHERE Name = 'Application'
															  AND Project__c = :oProject.Id];
		
		Project_Sub_Phase__c oProjectStagePreSelection = [SELECT Id
															   , Name
														  FROM Project_Sub_Phase__c
														  WHERE Name = 'Application Submittals'
														  AND Project_Phase__r.Name = 'Application'
														  AND Project__c = :oProject.Id];


		//project team member
		OpportunityTeamMember oProjectTeamMember1 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
																								'Loan Processor', 
																								oTestUser1.Id);

		//create pcli - pre selection approved & inactive
		Project_Checklist_Item__c oProjectChecklistItemPreSelection = TestDataUtility.createProjectChecklistItem(oProject.Id, 
																												 oFundingRoundChecklistItemPreSelection.Id,
																												 false,
																												 '1',
																												 'jpg',
																												 oProjectStagePreSelection.Id,
																												 oProjectChecklistPreSelection.Id);
	        oProjectChecklistItemPreSelection.Approval_First__c = oTestUser1.Id;
			oProjectChecklistItemPreSelection.Approval_Second__c = oTestUser2.Id;
			oProjectChecklistItemPreSelection.Approval_Third__c = oTestUser3.Id;

			update oProjectChecklistItemPreSelection;

        //run tests
        Test.startTest();
            //STAGE FILE CONTENT VERSIONS
            Set<ID> setCVId = new Set<ID>();       
            ContentVersion cvJPG = new ContentVersion(Title = 'cvJPG',
                                                        PathOnClient= '/Test.jpg',
                                                        VersionData = Blob.valueOf('Test Body'),
                                                        Origin = 'H');
            insert cvJPG;
            setCVId.add(cvJPG.Id);

            ContentVersion cvJPG2 =new ContentVersion(Title = 'cvJPG2',
                                                        PathOnClient= '/Test.jpg',
                                                        VersionData = Blob.valueOf('Test Body'),
                                                        Origin = 'H');
            insert cvJPG2;
            setCVId.add(cvJPG2.Id);

            ContentVersion cvXLSM =new ContentVersion(Title = 'cvXLSM',
                                                        PathOnClient= '/Test.xlsm',
                                                        VersionData = Blob.valueOf('Test Body'),
                                                        Origin = 'H');
            insert cvXLSM;
            setCVId.add(cvXLSM.Id);

            ContentVersion cvABC =new ContentVersion(Title = 'cvABC',
                                                        PathOnClient= '/Test.abc',
                                                        VersionData = Blob.valueOf('Test Body'),
                                                        Origin = 'H');
            insert cvABC;
            setCVId.add(cvABC.Id);

            //map of content version id's        
            Map<String, ID> mapCDTypeCDId = new Map<String, ID>();
            for(ContentVersion cv : [SELECT Id
                                            , Title
                                            , ContentDocumentId 
                                        FROM ContentVersion 
                                        WHERE Id IN :setCVId]){
                mapCDTypeCDId.put(cv.Title, cv.ContentDocumentId);
            }    
        
            //TEST: invalid pcli file type
            try{
                ContentDocumentLink cdlPCLIInvalidFile = new ContentDocumentLink(LinkedEntityId = oProjectChecklistItemPreSelection.Id,
                                                                                 ContentDocumentId = mapCDTypeCDId.get('cvXLSM'),
                                                                                 ShareType = 'V');
                insert cdlPCLIInvalidFile;
			} catch (Exception e) {
                    //assert
                    Boolean errorInvalidPCLIFileType = TRUE;
                    System.assertEquals(errorInvalidPCLIFileType, true, e.getMessage());
			}
        
            //TEST: invalid number of attachments
            ContentDocumentLink cdlPCLIMaxAttachment1 = new ContentDocumentLink(LinkedEntityId = oProjectChecklistItemPreSelection.Id,
                                                                                ContentDocumentId = mapCDTypeCDId.get('cvJPG'),
                                                                                ShareType = 'V');
            try {
				insert cdlPCLIMaxAttachment1;
            } catch (Exception ex) {
				if (ex.getMessage().contains('You do not have access to upload Files to this Project')) {
					//add user to the project team
					TestDataUtility.createOpportunityTeamMember(oProject.Id, 'Loan Processor', UserInfo.getUserId());
					insert cdlPCLIMaxAttachment1;
				}
            }

            try{
                ContentDocumentLink cdlPCLIMaxAttachment2 = new ContentDocumentLink(LinkedEntityId = oProjectChecklistItemPreSelection.Id,
                                                                                    ContentDocumentId = mapCDTypeCDId.get('cvJPG2'),
                                                                                    ShareType = 'V');
                insert cdlPCLIMaxAttachment2;
            } catch (Exception e){
                e.getMessage();
                //assert
                Boolean errorInvalidNumberOfAttachments = TRUE; 
                System.assertEquals(errorInvalidNumberOfAttachments, true, e.getMessage());
            }
        
            //TEST: invalid global file type
            ContentDocumentLink cdlInvalidGlobalFilePCLI = new ContentDocumentLink(LinkedEntityId = oProjectChecklistItemPreSelection.Id,
                                                                                    ContentDocumentId = mapCDTypeCDId.get('cvABC'),
                                                                                    ShareType = 'V');

            try{                
                insert cdlInvalidGlobalFilePCLI;   
            } catch (Exception e){
                //assert
                Boolean errorInvalidGlobalFileType = TRUE;
                System.assertEquals(errorInvalidGlobalFileType, true, e.getMessage());
            }
        
            //share with community
            ContentDocumentLink cdlCommunityShare = new ContentDocumentLink(LinkedEntityId = idFundingAppCommunityID,
                                                                            ContentDocumentId = mapCDTypeCDId.get('cvJPG'),
                                                                            ShareType = 'V');                  

            try{
                insert cdlCommunityShare;
            } catch (Exception e){
                e.getMessage();
                //assert
                Boolean errorCannotShareWithCommunity = TRUE;
                System.assertEquals(errorCannotShareWithCommunity, true, e.getMessage());
            }

            //delete contentdocumentlink
            //negative test case:  INSUFFICIENT_ACCESS_OR_READONLY
            System.runAs(oROUser){                
                try{
                    delete cdlPCLIMaxAttachment1;                  
                } catch(DMLException e){
                    system.assert(e.getMessage().contains('Delete failed.'),e.getMessage());
                }
            }

            //positive test case
            delete cdlPCLIMaxAttachment1; 
        Test.stopTest();        
    }

	@isTest static void testContentDocumentLinkPWBV() {
        //STAGE METADATA TYPES==================================================================================================
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ContentDocumentLink'];
		
		Approved_Attachment_Extension__mdt[] ApprovedAttachment = [SELECT MasterLabel
                                                                   FROM Approved_Attachment_Extension__mdt];

        List<File_Sharing_Settings_Objects__mdt> mdtSharingRules = [SELECT MasterLabel
                                                                         , Object_Prefix__c
                                                                         , Fields_To_Query__c
                                                                         , Prohibit_File_Upload__c
																	     , Update_Parent_Upload_Status__c
                                                                         , Update_Parent_File_Count__c 
                                                                    FROM File_Sharing_Settings_Objects__mdt];

        File_Sharing_Settings_Users__mdt[] fileSharingSetting = [SELECT MasterLabel
                                                                      , LinkedEntityId__c
                                                                      , ShareType__c
                                                                      , Visibility__c
                                                                FROM File_Sharing_Settings_Users__mdt];
        
		File_Sharing_Settings_Users__mdt [] FundingAppCommunityID = [SELECT MasterLabel
                                                                         , LinkedEntityId__c
                                                                      FROM File_Sharing_Settings_Users__mdt
                                                                      WHERE MasterLabel = 'Multifamily Customer Portal'];
		                                                            
        String idFundingAppCommunityID = FundingAppCommunityID[0].LinkedEntityId__c;
		
		Org_URLs__c csORGurls = new Org_URLs__c();         
        csORGurls.External_URL__c = 'https://mnhousing.force.com/MultifamilyPortal/';        
        csORGurls.Internal_URL__c = 'https://mnhousing.my.salesforce.com/';           
        insert csORGurls;

		Org_Environment__c csORGenvironment = new Org_Environment__c();
		csORGenvironment.Environment__c = 'ORG';
		insert csORGenvironment;
		
        //SUPPORTING DATA=======================================================================================================
		//create test users
		User oROUser = TestDataUtility.createUser('Multifamily Standard User',
													 'testRO@mhfatest.com');
		
		User oTestUser1 = TestDataUtility.createUser('Multifamily Standard User',
	                                                 'testOTM1@mhfatest.com');
	        
        User oTestUser2 = TestDataUtility.createUser('Multifamily Standard User',
	                                                 'testOTM2@mhfatest.com');
		
		//FUNIDNG ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		oFundingRound.Roles_To_Notify__c = 'LoanProcessor';
        update oFundingRound;

		//PROJECT FRAMEWORK=====================================================================================================
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);                       
        
		//fetch required data
		OpportunityTeamMember oProjectTeamMember1 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
	                                                                                           'Loan Processor', 
	                                                                                            oTestUser1.Id);
        //fetch project workbook (project workbook already created upon project insert)
		Project_Workbook__c oProjectWorkbook = [SELECT Id
											   FROM Project_Workbook__c
											   WHERE Project__c = :oProject.Id
											   AND Workbook_Level__c = 'Primary'];

        Project_Workbook_Version__c oProjectWorkbookVersion = TestDataUtility.createProjectWorkbookVersion(oProjectWorkbook.Id,
                                                                                                           'Selection Meeting');
        
        //run tests
        Test.startTest();  
            //STAGE FILE CONTENT VERSIONS
            Set<ID> setCVId = new Set<ID>();       
            ContentVersion cvJPG = new ContentVersion(Title = 'cvJPG',
                                                      PathOnClient= '/Test.jpg',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvJPG;
            setCVId.add(cvJPG.Id);

            ContentVersion cvXLSM =new ContentVersion(Title = 'cvXLSM',
                                                      PathOnClient= '/Test.xlsm',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvXLSM;
            setCVId.add(cvXLSM.Id);

            ContentVersion cvXLSM2 =new ContentVersion(Title = 'cvXLSM2',
                                                       PathOnClient= '/Test.xlsm',
                                                       VersionData = Blob.valueOf('Test Body'),
                                                       Origin = 'H');
            insert cvXLSM2;
            setCVId.add(cvXLSM2.Id);

            //map of content version id's        
            Map<String, ID> mapCDTypeCDId = new Map<String, ID>();
            for(ContentVersion cv : [SELECT Id
                                          , Title
                                          , ContentDocumentId 
                                     FROM ContentVersion 
                                     WHERE Id IN :setCVId]){
                mapCDTypeCDId.put(cv.Title, cv.ContentDocumentId);
            }    
             
            //invalid file type
            try{
                ContentDocumentLink cdlPWBVInvalidFile = new ContentDocumentLink(LinkedEntityId = oProjectWorkbookVersion.Id,
                                                                                ContentDocumentId = mapCDTypeCDId.get('cvJPG'),
                                                                                ShareType = 'V');
                insert cdlPWBVInvalidFile;
                } catch (Exception e) {
                    e.getMessage();
                    //assert
                    Boolean errorInvalidPWBVFileType = TRUE;
                    System.assertEquals(errorInvalidPWBVFileType, true, e.getMessage());
                }

            //invalid number of attachments
            ContentDocumentLink cdlPWBVMaxAttachment1 = new ContentDocumentLink(LinkedEntityId = oProjectWorkbookVersion.Id,
                                                                                ContentDocumentId = mapCDTypeCDId.get('cvXLSM'),
                                                                                ShareType = 'V');
			try {
				insert cdlPWBVMaxAttachment1;
            } catch (Exception ex) {
				if (ex.getMessage().contains('You do not have access to upload Files to this Project')) {
					//add user to the project team
					TestDataUtility.createOpportunityTeamMember(oProject.Id, 'Loan Processor', UserInfo.getUserId());
					insert cdlPWBVMaxAttachment1;
				}
            }


            try{
                ContentDocumentLink cdlPWBVMaxAttachment2 = new ContentDocumentLink(LinkedEntityId = oProjectWorkbookVersion.Id,
                                                                                    ContentDocumentId = mapCDTypeCDId.get('cvXLSM2'),
                                                                                    ShareType = 'V');
                insert cdlPWBVMaxAttachment2;
            } catch (Exception e){
                e.getMessage();
                //assert
                Boolean errorInvalidNumberOfAttachments = TRUE;
                System.assertEquals(errorInvalidNumberOfAttachments, true, e.getMessage());
            }
        Test.stopTest();
    }

	@isTest static void testContentDocumentLinkPDT() {
        //STAGE METADATA TYPES==================================================================================================
        Enable_Triggers__mdt[] RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ContentDocumentLink'];
		
		Approved_Attachment_Extension__mdt[] ApprovedAttachment = [SELECT MasterLabel
                                                                   FROM Approved_Attachment_Extension__mdt];

        List<File_Sharing_Settings_Objects__mdt> mdtSharingRules = [SELECT MasterLabel
                                                                         , Object_Prefix__c
                                                                         , Fields_To_Query__c
                                                                         , Prohibit_File_Upload__c
																	     , Update_Parent_Upload_Status__c
                                                                         , Update_Parent_File_Count__c 
                                                                    FROM File_Sharing_Settings_Objects__mdt];

        File_Sharing_Settings_Users__mdt[] fileSharingSetting = [SELECT MasterLabel
                                                                      , LinkedEntityId__c
                                                                      , ShareType__c
                                                                      , Visibility__c
                                                                FROM File_Sharing_Settings_Users__mdt];
        
		File_Sharing_Settings_Users__mdt [] FundingAppCommunityID = [SELECT MasterLabel
                                                                         , LinkedEntityId__c
                                                                      FROM File_Sharing_Settings_Users__mdt
                                                                      WHERE MasterLabel = 'Multifamily Customer Portal'];
		                                                            
        String idFundingAppCommunityID = FundingAppCommunityID[0].LinkedEntityId__c;
		
		Org_URLs__c csORGurls = new Org_URLs__c();         
        csORGurls.External_URL__c = 'https://mnhousing.force.com/MultifamilyPortal/';        
        csORGurls.Internal_URL__c = 'https://mnhousing.my.salesforce.com/';           
        insert csORGurls;

		Org_Environment__c csORGenvironment = new Org_Environment__c();
		csORGenvironment.Environment__c = 'ORG';
		insert csORGenvironment;
		
		//SUPPORTING DATA=======================================================================================================
		//create test users
		User oROUser = TestDataUtility.createUser('Multifamily Standard User',
													 'testRO@mhfatest.com');
		
		User oTestUser1 = TestDataUtility.createUser('Multifamily Standard User',
	                                                 'testOTM1@mhfatest.com');
	        
		//FUNIDNG ROUND FRAMEWORK===============================================================================================
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		oFundingRound.Roles_To_Notify__c = 'LoanProcessor';
        update oFundingRound;

		//PROJECT FRAMEWORK=====================================================================================================
		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
                                                        
		//project team member
		OpportunityTeamMember oProjectTeamMember1 = TestDataUtility.createOpportunityTeamMember(oProject.Id, 
																								'Loan Processor', 
																								oTestUser1.Id);
		
		//fetch required data (spun up on project creation)
		Project_Documentation__c oProjectDocumentation = [SELECT Id
															   , Maximum_Number_of_Attachments__c
															   , Required_File_Extension__c
		                                                  FROM Project_Documentation__c
												          WHERE Project__c = :oProject.Id
												          AND Bucket__c = 'Approvals'];
		
		oProjectDocumentation.Maximum_Number_of_Attachments__c = '1';
		oProjectDocumentation.Required_File_Extension__c = 'JPG';

		update oProjectDocumentation;		
		
		Project_Documentation_Type__c oProjectDocumentationType = [SELECT Id																		
															       FROM Project_Documentation_Type__c
															       WHERE Name = 'Board'
															       AND Project_Documentation__c = :oProjectDocumentation.Id]; 
        
        //run tests        
        Test.startTest();
            //STAGE FILE CONTENT VERSIONS
            Set<ID> setCVId = new Set<ID>();
            ContentVersion cvXLSM =new ContentVersion(Title = 'cvXLSM',
                                                      PathOnClient= '/Test.xlsm',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvXLSM;
            setCVId.add(cvXLSM.Id);

            ContentVersion cvJPG = new ContentVersion(Title = 'cvJPG',
                                                     PathOnClient= '/Test.jpg',
                                                     VersionData = Blob.valueOf('Test Body'),
                                                     Origin = 'H');
            insert cvJPG;
            setCVId.add(cvJPG.Id);

            ContentVersion cvJPG2 =new ContentVersion(Title = 'cvJPG2',
                                                      PathOnClient= '/Test.jpg',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvJPG2;
            setCVId.add(cvJPG2.Id);

            ContentVersion cvABC =new ContentVersion(Title = 'cvABC',
                                                      PathOnClient= '/Test.abc',
                                                      VersionData = Blob.valueOf('Test Body'),
                                                      Origin = 'H');
            insert cvABC;
            setCVId.add(cvABC.Id);

            //map of content version id's        
            Map<String, ID> mapCDTypeCDId = new Map<String, ID>();
            for(ContentVersion cv : [SELECT Id
                                          , Title
                                          , ContentDocumentId 
                                     FROM ContentVersion 
                                     WHERE Id IN :setCVId]){
              mapCDTypeCDId.put(cv.Title, cv.ContentDocumentId);
            }
          
          //invalid file type
          try{
              ContentDocumentLink cdlPDTInvalidFile = new ContentDocumentLink(LinkedEntityId = oProjectDocumentationType.Id,
                                                                                ContentDocumentId = mapCDTypeCDId.get('cvXLSM'),
                                                                                ShareType = 'V');
              insert cdlPDTInvalidFile;
              } catch (Exception e) {
                  e.getMessage();
                  //assert
                  Boolean errorInvalidPDTFileType = TRUE;
                  System.assertEquals(errorInvalidPDTFileType, true, e.getMessage());
              }
          
			//invalid number of attachments
			ContentDocumentLink cdlPDTMaxAttachment1 = new ContentDocumentLink(LinkedEntityId = oProjectDocumentationType.Id,
                                                                             ContentDocumentId = mapCDTypeCDId.get('cvJPG'),
                                                                             ShareType = 'V');
			try {
				insert cdlPDTMaxAttachment1;
            } catch (Exception ex) {
				if (ex.getMessage().contains('You do not have access to upload Files to this Project')) {
					//add user to the project team
					TestDataUtility.createOpportunityTeamMember(oProject.Id, 'Loan Processor', UserInfo.getUserId());
					insert cdlPDTMaxAttachment1;
				}
            }

          try{
              ContentDocumentLink cdlPDTMaxAttachment2 = new ContentDocumentLink(LinkedEntityId = oProjectDocumentationType.Id,
                                                                                 ContentDocumentId = mapCDTypeCDId.get('cvJPG2'),
                                                                                 ShareType = 'V');
              insert cdlPDTMaxAttachment2;
          } catch (Exception e){
              e.getMessage();
              //assert
              Boolean errorInvalidNumberOfAttachments = TRUE;
              System.assertEquals(errorInvalidNumberOfAttachments, true, e.getMessage());
          }

          //invalid global file type
          ContentDocumentLink cdlInvalidGlobalFilePDT = new ContentDocumentLink(LinkedEntityId = oProjectDocumentationType.Id,
                                                                                ContentDocumentId = mapCDTypeCDId.get('cvABC'),
                                                                                ShareType = 'V');

          try{                
              insert cdlInvalidGlobalFilePDT;   
          } catch (Exception e){
              //assert
              Boolean errorInvalidGlobalFileType = TRUE;
              System.assertEquals(errorInvalidGlobalFileType, true, e.getMessage());
          }
      Test.stopTest();
    }
}