/*
*   {purpose}  	Manage the functionality tied to Project Checklist Item automation                         
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   01/26/17	Kevin Johnson DCS		Created
*	07/11/19	Kevin Johnson DCS		Added method processApprovalUsers
*	07/26/19	Kevin Johnson DCS		Added method createPCLILinks
*   =============================================================================
*/

public with sharing class ProjectChecklistItemUtility {		
	public static void setPCLIOwner(List<Project_Checklist_Item__c> lstPCLI){
		/*
		*{purpose}		Manages PCLI record ownership
		*
		*{function}		Ensures owner of PCLI = owner of the related Project (Developer owns project).  Developer owning PCLI impacts
		*				record security/visibiliy model.  If Developer does not own PCLI record access to PCLI can be denied to Developer.
		*				
		*{trigger}  	ProjectChecklistItemEventListener: before insert
		*/

		//create id set used for soql queries
		Set<ID> setPCLIids = new Set<ID>();
		for(Project_Checklist_Item__c pcli : lstPCLI){
			setPCLIids.add(pcli.Project__c);
		}

		//create project owner map to be used in downstream logic
		Map<Id, Id> mapParentProjectOwner = new Map<Id, Id>();
		for(Opportunity oOppty : [SELECT Id
									   , OwnerId
								  FROM Opportunity
								  WHERE Id IN :setPCLIids]){
			mapParentProjectOwner.put(oOppty.Id, oOppty.OwnerId);
		}

		for(Project_Checklist_Item__c pcli : lstPCLI){
			if(pcli.OwnerId != mapParentProjectOwner.get(pcli.Project__c)){
				pcli.OwnerId = mapParentProjectOwner.get(pcli.Project__c);
			}
		}
	}

	public static string processApprovalUsers(List<Project_Checklist_Item__c> lstPCLI, List<String> lstRoleFieldUpdated){
        /*
        *{purpose}      Manages assignment of approvers
        *
        *{function}     Returns users role reflected in related Project Team as well as sub phase expected approvers designated at related
        *               Funding Round Sub Phase.  Approver assigned based on Project Team designation / approver role designation relationship
        *               
        *{trigger}      ProjectChecklistItemEventListener: after insert, after update       
        */

        Set<ID> setPCLIID = new Set<ID>();
        Set<ID> setOpptyID = new Set<ID>();
        Map<ID, Map<String, ID>> mapOTMRoles = new Map<ID, Map<String, ID>>();
        List<Project_Checklist_Item__c> lstPCLIToProcess = new List<Project_Checklist_Item__c>();
        List<Project_Checklist_Item__c> lstPCLIUpdate = new List<Project_Checklist_Item__c>();
        String errorMsg = null;

        //obtain pcli ids to be used in query
        for(Project_Checklist_Item__c pcli : lstPCLI){
            setPCLIID.add(pcli.Id);  
        }

        //obtain unique set of opportunity ids passed from trigger.  need to query so as to obtain related record attributes.       
        for(Project_Checklist_Item__c pcli : [SELECT Id
                                                   , Approval_First__c
                                                   , Approval_Second__c
                                                   , Approval_Third__c
                                                   , Approval_Role_First__c
                                                   , Approval_Role_Second__c
                                                   , Approval_Role_Third__c
                                                   , Project__r.Id
                                               FROM Project_Checklist_Item__c
                                               WHERE Id IN :setPCLIID]){
            setOpptyID.add(pcli.Project__r.Id);
            lstPCLIToProcess.add(pcli);
        }
        //return all team members existing on affected opportunities (projects)
        for(OpportunityTeamMember otm : [SELECT OpportunityId, 
                                                TeamMemberRole, 
                                                UserId 
                                        FROM OpportunityTeamMember 
                                        WHERE OpportunityId = :setOpptyID]){
            //build map to reference while looping through returned project checklist items passed from trigger
            Map<String, ID> mapUserRoles = new Map<String, ID>();
            if(mapOTMRoles.containsKey(otm.OpportunityId)){
                mapUserRoles = mapOTMRoles.get(otm.OpportunityId);
            }
            mapUserRoles.put(otm.TeamMemberRole, otm.UserId);
            mapOTMRoles.put(otm.OpportunityId, mapUserRoles);
        }
        system.debug('MAPOTMROLES: ' + mapOTMRoles);
        system.debug('lstRoleFieldUpdated: ' + lstRoleFieldUpdated); 
        for(Project_Checklist_Item__c pcli : lstPCLIToProcess){  //process passed pcli records
            ID approverFirstUserID = null;
            ID approverSecondUserID = null;
            ID approverThirdUserID = null;          
            //determine who/where to place team member based on their role/funding round checklist item approval mapping
            //system.debug('PCLI OPPTY ID: ' + pcli.Project__c);
            if(pcli.Approval_Role_First__c != null){
                if(!mapOTMRoles.isEmpty() &&  //is map of otm roles populated
				    mapOTMRoles.containsKey(pcli.Project__c) &&  //does project have team members assigned
				    mapOTMRoles.get(pcli.Project__c).containsKey(pcli.Approval_Role_First__c)){  //process users/roles assigned to project team
						approverFirstUserID = mapOTMRoles.get(pcli.Project__c).get(pcli.Approval_Role_First__c);
                }
                if(mapOTMRoles.isEmpty() && lstRoleFieldUpdated != null){  //process roles not assigned to project team - clear invalid name if exist
                    if(lstRoleFieldUpdated.contains('First')){
                        approverFirstUserID = null;
                    } 
                }
            }
            if(pcli.Approval_Role_Second__c != null){
                if(!mapOTMRoles.isEmpty() &&  //is map of otm roles populated
					mapOTMRoles.containsKey(pcli.Project__c) &&  //does project have team members assigned
					mapOTMRoles.get(pcli.Project__c).containsKey(pcli.Approval_Role_Second__c)){  //process users/roles assigned to project team
						approverSecondUserID = mapOTMRoles.get(pcli.Project__c).get(pcli.Approval_Role_Second__c);
                }
                if(mapOTMRoles.isEmpty() && lstRoleFieldUpdated != null){  //process roles not assigned to project team - clear invalid name
                    if(lstRoleFieldUpdated.contains('Second')){
                        approverSecondUserID = null;
                    }
                }
            }
            if(pcli.Approval_Role_Third__c != null){
                if(!mapOTMRoles.isEmpty() &&  //is map of otm roles populated
					mapOTMRoles.containsKey(pcli.Project__c) &&  //does project have team members assigned
					mapOTMRoles.get(pcli.Project__c).containsKey(pcli.Approval_Role_Third__c)){  //process users/roles assigned to project team
						approverThirdUserID = mapOTMRoles.get(pcli.Project__c).get(pcli.Approval_Role_Third__c);
                }
                if(mapOTMRoles.isEmpty() && lstRoleFieldUpdated != null){  //process roles not assigned to project team - clear invalid name
                    if(lstRoleFieldUpdated.contains('Third')){
                        approverThirdUserID = null;
                    }
                }
            }

            Project_Checklist_Item__c pcliToUpdate = new Project_Checklist_Item__c(Id = pcli.Id,
                                                                                   Approval_First__c = approverFirstUserID,                           
                                                                                   Approval_Second__c = approverSecondUserID,
                                                                                   Approval_Third__c = approverThirdUserID);                                                                                
            lstPCLIUpdate.add(pcliToUpdate);
        }
        system.debug('LSTPCLITOUPDATE: ' + lstPCLIUpdate);
        if(!lstPCLIUpdate.isEmpty()){
            Database.SaveResult[] updResult = Database.update(lstPCLIUpdate, false);
            
            for(Database.SaveResult sr : updResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding approvers: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
    }

	public static string createPCLILink(List<Project_Checklist_Item__c> lstPCLI){
        /*
        *{purpose}      Manages creating of Project Checklist Item Link records
        *
        *{function}     Returns Funding Round Checklist Item Link records related to parent Funding Round Checklist Item
        *               Returned records are created as Project Checklist Item Link records to their parent Project Checklist Item passed from trigger
		*				-Multiple Funding Round Project Checklist Item Link records can be related to a Funding Round Checklist Item
		*				-Link between Funding Round Checklist Item Link and Project Checklist Item = Funding Round Checklist Item
        *               
        *{trigger}      ProjectChecklistItemEventListener: after insert     
        */

		Set<Id> setFRCLIID = new Set<Id>();
		List<Project_Checklist_Item_Link__c> lstPCLILinkToCreate = new List<Project_Checklist_Item_Link__c>();
		List<Funding_Round_Checklist_Item_Link__c> lstFRCLILink = new List<Funding_Round_Checklist_Item_Link__c>();
		String errorMsg = null;

		//stage ids/objects to be used in processing
		for(Project_Checklist_Item__c pcli : lstPCLI){
			setFRCLIID.add(pcli.Funding_Round_Checklist_Item__c);
		}

		//fetch frclilink records to be processed and create related pclilink records to be inserted
		for(Funding_Round_Checklist_Item_Link__c frclilink : [SELECT Id
																   , Name
																   , URL__c
																   , Notes__c
																   , Funding_Round_Checklist_Item__c
															  FROM Funding_Round_Checklist_Item_Link__c
															  WHERE Funding_Round_Checklist_Item__c IN :setFRCLIID]){			
			lstFRCLILink.add(frclilink);
		}

		for(Project_Checklist_Item__c pcli : lstPCLI){  //loop through PCLI records passed from trigger
			for(Funding_Round_Checklist_Item_Link__c frclilink : lstFRCLILink){  //loop through returned frclilink records
				if(pcli.Funding_Round_Checklist_Item__c == frclilink.Funding_Round_Checklist_Item__c){
					Project_Checklist_Item_Link__c newPCLILink = new Project_Checklist_Item_Link__c(Project_Checklist_Item__c = pcli.Id,
																								    Name = frclilink.Name,
																								    Funding_Round_Checklist_Item_Link__c = frclilink.Id); 
					lstPCLILinkToCreate.add(newPCLILink);
				}
			}
		}
		system.debug('LSTPCLILINKTOCREATE: ' + lstPCLILinkToCreate);
        if(!lstPCLILinkToCreate.isEmpty()){
            Database.SaveResult[] insResult = Database.insert(lstPCLILinkToCreate, false);
            
            for(Database.SaveResult sr : insResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding resouce link record: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
	}
}