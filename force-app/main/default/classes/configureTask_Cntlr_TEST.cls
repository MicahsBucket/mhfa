@isTest 
private class configureTask_Cntlr_TEST {

	@isTest
	private static void test_configureTask() {
		Funding_Round__c oFundingRound = TestDataUtility.buildFundingRoundDataStructure();
		Funding_Round_Checklist_Item_Trigger__c oCLIChar = [Select Funding_Round_Checklist_Item__c
																	, Funding_Round_Trigger__c
															From Funding_Round_Checklist_Item_Trigger__c
															LIMIT 1];
		List<String> lstCharIds = new List<String>();
		lstCharIds.add(oCLIChar.Funding_Round_Trigger__c);

		Opportunity oProject = TestDataUtility.buildProjectDataStructure(oFundingRound.Id);
		Funding_Round_Task__c oTask = TestDataUtility.createFundingRoundStageTaskPreSelection(oFundingRound.Id);

		Test.startTest();

		configureTask_Cntlr.getGroupings(oFundingRound.Id, oTask.Id, 'Pre-Selection');
		configureTask_Cntlr.getAvailableCharacteristics(oFundingRound.Id, oTask.Id, 'Pre-Selection');
		configureTask_Cntlr.getAssignedCharacteristics(oFundingRound.Id, oTask.Id, 'Pre-Selection');
		configureTask_Cntlr.insertTaskCharacteristics(oTask.Id, lstCharIds, true);
		configureTask_Cntlr.removeTaskCharacteristics(oTask.Id, lstCharIds);

		Test.stopTest();


	}
}