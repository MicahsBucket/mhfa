/*
*   {purpose}  	Manage the functionality tied to Funding Round Checklist Item SubPhase automation
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    		Name             		Description
*	11/13/18		Kevin Johnson DCS		Created
*   =============================================================================
*/	

public with sharing class FundingRoundCLISubPhaseUtility  {
	public static string processApprovalRoles(List<Funding_Round_Checklist_Item_SubPhase__c> lstFRCLISP){
		/*
		*{purpose}		Cascades updated approver roles to related Projects Project Checklist Item Phase records
		*
		*{function}		Fetches related Project Checklist Item Attribute records linking the Project Checklist Item Phase to the Funding Round Checklist Item SubPhase.
		*				The returned Project Checklist Item Attribute records provide the data needed to update the related Project Checklist Item Phase records with the
		*				appropriate approver roles.
		*				
		*{trigger}  	FundingRoundCLISubPhaseEventListener: after update
		*				
		*/

		Set<Id> setFRCLISPid = new Set<Id>();
		Map<Id, Project_Checklist_Item_Phase__c> mapPCLIP = new Map<Id, Project_Checklist_Item_Phase__c>();
		List<Project_Checklist_Item_Phase__c> lstPCLIPUpdate = new List<Project_Checklist_Item_Phase__c>();
		String errorMsg = null; 

		for(Funding_Round_Checklist_Item_SubPhase__c oFRCLISP : lstFRCLISP){
			setFRCLISPid.add(oFRCLISP.Id);
		}

		for(Project_Checklist_Item_Attributes__c oPCLIA : [SELECT Project_Checklist_Item_Phase__r.Id
																, Funding_Round_Checklist_Item_SubPhase__r.Approval_First__c
														        , Funding_Round_Checklist_Item_SubPhase__r.Approval_Second__c
														        , Funding_Round_Checklist_Item_SubPhase__r.Approval_Third__c
														  FROM Project_Checklist_Item_Attributes__c
														  WHERE Funding_Round_Checklist_Item_SubPhase__c IN :setFRCLISPid]){

			Project_Checklist_Item_Phase__c pclipToUpdate = new Project_Checklist_Item_Phase__c(Id = oPCLIA.Project_Checklist_Item_Phase__r.Id,
													                                            Approval_Role_First__c = oPCLIA.Funding_Round_Checklist_Item_SubPhase__r.Approval_First__c,                           
													                                            Approval_Role_Second__c = oPCLIA.Funding_Round_Checklist_Item_SubPhase__r.Approval_Second__c,
													                                            Approval_Role_Third__c = oPCLIA.Funding_Round_Checklist_Item_SubPhase__r.Approval_Third__c);
			
			mapPCLIP.put(oPCLIA.Project_Checklist_Item_Phase__r.Id, pclipToUpdate);			
		}

		for(Project_Checklist_Item_Phase__c oPCLIP : mapPCLIP.values()) {
			lstPCLIPUpdate.add(oPCLIP);
		}
		System.debug('lstPCLIPUpdate size: ' + lstPCLIPUpdate.size());
		System.debug('lstPCLIPUpdate: ' + lstPCLIPUpdate);
		
		if(!lstPCLIPUpdate.isEmpty()){
			Database.SaveResult[] updResult = Database.update(lstPCLIPUpdate, false);
			
			for(Database.SaveResult sr : updResult){
				if(!sr.isSuccess()){
					for(Database.Error err : sr.getErrors()){
						errorMsg = 'Error updating approver roles: ' + err.getStatusCode() + ' - ' + err.getMessage();
					}
				}
			}
		}
		return errorMsg;
	}
}