/*
*   {purpose}   Manage the functionality tied to Funding Round Team Member automation           
*
*   CHANGE HISTORY
*   =============================================================================
*   Date        Name                    Description
*   07/31/19    Kevin Johnson DCS       Created
*   =============================================================================
*/

public class FundingRoundTeamMemberUtility  {
	public static string processProjectTaskOwner(List<Funding_Round_Team_Member__c> lstFRTM){
		/*
		*{purpose}		Cascades inserted/updated Funding Round Team Member owner to related Project Tasks
		*
		*{function}		Fetches open project tasks
		*				Processes returned project tasks respective to how related funding round task has been dispositioned
		*					if = internal task
		*						-Assign owner by designated project team role if project team designated
		*						-Assign owner by designated funding round team member if funding round team designated
		*					if = external task
		*						-Assign owner = project stage tasks project owner
		*  
		*{assumptions}	If returned project task does not have new owner reflecting related funding round task the owner
		*				will not be updated
		*     
		*{trigger}		FundingRoundTeamMemberEventListener: after insert, after update
		*/

		Set<String> setUserRoles = new Set<String>();
		Set<Id> setFundingRoundIds = new Set<Id>();
		Map<ID, Map<String, ID>> mapFRTMRoles = new Map<ID, Map<String, ID>>();
		List<Project_Task__c> lstProjectTasksToUpdate = new List<Project_Task__c>();
		String errorMsg = null;

		for(Funding_Round_Team_Member__c frtm : lstFRTM){
			setUserRoles.add(frtm.Team_Role__c);
			setFundingRoundIds.add(frtm.Funding_Round__c);
			//System.debug('FRTM setUserRoles: ' + setUserRoles);
			//System.debug('FRTM setFundingRoundIds: ' + setFundingRoundIds);
			
            Map<String, ID> mapTeamRoles = new Map<String, ID>();
			//System.debug('FRTM mapTeamRoles: ' + mapTeamRoles);
            if(mapFRTMRoles.containsKey(frtm.Funding_Round__c)){
                mapTeamRoles = mapFRTMRoles.get(frtm.Funding_Round__c);
            }
            mapTeamRoles.put(frtm.Team_Role__c, frtm.Team_Member__c);
            mapFRTMRoles.put(frtm.Funding_Round__c, mapTeamRoles);
		}

		//return affected project tasks
		for(Project_Task__c oPT :[SELECT Id
		                                , OwnerId
										, Project__r.Funding_Round__r.Id
										, Funding_Round_Task__c
										, Funding_Round_Task__r.Assignee_Type__c
										, Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c
									FROM Project_Task__c
									WHERE Project__r.Funding_Round__r.Id IN :setFundingRoundIds
									AND Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c IN :setUserRoles
									AND (Task_Status__c != 'Complete'
										OR Task_Status__c != 'NA')]){

			Project_Task__c pstUpdate = new Project_Task__c(Id = oPT.Id,
														   OwnerId = mapFRTMRoles.get(oPT.Project__r.Funding_Round__r.Id).get(oPT.Funding_Round_Task__r.Assignee_Role_Funding_Round_Team__c));
			//system.debug('FRT pstUpdate: ' + pstUpdate);
			lstProjectTasksToUpdate.add(pstUpdate);
		}
		//system.debug('FRT lstProjectTasksToUpdate: ' + lstProjectTasksToUpdate);
		if(!lstProjectTasksToUpdate.isEmpty()){
            Database.SaveResult[] updResult = Database.update(lstProjectTasksToUpdate, false);
            
            for(Database.SaveResult sr : updResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error adding assignee: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
		return errorMsg;
	}

	public static Map<Id, Map<String, ID>> retrieveFRTeamRoles(Set<Id> setFundingRoundIds) {
		/*
        *{purpose}      Returns funding round's team members
        *
        *{function}     Adds user (by role) designated at the funding round team to map to be passed back to calling function
		*/
		Map<Id, Map<String, ID>> mapFRTeamRoles = new Map<Id, Map<String, ID>>();
		for(Funding_Round_Team_Member__c frtm : [SELECT Funding_Round__c
														, Team_Role__c
														, Team_Member__c
												FROM Funding_Round_Team_Member__c
												WHERE Funding_Round__c in :setFundingRoundIds]){ 
			Map<String, Id> mapTeamRoles = new Map<String, Id>();
			if (mapFRTeamRoles.containsKey(frtm.Funding_Round__c)) {
				mapTeamRoles = mapFRTeamRoles.get(frtm.Funding_Round__c);
			}
			mapTeamRoles.put(frtm.Team_Role__c, frtm.Team_Member__c);
			mapFRTeamRoles.put(frtm.Funding_Round__c, mapTeamRoles);
		}
		return mapFRTeamRoles;
	}
}