global class batchResetPclipDates implements Database.Batchable<SObject> {

	global final Set<Id> setProjectIds;
    global final Map<Id, Map<Id, Date>> mapPSPs;
    global final String soqlStmt;

    //constructor
	public batchResetPclipDates(Set<Id> pSetProjectIds
								, Map<Id, Map<Id, Date>> pMapPSPs) {
		setProjectIds = pSetProjectIds;

		String strProjectIds = '';
		for (Id projId : setProjectIds) {
			strProjectIds += '\'' + projId + '\',';
		}
		if (strProjectIds.right(1) == ',') {
			strProjectIds = strProjectIds.left(strProjectIds.length() - 1);
		}

		mapPSPs = pMapPSPs;
		soqlStmt = 'SELECT Id'
				+ ', Checklist_Item__c' 
				+ ', Funding_Round_Checklist_Item__c'
				+ ', Project__c'
				+ ', Project__r.CloseDate'
				+ ', (Select Id'
					+ ', Project_Checklist_Item_Phase__c'
					+ ', Funding_Round_Sub_Phase__c'
					+ ', Funding_Round_Phase__c'
					+ ', Funding_Round_Phase__r.End_Date__c'
					+ ', Funding_Round_Phase__r.Due_Date_Baseline__c'
					+ ', Offset_Value__c'
					+ ' From Project_Checklist_Item_Attributes__r'
					+ ' Where Project_Checklist_Item_Phase__c != null) '
				+ 'FROM Project_Checklist_Item__c '
				+ 'WHERE Project__c IN :setProjectIds';

	}

    ////////////////////////////////////////////////
    //base method that runs when the class is instantiated.  Its return value is sent to the "execute" routine
    ////////////////////////////////////////////////
    global Database.QueryLocator start(Database.BatchableContext BC) {
	    system.debug('\n\n******Inside start.  soqlStmt:\n' + soqlStmt + '\n*********\n');
      
        return Database.getQueryLocator(soqlStmt);
    }

    ////////////////////////////////////////////////
    // EXECUTE
    ////////////////////////////////////////////////
    global void execute(Database.BatchableContext BC, List<SObject> jobRecords) {

        Map<Id, Project_Checklist_Item_Phase__c> mapUpdatePCLIPS = new Map<Id, Project_Checklist_Item_Phase__c>();

		system.debug('Inside execute.  Record count: ' + jobRecords.size());
		for (SObject oRec : jobRecords) {
			Project_Checklist_Item__c oPCLI = (Project_Checklist_Item__c)oRec;
            system.debug('Resetting due dates for ' + oPCLI);
            if (oPCLI.Project_Checklist_Item_Attributes__r.size() > 0) {
                for (Project_Checklist_Item_Attributes__c oCLIA : oPCLI.Project_Checklist_Item_Attributes__r) {

                    //look at the current phase to determine if the base date should be the phase due date or the project's close date
                    Date dtBaseDueDate;
                    if(oCLIA.Funding_Round_Phase__r.End_Date__c != null) {
                        dtBaseDueDate = date.newinstance(oCLIA.Funding_Round_Phase__r.End_Date__c.year()
                                                        , oCLIA.Funding_Round_Phase__r.End_Date__c.month()
                                                        , oCLIA.Funding_Round_Phase__r.End_Date__c.day());

                    }
                    if (oCLIA.Funding_Round_Phase__r.Due_Date_Baseline__c == 'Project Phase Due Date') {
                        dtBaseDueDate = oPCLI.Project__r.CloseDate;
                    }

                    if (mapPSPs.containsKey(oPCLI.Project__c)) {
                        if (mapPSPs.get(oPCLI.Project__c).containsKey(oCLIA.Funding_Round_Sub_Phase__c)) {
                            dtBaseDueDate = mapPSPs.get(oPCLI.Project__c).get(oCLIA.Funding_Round_Sub_Phase__c);
                        }
                    }

                    Integer intOffset = Integer.valueOf(oCLIA.Offset_Value__c);
                    system.debug('dtBaseDueDate: ' + dtBaseDueDate 
                                    + '\nintOffset: ' + intOffset); 

                    if (dtBaseDueDate != null) {
                        Project_Checklist_Item_Phase__c oPCLIP = new Project_Checklist_Item_Phase__c(id = oCLIA.Project_Checklist_Item_Phase__c
                                                                                                        , Due_Date__c = dtBaseDueDate.addDays(intOffset));
                        mapUpdatePCLIPS.put(oPCLIP.Id, oPCLIP);
                    }
                }
            }
        }

        if (mapUpdatePCLIPS.size() > 0) {
            update mapUpdatePCLIPS.values();
        }

    }

    ////////////////////////////////////////////////
    // FINISH
    ////////////////////////////////////////////////
    global void finish(Database.BatchableContext BC){
              
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [Select Id
                                , CompletedDate
                                , Status
                                , ExtendedStatus
                                , JobType
                                , NumberOfErrors
                                , JobItemsProcessed
                                , TotalJobItems 
                            from AsyncApexJob 
                            where Id = :BC.getJobId()];
        
        // Send an email to the admin notifying of job failure.
        if (a.NumberOfErrors > 0 || Test.isRunningTest()) {

		  	List<System_Notification_Setting__mdt> SystemSetting = [SELECT Email_Address__c
											  		       		 FROM System_Notification_Setting__mdt
											  		       		 WHERE MasterLabel = 'batchResetPclipDates'];

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {SystemSetting[0].Email_Address__c};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Error in Scheduled Routine : batchResetPclipDates');
            String strBody = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.\n' +
                            '\nCompletedDate: ' + a.CompletedDate +
                            '\nStatus: ' + a.Status +
                            '\nExtendedStatus: ' + a.ExtendedStatus +
                            '\nJobType: ' + a.JobType +
                            '\nJobItemsProcessed: ' + a.JobItemsProcessed;
                            
            mail.setPlainTextBody(strBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }    

}