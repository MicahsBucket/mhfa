@isTest
private class ProjectDocumentCustomerUtility_TEST  {
	@isTest static void testRecordValidation() {
		//stage data
		 //create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

		//create project document: customer
		Project_Documents_Customer__c pdc = TestDataUtility.createProjectDocumentCustomer(p.Id,
		                                                                                  'Selection Letter');

		Project_Documents_Customer__c pdc2 = TestDataUtility.createProjectDocumentCustomer(p.Id,
		                                                                                  'Project Launch Agenda');

		Test.startTest();
		    //test duplicate pdc
            //insert
			try{
                Project_Documents_Customer__c pdc3 = TestDataUtility.createProjectDocumentCustomer(p.Id,
		                                                                                           'Selection Letter');
            } catch (Exception e){  //'Document Selection Letter already exists for this Project and thus cannot be added.'
                Boolean validationErrorRecord =  TRUE;
                System.assertEquals(validationErrorRecord, true, e.getMessage());
            } 
			//update
			try{
                pdc2.Type__c = 'Selection Letter';
				update pdc2;
            } catch (Exception e){  //'Document Selection Letter already exists for this Project and thus cannot be added.'
                Boolean validationErrorRecord =  TRUE;
                System.assertEquals(validationErrorRecord, true, e.getMessage());
            } 
        Test.stopTest();
	}
}