@isTest 
private class FundingRoundCLISubPhaseUtility_TEST {
	@isTest
	private static void testFundingRoundCLISubPhaseUtility() {
		//stage custom settings
		//insert RunTrigger
		List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       				 FROM Enable_Triggers__mdt
					  		       				 WHERE MasterLabel = 'FundingRoundCLISubPhase'];

		//stage data	
		//create funding round and related records												
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundStructure();

		//return funding round checklist item
		Funding_Round_Checklist_Item__c oFRCLI = [SELECT Id
		                                               , Funding_Round__c 
												  FROM Funding_Round_Checklist_Item__c 
												  WHERE Funding_Round__c = :oFR.Id LIMIT 1];

		//return funding round phase												
		Phase__c oFRP = [SELECT Id
						 FROM Phase__c
						 WHERE Funding_Round__c = :oFR.Id LIMIT 1];

		//return funding round checklist item subphase				
		Funding_Round_Checklist_Item_SubPhase__c oFRCLISP = [SELECT Id
		                                                          , Approval_First__c
																  , Approval_Second__c
																  , Approval_Third__c
															 FROM Funding_Round_Checklist_Item_SubPhase__c 
															 WHERE Funding_Round_Checklist_Item__c = :oFRCLI.Id LIMIT 1];

		//create project
		Opportunity p = TestDataUtility.createProject(oFR.Id);

		//create pcli
		Project_Checklist_Item__c pcli = TestDataUtility.createProjectChecklistItem(p.id, 
																					null,
																					false,
																					null,
																					null);
      
		//create pclip
		Project_Checklist_Item_Phase__c pclip = TestDataUtility.createProjectChecklistItemPhase(pcli.id,
                                                												oFRP.Id,
                                                												false);

		//create pclia
		Project_Checklist_Item_Attributes__c pclia = TestDataUtility.createProjectChecklistItemAttribute(pcli.Id,
																										 pclip.Id,
																										 null,
																										 null,
																										 null,
																										 null,
																										 oFRCLISP.Id);																							

		//run tests                                                        
		Test.startTest();
			oFRCLISP.Approval_First__c = 'LegalTech';
			update oFRCLISP;
			oFRCLISP.Approval_Second__c = 'SHO';
			update oFRCLISP;
			oFRCLISP.Approval_Third__c = 'LoanProcessor';
			update oFRCLISP;	                                                
		Test.stopTest();
	}
}