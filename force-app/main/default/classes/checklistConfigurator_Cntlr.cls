public class checklistConfigurator_Cntlr  {
    
    @AuraEnabled
    public static List<Phase_Checklist__c> getItems(Id frid) {
        System.debug('KDJ: Inside getItems: ' + frid);
        return [SELECT Id, Name
                FROM Phase_Checklist__c
                WHERE Funding_Round__c = :frid];
    }

    @AuraEnabled
    public static List<String> getApprovers(){
        List<String> options = new List<String>();
        //cannot access picklist value set directly | object & field may be deprecated
        Schema.DescribeFieldResult fieldResult = Funding_Round_Checklist_Item_SubPhase__c.Approval_First__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }

    /*
/*    ///////////////////////////////////////////////////////////////////////////////////////////
    // Given an object names and a list of fields, return the field types for each so the fields
    //  can be displayed 
    ///////////////////////////////////////////////////////////////////////////////////////////
    @AuraEnabled
    public static List<Schema.DisplayType> retrieveFieldDetails(String objectName, List<String> lstFieldNames) {
        System.debug('KDJ: Inside retrieveFieldDetails.\nobjectName: ' + objectName + '\nlstFieldNames: ' + lstFieldNames);
        Map<String, Schema.DisplayType> mapFieldTypes = new Map<String, Schema.DisplayType>();
        List<Schema.DisplayType> lstFieldTypes = new List<Schema.DisplayType>();

        for (Schema.DescribeSObjectResult res : Schema.describeSObjects(new String[]{objectName})) {
            Map<String, Schema.SObjectField> fieldMap = res.fields.getMap();
            System.debug('KDJ: fieldMap: ' + fieldMap);
            for (String fieldName : lstFieldNames) {
                //remove any extra spaces that might cause problems
                fieldName = fieldName.trim();
                System.debug('KDJ: Processing field: ' + fieldName);
                if (fieldMap.containsKey(fieldName)) {
                    System.debug('KDJ: Type Description: ' + fieldMap.get(fieldName).getDescribe().getType());
                    mapFieldTypes.put(fieldName, fieldMap.get(fieldName).getDescribe().getType());
                    lstFieldTypes.add(fieldMap.get(fieldName).getDescribe().getType());
                } else {
                    mapFieldTypes.put(fieldName, null);
                    lstFieldTypes.add(null);
                }
            }
        }
        System.debug('KDJ: mapFieldTypes: ' + mapFieldTypes + '\nlstFieldTypes: ' + lstFieldTypes);
        return lstFieldTypes;
    }   
    
    @AuraEnabled
    public static List<sObject> getRecords(String objectName
                                            , List<String> lstDisplayFields
                                            , List<String> lstSearchFields
                                            , String searchCriteria) {


        System.debug('KDJ: Inside getRecords.\nObjectName: ' + objectName 
                    + '\nlstDisplayFields: ' + lstDisplayFields
                    + '\nlstSearchFields: ' + lstSearchFields
                    + '\nsearchCriteria: ' + searchCriteria);

        //add the list to a set to ensure we don't have duplicate field names
        Set<String> setDisplayFields = new Set<String>();
        setDisplayFields.addAll(lstDisplayFields);
        //we always retrieve the record id, so we don't want it in our set of fields
        if (setDisplayFields.contains('Id') == true) {
            setDisplayFields.remove('Id');
        }

        //build the soql statement using the provided fields
        String soqlStmt = 'SELECT Id';
        for (String sFieldName : setDisplayFields) {
            soqlStmt += ', ' + sFieldName;
        }
        soqlStmt += ' FROM ' + objectName;

        if (lstSearchFields.size() > 0 && String.isBlank(searchCriteria) == false) {
            //to guard against soql injection, escape the search criteria
            searchCriteria = String.escapeSingleQuotes(searchCriteria);

            //put the search fields in a set to eliminate duplicates
            Set<String> setFilterFields = new Set<String>();
            setFilterFields.addAll(lstSearchFields);

            List<String> lstWhereClause = new List<String>();

            for (String sSearchField : setFilterFields) {
                if (sSearchField.toLowerCase() == 'id' 
                        && (searchCriteria.length() == 15 || searchCriteria.length() == 18)) {
                    lstWhereClause.add(sSearchField + ' = ' + searchCriteria);
                } else if (sSearchField.toLowerCase() != 'id') {
                    lstWhereClause.add(sSearchField + ' LIKE \'' + searchCriteria + '%\'');
                }
            }

            soqlStmt += ' WHERE ';
            Integer whereClauseCounter = 0;
            for (String sWhereClause : lstWhereClause) {
                if (whereClauseCounter > 0) {
                    soqlStmt += ' OR ';
                }
                soqlStmt += sWhereClause;
                whereClauseCounter++;
            }
        }

        //now that the soql statement is built, execute it and return the values.  Use the function
        // "escapeSingleQuotes" to guard against SQL injection      
        System.debug('soqlStmt:\n' + soqlStmt);
        List<SObject> retVal = Database.query(soqlStmt);
        System.debug(retVal);
        return retVal;
    }
    */
}