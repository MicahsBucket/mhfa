@isTest 
private class FundingRound_Cntlr_Test {

	@isTest
	private static void retrievePhasesAndStages_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();											
		FundingRound_Cntlr.retrievePhasesAndStages(oFR.Id);
   		Test.stopTest();
	}

	@isTest
	private static void retrievePhaseStageTree_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();											
		FundingRound_Cntlr.retrievePhaseStageTree(oFR.Id, 'All Tasks');
   		Test.stopTest();
	}


	@isTest
	private static void retrievePhaseChecklistTree_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();											
		FundingRound_Cntlr.retrievePhaseChecklistTree(oFR.Id);
   		Test.stopTest();
	}

	@isTest
	private static void retrieveGroupCharacteristicTree_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();											
		FundingRound_Cntlr.retrieveGroupCharacteristicTree(oFR.Id);
   		Test.stopTest();
	}

	@isTest
	private static void retrieveFundingRoundBaseTasks_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();											
		FundingRound_Cntlr.retrieveFundingRoundBaseTasks(oFR.Id);
   		Test.stopTest();
	}

	@isTest
	private static void deleteRecord_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();											
		FundingRound_Cntlr.deleteRecord(oFR.Id, 'Characteristic Group');
		FundingRound_Cntlr.deleteRecord(oFR.Id, 'Characteristic');
   		Test.stopTest();
	}

	@isTest
	private static void cascadeDateChanges_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();

		Test.startTest();
		TestDataUtility.buildProjectDataStructure(oFR.Id);										
		FundingRound_Cntlr.fundingRoundDurationCount(oFR.Id);
		FundingRound_Cntlr.cascadeStageChanges(oFR.Id);
   		Test.stopTest();
	}

	public class hclsJSONTest {
		String id;
		String durationDays;
		Boolean notApplicable;

		public hclsJSONTest(Id recId, String days, Boolean notApp) {
			id = recId;
			durationDays = days;
			notApplicable = notApp;
		}
	}

	@isTest
	private static void saveChangedRecords_Duration_test() {
		Funding_Round__c oFR = TestDataUtility.buildFundingRoundDataStructure();
		System.debug('oFR: ' + oFR);
		Phase__c oStage;
		for (Phase__c oPhase : [Select Id
									, Name
									, Phase__c
									, (Select Id
											, Name
											, Sub_Phase__c 
										From SubPhases__r) 
								From Phase__c 
								Where Funding_Round__c = :oFR.Id]) {
			if (oPhase.SubPhases__r.size() > 0) {
				oStage = oPhase.SubPhases__r[0];
				break;
			}
		}
		Test.startTest();
												
		//json example: [{"durationDays":"30","id":"a0G0r000000GiAKEA0"}]
		List<hclsJSONTest> hlcTest = new List<hclsJSONTest>();
		hlcTest.add(new hclsJSONTest(oStage.Id, '30', false));
		FundingRound_Cntlr.saveChangedRecords_Duration(JSON.serialize(hlcTest));

   		Test.stopTest();
	}
}