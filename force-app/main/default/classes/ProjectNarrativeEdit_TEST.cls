@isTest
private class ProjectNarrativeEdit_TEST {
	@isTest static void testProjectNarrativeEdit() {
		//stage data
		//create funding round
        Funding_Round__c fr = TestDataUtility.createFundingRound(system.Now(), 
                                                                 system.Now().addMonths(9));

        //create funding round group
        Funding_Round_Narrative_Group__c frng = TestDataUtility.createFundingRoundNarrativeGroup(fr.Id);

        //create funding round author
        Funding_Round_Narrative_Author__c frna = TestDataUtility.createFundingRoundNarrativeAuthor(frng.Id);

        //create funding round narrative
        Funding_Round_Narrative__c frn = TestDataUtility.createFundingRoundNarrative(fr.Id,
                                                                                     frna.Id); 

        //create project                                                       
        Opportunity p = TestDataUtility.createProject(fr.Id);

        //delete pn w/o attributes (group)
        List<Project_Narrative__c> lstPNToDelete = new List<Project_Narrative__c>();
        for(Project_Narrative__c pn : [SELECT toLabel(Narrative_Group__c)
                                       FROM Project_Narrative__c 
                                       WHERE Project__c = :p.Id]){

            lstPNToDelete.add(pn);
        }

        delete lstPNToDelete;

        //create project narrative
        Project_Narrative__c pn = TestDataUtility.createProjectNarrative(p.Id,
        	 															 frn.Id,
        	 															 'Architect',
        	 															 'Architecture and Construction',
        	 															 'Selection Narratives',
        	 															 'In Process');        
       
        //set page
        PageReference editPage = Page.ProjectNarrative_Edit;
        Test.setCurrentPage(editPage);
        //set url parameters
        ApexPages.currentPage().getParameters().put('pnid',pn.Id);
        ApexPages.currentPage().getParameters().put('opptyid',p.Id);
        ApexPages.currentPage().getParameters().put('group', 'Selection Narratives');
        ApexPages.currentPage().getParameters().put('author', 'Architect');
        //set controller
        ProjectNarrativeEdit_Controller ext = new ProjectNarrativeEdit_Controller();

        //run tests
        Test.startTest();
        	ext.pnID = pn.Id;
        	ext.updateNarrative();
			PageReference pageRefRefresh = ext.refreshNarrative();
			PageReference pageRefManagePage = ext.returnToManagePage();
			PageReference pageReNarrativeList = ext.returnToNarrativeListPage();
        Test.stopTest();
	}
}