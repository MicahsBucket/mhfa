/*
*   {purpose}   Manage the functionality tied to ContentDocumentLink (Files) automation               
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date        Name                    Description
*   07/21/16    Kevin Johnson DCS       Created
*   05/16/17    Kevin Johnson DCS       Refactored updateParentUploadStatus to be object independent
*   09/26/17    Kevin Johnson DCS       Added emailNotification method - process PCLIP
*   11/30/17    Kevin Johnson DCS       Added check for null for roles to notify variable in emailNotification method
*   01/02/18    Kevin Johnson DCS       Updated emailNotification method to process PWBT
*   05/07/16    Kevin Johnson DCS       Deprecated updateParentUploadStatus method and moved to ContentFileHelper class
*   08/13/18    Kevin Johnson DCS       Updated where emailBody is built - build respective tempalte per object
*   08/30/18    Kevin Johnson DCS       Updated shareFileOpptyTeamMembers condition to process relevant object prefix
*	05/01/19	Kevin Johnson DCS		Updated emailNotification with condition segmenting file insert/update ensuring appropriate email is sent
*   =============================================================================
*/

public without sharing class ContentDocumentLinkUtility {
    /*
    *   ContentVersion triggers fire once as one version of a record is inserted whereas triggers on 
    *   ContentDocumentLink fire multiple times being multiple records reflecting 'n' number of 
    *   users shared with File are inserted to reflect specific users ContentDocumentLink share properties
    */

    //flag enabling when file validation should fire - only fire when files are uploaded not when files are shared to users
    public static boolean triggerFileValidations = true;

    public static String validateFileParameters(List<ContentDocumentLink> lstCDL){
        /*
        *{purpose}      Manages validation of Content Files being added at the ContentDocumentLink object
        *
        *{function}     Builds list of related Project Checklist Items (PCLI) with related Content File data so to use in validating attributes of uploaded File.  
        *               If a File is being uploaded (inserted new) that does not meet PCLI paramaters an error is passed notifying user of flagged validation.
        *               
        *{trigger}      ContentVersionEventListener: after insert
        */

        Set<Id> setCDids = new Set<Id>();
        Map<String, String> mapCV = new Map<String, String>();      

        for(ContentDocumentLink oCDL : lstCDL){
            setCDids.add(oCDL.ContentDocumentId);
        }
    
        //evaluate file extension at this point to ensure accurate results
        for(ContentVersion oCV : [SELECT ContentDocumentId
                                       , PathOnClient
                                  FROM ContentVersion
                                  WHERE ContentDocumentId IN :setCDids]){
            String strExtension = oCV.PathOnClient;
            strExtension = strExtension.substringAfterLast('.');
            mapCV.put(oCV.ContentDocumentId, strExtension);
        }
        String errorMsg = null;
        system.debug('TRIGGERFILEVALIDATIONS: ' + triggerFileValidations);
        if(triggerFileValidations == true){  //fire validations on uploaded file
            system.debug('***QUERY CHECK BEFORE validateContentFile: ' + limits.getQueries());
            errorMsg = ContentFileHelper.validateContentFile(setCDids, mapCV);
        }
        return errorMsg;            
    }
    
    public static string shareFileOpptyTeamMembers(List<ContentDocumentLink> lstCDL, String callingTrigger, String function){
        /*
        *{purpose}      Manages Project team members File sharing enabling collaboration rights on uploaded File for team members
        *               Manages update of File parent record's field 'Upload Status' = Uploaded upon insert of File
        *               -Upload Status function placed here because we are ensured that: 
        *                   1) File was successfully uploaded 
        *                   2) File parent record id can be obtained
        *
        *{function}     Determines Files parent Opportunity (Project) then queries Opportunites Team members.  Returned team members added to
        *               the ContentDocumentLink object enabling collaboration rights for the uploaded file.
        *               If method triggered other than ContentDocumentLink lstCDL is passed containing file share records ready for insert
        *               If method triggered by ContentDocumentLink process lstCDL to stage file share records to be inserted
        *               -breaking lstCDL record staging out in this fashion eliminates chance for recursion with ContentDocumentLink after insert trigger
        *               
        *{assumptions}  -Files parent object has formula field pulling parent opportunity id (API name = Opportuntiy_ID__c)
        *                
        *{trigger}      OpportunityTeamMemberEventListener: after insert (adding team members that need to be mapped to files)
        *               ContentDocumentLinkEventListener: after insert (adding files that need to be mapped to team members)
        */

        List<ContentDocumentLink> lstCDLShareToInsert = new List<ContentDocumentLink>();
        List<ContentDocumentLink> lstCDLShareToDelete = new List<ContentDocumentLink>();
        List<SObject> lstParentToUpdate = new List<SObject>();
        
        //add passed staged cdl records to list if cdl not passed from contentdocumentlink trigger
        //-lstCDL from non cdl trigger indicates a file has not been uploaded and only file sharing needs to be set and proceed to insert
        if(callingTrigger != 'contentdocumentlink' && function == 'insert'){
            system.debug('OTM TRIGGER CALLED');
            triggerFileValidations = false;  //do not fire file validation being users are being shared to existing file
            lstCDLShareToInsert.addAll(lstCDL);
        }
        if(callingTrigger != 'contentdocumentlink' && function == 'delete'){
            lstCDLShareToDelete.addAll(lstCDL);
        }

        String errorMsg = null;

        if(callingTrigger == 'contentdocumentlink'){
            system.debug('CDL TRIGGER CALLED');
            //process cdl if lstCDL is passed from cdl trigger 
            //-lstCDL from cdl trigger indicates a file has been uploaded and sharing needs to be determined and set
            //build list of parent objects that require file sharing
            List<File_Sharing_Settings_Objects__mdt> csSharingRules = [SELECT MasterLabel
                                                                            , Object_Prefix__c
                                                                            , Fields_To_Query__c
                                                                            , Parent_Lookup__c
                                                                            , No_Nelationship_To_Project__c
                                                                       FROM File_Sharing_Settings_Objects__mdt];                                                

            //create id set used for soql queries: contentdocument id, linkedentity id
            Set<ID> setCDid = new Set<ID>();
            Set<String> setLEidPrefix = new Set<String>();                
            for(ContentDocumentLink oCDL : lstCDL){
                setCDid.add(oCDL.ContentDocumentId);
                String strLEid = oCDL.LinkedEntityId;
                String objPrefix = strLEid.left(3);
                setLEidPrefix.add(objPrefix);
            }
            system.debug('SETCDID OTM: ' + setCDid);

            //fetch contentdocument records associated to passed contentdocumentlink records
            List<ContentDocument> lstCDtoProcess = new List<ContentDocument>();
            for(ContentDocument cd : [SELECT Id
                                           , OwnerId
                                      FROM ContentDocument
                                      WHERE Id IN :setCDid]){
                lstCDtoProcess.add(cd);
            }
            system.debug('LSTCDTOPROCESS OTM: ' + lstCDtoProcess);

            //fetch contentdocumentlink records associated to uploaded file
            Map<ID, ID> mapLEIdCDLId = new Map<ID, ID>();
            Set<ID> setLEidObj = new Set<ID>();
            for(ContentDocumentLink oCDL : [SELECT ContentDocumentId
                                                 , LinkedEntityId
                                            FROM ContentDocumentLink
                                            WHERE ContentDocumentId IN :setCDid]){
                setLEidObj.add(oCDL.LinkedEntityId);
                mapLEIdCDLId.put(oCDL.LinkedEntityId, oCDL.ContentDocumentId);      
            }
            system.debug('SETLEDIDS OTM: ' + setLEidObj);

            //fetch related opportunities (projects)
            Set<ID> setOpptyId = new Set<ID>();
            Map<ID, ID> mapOpptyIdCDLId = new Map<ID, ID>();            
            for(File_Sharing_Settings_Objects__mdt fss : csSharingRules){
                //NOTE: soql query in for loop - object prefix condition prevents query from running more than once
                if(setLEidPrefix.contains(fss.Object_Prefix__c) && fss.No_Nelationship_To_Project__c == FALSE){ //process only if object prefix ids match and object related to project
                    String obj = fss.MasterLabel;
                    String fields = fss.Fields_To_Query__c;
                    String query = 'SELECT ' + fields +
                                   ' FROM ' + obj +
                                   ' WHERE Id IN :setLEidObj';
                    system.debug('QUERY OTM: ' + query);               
                    List<SObject> lstParent = Database.query(query);
                    system.debug('LSTPARENT OTM: ' + lstParent);

                    for(SObject objParent : lstParent){
                        setOpptyId.add((String) objParent.get('Opportunity_ID__c'));
                        mapOpptyIdCDLId.put((String) objParent.get('Opportunity_ID__c'), mapLEIdCDLId.get((String) objParent.get('Id')));
                    }
                }
                system.debug('SETOPPTYID OTM: ' + setOpptyId);
            }

            //fetch opportunities opportunity team members
            //internal user cannot share file with external user thus filter out external users who are opportunity team members
            //external user will have access to file being they are shared with the files parent
            List<OpportunityTeamMember> lstOTM = new List<OpportunityTeamMember>();
            for(OpportunityTeamMember otm : [SELECT OpportunityId
                                                  , UserId
                                                 FROM OpportunityTeamMember 
                                                 WHERE OpportunityId = :setOpptyId
                                                 AND User_Type__c = 'Internal Portal User']){
                lstOTM.add(otm);
            }
            system.debug('OTM TO PROCESS FOR FILE SHARE: ' + lstOTM);
            //build list of contentdocumentlink records to insert for team member           
            for(ContentDocument cd : lstCDtoProcess){
                system.debug('CD OTM: ' + cd);
                for(OpportunityTeamMember otm : lstOtm){  //loop through opportunity team members
                    if(otm.UserId != cd.OwnerId){  //do not create share for cd owned by otm
                        ContentDocumentLink cdlShare = new ContentDocumentLink(LinkedEntityID = otm.UserId                                                     
                                                                             , ContentDocumentId = mapOpptyIdCDLId.get(otm.OpportunityId)
                                                                             , ShareType = 'C');
                        lstCDLShareToInsert.add(cdlShare);
                    }
                }
            }
        }       
        system.debug('LSTSHARETOINSERT CDL: ' + lstCDLShareToInsert);
        //insert contentdocumentlink share records
        if(!lstCDLShareToInsert.isEmpty()){
            Database.SaveResult[] insResult = Database.insert(lstCDLShareToInsert, false);
            
            for(Database.SaveResult sr : insResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error sharing File: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        system.debug('LSTSHARETODELETE CDL: ' + lstCDLShareToDelete);
        //delete contentdocumentlink share records
        if(!lstCDLShareToDelete.isEmpty()){
            Database.DeleteResult[] delResult = Database.delete(lstCDLShareToDelete, false);
            
            for(Database.DeleteResult dr : delResult){
                if(!dr.isSuccess()){
                    for(Database.Error err : dr.getErrors()){
                        errorMsg = 'Error deleting file share: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
    }

    public static string shareFileNonOpptyTeamMembers(List<ContentDocumentLink> lstCDL){
        /*
        *{purpose}      Manages designated user share for an inserted file
        *
        *{function}     Need to expiclitly share file to users in order to 'see' files via the API.  References custom setting detailing 
        *               users to share a newly inserted file too.  Based on custom setting a contentdocumentlink share record is inserted
        *               per content document record.
        *               
        *               
        *{trigger}      ContentVersionEventListener: after insert
        */

        //create id set used for soql queries: contentdocument id
        Set<ID> setCDid = new Set<ID>();        
        for(ContentDocumentLink oCDL : lstCDL){
            setCDid.add(oCDL.ContentDocumentId);
        }
        system.debug('SET CDID: ' + setCDid);
        List<File_Sharing_Settings_Users__mdt> fileShareSetting = [SELECT MasterLabel
                                                                        , LinkedEntityId__c
                                                                        , ShareType__c
                                                                        , Visibility__c
                                                                   FROM File_Sharing_Settings_Users__mdt];

        //create id sets used for soql queries: obtain neeeded linkedentityid id
        Set<ID> setLEIDUser = new Set<ID>();            
        for(File_Sharing_Settings_Users__mdt fss : fileShareSetting){
            setLEIDUser.add(fss.LinkedEntityId__c);
        }
        system.debug('SET LEIDID: ' + setLEIDUser);
        //create map of existing contactdocumentlink records specific to contentversion records being acted upon
        //reference map so as to ensure share being inserted does not already exist
        Map<Id, Id> mapExistingCDL = new Map<Id, Id>();
        for(ContentDocumentLink oCDL : [SELECT ContentDocumentId
                                             , LinkedEntityId
                                        FROM ContentDocumentLink
                                        WHERE ContentDocumentId IN :setCDid
                                        AND LinkedEntityId IN :setLEIDUser]){
            system.debug('OCDL: ' + oCDL);
            mapExistingCDL.put(oCDL.ContentDocumentId, oCDL.LinkedEntityId);
        }
        system.debug('MAP EXISTING CDL: ' + mapExistingCDL);
        //build list of contentdocumentlink records to insert for user if user does not exist in contentdocumentlink object
        List<ContentDocumentLink> lstCDLShareToInsert = new List<ContentDocumentLink>();
        String errorMsg = null;
        for(Id cdid : setCDid){
            if(!mapExistingCDL.containsKey(cdid)){
                for(File_Sharing_Settings_Users__mdt oFSS : fileShareSetting){
                    if(oFSS.MasterLabel != 'Multifamily Customer Portal'){  //do not insert community share/share file with all external users
                        ContentDocumentLink cdlShare = new ContentDocumentLink(LinkedEntityID = oFSS.LinkedEntityId__c
                                                                             , ContentDocumentId = cdid                                                                                           
                                                                             , ShareType = oFSS.ShareType__c
                                                                             , Visibility = oFSS.Visibility__c);
                    system.debug('CDLSHARE TO INSERT: ' + cdlShare);
                    lstCDLShareToInsert.add(cdlShare);
                    }
                }
            }
        }
        system.debug('LSTSHARETOINSERT NOTM: ' + lstCDLShareToInsert);
        if(!lstCDLShareToInsert.isEmpty()){
            Database.SaveResult[] insResult = Database.insert(lstCDLShareToInsert, false);
            
            for(Database.SaveResult sr : insResult){
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()){
                        errorMsg = 'Error sharing File: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;
    }

    public static string deleteRelatedContentFiles(List<ContentDocumentLink> lstCDL){
        /*
        *{purpose}      Manages deletion of related ContentDocument records unshared from parent object
        *               NOTE:   If File is deleted from an object related list the ContentDocument file is UNSHARED from parent object NOT deleted 
        *                       If File is deleted from file mgmt interface ContnetDocument file is deleted 
        *                       Thus, only process LinkedEntityID equal to parent object being intent is to delete the related ContentDocument file
        *                       from the parent object not unshare it.  ContentDocuments linked to LinkedEntityID equal to user are not processed being 
        *                       the related ContentDocument file should not be deleted being the intent to unshare the ContentDocument from the user should unshare
        *                       the ContentDocument not delete it.
        *               
        *{function}     Builds list of related ContentDocument records related to the  CLIP.  
        *               Resulting list of ContentDocument records are deleted along (via cascade delete) with related ContentDocumentLink (user) and ContentVersion
        *               records.
        *               
        *{trigger}      ContentDocumentLinkEventListener: after delete
        */

        //only process lstCDL ContentDocumentLink records where LinkedEntityId != User
        Set<ID> setCDid = new Set<ID>();
        String errorMsg = null;
        
        //create id sets used for soql queries
        for(ContentDocumentLink cdl : lstCDL){
            setCDid.add(cdl.ContentDocumentID);
        }

        //purge contentdocument records
        List<ContentDocument> lstCDToBeDeleted = [SELECT Id
                                                  FROM ContentDocument
                                                  WHERE Id IN :setCDid];

        //related content file records (contentdocumentlink, contentversion) are cascade deleted upon deletion of contentdocument
        system.debug('LSTCDTOBEDELETED: ' + lstCDToBeDeleted);
        if(!lstCDToBeDeleted.isEmpty()){
            Database.DeleteResult[] delResult = Database.delete(lstCDToBeDeleted, false);
            
            for(Database.DeleteResult dr : delResult){
                if(!dr.isSuccess()){
                    for(Database.Error err : dr.getErrors()){
                        errorMsg = 'File not deleted: ' + err.getStatusCode() + ' - ' + err.getMessage();
                    }
                }
            }
        }
        return errorMsg;        
    }

public static string emailNotification(List<ContentDocumentLink> lstCDL, String callingTrigger){
        /*
        *{purpose}      Manages email notifications triggered File inserts/version updates.  Code needed due to no declarative framework
        *               exists to trigger email notifications off of the File nomenclature in addition to need for dynamic email addressing
        *               across objects.
        *
        *{function}     1) Determine object to process
        *                   -ContentDocumentLink record passed to function.  Object prefix is used to segment shared object for processing.
        *               2) Determine ContentVersion record to process
        *                   -Obtain File that was inserted/updated          
        *               3) Fetch object specific attributes
        *               4) a)Determine if object requires email notification and process object
        *                   -Validate that passed object requires File upload/update notification
        *                  b) Process object
        *                   -Build object specific addressee list
        *               5) Build Project Team Member addressee list
        *               6) Build and send email
        *   
        *{assumptions}  -Mass upload of files will not occur only individual uploads/updates are passed to method - bulkification not required
        *               -Need to update code when new objects are to be added to File insert/update notification process
        *               -One standard notification message is needed across objects
        *               
        *{trigger}      ContentDocumentLinkEventListener: File inserts
        *               ContentVersionEventListener: File version updates 
        */
        system.debug('EMAIL CLASS CALLING FUNCTION:' + callingTrigger);

        Set<ID> setContDocId = new Set<ID>();
        Set<ID> setLEidObj = new Set<ID>();
        ID projId = null;
        List<String> userEmail = new List<String>();
        String objectName = null;
        List<ContentVersion> lstCV = new List<ContentVersion>();
        List<Project_Checklist_Item__c> lstPCLI = new List<Project_Checklist_Item__c>();
        List<Project_Workbook_Version__c> lstPWBT = new List<Project_Workbook_Version__c>();
        Set<String> setRoles = new Set<String>();
        String projectName = null;
        String projectNumber = null;
		String projectDNumber = null;
        String parentID = null;
        String parentName = null;
        String parentParentsName = null;
        String parentLabel = null;
        String parentParentsLabel = null;
        String parentStatus = null;
        String errorMsg = null;
        String emailBody = null;

        //FETCH ORG URLS====================================================================================================
        //assumes custom setting is configured to represent internal/external urls for org process is being run in
        //being emails can be sent to both internal and external users we need to capture both unique url's to ensure
        //links function respective to the user
        List<Org_URLs__c> csORGurls = [SELECT External_URL__c
                                            , Internal_URL__c
                                       FROM Org_URLs__c];                              

        //BUILD EMAILBODY GLOBAL VARIABLES==================================================================================
        String fileOperation = null;
        if(callingTrigger == 'ContentVersion'){
            fileOperation = 'Updated';
        } else if(callingTrigger == 'ContentDocumentLink'){
            fileOperation = 'Uploaded';
        }

        //format urls for both internal and external users
        String sfdcURLinternal = csORGurls[0].Internal_URL__c;
        String sfdcURLexternal = csORGurls[0].External_URL__c;      
        
        //DETERMINE OBJECT TO PROCESS (add objects which need file notifications as they are needed)========================
        for(ContentDocumentLink cdl : lstCDL){
            String leidID = cdl.LinkedEntityID;
            if(leidID.left(3).equals('a0J')){  //prefiex for PCLI object (.equals() performs case sensitive comparison)
                objectName = 'Project Checklist Item';
                setLEidObj.add(cdl.LinkedEntityID);
                setContDocId.add(cdl.ContentDocumentID); 
            }
            if(leidID.left(3).equals('a0L')){  //prefiex for Project Workbook Type (Version) object  (.equals() performs case sensitive comparison)
                objectName = 'Project Workbook Type';
                setLEidObj.add(cdl.LinkedEntityID);
                setContDocId.add(cdl.ContentDocumentID); 
            }
            //...add other objects to analyze as needed
        }
        //system.debug('EMAIL CLASS SETLEIDOBJ:' + setLEidObj);

        //DETERMINE CONTENTVERSION RECORD TO PROCESS (fetch latest version of uploaded file)================================
        if(!setContDocId.isEmpty()){
            lstCV = [SELECT ContentDocumentID
                          , Title
                          , VersionNumber
                    FROM ContentVersion
                    WHERE ContentDocumentID IN :setContDocId
                    AND IsLatest = TRUE];
            //system.debug('EMAIL CLASS LSTCV:' + lstCV);       
        }

        //on insert of File in LEX triggers at both contentdocumentlink and contentversion fire calling email method twice resulting in
        //both insert and update emails being sent. In Classic only contentdocumetlink was fired resulting in email method being called once
        //resulting in only insert email being sent.  Added following if statement determining state of File ensuring correct email being sent.
        if(lstCV.size() > 0 && 
			(lstCV[0].VersionNumber == '1' && fileOperation == 'Uploaded' || //cdl trigger fires upon insert
             lstCV[0].VersionNumber != '1' && fileOperation == 'Updated')){  //cv trigger fires upon insert/update (in LEX)
            //FETCH OBJECT SPECIFIC ATTRIBUTES==============================================================================
            if(objectName == 'Project Checklist Item'){
                //fetch PCLI specific attributes
                lstPCLI = [SELECT ID
                                 , Name
                                 , Owner.Email
                                 , Opportunity_ID__c
                                 , Project__r.Name
                                 , Project__r.Project_Number__c
								 , Project__r.D_Number__c
                                 , Approval_Status__c
                                 , Approval_First__r.Email
                                 , Approval_Second__r.Email
                                 , Approval_Third__r.Email
                                 , Funding_Round_Phase_Checklist_Item__r.Phase_Checklist__r.Send_Upload_Notification__c
                                 , Funding_Round_Phase_Checklist_Item__r.Phase_Checklist__r.Funding_Round__r.Roles_To_Notify__c
                           FROM Project_Checklist_Item__c
                           WHERE Id IN :setLEidObj];              
                system.debug('EMAIL CLASS LSTPCLI:' + lstPCLI);

                //system.debug('EMAIL CLASS LSTPCLI:' + lstPCLI);
                projectName = lstPCLI[0].Project__r.Name;
                projectNumber = lstPCLI[0].Project__r.Project_Number__c;
				projectDNumber = lstPCLI[0].Project__r.D_Number__c;
                parentID = lstPCLI[0].Id;
                parentName = lstPCLI[0].Name;            
                parentLabel = 'Project Checklist Item';               
                parentStatus = lstPCLI[0].Approval_Status__c;
                projId = lstPCLI[0].Opportunity_ID__c;

                //build objects addressee list
                if(lstPCLI[0].Funding_Round_Phase_Checklist_Item__r.Phase_Checklist__r.Send_Upload_Notification__c == TRUE){
                    //fetch & populate pclip approver email addresses
                    for(Project_Checklist_Item__c pcli : lstPCLI){
                        if(pcli.Approval_First__r.Email != null){
                            userEmail.add(pcli.Approval_First__r.Email);
                        }
                        if(pcli.Approval_Second__r.Email != null){
                            userEmail.add(pcli.Approval_Second__r.Email);
                        }
                        if(pcli.Approval_Third__r.Email != null){
                            userEmail.add(pcli.Approval_Third__r.Email);
                        }               
                    }
                    system.debug('EMAIL CLASS USEREMAIL APPROVERS:' + userEmail);

                    //determine email addressees via project team roles designated at related funding round
                    system.debug('EMAIL CLASS ROLES TO NOTIFY (FR):' + lstPCLI[0].Funding_Round_Phase_Checklist_Item__r.Phase_Checklist__r.Funding_Round__r.Roles_To_Notify__c);
                    if(lstPCLI[0].Funding_Round_Phase_Checklist_Item__r.Phase_Checklist__r.Funding_Round__r.Roles_To_Notify__c != null){
                        String[] splitrole = lstPCLI[0].Funding_Round_Phase_Checklist_Item__r.Phase_Checklist__r.Funding_Round__r.Roles_To_Notify__c.split(';');             
                            for(String r : splitrole){
                                setRoles.add(r);
                            }
                    }
                    system.debug('EMAIL CLASS EMAIL ROLES:' + setRoles);
                }

                //set status upon initial upload.  done here being trigger/apex fires before workflow update to pcli
                if(parentStatus == 'Unsubmitted'){ //value at run time - not value to be captured in subsequent email
                    parentStatus = 'Ready for Review';
                }

                //build objects email template
                emailBody = '<b>File ' + fileOperation + '</b>: <a href=' + sfdcURLinternal + parentID +'>'+ lstCV[0].Title +'</a><br/>';
                emailBody += '<b>Project</b>: ' + projectName + '<br/>';
				emailBody += '<b>D Number</b>: ' + projectDNumber + '<br/>';
                emailBody += '<b>Project Number</b>: ' + projectNumber + '<br/>';
                emailBody += '<b>' + parentLabel + '</b>: ' + parentName + '<br/>';
                emailBody += '<b>Status</b>: ' + parentStatus + '<br/>';
            }
         
            if(objectName == 'Project Workbook Type'){
                //fetch PWBT specific attributes
                lstPWBT = [SELECT ID
                                 , Project_Workbook__r.Name
                                 , Name
                                 , Project_Workbook__r.Workbook_Level__c
                                 , Project_Number__c
                                 , Opportunity_ID__c
                                 , Project_Name__c
                                 , Workbook_Version__c
                           FROM Project_Workbook_Version__c
                           WHERE Id IN :setLEidObj];              
        
                //system.debug('EMAIL CLASS LSTPCLIP:' + lstPCLIP);
                projectName = lstPWBT[0].Project_Name__c;
                projectNumber = lstPWBT[0].Project_Number__c;
                parentID = lstPWBT[0].Id;
                parentName = lstPWBT[0].Name;
                parentLabel = 'Project Workbook Type';
                projId = lstPWBT[0].Opportunity_ID__c;

                //build objects addressee list
                userEmail.add('mhfa.app@state.mn.us');          
            
                //build objects email template
                emailBody = '<b>File ' + fileOperation + '</b>: <a href=' + sfdcURLinternal + parentID +'>'+ lstCV[0].Title +'</a><br/>';
                emailBody += '<b>Project</b>: ' + projectName + '<br/>';
                emailBody += '<b>Project Number</b>: ' + projectNumber + '<br/>';
                emailBody += '<b>' + parentLabel + '</b>: ' + parentName + '<br/>';
            }
  
            //BUILD PROJECT TEAM MEMBER ADDRESSEE LIST===========================================================================
            if(setRoles != null){
                //system.debug('EMAIL CLASS PROJID:' + projId);
                //fetch & populate team member email addresses
                for(OpportunityTeamMember otm : [SELECT OpportunityId
                                                      , User.Email
                                                      , Opportunity.Name
                                                FROM OpportunityTeamMember
                                                WHERE TeamMemberRole IN: setRoles
                                                AND OpportunityId = :projId]){
                    //system.debug('EMAIL CLASS OTM LOOP:' + otm);                
                    userEmail.add(otm.User.Email);
                }
                system.debug('EMAIL CLASS USEREMAIL OTM:' + userEmail);
            }

            //BUILD AND SEND MESSAGE=============================================================================================
            if(!userEmail.isEmpty()){
                //target object user: user should always exist as it is the owner of Multifamily records (Multifamily Accounts and Contacts for example)
                User userTarget = [SELECT Id from User WHERE LastName = 'Multifamily'];
                String imageURL = 'https://mnhousing--c.documentforce.com/servlet/servlet.ImageServer?id=01537000000ECUN&oid=00D37000000Hyfe&lastMod=1504619179000';

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(userTarget.Id);
                    mail.setTreatTargetObjectAsRecipient(false);
                    mail.setSubject('File ' + fileOperation + ': ' + projectName + '-' + parentName);               
                    mail.setToAddresses(userEmail);
                    mail.setHTMLBody('<table><tr><img align="left" alt="MHFA Logo" width="80" height="80" src="' + imageURL + '"/></tr><tr></tr><tr></tr><tr></tr><tr><p style="font-family:Arial;font-size:16px;">' + emailBody + '</p></tr></table>');
                    mail.saveAsActivity = FALSE;
                    mail.setOrgWideEmailAddressId('0D237000000TNgm');  //'minnesota housing' org wide address 
                    system.debug('EMAIL CLASS EMAIL:' + mail);
                                
                Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                if(result[0].IsSuccess() == false){
                    errorMsg = 'The email failed to send: ' + result[0].errors[0].message;
                }
            }
        }
        return errorMsg;
    }
}