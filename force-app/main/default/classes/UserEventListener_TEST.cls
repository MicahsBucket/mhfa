@isTest
private class UserEventListener_TEST  {
	@isTest static void testUserEventListener() {
        //stage custom settings
        List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
                                                 FROM Enable_Triggers__mdt
                                                 WHERE MasterLabel = 'User'];

        //run tests
        Test.startTest();
            //create user
			User u = TestDataUtility.createUser('Multifamily Standard User',
												'test@mhfatest.com');
        Test.stopTest();
    }
}