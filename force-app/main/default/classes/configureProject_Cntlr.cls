public without sharing class configureProject_Cntlr  {

	//wrapper classes
	public class groupCharacteristics {
		//properties
		@AuraEnabled
		public Trigger_Group__c oGroup {get;set;}
		@AuraEnabled
		public List<Funding_Round_Trigger__c> oGrpCharacteristics {get;set;}
		@AuraEnabled
		public List<Id> selectedChars {get;set;}

		//constructor
		public groupCharacteristics() {
			this.oGroup = new Trigger_Group__c();
			this.oGrpCharacteristics = new List<Funding_Round_Trigger__c>();
			this.selectedChars = new List<Id>();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and associated characteristics for funding round
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureProject_Cntlr.groupCharacteristics> getAvailableCharacteristics(Id frId, Id projectId, String selectionType) {

		List<configureProject_Cntlr.groupCharacteristics> retVal = new List<configureProject_Cntlr.groupCharacteristics>();
		Map<String, Set<Id>> mapGrpSelectedValues = new Map<String, Set<Id>>();
		Set<Id> setAvailableValues = new Set<Id>();
		Set<Id> setSelectedValues = new Set<Id>();
		System.debug('frId: ' + frId + ' projectId: ' + projectId + ' selectionType: ' + selectionType);
		for (Funding_Round_Trigger__c oFRT : [Select Id
													, Name
													, Trigger_Group__c
													, Trigger_Group__r.Name
													, Trigger_Group__r.Selection_Type__c
													, (Select Id From Funding_Round_Task_Characteristics__r)
													, (Select Id From Funding_Round_Checklist_Item_Triggers__r)
												From Funding_Round_Trigger__c
												Where Trigger_Group__r.Funding_Round__c = :frId]) {
			System.debug('Task Char Count: ' + oFRT.Funding_Round_Task_Characteristics__r.size() 
							+ '; CLI Char Count: ' + oFRT.Funding_Round_Checklist_Item_Triggers__r.size());
			//only display a characteristic if it is associated to a task or checklist item
			if (oFRT.Funding_Round_Task_Characteristics__r.size() > 0
					|| oFRT.Funding_Round_Checklist_Item_Triggers__r.size() > 0) {
				System.debug('Available: ' + oFRT.Trigger_Group__r.Name + ' - ' + oFRT.Name);
				setAvailableValues.add(oFRT.Id);
			} else {
				System.debug('Not Available: ' + oFRT.Trigger_Group__r.Name + ' - ' + oFRT.Name);
			}

		}
		System.debug('setAvailableValues: ' + setAvailableValues);
		//locate characteristics already associated to the given project
		for (Project_Characteristic__c oPC : [Select Funding_Round_Characteristic__c 
												From Project_Characteristic__c 
												Where Project__c = :projectId]) {
			setSelectedValues.add(oPC.Funding_Round_Characteristic__c);
		}

		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
                                               	Where Id in :setAvailableValues
													And Id not in :setSelectedValues
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			System.debug('oGrp: ' + oGrp);
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureProject_Cntlr.groupCharacteristics oGrpChars = new configureProject_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				if (mapGrpSelectedValues.containsKey(oGrp.Id)) {
					oGrpChars.selectedChars.addAll(mapGrpSelectedValues.get(oGrp.Id));
				}
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getGroupings.\n' + retVal);
		return retVal;
	}
    
    //////////////////////////////////////////////////////////////////////////////////////////////
	//retrieve the characteristic groups and characteristics already associated to the given item
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static List<configureProject_Cntlr.groupCharacteristics> getAssignedCharacteristics(Id frId, Id projectId, String selectionType) {

		List<configureProject_Cntlr.groupCharacteristics> retVal = new List<configureProject_Cntlr.groupCharacteristics>();
		Set<Id> setSelectedGroups = new Set<Id>();
		Set<Id> setSelectedTriggers = new Set<Id>();
        
		for (Project_Characteristic__c oPC : [Select Id
													, Funding_Round_Characteristic__c
													, Funding_Round_Characteristic__r.Trigger_Group__c
													, Funding_Round_Characteristic__r.Trigger_Group__r.Name
												From Project_Characteristic__c 
												Where Project__c = :projectId]) {
			setSelectedGroups.add(oPC.Funding_Round_Characteristic__r.Trigger_Group__c);
			setSelectedTriggers.add(oPC.Funding_Round_Characteristic__c);
		}

		for (Trigger_Group__c oGrp : [Select Id
											, Name
											, Description__c
											, Sort_Order__c
											, Characteristic_Count__c
											, (Select Id
														, Name
                                               			, Trigger__r.Help_Text__c
														, Required_for_All__c
														, Sort_Order__c
												From Funding_Round_Triggers__r
                                               	Where Id in :setSelectedTriggers
												Order By Sort_Order__c)
										From Trigger_Group__c
										Where Funding_Round__c = :frId
                                      		And Id in :setSelectedGroups
											And Active__c = true
											And Selection_Type__c = :selectionType
										Order By Sort_Order__c]) {
			if (oGrp.Funding_Round_Triggers__r.size() > 0) {
				configureProject_Cntlr.groupCharacteristics oGrpChars = new configureProject_Cntlr.groupCharacteristics();
				oGrpChars.oGroup = oGrp;
				oGrpChars.oGrpCharacteristics = oGrp.Funding_Round_Triggers__r;
				retVal.add(oGrpChars);
			}
		}

		System.debug('Inside getAssigned.\n' + retVal);
		return retVal;
	}      

	//////////////////////////////////////////////////////////////////////////////////////////////
	// insert project characteristics for the given funding round project
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
	public static void insertProjectCharacteristics(Id projectId, List<String> lstCharIds, Boolean blnClearExisting) {
		System.debug('Inside insertProjectCharacteristics'
						+ '\nprojectId: ' + projectId
						+ '\nlstCharIds: ' + lstCharIds
						+ '\nblnClearExisting: '+ blnClearExisting);

		//delete existing values for the given task if told to do so
		if (blnClearExisting == true) {
			List<Project_Characteristic__c> delRecs = new List<Project_Characteristic__c>();
			for (Project_Characteristic__c oPC :  [Select Id 
                                                    From Project_Characteristic__c 
                                                    Where Project__c = :projectId]) {
				delRecs.add(oPC);
			}
			if (delRecs.size() > 0) {
				System.debug('Deleting ' + delRecs);
				delete delRecs;
			}
		}

		//insert the selected items
		List<Project_Characteristic__c> lstInsertRecs = new List<Project_Characteristic__c>();
		List<Id> lstIds = new List<Id>();
		//the provided list of ids might have commas, so we need to split those entries to get the individual ids
		System.debug('lstCharIds.size: ' + lstCharIds.size());
		for (Integer i=0; i<lstCharIds.size(); i++) {
			System.debug('sItems: ' + lstCharIds[i]);
			lstIds.add((Id)lstCharIds[i]);
		}
		System.debug('lstIds: ' + lstIds);

		//retrieve selection type from characteristic group
		Map<Id, String> mapCharSelectionType = new Map<Id, String>();
		for (Funding_Round_Trigger__c oCharacteristic : [Select Id, Trigger_Group__r.Selection_Type__c
															From Funding_Round_Trigger__c
															Where Id in :lstIds]) {
			mapCharSelectionType.put(oCharacteristic.Id, oCharacteristic.Trigger_Group__r.Selection_Type__c);
		}

		for (Id charId : lstIds) {
			lstInsertRecs.add(new Project_Characteristic__c(
										Project__c = projectId
										, Funding_Round_Characteristic__c = charId
										, Selection_Type__c = mapCharSelectionType.get(charId)));
		}
		if (lstInsertRecs.size() > 0) {
			System.debug('Inserting ' + lstInsertRecs);
			insert lstInsertRecs;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// delete characteristics
	//////////////////////////////////////////////////////////////////////////////////////////////
	@AuraEnabled
    public static void removeProjectCharacteristics(Id projectId, List<String> lstCharIds) {
		//delete given values
		System.debug('Project Id: ' + projectId + '\nLocate characteristics: ' + lstCharIds);
        List<Project_Characteristic__c> delRecs = new List<Project_Characteristic__c>();
        for (Project_Characteristic__c oPC :  [Select Id 
                                                From Project_Characteristic__c 
                                                Where Project__c = :projectId
                                                And Funding_Round_Characteristic__c in :lstCharIds]) {
        	delRecs.add(oPC);
        }
        if (delRecs.size() > 0) {
            System.debug('Deleting ' + delRecs);
            delete delRecs;
        }
    }

}