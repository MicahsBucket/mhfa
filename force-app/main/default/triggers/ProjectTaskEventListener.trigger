trigger ProjectTaskEventListener on Project_Task__c (before insert, before update, after insert, after update)  { 

	//trigger switch custom setting enabling/disabling trigger
	Boolean runThisTrigger = false;
	for (Enable_Triggers__mdt oRunTrigger : [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'ProjectTask']) {
		runThisTrigger = oRunTrigger.Enable_Trigger__c;
	}

	if (!runThisTrigger) {
		return;
	}
	//List<Project_Task__c> lstProjectTasksToProcess = new List<Project_Task__c>();

	if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
		/*
		for(Project_Task__c oPT : Trigger.new){
            //do not process final tasks
            if(oPT.Final_Stage_Task__c == FALSE){
                lstProjectTasksToProcess.add(oPT);
            }
		}	
		*/	
		//if(lstProjectTasksToProcess.size() > 0){
			ProjectTaskUtility.assignDueDate(trigger.new, trigger.newMap, trigger.oldMap);
			ProjectTaskUtility.assignTaskOwner(trigger.new);        
			//ProjectTaskUtility.assignDueDate(lstProjectTasksToProcess, trigger.oldMap);
		//}		
	}

	//if a final stage task is marked 'Complete', mark the stage Complete.  This step will fire a trigger
	// to set the current phase and stage fields on the project
	if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) {
		ProjectTaskUtility.completeProjectStage(trigger.new, trigger.oldMap);
	}
}