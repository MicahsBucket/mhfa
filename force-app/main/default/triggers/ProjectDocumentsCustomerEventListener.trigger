trigger ProjectDocumentsCustomerEventListener on Project_Documents_Customer__c (before insert, before update)  {
	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'ProjectDocumentsCustomer'];

  	//before insert methods
  	if(Trigger.isBefore && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			//run validation for inserted records			
			String validationErrorRecord = ProjectDocumentCustomerUtility.validateRecordParameters(Trigger.new);
			if(validationErrorRecord != null){
				trigger.new[0].addError(validationErrorRecord);
			}	
  		}
  	}

	//before update methods
  	if(Trigger.isBefore && Trigger.isUpdate){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			//run validation for updated records
			String validationErrorRecord = ProjectDocumentCustomerUtility.validateRecordParameters(Trigger.new);
			if(validationErrorRecord != null){
				trigger.new[0].addError(validationErrorRecord);
			}	
  		}
	}
}