trigger FundingRoundTaskCharEventListener on Funding_Round_Task_Characteristic__c (after insert, after update, after delete)  {
	
	if (Trigger.isAfter && Trigger.isInsert) {
		System.debug('Calling assignChildTaskCharacteristics with ' + Trigger.newMap.keySet());
		//since a future method can cause this trigger to fire, we need to ensure we don't call the future method again or else the insert fails
		if (System.isFuture()) {
			configureTask_Cntlr.assignChildTaskCharacteristics(Trigger.newMap.keySet());
		} else {
			configureTask_Cntlr.assignChildTaskCharacteristics_Future(Trigger.newMap.keySet());
		}
	}

	if (Trigger.isAfter && Trigger.isUpdate) {

	}

	if (Trigger.isAfter && Trigger.isDelete) {
		Map<Id, List<String>> mapTaskCharacteristics = new Map<Id, List<String>>();
		for (Funding_Round_Task_Characteristic__c oFRTC : Trigger.oldMap.values()) {
			List<Id> lstCharIds = new List<Id>();
			if (mapTaskCharacteristics.containsKey(oFRTC.Funding_Round_Task__c)) {
				lstCharIds.add(oFRTC.Funding_Round_Characteristic__c);
			}
			mapTaskCharacteristics.put(oFRTC.Funding_Round_Task__c, lstCharIds);
		}

		for (Id taskId : mapTaskCharacteristics.keySet()) {
			configureTask_Cntlr.removeTaskCharacteristics(taskId, mapTaskCharacteristics.get(taskId));
		}

	}


}