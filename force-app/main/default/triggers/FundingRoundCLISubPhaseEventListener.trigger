trigger FundingRoundCLISubPhaseEventListener on Funding_Round_Checklist_Item_SubPhase__c (after update)  { 
	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'FundingRoundCLISubPhase'];

	List<Funding_Round_Checklist_Item_SubPhase__c> lstFRCLISP = new List<Funding_Round_Checklist_Item_SubPhase__c>();

	//after update methods
  	if(Trigger.isAfter && Trigger.isUpdate){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			for(Funding_Round_Checklist_Item_SubPhase__c frclisp : Trigger.new){
				//process if approval role(s) changed: value or to null
				if((Trigger.newMap.get(frclisp.Id).Approval_First__c != null && 
					Trigger.oldMap.get(frclisp.Id).Approval_First__c != Trigger.newMap.get(frclisp.Id).Approval_First__c ||					
					Trigger.newMap.get(frclisp.Id).Approval_First__c == null && 
					Trigger.oldMap.get(frclisp.Id).Approval_First__c != null) ||
				  (Trigger.newMap.get(frclisp.Id).Approval_Second__c != null &&
					Trigger.oldMap.get(frclisp.Id).Approval_Second__c != Trigger.newMap.get(frclisp.Id).Approval_Second__c ||
					Trigger.newMap.get(frclisp.Id).Approval_Second__c == null &&
					Trigger.oldMap.get(frclisp.Id).Approval_Second__c != null) ||
				  (Trigger.newMap.get(frclisp.Id).Approval_Third__c != null && 
					Trigger.oldMap.get(frclisp.Id).Approval_Third__c != Trigger.newMap.get(frclisp.Id).Approval_Third__c ||
					Trigger.newMap.get(frclisp.Id).Approval_Third__c == null && 
					Trigger.oldMap.get(frclisp.Id).Approval_Third__c != null)){
					lstFRCLISP.add(frclisp);
				}
			}
		}
		if(!lstFRCLISP.isEmpty()){ //if approver roles updated then fire method
			String validationApprovalRole = FundingRoundCLISubPhaseUtility.processApprovalRoles(lstFRCLISP);
			if(validationApprovalRole != null){
				trigger.new[0].addError(validationApprovalRole);
			}
		}
	}
}