trigger ProjectNarrativeEventListener on Project_Narrative__c (after update) {
	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'ProjectNarrative'];

	//after update
	if(Trigger.isAfter && Trigger.isUpdate){
		//fire trigger if trigger switch custom setting is enabled		
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			List<Project_Narrative__c> lstProjNarrativeToProcess = new List<Project_Narrative__c>();

			for(Project_Narrative__c pn : Trigger.new){
				//check for status update
				if(trigger.oldMap.get(pn.Id).Status__c != trigger.newMap.get(pn.Id).Status__c){
					lstProjNarrativeToProcess.add(pn);
				}
			}
			if(!lstProjNarrativeToProcess.isEmpty()){
				String validationParentUpdate = ProjectNarrativeUtility.updateNarrativeParent(lstProjNarrativeToProcess);
				if(validationParentUpdate != null){
	            	trigger.new[0].addError(validationParentUpdate);
	        	}	        	
			}
		}
	}
}