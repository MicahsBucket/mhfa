trigger ContentDocumentLinkEventListener on ContentDocumentLink (before insert, after insert, before delete, after delete) {
	/*
		run file validation from contentdocumentlink being we need to ensure the contentdocumentlink record for 
		the pclip has been inserted so it can be processed
	*/

	//do not run this trigger if it was fired as a result of the purge routine for projects.  The statement below utilizes a class with a
	// static variable set in OpportunityUtility.purgeProjectRecords.  Typically this routine is called via batchPurgeProjectFiles.
    system.debug('Inside ContentDocumentLinkEventListener.  PurgePreventUpdates.prevent: ' + PurgePreventUpdates.prevent);
	if (PurgePreventUpdates.prevent) {
        return;
	}

	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
											 FROM Enable_Triggers__mdt
											 WHERE MasterLabel = 'ContentDocumentLink'];

	List<File_Sharing_Settings_Objects__mdt> csSharingRules = [SELECT Object_Prefix__c
															   FROM File_Sharing_Settings_Objects__mdt];                                                        

	Set<ID> setCDID = new Set<Id>();
	//build set containing file parent object id
	Set<String> setObjID = new Set<String>();
	for(File_Sharing_Settings_Objects__mdt fss : csSharingRules){
		setObjID.add(fss.Object_Prefix__c);
	}
	List<ContentDocumentLink> lstCDLtoProcess = new List<ContentDocumentLink>();

    //before insert methods
    if(Trigger.isBefore && Trigger.isInsert){
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
        //set file visibility to allusers to ensure that both internal and external users can see the file
        //-by default files uploaded internally cannot be viewed by partner users
			for(ContentDocumentLink cdl : Trigger.new){
				if(cdl.ContentDocumentId != null){
					System.debug('CDL IN BEFORE INSERT'+ cdl);
					cdl.Visibility='AllUsers'; 
				}
			}
		}       
    }	

    //after insert methods
 	String validationErrorFile = null;
    if(Trigger.isAfter && Trigger.isInsert){
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){            
			system.debug('***QUERY CHECK CDL TRIGGER after INSERT: ' + limits.getQueries());
            //run validation on Files that have been uploaded
            if(FileProcessingParameters.runFileValidation()){
				validationErrorFile = ContentDocumentLinkUtility.validateFileParameters(trigger.new);
				if(validationErrorFile != null){
					trigger.new[0].addError(validationErrorFile);
				}
			}

			//add custom file share records
			if(validationErrorFile == null){
				//-non opportunity team members
				system.debug('***QUERY CHECK CDL TRIGGER after INSERT BEFORE SHARE FILE: ' + limits.getQueries());
				if(FileProcessingParameters.runShareFileNonOpptyTeamMembers()){
					String validationUserShare = ContentDocumentLinkUtility.shareFileNonOpptyTeamMembers(trigger.new);
					if(validationUserShare != null){
						trigger.new[0].addError(validationUserShare);
					}
				}
				//-opportunity team members
				//only pass parent record leid for processing - do not pass all other leid being shared to file
				for(ContentDocumentLink cdl : trigger.new){
					String leid = cdl.LinkedEntityID;
					if(setObjID.contains(leid.left(3))){
						lstCDLtoProcess.add(cdl);
						setCDID.add(cdl.ContentDocumentId);
					}
				}

				if(!lstCDLtoProcess.isEmpty()){
					//share file to oppty team members
					system.debug('***QUERY CHECK CDL TRIGGER after INSERT BEFORE TEAM SHARE: ' + limits.getQueries());
					if(FileProcessingParameters.runShareFileOpptyTeamMembers()){
						String validationTeamShare = ContentDocumentLinkUtility.shareFileOpptyTeamMembers(lstCDLtoProcess, 'contentdocumentlink', 'insert');
						if(validationTeamShare != null){
							trigger.new[0].addError(validationTeamShare);
						}
					}
					system.debug('***QUERY CHECK CDL TRIGGER after INSERT BEFORE EMAIL NOTIFICATION: ' + limits.getQueries());
					//send email notification upon file upload
					if(FileProcessingParameters.runEmailNotification()){
						String validateEmailNotification = ContentDocumentLinkUtility.emailNotification(lstCDLtoProcess, 'ContentDocumentLink');
						if(validateEmailNotification != null){
							trigger.new[0].addError(validateEmailNotification);
						}
					}
					system.debug('***QUERY CHECK CDL TRIGGER after INSERT AFTER EMAIL NOTIFICATION: ' + limits.getQueries());
					//if lstCDLtoProcess exists so will setCDID - setCDID passed to updateParent methods
					//set file parent attributes
					//configruation of parent updates set at Metadata Type 'File Sharing Settings: Objects'
					if(FileProcessingParameters.runUpdateFileParentAttributes()){
						String validationFileParentUpdate = ContentFileHelper.updateFileParentAttributes(setCDID, 'insert');    
						if(validationFileParentUpdate != null){
							trigger.new[0].addError(validationFileParentUpdate);
						}
					}                   
				}
			}			
        }       
    }

	//before delete methods
	if(Trigger.isBefore && Trigger.isDelete){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			for(ContentDocumentLink cdl : trigger.old){
				String leid = cdl.LinkedEntityID;
				if(!leid.startsWith('005')){  //only pass File parent record id that was unshared to method for processing not user id
					setCDID.add(cdl.ContentDocumentId);
				}
			}

			if(!setCDID.isEmpty()){
				//process files deleted from Files related list - process before delete to ensure all needed ids are present for processing
				//validate if file can be deleted from pcli
				String validateFileDelete = ContentFileHelper.validatePCLIFileDelete(setCDID);
				if(validateFileDelete != null){
					trigger.old[0].addError(validateFileDelete);
				}

				//set file parent attributes
				//configruation of parent updates set at Metadata Type 'File Sharing Settings: Objects'
				if(validateFileDelete == null){
					String validationFileParentUpdate = ContentFileHelper.updateFileParentAttributes(setCDID, 'delete');        
					if(validationFileParentUpdate != null){
						trigger.new[0].addError(validationFileParentUpdate);
					}   				
				}
			}   
		}
	}

	//after delete methods
	if(Trigger.isAfter && Trigger.isDelete){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			for(ContentDocumentLink cdl : trigger.old){
				String leid = cdl.LinkedEntityID;
				if(!leid.startsWith('005')){  //only pass File parent record id that was unshared to method for processing not user id
					lstCDLtoProcess.add(cdl);
				}
			}
			System.debug('CDLTRIGGER AFTER DELETE:'  + lstCDLtoProcess);
			if(!lstCDLtoProcess.isEmpty()){
				//delete content file
				String validateFileDelete = ContentDocumentLinkUtility.deleteRelatedContentFiles(lstCDLtoProcess);
				if(validateFileDelete != null){
					trigger.old[0].addError(validateFileDelete);
				}
			}
		}
	}
}