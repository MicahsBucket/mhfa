trigger FundingRoundCLIEventListener on Funding_Round_Checklist_Item__c (before insert, before update) {
	//trigger switch custom setting enabling/disabling trigger
	Boolean runThisTrigger = false;
	for (Enable_Triggers__mdt oRunTrigger : [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'FundingRoundChecklistItem']) {
		runThisTrigger = oRunTrigger.Enable_Trigger__c;
	}
	if (!runThisTrigger) {
		return;
	}

	if (trigger.isUpdate || trigger.isInsert) {
		FundingRoundCLI_Methods.processFRCLIs(trigger.new, trigger.oldMap);
	}
}