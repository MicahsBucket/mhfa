trigger FundingRoundDocumentTypeEventListener on Funding_Round_Documentation_Type__c (after insert)  {
	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
												FROM Enable_Triggers__mdt
												WHERE MasterLabel = 'FundingRoundDocumentType'];

	//after insert methods
  	if(Trigger.isAfter && Trigger.isInsert){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			String validationFRDTInsert = FundingRoundDocumentTypeUtility.processNewFRDType(trigger.new);
			if(validationFRDTInsert != null){
				trigger.new[0].addError(validationFRDTInsert);
			}
		}
	}
 }