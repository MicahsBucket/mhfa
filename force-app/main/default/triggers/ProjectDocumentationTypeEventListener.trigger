trigger ProjectDocumentationTypeEventListener on Project_Documentation_Type__c (before delete) {
  //trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
					  		       		 FROM Enable_Triggers__mdt
					  		       		 WHERE MasterLabel = 'ProjectDocumentationType'];
	//before delete methods
  if(Trigger.isBefore && Trigger.isDelete){
    if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
      String validationDeleteRecord = ContentFileHelper.validateParentDelete(Trigger.oldMap.Keyset());

      if(validationDeleteRecord != null){
      	trigger.old[0].addError(validationDeleteRecord);
      }
    }
  }
}