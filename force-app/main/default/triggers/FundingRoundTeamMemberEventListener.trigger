trigger FundingRoundTeamMemberEventListener on Funding_Round_Team_Member__c (after insert, after update)  { 
	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
											 WHERE MasterLabel = 'FundingRoundTeamMember'];	

	//after insert methods
	if(Trigger.isAfter && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			String validationProjectTaskOwnerUpdate  = FundingRoundTeamMemberUtility.processProjectTaskOwner(Trigger.new);				
			if(validationProjectTaskOwnerUpdate  != null){
				trigger.new[0].addError(validationProjectTaskOwnerUpdate );
			}
		}
	}

	//after update methods
	if(Trigger.isAfter && Trigger.isUpdate){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			String validationProjectTaskOwnerUpdate = FundingRoundTeamMemberUtility.processProjectTaskOwner(Trigger.new);				
			if(validationProjectTaskOwnerUpdate  != null){
				trigger.new[0].addError(validationProjectTaskOwnerUpdate );
			}
		}
	}
}