trigger ProjectChecklistItemEventListener on Project_Checklist_Item__c (before insert, after insert, after update) {
    //trigger switch custom setting enabling/disabling trigger
    List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ProjectChecklistItem'];
  	
    List<Project_Checklist_Item__c> lstPCLI = new List<Project_Checklist_Item__c>();
	List<String> lstRoleFieldUpdated = new List<String>();
	
	//before insert methods
  	if(Trigger.isBefore && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
  		  //set file ownership if needed to = related Project owner
  		  ProjectChecklistItemUtility.setPCLIOwner(Trigger.new);
  		}		
  	}

    //after insert methods
	if(Trigger.isAfter && Trigger.isInsert){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			//stage data specific to object to pass to apex sharing utility class
			Set<ID> setObjID = new Set<ID>();
			Set<ID> setOpptyID = new Set<ID>();
			Map<ID, ID> mapObjIdOpptyId = new Map<ID, ID>();
			for(Project_Checklist_Item__c pcli : trigger.new){
			  setObjID.add(pcli.Id); 
			}
			for(Project_Checklist_Item__c pcli : [SELECT Id
														, Name
														, Project__r.Id  //need to query relationship fields
														, Funding_Round_Checklist_Item__c
														, OwnerId
												  FROM Project_Checklist_Item__c 
												  WHERE Id IN :setObjID]){
			  lstPCLI.add(pcli);
			  setOpptyID.add(pcli.Project__r.Id);
			  mapObjIdOpptyId.put(pcli.Id, pcli.Project__r.Id);
			}
			System.debug('PCLI LINK - TRIGGER - LSTPCLI: ' + lstPCLI);
			//set sharing for newly created records
			String validationErrorRecord = APEXSharingUtility.processAPEXSharing(lstPCLI, setOpptyID, mapObjIdOpptyId, 'Project_Checklist_Item');
			if(validationErrorRecord != null){
			  trigger.new[0].addError(validationErrorRecord);
			}

			//process approval users
			String validationApprovalUser = ProjectChecklistItemUtility.processApprovalUsers(lstPCLI, null);
			if(validationApprovalUser != null){
				trigger.new[0].addError(validationApprovalUser);
			}

			//process project checklist item links
			String validationCreatePCLILink = ProjectChecklistItemUtility.createPCLILink(lstPCLI);
			if(validationCreatePCLILink != null){
				trigger.new[0].addError(validationCreatePCLILink);
			}
		}   
    }
	//after update methods
  	if(Trigger.isAfter && Trigger.isUpdate){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			for(Project_Checklist_Item__c pcli : Trigger.new){
				//process approval users. if role changed process user(s): value or to null
				if(Trigger.newMap.get(pcli.Id).Approval_Role_First__c != null && 
					Trigger.oldMap.get(pcli.Id).Approval_Role_First__c != Trigger.newMap.get(pcli.Id).Approval_Role_First__c ||
					Trigger.newMap.get(pcli.Id).Approval_Role_First__c == null && 
					Trigger.oldMap.get(pcli.Id).Approval_Role_First__c != null){
					lstRoleFieldUpdated.add('First');
				}
				if(Trigger.newMap.get(pcli.Id).Approval_Role_Second__c != null &&
					Trigger.oldMap.get(pcli.Id).Approval_Role_Second__c != Trigger.newMap.get(pcli.Id).Approval_Role_Second__c ||
					Trigger.newMap.get(pcli.Id).Approval_Role_Second__c == null &&
					Trigger.oldMap.get(pcli.Id).Approval_Role_Second__c != null){
					lstRoleFieldUpdated.add('Second');
				}
				if(Trigger.newMap.get(pcli.Id).Approval_Role_Third__c != null && 
					Trigger.oldMap.get(pcli.Id).Approval_Role_Third__c != Trigger.newMap.get(pcli.Id).Approval_Role_Third__c ||
					Trigger.newMap.get(pcli.Id).Approval_Role_Third__c == null && 
					Trigger.oldMap.get(pcli.Id).Approval_Role_Third__c != null){
					lstRoleFieldUpdated.add('Third');
				}					
				lstPCLI.add(pcli);
			}
			if(!lstRoleFieldUpdated.isEmpty()){ //if approver roles updated then fire method
				String validationApprovalUser = ProjectChecklistItemUtility.processApprovalUsers(lstPCLI, lstRoleFieldUpdated);
				if(validationApprovalUser != null){
					trigger.new[0].addError(validationApprovalUser);
				}
			}
  		}
	}
}