trigger ProjectChecklistEventListener on Project_Checklist__c (before update, after update)  {	
	//trigger switch custom setting enabling/disabling trigger
	Boolean runThisTrigger = false;
	for (Enable_Triggers__mdt oRunTrigger : [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'ProjectChecklist']) {
		runThisTrigger = oRunTrigger.Enable_Trigger__c;
	}
	if (!runThisTrigger) {
		return;
	}

	if (trigger.isBefore && trigger.isUpdate) {
		//If the status of a Project Checklist changes from 'Submitted' to 'Open'
		//change the Approval Status value on all PCLIs to 'Unsubmitted'
		for(Project_Checklist__c pcl : Trigger.new){
			if(Trigger.oldMap.get(pcl.Id).Checklist_Status__c != Trigger.newMap.get(pcl.Id).Checklist_Status__c){
				ProjectCLI_Methods.processProjectChecklists(trigger.new, trigger.oldMap);
			}
		}			
	}

	if (trigger.isAfter && trigger.isUpdate) {
		//20170816 EBG/DCS - determine if PCLIP dates need to be reset
		//if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){		
		//	ProjectCLI_Methods.resetDueDates_ProjectPhase(trigger.new, trigger.oldMap);
		//}

		//20190917 EBG/DCS - if status changed to 'Submitted', set a due date on appropriate tasks
		ProjectTaskUtility.assignChecklistTaskDueDate(trigger.new, trigger.oldMap);
	}


}