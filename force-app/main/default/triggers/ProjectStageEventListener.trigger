trigger ProjectStageEventListener on Project_Sub_Phase__c (after insert, after update)  {

    //trigger switch custom setting enabling/disabling trigger
    Boolean runThisTrigger = false;
    for (Enable_Triggers__mdt oRunTrigger : [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ProjectStage']) {
        runThisTrigger = oRunTrigger.Enable_Trigger__c;
    }
    if (!runThisTrigger) {
        return;
    }

	if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		//if the status is Complete, update the current phase and stage of the project
		ProjectStageUtility.assignProjectPhaseAndStage(trigger.new, trigger.oldMap);

		//if the duration changed on a stage, reset due dates for phases and stages for the project
		if (utilityMethods_DCS.firstTriggerTime_ProjectStageEventListener) {
			utilityMethods_DCS.firstTriggerTime_ProjectStageEventListener = false;
			ProjectStageUtility.updateDueDates(trigger.newMap, trigger.oldMap);
		}
	}
}