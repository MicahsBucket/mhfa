trigger ProjectPhaseDesisionEventListener on Project_Phase_Decision__c (before insert, after insert, before update) {
  	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'ProjectPhaseDecision'];
  	
  	//before insert methods
  	if(Trigger.isBefore && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			//run validation for inserted records
			String validationErrorFile = ProjectPhaseDecisionUtility.validateRecordParameters(Trigger.new);
			if(validationErrorFile != null){
				trigger.new[0].addError(validationErrorFile);
			}	
  		}
  	}

  	//after insert methods
    if(Trigger.isAfter && Trigger.isInsert){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
		    //stage data specific to object to pass to apex sharing utility class
	        Set<ID> setObjID = new Set<ID>();
	        Set<ID> setOpptyID = new Set<ID>();
	        List<Project_Phase_Decision__c> lstPPDRecords = new List<Project_Phase_Decision__c>();
	        Map<ID, ID> mapObjIdOpptyId = new Map<ID, ID>();
	        for(Project_Phase_Decision__c ppd : trigger.new){
	          setObjID.add(ppd.Id); 
	        }
	        for(Project_Phase_Decision__c ppd : [SELECT Id
	                                                  , Project__r.Id  //need to query relationship fields
	                                                  , OwnerId
	                                             FROM Project_Phase_Decision__c 
	                                             WHERE Id IN :setObjID]){
	          lstPPDRecords.add(ppd);
	          setOpptyID.add(ppd.Project__r.Id);
	          mapObjIdOpptyId.put(ppd.Id, ppd.Project__r.Id);
	        }
			//set sharing for newly created records
			String validationErrorRecord = APEXSharingUtility.processAPEXSharing(lstPPDRecords, setOpptyID, mapObjIdOpptyId, 'Project_Phase_Decision');
			if(validationErrorRecord != null){
				trigger.new[0].addError(validationErrorRecord);
			}
		}   
    }

  	//before update methods
  	if(Trigger.isBefore && Trigger.isUpdate){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			//run validation for updated records
			String validationErrorFile = ProjectPhaseDecisionUtility.validateRecordParameters(Trigger.new);
			if(validationErrorFile != null){
				trigger.new[0].addError(validationErrorFile);
			}	
  		}
  	}
}