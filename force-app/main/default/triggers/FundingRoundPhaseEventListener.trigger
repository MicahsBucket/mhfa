trigger FundingRoundPhaseEventListener on Phase__c (before insert, after insert, before update, after update) {

    //trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
													FROM Enable_Triggers__mdt
													WHERE MasterLabel = 'FundingRoundPhase'];

    //20170816 EBG/DCS - determine if PCLIP dates need to be reset
    if (trigger.isAfter && trigger.isUpdate) {
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){		
			ProjectCLI_Methods.resetDueDates_FundingRoundPhase(trigger.new, trigger.oldMap);
		}
	}

    //20171208 EBG/DCS - prevent multiple sub-phases from being created for phases requiring submission
    if (trigger.isBefore && trigger.isUpdate) {
        system.debug('FundingRoundPhaseEvent Listener: Before Update');
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			//Commented out by EBG/DC on 20200313 - phase are no longer submitted
			//FundingRoundPhaseUtility.validateSubmissionRules(trigger.new, trigger.oldMap);
			FundingRoundPhaseUtility.updateStageDueDate_StageChange(trigger.new, trigger.oldMap);
		}
	}

    if (trigger.isBefore && trigger.isInsert) {
        system.debug('FundingRoundPhaseEvent Listener: Before Insert');
		//Commented out by EBG/DC on 20200313 - phase are no longer submitted
        //FundingRoundPhaseUtility.validateSubmissionRules(trigger.new, null);
		//FundingRoundPhaseUtility.updateStageDueDate_StageChange(trigger.new, trigger.oldMap);
    }

    if (Trigger.isAfter && Trigger.isUpdate) {
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			//FundingRoundPhaseUtility.updatePhaseStartDates(trigger.new, trigger.oldMap);
			//20191217 EBG - changed to reset phase and stage dates based on phase due date baseline and stage duration; prevent recursive trigger
			if (utilityMethods_DCS.firstTriggerTime_FundingRoundPhaseEventListener) {
				utilityMethods_DCS.firstTriggerTime_FundingRoundPhaseEventListener = false;
				FundingRoundPhaseUtility.updateDueDates_PhaseBaseline(trigger.new, trigger.oldMap);
				FundingRoundPhaseUtility.updateDueDates_StageDuration(trigger.new, trigger.oldMap);
        
				FundingRoundPhaseUtility.updateTaskDueDates(trigger.new, trigger.oldMap);
				//FundingRoundPhaseUtility.updateStageDueDates_PhaseChange(trigger.new, trigger.oldMap);
			}
		}
    }
}