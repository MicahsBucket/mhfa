/*
*   {Purpose}	Custom trigger manager for the OpportunityTeamMember object that
*               marshalls bulk operations triggered from OpportunityTeamMember object       
*
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             	Description
*   06/29/16 	Kevin Johnson DCS   Created
*	02/12/18	Kevin Johnson DCS 	Added processPublicRecords to after insert trigger
*	07/30/19	Kevin Johnson DCS	Refactored to call new processProjectTeamMembers method in lieu of multiple processing methods
*   =============================================================================
*/

trigger OpportunityTeamMemberEventListener on OpportunityTeamMember (before insert, after insert, before update, after update, after delete) {
	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
													FROM Enable_Triggers__mdt
													WHERE MasterLabel = 'OpportunityTeamMember'];
	//before insert methods
	if(trigger.isBefore && trigger.isInsert) {
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			//fire trigger if trigger switch custom setting is enabled
			OpportunityTeamMemberUtility.validateOTMRoles(trigger.new);  //maintain team member role integrity.  error passed by validation rule.
  		}
	}

	//after insert methods
	if(trigger.isAfter && trigger.isInsert) {
		//fire trigger if trigger switch custom setting is enabled
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			OpportunityTeamMemberUtility.processOpportunityShare(trigger.new);  //set project(opportunity) access to All	

			String validationProcessTeamMemberUser = OpportunityTeamMemberUtility.processProjectTeamMembers(trigger.new);
			if(validationProcessTeamMemberUser != null){
				trigger.new[0].addError(validationProcessTeamMemberUser);
			}							
			String validationRelatedRecordShare = OpportunityTeamMemberUtility.processOpportunityRelatedRecordShare(trigger.new, 'Insert');  //set project(opportunity) related record access			
			if(validationRelatedRecordShare != null){
				trigger.new[0].addError(validationRelatedRecordShare);
			}
			String validationShareInsert = OpportunityTeamMemberUtility.processOpportunityRelatedFileShare(trigger.new, 'Insert');  //add project(opportunity) related file access			
			if(validationShareInsert != null){
				trigger.new[0].addError(validationShareInsert);
			}
			String validationPublicRecordShare = OpportunityTeamMemberUtility.processPublicRecords(trigger.new, 'Insert');  //add public record access			
			if(validationPublicRecordShare != null){
				trigger.new[0].addError(validationPublicRecordShare);
			}
		}
  	}

	//before update methods
	if(trigger.isBefore && trigger.isUpdate) {
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			//fire trigger if trigger switch custom setting is enabled
			OpportunityTeamMemberUtility.validateOTMRoles(trigger.new);  //maintain team member role integrity
  		}
	}

  	//after update methods
  	if(trigger.isAfter && trigger.isUpdate) {
  		//fire trigger if trigger switch custom setting is enabled
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			String validationProcessTeamMemberUser = OpportunityTeamMemberUtility.processProjectTeamMembers(trigger.new);
			if(validationProcessTeamMemberUser != null){
				trigger.new[0].addError(validationProcessTeamMemberUser);
			}
			String validationRelatedRecordShare = OpportunityTeamMemberUtility.processOpportunityRelatedRecordShare(trigger.new, 'Update');  //update project(opportunity) related record access	
			if(validationRelatedRecordShare != null){
				trigger.new[0].addError(validationRelatedRecordShare);
			}
		}
  	}

  	//after delete methods
  	if(trigger.isAfter && trigger.isDelete) {
  		//fire trigger if trigger switch custom setting is enabled
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){	
			String validationProcessTeamMemberUser = OpportunityTeamMemberUtility.processProjectTeamMembers(trigger.old);
			if(validationProcessTeamMemberUser != null){
				trigger.new[0].addError(validationProcessTeamMemberUser);
			}
			String validationErrorRecord = OpportunityTeamMemberUtility.processOpportunityRelatedRecordShare(trigger.old, 'Delete');  //delete project(opportunity) related record access
			if(validationErrorRecord != null){
				trigger.new[0].addError(validationErrorRecord);
			}
			String validationShareDelete = OpportunityTeamMemberUtility.processOpportunityRelatedFileShare(trigger.old, 'Delete');  //remove project(opportunity) related file access	
			if(validationShareDelete != null){
				trigger.old[0].addError(validationShareDelete);
			}
			String validationPublicRecordShareDelete = OpportunityTeamMemberUtility.processPublicRecords(trigger.old, 'Delete');  //remove public record access
			if(validationPublicRecordShareDelete != null){
				trigger.old[0].addError(validationPublicRecordShareDelete);
			}
  		}
	}
}