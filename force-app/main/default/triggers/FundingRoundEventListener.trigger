trigger FundingRoundEventListener on Funding_Round__c (after insert, after update) {
	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
													FROM Enable_Triggers__mdt
													WHERE MasterLabel = 'FundingRound'];

	//after insert methods
	if(Trigger.isAfter && Trigger.isInsert){
		//fire trigger if trigger switch custom setting is enabled		
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			for(Funding_Round__c fr : Trigger.new){
				if(!fr.Name.contains('Clone')){  //run if funding round is not cloned
					FundingRoundUtility.loadPhasesAndStages(trigger.newMap);
				}
			}	
		}	
	}

	//after update methods
	if(Trigger.isAfter && Trigger.isUpdate){
		//determine if show selection has changed and if changed build map of funding round id to its respective 
		//show selection value and pass to class for processing
				//fire trigger if trigger switch custom setting is enabled		
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){		
			Map<ID, String> mapFRSelectionStatus = new Map<ID, String>();
			String strSelectionBy = null;
			for(Funding_Round__c fr : trigger.new){
				strSelectionBy = fr.Project_Selection_By__c;
				if(fr.Show_Selection__c == TRUE && trigger.oldMap.get(fr.Id).Show_Selection__c == FALSE){
					mapFRSelectionStatus.put(fr.Id, 'Select');
				} else if(fr.Show_Selection__c == FALSE && trigger.oldMap.get(fr.Id).Show_Selection__c == TRUE){
					mapFRSelectionStatus.put(fr.Id, 'PreSelect');
				}
			}
			if(!mapFRSelectionStatus.isEmpty() && strSelectionBy == 'Funding Round'){
				String validationRecordTypeUpdate = FundingRoundUtility.setProjectRecordTypes(trigger.new, mapFRSelectionStatus);
				if(validationRecordTypeUpdate != null){
					trigger.new[0].addError(validationRecordTypeUpdate);
				}	
			} else if(!mapFRSelectionStatus.isEmpty() && strSelectionBy != 'Funding Round'){
				trigger.new[0].addError('Project selection view change is driven at the Project level.');
			}
		
			//20191217 EBG - if open date changes, recalculate start and due dates for phases and stages
			FundingRoundPhaseUtility.updateDueDates_FROpenDate(trigger.new, trigger.oldMap);			
  		}
	}
}