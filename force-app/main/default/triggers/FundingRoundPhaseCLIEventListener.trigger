trigger FundingRoundPhaseCLIEventListener on Phase_Checklist_Item__c (after update)  {
	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'FundingRoundPhaseChecklistItem'];

	List<Phase_Checklist_Item__c> lstFRPCLI = new List<Phase_Checklist_Item__c>();

	//after update methods
  	if(Trigger.isAfter && Trigger.isUpdate){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			for(Phase_Checklist_Item__c frpcli : Trigger.new){
				//process if approval role(s) changed: value or to null
				if((Trigger.newMap.get(frpcli.Id).Approval_First__c != null && 
					Trigger.oldMap.get(frpcli.Id).Approval_First__c != Trigger.newMap.get(frpcli.Id).Approval_First__c ||					
					Trigger.newMap.get(frpcli.Id).Approval_First__c == null && 
					Trigger.oldMap.get(frpcli.Id).Approval_First__c != null) ||
				  (Trigger.newMap.get(frpcli.Id).Approval_Second__c != null &&
					Trigger.oldMap.get(frpcli.Id).Approval_Second__c != Trigger.newMap.get(frpcli.Id).Approval_Second__c ||
					Trigger.newMap.get(frpcli.Id).Approval_Second__c == null &&
					Trigger.oldMap.get(frpcli.Id).Approval_Second__c != null) ||
				  (Trigger.newMap.get(frpcli.Id).Approval_Third__c != null && 
					Trigger.oldMap.get(frpcli.Id).Approval_Third__c != Trigger.newMap.get(frpcli.Id).Approval_Third__c ||
					Trigger.newMap.get(frpcli.Id).Approval_Third__c == null && 
					Trigger.oldMap.get(frpcli.Id).Approval_Third__c != null)){
					lstFRPCLI.add(frpcli);
				}
			}
		}
		if(!lstFRPCLI.isEmpty()){ //if approver roles updated then fire method
			String validationApprovalRole = FundingRoundPhaseCLIUtility.processApprovalRoles(lstFRPCLI);
			if(validationApprovalRole != null){
				trigger.new[0].addError(validationApprovalRole);
			}
		}
	}
}