trigger FundingRoundNarrativeAuthorEventListener on Funding_Round_Narrative_Author__c (after update) {
	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'FundingRoundNarrativeAuthor'];

	//after update
	if(Trigger.isAfter && Trigger.isUpdate){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			List<Funding_Round_Narrative_Author__c> lstFRNAToUpdate = new List<Funding_Round_Narrative_Author__c>();

			for(Funding_Round_Narrative_Author__c fna : Trigger.new){
				//determine if due date has changed.  if so, add to list of FRNA to be passed to update method.
				if(trigger.oldMap.get(fna.Id).Due_Date__c != trigger.newMap.get(fna.Id).Due_Date__c ||
				   trigger.oldMap.get(fna.Id).Final_Due_Date__c != trigger.newMap.get(fna.Id).Final_Due_Date__c){
					lstFRNAToUpdate.add(fna);
				}				
			}
		
			if(!lstFRNAToUpdate.isEmpty()){
				String validationPNAUpdate = FundingRoundNarrativeAuthorUtility.updateRelatedProjectNarrativeAuthors(lstFRNAToUpdate);
				if(validationPNAUpdate != null){
	            	trigger.new[0].addError(validationPNAUpdate);
	        	}	        	
			}
		}
	}
}