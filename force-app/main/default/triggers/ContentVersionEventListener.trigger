trigger ContentVersionEventListener on ContentVersion (after insert) {
     /*
       run file validation from contentversion when file is updated being we need to evaluate 
       the inserted file version instead of the contentdocumentlink share record
    */
    //trigger switch custom setting enabling/disabling trigger
    List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ContentDocumentLink'];

    List<File_Sharing_Settings_Objects__mdt> csSharingRules = [SELECT Object_Prefix__c
                                                               FROM File_Sharing_Settings_Objects__mdt];
    
    //build set containing file parent object id
    Set<String> setObjID = new Set<String>();
    for(File_Sharing_Settings_Objects__mdt fss : csSharingRules){
        setObjID.add(fss.Object_Prefix__c);
    }

    List<ContentDocumentLink> lstCDLtoProcess = new List<ContentDocumentLink>();

    //after insert methods
    if(Trigger.isAfter && Trigger.isInsert){
        system.debug('***CV TRIGGER after INSERT: ' + limits.getQueries());
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
            //run validation on Files that have been updated
            String validationErrorFile = ContentVersionUtility.validateFileParameters(trigger.new);
            if(validationErrorFile != null){
                trigger.new[0].addError(validationErrorFile);
            }
			
			if(validationErrorFile == null){               
				//compile data to facilitate file notification
				Set<ID> setCDId = new Set<ID>();
				for(ContentVersion cv : trigger.new){
					setCDId.add(cv.ContentDocumentId);
				}

				//update last modified user/date
				String validateParentUpdate = ContentFileHelper.updateFileParentAttributes(setCDId, 'update');
				if(validateParentUpdate != null){
					trigger.new[0].addError(validateParentUpdate);
				}

				//fetch related contentdocumentlink records
				List<ContentDocumentLink> lstCDL = new List<ContentDocumentLink>([SELECT LinkedEntityId
																					   , ContentDocumentId
																				  FROM ContentDocumentLink
																				  WHERE ContentDocumentId IN :setCDId]);
				system.debug('CV LSTCDL: ' + lstCDL);
				//only pass parent record leid for processing - do not pass all other leid being shared to file
				for(ContentDocumentLink cdl : lstCDL){
					String leid = cdl.LinkedEntityID;
					if(setObjID.contains(leid.left(3))){
						lstCDLtoProcess.add(cdl);
					}
				}

				//send email notification upon file update
				if(!lstCDLtoProcess.isEmpty()){
					String validateEmailNotification = ContentDocumentLinkUtility.emailNotification(lstCDLtoProcess, 'ContentVersion');
					if(validateEmailNotification != null){
						trigger.new[0].addError(validateEmailNotification);
					}
				}
			}
		}   
    }
}