trigger ContentDocumentEventListener on ContentDocument (before delete)  {
    system.debug('Inside ContentDocumentEventListener.  PurgePreventUpdates.prevent: ' + PurgePreventUpdates.prevent);
    if (PurgePreventUpdates.prevent) {
        return;
    }    
    //trigger switch custom setting enabling/disabling trigger
    List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
                                             FROM Enable_Triggers__mdt
                                             WHERE MasterLabel = 'ContentDocument'];

    //before insert methods
    //  -utilize before delete being deleted record is needed for downstream processing to obtain linkedentityid
    //  -handles delete from File mgmt interface
    if(Trigger.isBefore && Trigger.isDelete){
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
            Set<ID> setCDID = new Set<Id>();
            for(ContentDocument cd : trigger.old){
                setCDID.add(cd.Id);
            }

            if(!setCDID.isEmpty()){
				//validate if file can be deleted from pcli
				String validateFileDelete = ContentFileHelper.validatePCLIFileDelete(setCDID);
				if(validateFileDelete != null){
					trigger.old[0].addError(validateFileDelete);
				}

                //process files deleted from Files interface - process before delete to ensure all needed ids are present for processing
                //set file parent attributes
				if(validateFileDelete == null){ 
					String validationFileParentUpdate = ContentFileHelper.updateFileParentAttributes(setCDID, 'delete');        
					if(validationFileParentUpdate != null){
						trigger.new[0].addError(validationFileParentUpdate);
					}
				}
            }       
        }
    }
 }