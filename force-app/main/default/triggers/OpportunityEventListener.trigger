trigger OpportunityEventListener on Opportunity (after insert, after update, before delete, after delete) {
    //trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
													FROM Enable_Triggers__mdt
													WHERE MasterLabel = 'Opportunity'];

    //after insert methods
    if(Trigger.isAfter && Trigger.isInsert){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			//create related project records respective to linked Funding Round - make sure to deactivate the process builder which creates project phases/stages
			//Need to add Multifamily user as the owner of the records
			OpportunityUtility.createPhasesStagesChecklists(trigger.newMap.keySet());
			OpportunityUtility.createProjectNarrative(trigger.new);
			OpportunityUtility.createProjectWorkbook(trigger.new);
			OpportunityUtility.createSupportingDocumentation(trigger.new);
			OpportunityUtility.createUnacceptablePractice(trigger.new);
		}
	}

    //after update
    if(Trigger.isAfter && Trigger.isUpdate){
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			List<Opportunity> lstOpptyToUpdate = new List<Opportunity>();
			List<ID> lstPropertyToUpdate = new List<ID>();

			//update due dates
			ProjectCLI_Methods.resetDueDates(trigger.new, trigger.oldMap);          

			for(Opportunity oppty : Trigger.new){
				//determine if owner has changed
				if(trigger.oldMap.get(oppty.Id).OwnerId != trigger.newMap.get(oppty.Id).OwnerId){
					lstOpptyToUpdate.add(oppty);
				}
                
				//determine if property has been added to project
				if(trigger.oldMap.get(oppty.Id).Property__c != trigger.newMap.get(oppty.Id).Property__c){
					if(trigger.oldMap.get(oppty.Id).Property__c == null && trigger.newMap.get(oppty.Id).Property__c != null){  
						//null -> property - update one property count
						lstPropertyToUpdate.add(trigger.newMap.get(oppty.Id).Property__c);
					} else if(trigger.oldMap.get(oppty.Id).Property__c != null && trigger.newMap.get(oppty.Id).Property__c != null){  
						//property -> new property - update two properties count
						lstPropertyToUpdate.add(trigger.newMap.get(oppty.Id).Property__c);
						lstPropertyToUpdate.add(trigger.oldMap.get(oppty.Id).Property__c);
					} else if(trigger.newMap.get(oppty.Id).Property__c == null){  
						//property -> null - update one property count
						lstPropertyToUpdate.add(trigger.oldMap.get(oppty.Id).Property__c);
					}
				}
			}

			//update related project records when project owner is changed
			if(!lstOpptyToUpdate.isEmpty()){
				//create Project Unacceptable Practice record
				OpportunityUtility.createUnacceptablePractice(trigger.new);
				//update PCLI owner
				String validationPCLIOwner = OpportunityUtility.setPCLIOwner(lstOpptyToUpdate);
				if(validationPCLIOwner != null){
					trigger.new[0].addError(validationPCLIOwner);
				}
				//update PNG owner
				String validationPNGOwner = OpportunityUtility.setPNGOwner(lstOpptyToUpdate);
				if(validationPNGOwner != null){
					trigger.new[0].addError(validationPNGOwner);
				}
				//update PP attributes
				String validationPPUpdate = OpportunityUtility.setPPAttributes(lstOpptyToUpdate);
				if(validationPPUpdate != null){
					trigger.new[0].addError(validationPPUpdate);
				}                   
			}
			//update related property count when property is added to project
			if(!lstPropertyToUpdate.isEmpty()){
				String validationProjectCount = OpportunityUtility.updateRelatedProperty(lstPropertyToUpdate);
				if(validationProjectCount != null){
					trigger.new[0].addError(validationProjectCount);
				}
			}           
		}
    }

    //before delete
    if(Trigger.isBefore && Trigger.isDelete){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			OpportunityUtility.deleteChildrenRecords(trigger.oldMap.keySet());
		}
	}

    //after delete
    if(Trigger.isAfter && Trigger.isDelete){
        if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			List<ID> lstPropertyToUpdate = new List<ID>();

			for(Opportunity oppty : Trigger.old){
				if(trigger.oldMap.get(oppty.Id).Property__c != null){
					//update related property total project count
					lstPropertyToUpdate.add(trigger.oldMap.get(oppty.Id).Property__c);
				}
			}

			if(lstPropertyToUpdate.size() > 0){
				String validationProjectCount = OpportunityUtility.updateRelatedProperty(lstPropertyToUpdate);
				if(validationProjectCount != null){
					trigger.old[0].addError(validationProjectCount);
				}
			}
		}
	}
}