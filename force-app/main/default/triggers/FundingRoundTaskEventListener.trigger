trigger FundingRoundTaskEventListener on Funding_Round_Task__c (before insert, before update, after update, after insert)  {
	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
											 WHERE MasterLabel = 'FundingRoundTask'];	

	//before insert methods
	if(Trigger.isBefore && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			System.debug('FR Task before insert');
			//run validation for inserted records
			String validationErrorRecord = FundingRoundTaskUtility.processNewFRTask(Trigger.new, true);
				
			if(validationErrorRecord != null){
				System.debug('Error on insert.  ' + validationErrorRecord);
				trigger.new[0].addError(validationErrorRecord);
			}
  		}
	}

	//after insert methods
	if(Trigger.isAfter && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){		
			System.debug('FR Task after insert');	
			//when a funding round task is inserted, inherited the parent's characteristic (if a parent is assigned)
			configureTask_Cntlr.inheritParentTaskCharacteristics(Trigger.newMap.keySet());

			/* COMMENTED 2/7/20 BY KDJ - NO NEED TO CASCADE OWNERS TO NEW FR TASKS AS THE NEW FR TASK HAS NOT YET BEEN CREATED AGAINT A PROJECT
			//determine if task ownership assignment needs to be cascaded to related project tasks
			List<Funding_Round_Task__c> lstFRTaskOwnerCascade = new List<Funding_Round_Task__c>();
			for(Funding_Round_Task__c newFRTMap : Trigger.new) {
				if(newFRTMap.Assignee_Role_Funding_Round_Team__c != null || newFRTMap.Assignee_Role_Project_Team__c != null){
					lstFRTaskOwnerCascade.add(newFRTMap);
				}	
			}

			//run validation for inserted records
			if(lstFRTaskOwnerCascade.size() > 0){
				String validationErrorRecord = FundingRoundTaskUtility.processProjectTaskOwner(lstFRTaskOwnerCascade);
				
				if(validationErrorRecord != null){
					trigger.new[0].addError(validationErrorRecord);
				}	
			}	
			*/
		}
	}
	
	//before update methods
	if(Trigger.isBefore && Trigger.isUpdate){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			System.debug('FR Task before update');			
			//run validation for inserted records
			String validationErrorRecord = FundingRoundTaskUtility.processNewFRTask(Trigger.new, false);
				
			if(validationErrorRecord != null){
				trigger.new[0].addError(validationErrorRecord);
			}
  		}
	}

	//after update methods
	if(Trigger.isAfter && Trigger.isUpdate){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){
			System.debug('FR Task after update');			
			//run validation for updated records
			String validationErrorRecord = FundingRoundTaskUtility.processUpdatedFRTask(Trigger.new, Trigger.oldMap);
				
			if(validationErrorRecord != null){
				trigger.new[0].addError(validationErrorRecord);
			}
  		}
	}
}