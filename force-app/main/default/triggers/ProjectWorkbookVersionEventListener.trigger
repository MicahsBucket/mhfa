trigger ProjectWorkbookVersionEventListener on Project_Workbook_Version__c (before insert, before update, after insert, after update, before delete) {
    //trigger switch custom setting enabling/disabling trigger
	Boolean runTrigger = true;
	List<Enable_Triggers__mdt> lstRunTrigger = [SELECT Enable_Trigger__c
												FROM Enable_Triggers__mdt
												WHERE MasterLabel = 'ProjectWorkbookVersion'];
	if (lstRunTrigger != null && lstRunTrigger[0].Enable_Trigger__c) {
		runTrigger = true;
	} else {
		runTrigger = false;
	}

	if (!runTrigger) {
		return;
	}

    Set<Id> setPWBVIds = new Set<Id>();
    Map<Id, Id> mapPWBVProjRecType = new Map<Id, Id>();    

    if(Trigger.isInsert || Trigger.isUpdate){ //only run on insert/update
        for(Project_Workbook_Version__c pwbv : Trigger.new) {
            setPWBVIds.add(pwbv.Id);
        }

        //obtain pwbv related project recordtype
        for(Project_Workbook_Version__c pwbvrt : [SELECT Id
                                                       , Project_Workbook__r.Project__r.RecordTypeId
                                                  FROM Project_Workbook_Version__c
                                                  WHERE Id = :setPWBVIds]){
            mapPWBVProjRecType.put(pwbvrt.Id, pwbvrt.Project_Workbook__r.Project__r.RecordTypeId);
        }
        system.debug('PWBV PROJECT RT: ' + mapPWBVProjRecType);
    }

    //before insert methods
    if(Trigger.isBefore && Trigger.isInsert){
        //run validation for inserted records
        String validationErrorFile = ProjectWorkbookVersionUtility.validateRecordParameters(Trigger.new);
        if(validationErrorFile != null){
            trigger.new[0].addError(validationErrorFile);
        }   
    }

    //after insert methods
    if(Trigger.isAfter && Trigger.isInsert){
        for(Project_Workbook_Version__c pwbv : Trigger.new){
            //manage pwbv access for partner users - record insert
            if(mapPWBVProjRecType.get(pwbv.Id) == '01237000000TgAiAAK' && pwbv.Make_Public__c == TRUE){  //pre-selection
                String validationErrorRecord = ProjectWorkbookVersionUtility.processPWBVExternalShare(Trigger.new, 'Insert', 'PreSelection');
                if(validationErrorRecord != null){
                    trigger.new[0].addError(validationErrorRecord);
                }
            } else if(mapPWBVProjRecType.get(pwbv.Id) == '01237000000TgAjAAK' && pwbv.Make_Public__c == TRUE){  //post selection
                String validationErrorRecord = ProjectWorkbookVersionUtility.processPWBVExternalShare(Trigger.new, 'Insert', 'PostSelection');
                if(validationErrorRecord != null){
                    trigger.new[0].addError(validationErrorRecord);
                }
            }  
        }

        //stage data specific to object to pass to apex sharing utility class
        Set<ID> setObjID = new Set<ID>();
        Set<ID> setOpptyID = new Set<ID>();
        List<Project_Workbook_Version__c> lstPWBVRecords = new List<Project_Workbook_Version__c>();
        Map<ID, ID> mapObjIdOpptyId = new Map<ID, ID>();
        for(Project_Workbook_Version__c pwv : trigger.new){
            setObjID.add(pwv.Id);   
        }
        for(Project_Workbook_Version__c pwbv : [SELECT Id
                                                        , Project_Workbook__r.Project__r.Id  //need to query relationship fields
                                                        , OwnerId
                                                FROM Project_Workbook_Version__c 
                                                WHERE Id IN :setObjID]){
            lstPWBVRecords.add(pwbv);
            setOpptyID.add(pwbv.Project_Workbook__r.Project__r.Id);
            mapObjIdOpptyId.put(pwbv.Id, pwbv.Project_Workbook__r.Project__r.Id);
        }           
        //set sharing for newly created records
        String validationErrorRecord = APEXSharingUtility.processAPEXSharing(lstPWBVRecords, setOpptyID, mapObjIdOpptyId, 'Project_Workbook_Version');
        if(validationErrorRecord != null){
            trigger.new[0].addError(validationErrorRecord);
        }
    }

    //before update methods
    if(Trigger.isBefore && Trigger.isUpdate){
        //run validation for inserted records
        String validationErrorFile = ProjectWorkbookVersionUtility.validateRecordParameters(Trigger.new);
        if(validationErrorFile != null){
            trigger.new[0].addError(validationErrorFile);
        }   
    }

    //after update methods
    if(Trigger.isAfter && Trigger.isUpdate){
        for(Project_Workbook_Version__c pwbv : Trigger.new){
            //manage pwbv access for partner user - record update
            //-process pwbv when pwbv public trigger has changed
            //-pre-selection project recordtypeid = 01237000000TgAiAAK
            //-post selection project recordtypeid = 01237000000TgAjAAK
            if(mapPWBVProjRecType.get(pwbv.Id) == '01237000000TgAiAAK' //pre-selection - add share
                && trigger.oldMap.get(pwbv.Id).Make_Public__c != trigger.newMap.get(pwbv.Id).Make_Public__c
                && trigger.newMap.get(pwbv.Id).Make_Public__c == TRUE){
                String validationErrorRecord = ProjectWorkbookVersionUtility.processPWBVExternalShare(Trigger.new, 'Insert', 'PreSelection');
                if(validationErrorRecord != null){
                    trigger.new[0].addError(validationErrorRecord);
                }
            } else if(mapPWBVProjRecType.get(pwbv.Id) == '01237000000TgAjAAK'  //post selection - add share
                        && trigger.oldMap.get(pwbv.Id).Make_Public__c != trigger.newMap.get(pwbv.Id).Make_Public__c
                        && trigger.newMap.get(pwbv.Id).Make_Public__c == TRUE){
                    String validationErrorRecord = ProjectWorkbookVersionUtility.processPWBVExternalShare(Trigger.new, 'Insert', 'PostSelection');
                    if(validationErrorRecord != null){
                        trigger.new[0].addError(validationErrorRecord);
                    }   
            } else if(trigger.oldMap.get(pwbv.Id).Make_Public__c != trigger.newMap.get(pwbv.Id).Make_Public__c  //remove share
                        && trigger.newMap.get(pwbv.Id).Make_Public__c == FALSE){
                    String validationErrorRecord = ProjectWorkbookVersionUtility.processPWBVExternalShare(Trigger.new, 'Delete', null);
                    if(validationErrorRecord != null){
                        trigger.new[0].addError(validationErrorRecord);
                    }
            }
            
            //manage update of parent pwb record
            if(trigger.oldMap.get(pwbv.Id).Number_of_Uploaded_Files__c != trigger.newMap.get(pwbv.Id).Number_of_Uploaded_Files__c){
                String validationErrorRecord = ProjectWorkbookVersionUtility.updateParentPWBAttributes(Trigger.new);
                if(validationErrorRecord != null){
                    trigger.new[0].addError(validationErrorRecord);
                }
            }   
        }
    }

    //before delete methods
    if(Trigger.isBefore && Trigger.isDelete){
        String validationDeleteRecord = ContentFileHelper.validateParentDelete(Trigger.oldMap.Keyset());

        if(validationDeleteRecord != null){
            trigger.old[0].addError(validationDeleteRecord);
        }
    }
}