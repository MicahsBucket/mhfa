trigger FundingRoundCLILinkEventListener on Funding_Round_Checklist_Item_Link__c (after insert)  { 
	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'FundingRoundChecklistItemLink'];
	
	//after insert methods
	if (trigger.isAfter && trigger.isInsert) {
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			//cascade new frclilink records to related pcli records
			String validationCreatePCLILink = FundingRoundCLILinkUtility.createPCLILink(trigger.new);				
			if(validationCreatePCLILink != null){
				trigger.new[0].addError(validationCreatePCLILink);
			}
		}
	}
}