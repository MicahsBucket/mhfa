trigger ProjectCharacteristicEventListener on Project_Characteristic__c (after insert, after delete)  { 
	//trigger switch custom setting enabling/disabling trigger
	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
											 FROM Enable_Triggers__mdt
											 WHERE MasterLabel = 'ProjectCharacteristic'];

	//after insert methods
  	if(Trigger.isAfter && Trigger.isInsert){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			ProjectCharacteristicUtility.createProjectChecklists(Trigger.new);	
  		}		
  	}

	//after delete methods
  	if(Trigger.isAfter && Trigger.isDelete){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			ProjectCharacteristicUtility.deleteProjectChecklistAndTaskCharacteristics(Trigger.old);	
  		}		
  	}
}