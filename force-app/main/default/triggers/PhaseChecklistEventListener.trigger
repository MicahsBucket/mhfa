trigger PhaseChecklistEventListener on Phase_Checklist__c (before insert, before update)  { 
	//trigger switch custom setting enabling/disabling trigger
	Boolean runTrigger = true;
	List<Enable_Triggers__mdt> lstRunTrigger = [SELECT Enable_Trigger__c
												FROM Enable_Triggers__mdt
												WHERE MasterLabel = 'PhaseChecklist'];
	if (lstRunTrigger != null && lstRunTrigger[0].Enable_Trigger__c) {
		runTrigger = true;
	} else {
		runTrigger = false;
	}

	if(!runTrigger) {
		//don't run the trigger if it isn't enabled in the custom metadata
		return;
	}

	if (trigger.isUpdate || trigger.isInsert) {
		FundingRoundPhaseUtility.assignPhaseLookup(trigger.new);
	}
}