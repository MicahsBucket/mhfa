trigger ProjectChecklistItemPhaseEventListener on Project_Checklist_Item_Phase__c (before insert, after insert, after update) {
  	//trigger switch custom setting enabling/disabling trigger
  	List<Enable_Triggers__mdt> RunTrigger = [SELECT Enable_Trigger__c
						  		       		 FROM Enable_Triggers__mdt
						  		       		 WHERE MasterLabel = 'ProjectChecklistItemPhase'];

  	List<Project_Checklist_Item_Phase__c> lstPCLIP = new List<Project_Checklist_Item_Phase__c>();
	List<String> lstRoleFieldUpdated = new List<String>();

  	//before insert methods
  	if (Trigger.isBefore) {
  		//set the name of the PCLIP
  		ProjectCLI_Methods.buildPclipName(Trigger.new);
  	}
  	//after insert methods
  	if(Trigger.isAfter && Trigger.isInsert){
  		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			for(Project_Checklist_Item_Phase__c pclip : Trigger.new){
				//process approval users
				if(pclip.Funding_Round_Sub_Phase__c != null){  //only process sub-phase records
					lstPCLIP.add(pclip);
				}
			}
			if(!lstPCLIP.isEmpty()){
				String validationApprovalUser = ProjectChecklistItemPhaseUtility.processApprovalUsers(lstPCLIP, null);
				if(validationApprovalUser != null){
					trigger.new[0].addError(validationApprovalUser);
				}
			}
  		}
  	}
	//after update methods
  	if(Trigger.isAfter && Trigger.isUpdate){
		if(RunTrigger != null && RunTrigger[0].Enable_Trigger__c){			
			for(Project_Checklist_Item_Phase__c pclip : Trigger.new){
				//process approval users
				if(pclip.Funding_Round_Sub_Phase__c != null){  //only process sub-phase records					
					//if role changed process user(s): value or to null
					if(Trigger.newMap.get(pclip.Id).Approval_Role_First__c != null && 
					   Trigger.oldMap.get(pclip.Id).Approval_Role_First__c != Trigger.newMap.get(pclip.Id).Approval_Role_First__c ||
					   Trigger.newMap.get(pclip.Id).Approval_Role_First__c == null && 
					   Trigger.oldMap.get(pclip.Id).Approval_Role_First__c != null){
						lstRoleFieldUpdated.add('First');
					}
					if(Trigger.newMap.get(pclip.Id).Approval_Role_Second__c != null &&
					   Trigger.oldMap.get(pclip.Id).Approval_Role_Second__c != Trigger.newMap.get(pclip.Id).Approval_Role_Second__c ||
					   Trigger.newMap.get(pclip.Id).Approval_Role_Second__c == null &&
					   Trigger.oldMap.get(pclip.Id).Approval_Role_Second__c != null){
						lstRoleFieldUpdated.add('Second');
					}
					if(Trigger.newMap.get(pclip.Id).Approval_Role_Third__c != null && 
					   Trigger.oldMap.get(pclip.Id).Approval_Role_Third__c != Trigger.newMap.get(pclip.Id).Approval_Role_Third__c ||
					   Trigger.newMap.get(pclip.Id).Approval_Role_Third__c == null && 
					   Trigger.oldMap.get(pclip.Id).Approval_Role_Third__c != null){
						lstRoleFieldUpdated.add('Third');
					}					
					lstPCLIP.add(pclip);
				}
			}
			if(!lstRoleFieldUpdated.isEmpty()){ //if approver roles updated then fire method
				String validationApprovalUser = ProjectChecklistItemPhaseUtility.processApprovalUsers(lstPCLIP, lstRoleFieldUpdated);
				if(validationApprovalUser != null){
					trigger.new[0].addError(validationApprovalUser);
				}
			}
  		}
	}
}