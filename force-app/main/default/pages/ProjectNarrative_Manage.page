<apex:page standardController="Opportunity" 
            extensions="ProjectNarrativeManage_Controller" 
            sideBar="false"
            lightningStylesheets="true">

    <apex:includeScript value="{!URLFOR($Resource.jquery3_1_1_min)}"/>
    <apex:includeScript value="{!URLFOR($Resource.jquery_ui_1_12_1_custom_zip, '/jquery-ui-1.12.1.custom/jquery-ui.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jQuery_DataTables_1_10_12_zip, '/DataTables-1.10.12/media/js/jquery.dataTables.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jQuery_DataTables_1_10_12_zip, '/DataTables-1.10.12/media/js/dataTables.jqueryui.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.jquery_ui_1_12_1_custom_zip, '/jquery-ui-1.12.1.custom/jquery-ui.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.jquery_ui_1_12_1_custom_zip, '/jquery-ui-1.12.1.custom/jquery-ui.theme.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.jQuery_DataTables_1_10_12_zip, '/DataTables-1.10.12/media/css/jquery.dataTables.min.css')}"/>

    <!--prevent wrapping in page block titles-->
    <style type="text/css">
        .pbTitle {
            white-space: nowrap
        }
    </style>

    <script>
        j$ = jQuery.noConflict();
        loadTable();

        function loadTable() {
            j$(document).ready(function() {
                j$('table.display').DataTable({
                    "retrieve":true,
                    "lengthMenu":[[-1, 25, 50, 100], ["All", 25, 50, 100]]
                });
            });
        }
    </script>

    <apex:form id="frmProjectNarrative">
        <apex:pageBlock id="pbProjNarrHeader" title="Project Details">
            <apex:pageBlockSection id="pbsProjectHeader" columns="2" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Project"/>
                    <apex:outputText ><a href="{!$Site.Prefix}/{!Opportunity.Id}">{!Opportunity.Name}</a></apex:outputText>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Funding Round"/>
                    <apex:outputField value="{!Opportunity.Funding_Round__c}"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>       

        <apex:outputPanel id="DataTable">
            <table border="1" width="100%">
                <tr valign="top">
                    <td width="17%">
                        <apex:outputPanel id="pnlFilterGroup">
                            <apex:pageBlock id="pbFilterNarrativeGroup" title="Narrative Filters">
                                <apex:outputText value="Select filter option to filter narrative list:"/>
                                <apex:pageBlockSection showHeader="false" rendered="true" columns="1">
                                    <apex:pageBlockSectionItem >
                                        <apex:selectList id="selGroup" value="{!strSelectedGroup}" size="1">
                                            <apex:selectOptions value="{!lstFilterGroupOptions}"/>
                                                <apex:actionSupport event="onchange" action="{!pgNarrative}" rerender="DataTable" oncomplete="loadTable();"/>
                                        </apex:selectList>
                                    </apex:pageBlockSectionItem>
                                    <apex:pageBlockSectionItem >
                                        <apex:selectList id="selAuthor" value="{!strSelectedAuthor}" size="1">
                                            <apex:selectOptions value="{!lstFilterAuthorOptions}"/>
                                                <apex:actionSupport event="onchange" action="{!pgNarrative}" rerender="DataTable" oncomplete="loadTable();"/>
                                        </apex:selectList>                                      
                                    </apex:pageBlockSectionItem>
                                    <apex:pageBlockSectionItem >
                                        <apex:selectList id="selStatus" value="{!strSelectedStatus}" size="1">
                                            <apex:selectOptions value="{!lstFilterStatusOptions}"/>
                                                <apex:actionSupport event="onchange" action="{!pgNarrative}" rerender="DataTable" oncomplete="loadTable();"/>
                                            </apex:selectList>                                    
                                    </apex:pageBlockSectionItem>
                                </apex:pageBlockSection>
                            </apex:pageBlock>          
                        </apex:outputPanel>
                    </td>
                    <td>
                        <table id="tblNarrative" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Group / Author</th>
                                    <th>Assigned Author</th>
                                    <th>Status</th>
                                    <th>Draft Due Date</th>
                                    <th>Final Due Date</th>                    
                                </tr>
                            </thead>
                        
                            <tbody id="tblBody">
                                <apex:actionFunction name="manageselection" action="{!manageFilters}" reRender="DataTable"/>
                                <apex:repeat value="{!Narratives}" var="pn">
                                    <tr>
                                         <td><apex:outputLink value="/apex/ProjectNarrative_Edit?opptyid={!pn.Project__c}&group={!pn.Narrative_Group__c}&author={!pn.Author__c}" onclick="manageselection();" id="theLink">{!pn.Narrative_Group__c} / {!pn.Author__c}</apex:outputLink></td> 
                                         <td>{!mapAuthorNames[pn.Author__c]}</td>
                                         <td><apex:outputText value="{!pn.Project_Narrative_Author_Status__c}"/></td>
                                         <td><apex:outputText value="{0, date, M/d/yyyy}">
                                            <apex:param value="{!pn.Project_Narrative_Author_Due_Date__c}" />
                                         </apex:outputText></td>
                                         <td><apex:outputText value="{0, date, M/d/yyyy}">
                                            <apex:param value="{!pn.Project_Narrative_Author_Final_Date__c}" />
                                         </apex:outputText></td>
                                    </tr>
                                </apex:repeat>                              
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </apex:outputPanel>                 
    </apex:form>
</apex:page>