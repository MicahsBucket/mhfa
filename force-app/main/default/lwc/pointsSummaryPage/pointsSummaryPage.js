import { LightningElement, api, track, wire } from 'lwc';
import getProjectData from '@salesforce/apex/ProjectDataController.getProjectData';
import maxCatPoints from '@salesforce/label/c.Scoresheet_Max_Points';
import availableCatPoints from '@salesforce/label/c.Scoresheet_Points_Available';
import claimedCatPoints from '@salesforce/label/c.Scoresheet_Points_Claimed';
// import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

// const MAXROWCOUNT = 1000;   // Limit the total number of records to be handled by this component
// const ROUNDWIDTH = 5;       // Used to round off the column widths during Config Mode to nearest value

// const MYDOMAIN = 'https://' + window.location.hostname.split('.')[0].replace('--c','');

export default class PointsSummaryPage extends LightningElement {
    @api recordId;
    @track record;
    @track error;
    @track maxPointsLabel = maxCatPoints;
    @track availablePointsLabel = availableCatPoints;
    @track claimedPointsLabel = claimedCatPoints;
    
    connectedCallback(){
        console.log('recordId', this.recordId);
        if(this.recordId != null){
            getProjectData({ recordId: this.recordId})
            .then(result => {
                console.log('result', result);
                this.record = result;
                this.error = undefined;
            })
            .catch(error => {
                console.log('error', error);
                this.error = error;
                this.record = undefined;
            });
        }
    }
    refreshParentData(){
        console.log('in refreshParentData on PointsSummaryPage');
        getProjectData({ recordId: this.recordId})
            .then(result => {
                console.log('result', result);
                this.record = result;
                this.error = undefined;
            })
            .catch(error => {
                console.log('error', error);
                this.error = error;
                this.record = undefined;
            });
    }

}