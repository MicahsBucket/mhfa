import { LightningElement , track, api, wire} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
// import { CurrentPageReference } from 'lightning/navigation';

import saveFile from '@salesforce/apex/ProjectDataController.saveFile';
import releatedFiles from '@salesforce/apex/ProjectDataController.releatedFiles';
import getApprovedFileMetadata from '@salesforce/apex/ProjectDataController.getApprovedFileMetadata';
import deleteFile from '@salesforce/apex/ProjectDataController.deleteFile';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

const columns = [
    {label: 'Title', fieldName: 'Title', hideDefaultActions: "true"},
    {label: 'Date Uploaded', fieldName: 'CreatedDate', type: 'date', hideDefaultActions: "true" },
    {label: 'Delete', fieldName: 'Id', type: 'Id', cellAttributes:
     { iconName: { fieldName: 'delete' }, iconPosition: 'right' }}
];

export default class FileUploadLWC extends NavigationMixin(LightningElement) {
    @api incomingRecord;
    @track columns = columns;
    @track data;
    @track fileName = '';
    @track UploadFile = 'Upload File';
    @track showLoadingSpinner = false;
    @track isTrue = false;
    @track recordToLink;
    @track CardTitle;
    @track FileTitle;
    @track dataAvailable = false;
    selectedRecords;
    filesUploaded = [];
    file;
    fileContents;
    fileReader;
    content;
    MAX_FILE_SIZE = 1500000;
    @track showDeleteModal = false;
    @track maxPoints;
    @track claimedPoints;
    @track showClaimToggle = false;
    @track toggleMessage;
    @api sheetList;
    @track sheetListToUse;
    @track isUploadDisabled = false;
    @track acceptedFormats;
    @track cdId;
    @track showFiles = false;
    @track showThisImage;

    // @wire(CurrentPageReference)
    // pageRef;

    @wire(getApprovedFileMetadata) 
    wiredFileTypes({ error, data }) {
        if (data) {
            this.acceptedFormats = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.acceptedFormats = null;
        }
    }
    connectedCallback(){
        if(this.incomingRecord.pCategoryId != null){
            this.recordToLink = this.incomingRecord.pCategoryFileId;
            this.maxPoints = this.incomingRecord.pCategoryMaxPoints;
            this.toggleMessage = `Claimed Points: ${this.incomingRecord.pCategoryClaimedPoints}`;
            this.claimedPoints = this.incomingRecord.pCategoryClaimedPoints;
            this.CardTitle = `${this.incomingRecord.pCategory} Category Files`;
            this.FileTitle = 'Attach Category Files'
            this.showClaimToggle = false;
            this.sheetListToUse = this.sheetList;
            this.isUploadDisabled = false;
        }
        else{
            this.recordToLink = this.incomingRecord.Id;
            if(this.incomingRecord.Points_Claimed__c > 0){
                this.toggleMessage = `Claimed Points: ${this.incomingRecord.Available_Points__c}`;
            }
            else{
                this.toggleMessage = `Claimed Points: 0`;
            }
            this.maxPoints = this.incomingRecord.Max_Points__c;
            this.claimedPoints = this.incomingRecord.Available_Points__c;
            this.CardTitle = `${this.incomingRecord.Name}`;
            this.FileTitle = 'Attach Question Files'
            this.showClaimToggle = true;
            this.sheetListToUse = false;
            if(this.incomingRecord.Available_Points__c > 0){
                this.isUploadDisabled = false;
            }
            else{
                this.isUploadDisabled = true;
            }
        }
        this.getRelatedFiles();
    }

    uploadHelper() {
        this.file = this.filesUploaded[0];
       if (this.file.size > this.MAX_FILE_SIZE) {
            return ;
        }
        this.showLoadingSpinner = true;
        // create a FileReader object 
        this.fileReader= new FileReader();
        // set onload function of FileReader object  
        this.fileReader.onloadend = (() => {
            this.fileContents = this.fileReader.result;
            let base64 = 'base64,';
            this.content = this.fileContents.indexOf(base64) + base64.length;
            this.fileContents = this.fileContents.substring(this.content);
            
            // call the uploadProcess method 
            this.saveToFile();
        });
    
        this.fileReader.readAsDataURL(this.file);
    }

    // Calling apex class to insert the file
    saveToFile() {
        console.log('this.recordToLink',this.recordToLink);
        saveFile({ idParent: this.recordToLink, strFileName: 'Test This', base64Data: encodeURIComponent(this.fileContents)})
        .then(result => {
            window.console.log('result ====> ' +result);
            // refreshing the datatable
            this.getRelatedFiles();
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!!',
                    message: ' - Uploaded Successfully!!!',
                    variant: 'success',
                }),
            );

        })
        .catch(error => {
            // Showing errors if any while inserting the files
            window.console.log('Error>>> ', error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error while uploading File',
                    message: error.message,
                    variant: 'error',
                }),
            );
        });
    }
    
    // Getting releated files of the current record
    getRelatedFiles() {
        releatedFiles({idParent: this.recordToLink})
        .then(data => {
            if(data){
                console.log('File Data'+ JSON.stringify(data));
                this.dataAvailable = true;
                this.data = data;
            }
            else{
                this.dataAvailable = false;
                this.data = null;
            }
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: error.message,
                    variant: 'error',
                }),
            );
        });
    }

    // Getting selected rows to perform any action
    getSelectedRecords(event) {
        let conDocIds;
        const selectedRows = event.detail.selectedRows;
        conDocIds = new Set();
        // Display that fieldName of the selected rows
        for (let i = 0; i < selectedRows.length; i++){
            conDocIds.add(selectedRows[i].ContentDocumentId);
        }

        this.selectedRecords = Array.from(conDocIds).join(',');
    }
    @track IdOfFileToDelete;
    @track NameOfFileToDelete;
    openDeleteFileModal(event){
        this.IdOfFileToDelete = event.target.value.ContentDocumentId;
        this.NameOfFileToDelete = event.target.value.Title;
        this.showDeleteModal = true;
    }

    closeDeleteFileModal(){
        this.showDeleteModal = false;
        this.IdOfFileToDelete = null;
    }

    deleteFile(event){
        deleteFile({FileIdToDelete: this.IdOfFileToDelete})
        .then(data => {
            if(data == 'Success'){
                this.getRelatedFiles();
            }
        })
        .catch(error => {
            console.log('error>>>> ',error);
        });
        
    }

    recordSelected(){
        if(this.incomingRecord.Points_Claimed__c == true){
            // this.incomingRecord.Points_Claimed__c = false;
            this.toggleMessage = `Claimed Points: 0`;
            this.dispatchEvent(new CustomEvent('pointsselected', {detail: false}));
            this.refreshParentData();
        }
        if(this.incomingRecord.Points_Claimed__c == false){
            this.toggleMessage = `Claimed Points: ${this.incomingRecord.Available_Points__c}`;
            // this.incomingRecord.Points_Claimed__c = true;
            this.dispatchEvent(new CustomEvent('pointsselected', {detail: true}));
            this.refreshParentData();
        }
        
    }

    sheetRecordSelected(event){
        console.log(event.target.value);

    }

    refreshParentData(){
        this.dispatchEvent(new CustomEvent('refreshparentdata'));
    }

    filePreview(event) {
        this.showFiles = true;
        this.showThisImage = event.target.value;
        console.log('this.showThisImage'+ this.showThisImage);
        //navigateToFiles() {
      this[NavigationMixin.Navigate]({
        type: 'comm__namedPage',
        attributes: {
            pageName: 'filePreview'
        },
        state : {
            // recordIds: this.showThisImage,
            selectedRecordId:this.showThisImage 
        }
      })
    }

    showFilePreview(event){
        let data = event.target.value;
        var bytes = new Uint8Array(data.length / 2);

        for (var i = 0; i < data.length; i += 2) {
            bytes[i / 2] = parseInt(data.substring(i, i + 2), 16);
        }

        // Make a Blob from the bytes
        var blob = new Blob([bytes], {type: 'image/bmp'});

        // Use createObjectURL to make a URL for the blob
        var image = new Image();
        this.showThisImage = image.src = URL.createObjectURL(blob);
        // document.body.appendChild(image);
        console.log('this.showThisImage'+ this.showThisImage);
    }
    
    closePreviewModal(){
        this.showFiles = false;
    }
}