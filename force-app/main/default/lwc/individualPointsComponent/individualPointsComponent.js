import { LightningElement, api, track } from 'lwc';
import setPointsClaimed from '@salesforce/apex/ProjectDataController.setPointsClaimed';

export default class IndividualPointsComponent extends LightningElement {
    
    @api individualData;
    @api fileLevel;
    @track showAvailable = false;
    @track pointsSelected = false;
    @track pointsToggleMessage = '';
    @track showFileUpload = false;
    @track showSmallerSheetList = false;
    connectedCallback() {
        if(this.individualData.Available_Points__c != 0){
            this.showAvailable = true;
            if(this.individualData.Points_Claimed__c == true){
                this.pointsSelected = true;
                this.pointsToggleMessage = `Claimed ${this.individualData.Available_Points__c} Points`;
            }
            else{
                this.pointsToggleMessage = `Claimed 0 Points`;
            }
        }
        else{
            this.showAvailable = false;
            this.pointsSelected = false;
        }
        if(this.fileLevel == 'Question Level'){
            this.showFileUpload = true;
        }
        else if(this.fileLevel == 'Category Level'){
            this.showFileUpload = false;
            this.showSmallerSheetList = false;
        }
        else{
            this.showFileUpload = false;
            this.showSmallerSheetList = true;
        }
    }

    recordSelected(event){
        if(event.target.checked == true || event.detail == true){
            this.pointsSelected = true;
            this.pointsToggleMessage = `Claimed ${this.individualData.Available_Points__c} Points`
            setPointsClaimed({ recordId: this.individualData.Id, claimPoints: true})
            .then(result => {
                this.individualData = result;
                // this.error = undefined;
                this.dispatchEvent(new CustomEvent('refreshparentdata'));
            })
            .catch(error => {
                console.log('error', error);
                // this.error = error;
                // this.record = undefined;
            });
        }
        if(event.target.checked == false || event.detail == false){
            this.pointsSelected = false;
            this.pointsToggleMessage = `Claimed 0 Points`
            setPointsClaimed({ recordId: this.individualData.Id, claimPoints: false})
            .then(result => {
                this.individualData = result;
                this.dispatchEvent(new CustomEvent('refreshparentdata'));
                // this.error = undefined;
            })
            .catch(error => {
                console.log('error', error);
                // this.error = error;
                // this.record = undefined;
            });
        }
        
    }

    recordDeselcted(){
        this.pointsSelected = false;
    }

    
}