import { LightningElement, api } from 'lwc';

export default class DeleteModal extends LightningElement {
    @api showModal = false;
    @api message;
    @api modalHeading;
    @api fileName;
    @api fileId;

    @api
    openModal() {
        this.dispatchEvent(new CustomEvent('openmodal'));
    }

    @api
    closeModal() {
        this.dispatchEvent(new CustomEvent('closemodal'));
    }

    deleteFile(event){
        this.dispatchEvent(new CustomEvent('deletefile', {detail: event.target.value}));
        this.dispatchEvent(new CustomEvent('closemodal'));
    }
}