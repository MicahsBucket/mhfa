import { LightningElement , api, track} from 'lwc';
import maxCatPoints from '@salesforce/label/c.Scoresheet_Category_Max_Points';
import availableCatPoints from '@salesforce/label/c.Scoresheet_Category_Points_Available';
import claimedCatPoints from '@salesforce/label/c.Scoresheet_Category_Points_Claimed';

export default class PointsSummaryCategory extends LightningElement {
    @api categoryMap;
    @track expanded;
    @track showFileUpload = false;
    @track maxPointsLabel = maxCatPoints;
    @track availablePointsLabel = availableCatPoints;
    @track claimedPointsLabel = claimedCatPoints;
    @track showFullDescriptionPopup = false;

    connectedCallback(){
        this.expanded = false;
        if(this.categoryMap.pCategoryFileLevel == 'Category Level'){
            this.showFileUpload = true;
        }
        else{
            this.showFileUpload = false;
        }
    }

    expandSection(){
        this.expanded = true;
    }

    closeSection(){
        this.expanded = false;
    }

    refreshParentData(){
        this.dispatchEvent(new CustomEvent('refreshparentdata'));
    }
    
    showFullDescriptionHover(){
        console.log('MOUSE OVER');
        this.showFullDescriptionPopup = true;
    }

    hideFullDescriptionHover(){
        console.log('MOUSE Out');
        this.showFullDescriptionPopup = false;
    }
}