import { LightningElement, api } from 'lwc';

export default class FilePreviewModal extends LightningElement {
    @api imageIncoming;
    

    @api
    openModal() {
        this.dispatchEvent(new CustomEvent('openmodal'));
    }

    @api
    closeModal() {
        this.dispatchEvent(new CustomEvent('closemodal'));
    }
}