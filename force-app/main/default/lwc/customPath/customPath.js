import { LightningElement, api, track, wire } from 'lwc';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent } from 'lightning/flowSupport';

export default class CustomPath extends LightningElement {
    // Component Input & Output Attributes
    @api pathValues = [];
    @api isInteractive;
    @api defaultPathValue;
    @api indicatorType;
    @api selectedPathValue;

    @track pathValue;
    @track indicatorStyle;

    connectedCallback() {
        this.indicatorStyle = this.indicatorType;
        if (this.indicatorStyle == undefined || this.indicatorStyle == '') {
            this.indicatorStyle = 'path';
        }
        this.pathValue = this.defaultPathValue;
        this.selectedPathValue = this.assignNextPathItem(this.pathValue);
        console.log('Inside connectedCallback.  this.pathValue: ' + this.pathValue + '; this.selectedPathValue: ' + this.selectedPathValue + '; this.indicatorType: ' + this.indicatorType + '; this.indicatorStyle: ' + this.indicatorStyle);
    }

    renderedCallback(){
        this.indicatorStyle = this.indicatorType;
        if (this.indicatorStyle == undefined || this.indicatorStyle == '') {
            this.indicatorStyle = 'path';
        }
        this.pathValue = this.defaultPathValue;
        this.selectedPathValue = this.assignNextPathItem(this.pathValue);
        console.log('Inside connectedCallback.  this.pathValue: ' + this.pathValue + '; this.selectedPathValue: ' + this.selectedPathValue + '; this.indicatorType: ' + this.indicatorType + '; this.indicatorStyle: ' + this.indicatorStyle);
    }

    //Find the next item in the path; if already on the last item, return "Finished"
    assignNextPathItem(currentPathItem) {
        var iLastItem = this.pathValues.length - 1;
        var retVal;
        for (var i = 0; i < this.pathValues.length; i++) {
            if (this.pathValues[i] == currentPathItem ) {
                if (i < iLastItem) {
                    retVal = this.pathValues[i + 1];
                } else {
                    retVal = 'Finished';
                }
                break;
            }
        }
        console.log('Inside assignNextPathItem.  currentPathItem: ' + currentPathItem + '; Next Item: ' + retVal);
        return retVal;
    }

    get hasPathValues() {
        return this.pathValues && this.pathValues.length > 0;
    }


    handleStepSelected(event) {
        console.log('Inside handleStepSelected: ' + event.detail.index + ' - ' + this.pathValues[event.detail.index]);
        if (this.isInteractive) {
            this.selectedPathValue = this.pathValues[event.detail.index];
            this.handleGoNext();
        }
    }

    handleUpdateButtonClick() {
        this.selectedPathValue = this.pathValue;
        this.handleGoNext();
    }

    handleGoNext() {
        console.log('Inside handleGoNext.  this.pathValue: ' + this.pathValue + '; this.selectedPathValue: ' + this.selectedPathValue);
        // check if NEXT is allowed on this screen
        //if (this.availableActions.find(action => action === 'NEXT')) {
            // navigate to the next screen
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        //}
    }

}